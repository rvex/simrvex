#ifndef __STATE_H__
#define __STATE_H__

#include "simtypes.h"

/* Hints to compiler for likely branch direction */
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define BROADCAST_GLOBALS 1
#define BROADCAST_ALL 2

#define NBREAKPOINTS 4 //H TODO: generate this
#define NCONTEXTS 4
#define ALLCONTEXTS ((1<<NCONTEXTS)-1)

#define NTIMERS 4

#define ALLREGS

#define CID(context) (context->sccr>>CR_SCCR_ID_BIT)

//Define this if you want to include all registers in the state, including a lot of obscure ones that you probably won't need
//#define ALLREGS

//Define this if you do not want to use the trap/panick handlers but crash on any trap
//#define NOHANDLERS

typedef struct globstate{
	u32 gsr;
	//skipping BCRR
	u32 cc;
	u32 aff;
	u64 cnt;

	//The rest is version and capabilities registers
#ifdef ALLREGS

	//Long immediate capability registers
	u32 limc0;
	u32 limc1;
	u32 limc2;
	u32 limc3;
	u32 limc4;
	u32 limc5;
	u32 limc6;
	u32 limc7;

	//Syllable index capability registers
	u32 sic0;
	u32 sic1;
	u32 sic2;
	u32 sic3;

	//General purpose register delay registers
	u32 gps0;
	u32 gps1;

	//Special delay registers
	u32 sps0;
	u32 sps1;

	//Extension registers
	u32 ext0;
	u32 ext1;
	u32 ext2;

	//Design-time Configuration registers
	u32 dcfg;

	//Core version registers
	u32 cver0;
	u32 cver1;

	//platform version registers
	u32 pver0;
	u32 pver1;


#endif

	//timer-related control registers
	u32 timer_scaler;
	u32 timer_scaler_reload;
	u32 timer_config;
	u32 timer_tim_ctr[NTIMERS];
	u32 timer_tim_reload[NTIMERS];
	u32 timer_tim_config[NTIMERS];

	//H These I have added to the simulator for convenience
	u32 nReconfigurations;

} glob_state_t;


typedef struct state {
	union {
		u32 r[64];		/* 64 Main registers, r0 "special" */
		float r_float[64];
		s32 r_signed[64];	/* Same again, but signed instead of unsigned */
	};
	u32 lr;			/* Link register */
	u8 br[8];		/* Branch bits */
	u32 pc;			/* PC of this bundle */

	//H adding all cregs here
	u32 ccr;
	u32 sccr;

	u32 th; //trap handler
	u32 ph; //panic handler

	u32 tp; //trap point
	u32 ta; //trap argument

	u32 brk[NBREAKPOINTS];

	u32 dcr;  //debug ctrl reg
	u32 dcr2; //debug ctrl reg 2

	u32 crr; //reconf request reg

	//scratchpad regs
	u32 scrp1;
	u32 scrp2;
	u32 scrp3;
	u32 scrp4;

	//Context counters
	u64 cr_cyc;
	u64 cr_stall;
	u64 cr_bun;
	u64 cr_syl;
	u64 cr_nop;
	u64 cr_stop;

	//Cache counters
	u64 cr_iacc;
	u64 cr_imiss;
	u64 cr_dracc;
	u64 cr_drmiss;
	u64 cr_dwacc;
	u64 cr_dwmiss;
	u64 cr_dbypass;
	u64 cr_dwbuf;
	u64 cr_broadcast;

	//Bank conflict counters
	u64 bank_r1;
	u64 bank_r2;
	u64 bank_wr;

	/*
	 * From here it's all bullshit register you would normally not need
	 */
#ifdef ALLREGS
	u32 wcfg; //wake-up config reg (note: only context 0 has one)
	u32 sawc; //sleep and wake-up reg (note: only context 0 has one)

	u32 rsc; //requested software context
	u32 csc; //current software context

	//Requested/Current swctxt on hwctxt $n$ (normally only needs 4)
#if NCONTEXTS > 1
	u32 rsc1;
	u32 csc1;
#endif
#if NCONTEXTS > 2
	u32 rsc2;
	u32 csc2;
#endif
#if NCONTEXTS > 3
	u32 rsc3;
	u32 csc3;
#endif
#if NCONTEXTS > 4
	u32 rsc4;
	u32 csc4;
#endif
#if NCONTEXTS > 5
	u32 rsc5;
	u32 csc5;
#endif
#if NCONTEXTS > 6
	u32 rsc6;
	u32 csc6;
#endif
#if NCONTEXTS > 7
	u32 rsc7;
	u32 csc7;
#endif

#endif

	//To save a trap during decoding so the trap function can write it into the regs
	/*
	 * TODO: I'm not sure this is how I want it; maybe I should write to the real regs directly in instr.c
	 * or find some way to do prioritizing of traps (because this stuff doesn't support that)
	 */

	u32 trapcause;
	u32 traparg;

	u32 int_force;
	u32 int_mask;

	//H penalty for bus accesses
	u32 busWaitCycles;
	//This should be set by an instruction that causes a branch delay.
	u32 branch_delay;
} cx_state_t;

struct cg_select_format {
	u32 bdest;
	u32 scond;
};

/* Used by call, br etc */
struct branch_format {
	s32 btarg;		/* note always signed */
	u32 bcond;
};

struct sysop_format {
	u32 number;
};

struct decoded_ins_params {
	/* Always assume we have these */
	union { /* Arranged like this to allow array access as well */
		u32 op[3];
		struct {
			u32 op1;
			u32 op2;
			u32 op3;
		};
	};
	/* We don't field showing what type params are, because the
	 * instruction implicitly knows what they are. Trace info 
	 * is held in a different struct, as it does need to know 
	 * because the trace print routines are generic. 
	 */
	union {
		struct cg_select_format cgs;
		struct sysop_format sysop;
		struct branch_format branch;
	};
};

/* These are what the individual instructions in a bundle look like */
typedef void (*instr_func) (cx_state_t * current, struct state * new,
			    struct decoded_ins_params * params)
    __attribute__ ((regparm(3)));

    
struct decoded_ins {
	unsigned int raw_ins;
	instr_func op;				/* We could point at the table, but faster to copy pointer over */
	struct instr_info *trace_marker;	/* Contains instruction type and index into table */
	struct decoded_ins_params params;
};

/* if the bundle contains a load/store, 
 * we ensure that this is executed *last*, 
 * so we know that the rest of the bundle will not 
 * issue an exception, as once we have done the load 
 * or store, we have to proceed to the commit stage
 */

struct decoded_bundle {
	struct decoded_ins ins[8];	/* Max 8 per bundle */
	/* How many bytes are in the bundle. Used to compute the 
	 * next PC you got to. PC is not stored here because the 
	 * same instructions can be executed with different virtual
	 * space 
	 */
	unsigned char num;	/* Number of instructions (including NOPs) in bundle */
	unsigned char numNOPs;	/* Number of NOPs in bundle */
	unsigned char size;
	/* Did this bundle contain a stop bit? */
	unsigned char stop_bit;
	/* We know which registers we will update, */
	unsigned char num_r_updates;
	unsigned char num_b_updates;
	unsigned char r_index[8];	/* Which register to change, ST240 has 5 ports */
	unsigned char b_index[8];	/* Which branch bit to change */
	unsigned char update_lr; 	/* Whether or not to change link register */
	
	/* Banking: r1[n] is the number of n-way read1 conflicts */
	unsigned char bank_r1t;  /* read1 */
	unsigned char bank_r2t;  /* read2 */
	unsigned char bank_wrt;  /* write */
	
	//unsigned char bank_r1[8];
	//unsigned char bank_r2[8];
	//unsigned char bank_wr[8];
};

extern u32 time_count;
extern u32 time_control;
extern u32 time_const;

extern u32 last_phys;		/* Last physical address stored/loaded */
extern int last_bundle_change_psw;
extern long run_cycles;
extern glob_state_t global_state;
extern int syscall_emulation;
extern u32 sim_brk;

extern int banked_regfile;

int contextActive(cx_state_t *context);
int issueWidth(cx_state_t *context);
int getCoupledLaneGroups(cx_state_t *context);

void print_register_state(int context);

void set_interrupt(int line);
void clear_interrupt(int line);
void update_interrupt_line(void);

extern void triggerTrap(cx_state_t *current, int cause, int argument);

#if 0
void sleep_cpu(void);
void wake_cpu(void);
#endif

void simulator_exit(int code);

#endif

