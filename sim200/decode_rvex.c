#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "state.h"
#include "memory.h"
#include "instr.h"
#include "tlb.h"
#include "ctrlregdef.h"

#include "trace.h"

#include "decode_formats.h"
#include "icache.h"
#include "decode.h"

#define DEBUG_LIMMH
//#define PRINT_LIMMH

static struct instr_table {
	struct instr_info integer1_table[RVEX_FORMAT_INTEGER1_ENTRIES];
	struct instr_info integer2_table[RVEX_FORMAT_INTEGER2_ENTRIES];
	struct instr_info mul_table[RVEX_FORMAT_MUL_ENTRIES];
	struct instr_info logic_table[RVEX_FORMAT_LOGIC_ENTRIES];
	struct instr_info slct_table[RVEX_FORMAT_SLCT_ENTRIES];
	struct instr_info sysop_table[RVEX_FORMAT_SYSOP_ENTRIES];
	struct instr_info sysop2_table[RVEX_FORMAT_SYSOP_ENTRIES];
	struct instr_info load_store_table[RVEX_FORMAT_LOAD_STORE_ENTRIES];
    struct instr_info conditional_load_store_table[2];
	struct instr_info call_table[RVEX_FORMAT_CALL_ENTRIES];
	struct instr_info pseudo_table[RVEX_FORMAT_PSEUDO_ENTRIES];
} itable = {
		  {
			//1st part of ALU entries including NOP (32 entries)
		    INT3R_ENTRY(nop),
			ILLEGAL_ENTRY,
			ILLEGAL_ENTRY,
			ILLEGAL_ENTRY,

		    INT3R_ENTRY(add),
		    INT3I_ENTRY(add),
		    INT3R_ENTRY(and),
		    INT3I_ENTRY(and),
		    INT3R_ENTRY(andc),
		    INT3I_ENTRY(andc),
		    INT3R_ENTRY(max),
		    INT3I_ENTRY(max),
		    INT3R_ENTRY(maxu),
		    INT3I_ENTRY(maxu),
		    INT3R_ENTRY(min),
		    INT3I_ENTRY(min),
		    INT3R_ENTRY(minu),
		    INT3I_ENTRY(minu),
		    INT3R_ENTRY(or),
		    INT3I_ENTRY(or),
		    INT3R_ENTRY(orc),
		    INT3I_ENTRY(orc),
		    INT3R_ENTRY(sh1add),
		    INT3I_ENTRY(sh1add),
		    INT3R_ENTRY(sh2add),
		    INT3I_ENTRY(sh2add),
		    INT3R_ENTRY(sh3add),
		    INT3I_ENTRY(sh3add),
		    INT3R_ENTRY(sh4add),
		    INT3I_ENTRY(sh4add),
		    INT3R_ENTRY(shl),
		    INT3I_ENTRY(shl)
		  },
		  {
			//2nd part of ALU opcodes (16 entries)
		    INT3R_ENTRY(shr),
		    INT3I_ENTRY(shr),
		    INT3R_ENTRY(shru),
		    INT3I_ENTRY(shru),
			INT3R_ENTRY(sub),
		    INT3I_ENTRY(sub),
		    MONADIC_ENTRY(sxtb),
			ILLEGAL_ENTRY,
		    MONADIC_ENTRY(sxth),
			ILLEGAL_ENTRY,
			MONADIC_ENTRY(zxtb),
			ILLEGAL_ENTRY,
		    MONADIC_ENTRY(zxth),
			ILLEGAL_ENTRY,
		    INT3R_ENTRY(xor),
		    INT3I_ENTRY(xor),
		  },
		  {
			//mpy and link register opcodes (32 entries)
			INT3R_ENTRY(mpyll),
		    INT3I_ENTRY(mpyll),
			INT3R_ENTRY(mpyllu),
		    INT3I_ENTRY(mpyllu),
			INT3R_ENTRY(mpylh),
		    INT3I_ENTRY(mpylh),
			INT3R_ENTRY(mpylhu),
		    INT3I_ENTRY(mpylhu),
			INT3R_ENTRY(mpyhh),
		    INT3I_ENTRY(mpyhh),
			INT3R_ENTRY(mpyhhu),
		    INT3I_ENTRY(mpyhhu),
		    INT3R_ENTRY(mpyl),
		    INT3I_ENTRY(mpyl),
			INT3R_ENTRY(mpylu),
		    INT3I_ENTRY(mpylu),
			INT3R_ENTRY(mpyh),
		    INT3I_ENTRY(mpyh),
			INT3R_ENTRY(mpyhu),
		    INT3I_ENTRY(mpyhu),
			INT3R_ENTRY(mpyhs),
		    INT3I_ENTRY(mpyhs),

			INT3R_ENTRY(mtl),
			INT3I_ENTRY(mtl),

			INT3R_ENTRY(mfl),
			ILLEGAL_ENTRY,

			ILLEGAL_ENTRY,
			LOAD_ENTRY(ldl),
			ILLEGAL_ENTRY,
			STORE_ENTRY(stl),
			ILLEGAL_ENTRY,
			ILLEGAL_ENTRY
		  },
		  {
			//Logic opcodes (4*16 entries)
		    CMP3R_ENTRY(cmpeq),
		    CMP3I_ENTRY(cmpeq),
		    CMP3R_BR_ENTRY(cmpeq),
		    CMP3I_BR_ENTRY(cmpeq),

		    CMP3R_ENTRY(cmpge),
		    CMP3I_ENTRY(cmpge),
		    CMP3R_BR_ENTRY(cmpge),
		    CMP3I_BR_ENTRY(cmpge),

		    CMP3R_ENTRY(cmpgeu),
		    CMP3I_ENTRY(cmpgeu),
		    CMP3R_BR_ENTRY(cmpgeu),
		    CMP3I_BR_ENTRY(cmpgeu),

		    CMP3R_ENTRY(cmpgt),
		    CMP3I_ENTRY(cmpgt),
		    CMP3R_BR_ENTRY(cmpgt),
		    CMP3I_BR_ENTRY(cmpgt),

		    CMP3R_ENTRY(cmpgtu),
		    CMP3I_ENTRY(cmpgtu),
		    CMP3R_BR_ENTRY(cmpgtu),
		    CMP3I_BR_ENTRY(cmpgtu),

		    CMP3R_ENTRY(cmple),
		    CMP3I_ENTRY(cmple),
		    CMP3R_BR_ENTRY(cmple),
		    CMP3I_BR_ENTRY(cmple),

		    CMP3R_ENTRY(cmpleu),
		    CMP3I_ENTRY(cmpleu),
		    CMP3R_BR_ENTRY(cmpleu),
		    CMP3I_BR_ENTRY(cmpleu),

		    CMP3R_ENTRY(cmplt),
		    CMP3I_ENTRY(cmplt),
		    CMP3R_BR_ENTRY(cmplt),
		    CMP3I_BR_ENTRY(cmplt),

		    CMP3R_ENTRY(cmpltu),
		    CMP3I_ENTRY(cmpltu),
		    CMP3R_BR_ENTRY(cmpltu),
		    CMP3I_BR_ENTRY(cmpltu),

		    CMP3R_ENTRY(cmpne),
		    CMP3I_ENTRY(cmpne),
		    CMP3R_BR_ENTRY(cmpne),
		    CMP3I_BR_ENTRY(cmpne),

		    CMP3R_ENTRY(nandl),
		    CMP3I_ENTRY(nandl),
		    CMP3R_BR_ENTRY(nandl),
		    CMP3I_BR_ENTRY(nandl),

			CMP3R_ENTRY(norl),
		    CMP3I_ENTRY(norl),
		    CMP3R_BR_ENTRY(norl),
		    CMP3I_BR_ENTRY(norl),

		    CMP3R_ENTRY(orl),
		    CMP3I_ENTRY(orl),
		    CMP3R_BR_ENTRY(orl),
		    CMP3I_BR_ENTRY(orl),

		    CMP3R_ENTRY(andl),
		    CMP3I_ENTRY(andl),
		    CMP3R_BR_ENTRY(andl),
		    CMP3I_BR_ENTRY(andl),

			ILLEGAL_ENTRY,
			ILLEGAL_ENTRY,
		    CMP3I_ENTRY(tbit),
		    CMP3I_BR_ENTRY(tbit),

			ILLEGAL_ENTRY,
			ILLEGAL_ENTRY,
		    CMP3I_ENTRY(tbitf),
		    CMP3I_BR_ENTRY(tbitf),

//			[16*4 ... (RVEX_FORMAT_MONADIC_ENTRIES - 1)] = ILLEGAL_ENTRY
		  },
		  {
			//other logic and carry generating opcodes (6 entries)
		    CGEN_ENTRY(divs),
		    CGEN_ENTRY(addcg), 
		    SELECTR_ENTRY(slctf),
		    SELECTR_ENTRY(slct),
		    SELECTI_ENTRY(slctf),
		    SELECTI_ENTRY(slct), 
//		    INT3R_ENTRY(monadic)	/* just for subdecode in sysop */
		},
		{
				//Other, instruction extensions part 1
		    SYSOP_ENTRY_NAME(instr_trap, trap),
			SYSOP_ENTRY_NAME(instr_trap, trap), //H TODO: create new SYSOP_ENTRY thing
		    MONADIC_ENTRY(clz),
			ILLEGAL_ENTRY,
			INT3R_ENTRY(mpylhus),
			INT3I_ENTRY(mpylhus),
			INT3R_ENTRY(mpyhhs),
			INT3I_ENTRY(mpyhhs),
			ILLEGAL_ENTRY,
			STORE_ENTRY(xchg),
			//floating point instructions
			INT3R_ENTRY(convif),
		    ILLEGAL_ENTRY,
			INT3R_ENTRY(convfi),
			ILLEGAL_ENTRY,
			INT3R_ENTRY(addf),
			ILLEGAL_ENTRY,
			INT3R_ENTRY(subf),
		    ILLEGAL_ENTRY,
			INT3R_ENTRY(mpyf),
			ILLEGAL_ENTRY,
			CMP3R_ENTRY(cmpgef),
			ILLEGAL_ENTRY,
			CMP3R_BR_ENTRY(cmpgef),
			ILLEGAL_ENTRY,
			CMP3R_ENTRY(cmpeqf),
			ILLEGAL_ENTRY,
			CMP3R_BR_ENTRY(cmpeqf),
			ILLEGAL_ENTRY,
			CMP3R_ENTRY(cmpgtf),
			ILLEGAL_ENTRY,
			CMP3R_BR_ENTRY(cmpgtf),
			ILLEGAL_ENTRY
//			[10 ... (RVEX_FORMAT_SYSOP_ENTRIES - 1)] = ILLEGAL_ENTRY
		},
		{
			//Other, instruction extensions part 2
			INT3R_ENTRY(mpy32),
			INT3I_ENTRY(mpy32),
			[2 ... (RVEX_FORMAT_SYSOP_ENTRIES - 1)] = ILLEGAL_ENTRY
		},
		{
			//MEM opcodes (8 entries)
		    LOAD_ENTRY(ldw),
		    LOAD_ENTRY(ldh),
		    LOAD_ENTRY(ldhu),
		    LOAD_ENTRY(ldb),
		    LOAD_ENTRY(ldbu),
		    STORE_ENTRY(stw),
		    STORE_ENTRY(sth),
		    STORE_ENTRY(stb),
//		    [8 ... (ST231_FORMAT_LOAD_STORE_ENTRIES - 1)] = ILLEGAL_ENTRY
		},
        {
            LOAD_ENTRY(ldwl),
            LOAD_ENTRY(stwl),
        },
	    {
	    	//CTRL ops, stop, some other insns. (16 entries) opcode 0x2-
		    CALL_ENTRY_NAME(go_to, goto),
		    CALL_ENTRY_NAME(go_to_lr, goto),
		    CALL_ENTRY(call),
		    CALL_ENTRY_NAME(call_lr, call),
		    BRANCH_ENTRY(br),
			BRANCH_ENTRY(brf),
			CALL_ENTRY_NAME(return_lr, return),
		    CALL_ENTRY(rfi),

			SYSOP_ENTRY_NAME(stop, stop),
			ILLEGAL_ENTRY,
			ILLEGAL_ENTRY, //INTR_SEND
			ILLEGAL_ENTRY, //INTR_RECV
			INT3R_ENTRY(sbit),
			INT3R_ENTRY(sbitf),
			LOAD_ENTRY(ldbr),
			STORE_ENTRY(stbr)
//		    [5 ... (ST231_FORMAT_CALL_ENTRIES - 1)] = ILLEGAL_ENTRY
		},
		{
		    PSEUDO_ENTRY(illegal), PSEUDO_ENTRY(sim_syscall), PSEUDO_ENTRY(limm)
		}
};

#define NUM_INSTRUCTIONS (sizeof(itable)/sizeof(struct instr_info))

//H TODO: stbr and ldbr, ldl and stl are not in this table

/* The load store tables with dcache enabled */
struct instr_info load_store_table_dcache[RVEX_FORMAT_LOAD_STORE_ENTRIES] = {
	LOAD_ENTRY_DCACHE(ldw),
	LOAD_ENTRY_DCACHE(ldh),
	LOAD_ENTRY_DCACHE(ldhu),
	LOAD_ENTRY_DCACHE(ldb),
	LOAD_ENTRY_DCACHE(ldbu),
	STORE_ENTRY_DCACHE(stw),
	STORE_ENTRY_DCACHE(sth),
	STORE_ENTRY_DCACHE(stb),
//	[8 ... (RVEX_FORMAT_LOAD_STORE_ENTRIES - 1)] = ILLEGAL_ENTRY
};

struct instr_info other_load_store_table_dcache[4] = {
	LOAD_ENTRY_DCACHE(ldl),
	STORE_ENTRY_DCACHE(stl),
	LOAD_ENTRY_DCACHE(ldbr),
	STORE_ENTRY_DCACHE(stbr)
};

/* Gets the next word of memory given the correct virtual pc */
/* Never looks at the cache */
static inline unsigned get_next_ins_word(cx_state_t *current, unsigned pc, int first_syl)
{
//	struct tlb_entry *t;
	unsigned raw_ins;

//	if (psw & PSW_TLB_ENABLE) {
//		t = iside_lookup_tlb(pc);
//		if (exception)
//			return 0;
//		pc = TRANSLATE(t, pc);
//	}

	raw_ins = fetch(current, pc, first_syl);
	return raw_ins;
}

/* Calculate bank port number based on configuration */
static inline int bank_port(int reg, int lane)
{
	/* The bank the register is in */
	int bank = reg & (banked_regfile - 1);
	
	/* The bank port offset this lane uses */
	int bport = lane & (8/banked_regfile - 1);
	
	/* The final register file bank port */
	return (8/banked_regfile) * bank + bport;
}

struct decoded_bundle *rvex_slow_get_next_decoded_bundle(cx_state_t *current,
						    struct icache_entry **icache_slot)
{
	unsigned i, j;
	unsigned pc = current->pc;
	unsigned raw_ins;
	struct decoded_ins *ins, *prev_ins;
	int previous_was_immr;
	int extension_possible;
	unsigned immr = 0;
	unsigned btarg;
	struct decoded_bundle *bundle;
	struct icache_entry *ic;
	int index;
	int iw, iw_idx;

	int syl_index = 0; //index of the current syllable in the total bundle (needed for generic binaries)

	//to keep track of bank conflicts
	int bank_r1[8];
	int bank_r2[8];
	int bank_wr[8];

	for (i = 0; i < 8; i++)
	{
		/* Reset port access counters */
		bank_r1[i] = 0;
		bank_r2[i] = 0;
		bank_wr[i] = 0;
	}

	iw = issueWidth(current);
	if (iw == 2)
	  iw_idx = 0;
	else if (iw == 4)
	  iw_idx = 1;
	else
	  iw_idx = 2;

	ic = icache_buffer[iw_idx] + icache_next;

	bundle = &(ic->bundle);

	/* Blat the old entry, if there was one, we have 
	 * to do now this in case this is an illegal
	 */
	if (ic->slot)
		*(ic->slot) = NULL;

	extension_possible = 0;

	bundle->size = 0;
	bundle->num_r_updates = 0;
	bundle->num_b_updates = 0;
	bundle->num = 0;
	bundle->numNOPs = 0;
	bundle->update_lr = 0;
	bundle->stop_bit = 0;


	extension_possible = 0;
	pc = current->pc;

	for (i = 0; i < iw; i++, pc += 4) {
		ins = bundle->ins + bundle->num;


		raw_ins = get_next_ins_word(current, pc, i==0); //H only flag first_syl the first time we read the syllables
		ins->raw_ins = raw_ins;

		previous_was_immr = 0;

		/* Check if the next instrucion will be extended */
		if (LIMM_SEL_MASK(raw_ins) == LIMM_MASK)
		{
			if (LIMM_INDEX(raw_ins) & 1)//(LIMM_SEL_MASK(raw_ins) == IMMR_MASK) {
			{
				pc += 4;
				previous_was_immr = 1;
				immr = IMM(raw_ins);
				raw_ins = get_next_ins_word(current, pc, i==0);
				/* Presumably you can't have an immr with a stop bit? */
			} else //if (IMM_SEL_MASK(raw_ins) == IMML_MASK) { //H now only else - opposite the LIMM_INDEX & 1
			{
				/* Previous instruction can be extended */
				if (!extension_possible)
				{
					triggerTrap(current, TRAP_LIMMH_FAULT, i);
					 return NULL;
				}
				/* Recalculate the operand */
				/* We have to undo the sign extension that may already
				 * have been done
				 */
				prev_ins->params.op2 =
					(IMM(raw_ins) << 9) | (prev_ins->params.
							   op2 & 0x1ff);
			}
		}

		bundle->num++;

//		printf("raw syllable is 0x%08x\n", raw_ins);

		if (LIMM_SEL_MASK(raw_ins) == LIMM_MASK)
		{
			index = 2;
			ins->op = itable.pseudo_table[index].op;
			ins->trace_marker = itable.pseudo_table + index;

			//abuse the op1 and op2 fields to store the index and value so we can print it in the trace
			ins->params.op1 = BITMASK_EXTRACT(27:25, raw_ins);
			ins->params.op2 = IMM(raw_ins);

			//needed because of the continue
			if ((raw_ins & STOP_BIT) && (pc&0x7)) //ignore unaligned stopbits
			{
				/* Add 4 because we break out of the loop */
				pc += 4;
				bundle->stop_bit = 1;
				break;
			}
			continue;
		}


		/*
		 * Why so ugly, you ask?
		 * Because st200 has a nice instruction formatting and we do not.
		 * I have considered making 1 huge table with all instructions,
		 * But remodeling this a bit and just implementing the abnormal cases was quicker.
		 * I will need to test to see if the monolithic table is faster or not.
		 */

		//NOP, ALU
		if (OPCODE(raw_ins) == 0x60)
		{
			index = RVEX_FORMAT_INTEGER1_OPC(raw_ins);
			ins->op = itable.integer1_table[index].op;
			ins->trace_marker = itable.integer1_table + index;
			bundle->numNOPs++;
		} else if (((OPCODE(raw_ins) >> 4) == 6))
		{
			index = RVEX_FORMAT_INTEGER1_OPC(raw_ins);
			ins->op = itable.integer1_table[index].op;
			ins->trace_marker = itable.integer1_table + index;

			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);

			if (ins->params.op1 != 0)
				//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
				//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
				bank_r2[bank_port(ins->params.op2, i)]++;
			if (ins->params.op3 != 0)
				//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
				bank_wr[bank_port(ins->params.op3, i)]++;

			bundle->r_index[bundle->num_r_updates++] =
							    ins->params.op3;

			extension_possible = EXTEND_BIT(raw_ins);
		}
		else if (((OPCODE(raw_ins) >> 3) == 3))
		{
			index = RVEX_FORMAT_INTEGER2_OPC(raw_ins);
			ins->op = itable.integer2_table[index].op;
			ins->trace_marker = itable.integer2_table + index;

			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);

			if (ins->params.op1 != 0)
				//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
				//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
				bank_r2[bank_port(ins->params.op2, i)]++;
			if (ins->params.op3 != 0)
				//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
				bank_wr[bank_port(ins->params.op3, i)]++;

			bundle->r_index[bundle->num_r_updates++] =
							    ins->params.op3;

			extension_possible = EXTEND_BIT(raw_ins);
		}

		 //H rvex load/store MEM op: 00010---
		else if (((OPCODE(raw_ins) >> 3) == 2))
		{
			index = RVEX_FORMAT_LOAD_STORE_OPC(raw_ins);
			ins->op = itable.load_store_table[index].op;
			ins->trace_marker = itable.load_store_table + index;

			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);

			if (ins->params.op1 != 0)
				//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
				//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
				bank_r2[bank_port(ins->params.op2, i)]++;


			if (index < RVEX_FORMAT_LOAD_STORE_FIRST_STORE) //load insn
			{
				if (ins->params.op3 != 0)
					//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
					bank_wr[bank_port(ins->params.op3, i)]++;
				bundle->r_index[bundle->num_r_updates++] =
				    ins->params.op3;
			}
			extension_possible = 1;
		} else if (OPCODE(raw_ins) == 0xA0) {
            // ldwl (load-linked) instruction
            ins->op = itable.conditional_load_store_table[0].op;
            ins->trace_marker = itable.conditional_load_store_table;
			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);

			if (ins->params.op1 != 0)
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(OP2(raw_ins)))
				bank_r2[bank_port(ins->params.op2, i)]++;

            if (ins->params.op3 != 0)
                bank_wr[OP3(raw_ins) & (banked_regfile-1)]++;
            bundle->r_index[bundle->num_r_updates++] =
                ins->params.op3;
			extension_possible = 1;
        } else if ((OPCODE(raw_ins) & 0xF8) == 0xA8){
            // stwl (store-conditional) instruction
            ins->op = itable.conditional_load_store_table[1].op;
            ins->trace_marker = &itable.conditional_load_store_table[1];
            ins->params.op1 = OP1(raw_ins);
            ins->params.op2 = OP2(raw_ins);
            ins->params.op3 = IDEST(raw_ins);
            ins->params.cgs.scond = SCOND(raw_ins);
			if (ins->params.op1 != 0)
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(OP2(raw_ins)))
				bank_r2[bank_port(ins->params.op2, i)]++;
			extension_possible = 1;
        }

		//mul (0000----) and ldl/stl 000011-- (only 01 and 10 actually)
		else if (((OPCODE(raw_ins) >> 4) == 0))
		{
			index = RVEX_FORMAT_MUL_OPC(raw_ins);
			ins->op = itable.mul_table[index].op;
			ins->trace_marker = itable.mul_table + index;

			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);

			if (ins->params.op1 != 0)
				//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
				//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
				bank_r2[bank_port(ins->params.op2, i)]++;
			//moved bank write to the ldl/mtl check

			extension_possible = EXTEND_BIT(raw_ins);

			if (index == 22 || index == 23 || index == 27) //ldl, mtl
			{
				bundle->update_lr = 1;
			}
			else
			{
				if (ins->params.op3 != 0)
					//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
					bank_wr[bank_port(ins->params.op3, i)]++;
				bundle->r_index[bundle->num_r_updates++] =
										    ins->params.op3;
			}
		}

		//H rvex CTRL op: 0010---- and sbit/sbitf or ldbr/stbr
		else if ((OPCODE(raw_ins) >> 4) == 2)
		{
			index = RVEX_FORMAT_CALL_OPC(raw_ins);
			ins->op = itable.call_table[index].op;
			ins->trace_marker = itable.call_table + index;
			if (BITMASK_EXTRACT(27:27, raw_ins) == 1) // not a CTRL op - sbit or ldbr/stbr
			{
				ins->params.op1 = OP1(raw_ins);
				ins->params.op2 = OP2(raw_ins);
				ins->params.op3 = OP3(raw_ins);

				if (ins->params.op1 != 0)
					//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
					bank_r1[bank_port(ins->params.op1, i)]++;
				if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
					//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
					bank_r2[bank_port(ins->params.op2, i)]++;

				extension_possible = EXTEND_BIT(raw_ins);

				if (OPCODE(raw_ins) == 0x2e) //ldbr
				{
					//H this ins should be the only one modifying the br regs
					for (j = 0; j < 8; j++)
						bundle->b_index[j] = j;
					bundle->num_b_updates = 8;
				}
				else
				{
					if (ins->params.op3 != 0)
						//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
						bank_wr[bank_port(ins->params.op3, i)]++;
					bundle->r_index[bundle->num_r_updates++] =
										    ins->params.op3;

				}
			}
			else //CTRL op
			{
				/* Branch instructions do not affect register state */
				btarg = SIGN_EXTEND(BTARG(raw_ins), 18); //18 is the position of the sign bit
				ins->params.branch.btarg = btarg;

				if (BR(raw_ins) )
				{
					ins->params.branch.bcond = BCOND(raw_ins);
				}
				else
				{
					if (ins->op == call || ins->op == call_lr)
					{
						bundle->update_lr = 1;
					}
					if (ins->op == return_lr || ins->op == rfi)
					{
						bundle->r_index[bundle->num_r_updates++] = 1;
						ins->params.op1 = 1;
						//bank_writes[1 & (banked_regfile-1)]++;
						bank_wr[bank_port(1, i)]++;
					}
				}
			}
		}

		//H Select/logic op: rvex 010----- or 0111---- or 0011---- (divs, addcg)
		else if (((OPCODE(raw_ins)>> 5) == 2) || ((OPCODE(raw_ins) >> 4) == 7) || ((OPCODE(raw_ins) >> 4) == 3))
		{
			if ((OPCODE(raw_ins) >> 4) == 7) //addcg, divs
			{
				index = BITMASK_EXTRACT(27:27, raw_ins);

				ins->op = itable.slct_table[index].op;
				ins->trace_marker = itable.slct_table + index;

				ins->params.op1 = OP1(raw_ins);
				ins->params.op2 = SRC2(raw_ins); //SRC2 because EXTEND_BIT is undefined for this instruction format
				bundle->r_index[bundle->num_r_updates++] = ins->params.op3 =
						OP3(raw_ins);
				bundle->b_index[bundle->num_b_updates++] =
						ins->params.cgs.bdest = BDEST2(raw_ins);
				ins->params.cgs.scond = SCOND(raw_ins);

				if (ins->params.op1 != 0)
					//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
					bank_r1[bank_port(ins->params.op1, i)]++;
				if (ins->params.op2 != 0)
					//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
					bank_r2[bank_port(ins->params.op2, i)]++;
				if (ins->params.op3 != 0)
					//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
					bank_wr[bank_port(ins->params.op3, i)]++;
			}
			else if ((OPCODE(raw_ins) >> 4) == 3) //slct, slctf
			{
				index = 2 + BITMASK_EXTRACT(27:27, raw_ins) + (BITMASK_EXTRACT(23:23, raw_ins) * 2); //the *2 is to get the index of the imm versions in the table
				ins->op = itable.slct_table[index].op;
				ins->trace_marker = itable.slct_table + index;

				ins->params.op1 = OP1(raw_ins);
				ins->params.op2 = OP2(raw_ins);
				ins->params.op3 = OP3(raw_ins);
				ins->params.cgs.scond = SCOND(raw_ins);
				bundle->r_index[bundle->num_r_updates++] = ins->params.op3;

				if (ins->params.op1 != 0)
					//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
					bank_r1[bank_port(ins->params.op1, i)]++;
				if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
					//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
					bank_r2[bank_port(ins->params.op2, i)]++;
				if (ins->params.op3 != 0)
					//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
					bank_wr[bank_port(ins->params.op3, i)]++;

				extension_possible = EXTEND_BIT(raw_ins);
			}
			else //other logic op (cmpeq etc.)
			{
				index = RVEX_FORMAT_LOGIC_OPC(raw_ins);
				ins->op = itable.logic_table[index].op;
				ins->trace_marker = itable.logic_table + index;

				ins->params.op1 = OP1(raw_ins);
				ins->params.op2 = OP2(raw_ins);
				ins->params.op3 = OP3(raw_ins);

				if (ins->params.op1 != 0)
					//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
					bank_r1[bank_port(ins->params.op1, i)]++;
				if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
					//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
					bank_r2[bank_port(ins->params.op2, i)]++;

				if (BITMASK_EXTRACT(24:24, raw_ins) == 1) //target is a branch register
				{
					ins->params.op3 &= 0x7;
					bundle->b_index[bundle->num_b_updates++] = ins->params.op3;
				}
				else
				{
					if (ins->params.op3 != 0)
						//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
						bank_wr[bank_port(ins->params.op3, i)]++;
					bundle->r_index[bundle->num_r_updates++] = ins->params.op3;
				}


				extension_possible = EXTEND_BIT(raw_ins);
			}
		}

		//H special insns and extensions
		else if (((OPCODE(raw_ins) >> 4) == 9))
		{
			index = RVEX_FORMAT_SYSOP_OPC(raw_ins);
			ins->op = itable.sysop_table[index].op;
			ins->trace_marker = itable.sysop_table + index;



			if (index >= 4) //mullhus, mulhhs
			{
				extension_possible = EXTEND_BIT(raw_ins);
			}

			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);

			if (ins->params.op1 != 0)
				//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
				//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
				bank_r2[bank_port(ins->params.op2, i)]++;

			if (OPCODE(raw_ins) >= 0x9a && (OPCODE(raw_ins) & 1)) //dest is a branch reg
			{
				ins->params.op3 &= 0x7;
				bundle->b_index[bundle->num_b_updates++] = ins->params.op3;
			}
			else
			{
				if (ins->params.op3 != 0)
					//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
					bank_wr[bank_port(ins->params.op3, i)]++;
				bundle->r_index[bundle->num_r_updates++] = ins->params.op3;
			}
			if (index <= 1)
			{
				if (ins->params.op2 == 0x90 && syscall_emulation) //uCLibc syscall to be emulated by the simulator)
				{
					/* Syscall return value in r3 */
					bundle->r_index[bundle->num_r_updates++] = 3;
					/* using b0.0 to flag syscall errors. */
					bundle->b_index[bundle->num_b_updates++] = 0;
				}
				else if (ins->params.op2 == 0x91 && syscall_emulation) //newlib syscall to be emulated by the simulator)
				{
					/* Syscall return value in $r0.3:$r0.4 */
					bundle->r_index[bundle->num_r_updates++] = 3;
					bundle->r_index[bundle->num_r_updates++] = 4;
					/* $r0.5 = errno if error, 0 if success */
					bundle->r_index[bundle->num_r_updates++] = 5;
				}
			}
		}

		//H special insns and extensions part 2
		else if (((OPCODE(raw_ins) >> 4) == 10))
		{
			index = RVEX_FORMAT_SYSOP2_OPC(raw_ins);
			ins->op = itable.sysop2_table[index].op;
			ins->trace_marker = itable.sysop2_table + index;

			ins->params.op1 = OP1(raw_ins);
			ins->params.op2 = OP2(raw_ins);
			ins->params.op3 = OP3(raw_ins);
			bundle->r_index[bundle->num_r_updates++] = ins->params.op3;

			if (ins->params.op1 != 0)
				//bank_reads[OP1(raw_ins) & (banked_regfile-1)]++;
				bank_r1[bank_port(ins->params.op1, i)]++;
			if (ins->params.op2 != 0 && !EXTEND_BIT(raw_ins))
				//bank_reads[OP2(raw_ins) & (banked_regfile-1)]++;
				bank_r2[bank_port(ins->params.op2, i)]++;
			if (ins->params.op3 != 0)
				//bank_writes[OP3(raw_ins) & (banked_regfile-1)]++;
				bank_wr[bank_port(ins->params.op3, i)]++;
		}

		else //opcode not recognized
		{
			printf("Error; Unrecognized (illegal) opcode for syllable 0x%08x at address 0x%08x\n", raw_ins, pc);
			triggerTrap(current, TRAP_INVALID_OP, i);
			return NULL;
		}

		/* If the instruction can be extended, then do it */
		if (extension_possible) {
			if (previous_was_immr) {
				ins->params.op2 = (immr << 9) | OP2(raw_ins);
#ifdef PRINT_LIMMH
				printf("Extending immediate value 0x%x with limm extension 0x%x, ", ins->params.op2, immr);
				printf("result 0x%08x\n", ins->params.op2);
#endif

			} else if (ins->params.op2 & (1 << 8)) {
				/* Sign bit set, therefore sign extend */
				ins->params.op2 |= (0x7fffff << 9);
			}
		}

		/* Keep a pointer, so we can extend if needed */
		prev_ins = ins;

		/* Jump out once we hit the stop bit */
		if ((raw_ins & STOP_BIT) && (pc&0x7)) //ignore unaligned stopbits
		{
			bundle->stop_bit = 1;
			/* Add 4 because we break out of the loop */
			pc += 4;
			break;
		}
	}


	//8 syllables found and we didn't see a stop bit
	if (i == 8) {
		printf("Error; no stop bit found (Context: %d, PC: 0x%08x)\n", CID(current), current->pc);
		triggerTrap(current, TRAP_INVALID_OP, 0);
		return NULL;
	}

	bundle->size = pc - current->pc;


	if (current->trapcause)
		return NULL;

	if (icache_slot) {
		*icache_slot = ic;
		/* Update back ptr */
		ic->slot = icache_slot;
	}

	ic->invalidate_cycle = invalidate_cycle_count;

	icache_next = (icache_next+1) % ICACHE_NUM_ENTRIES;

	//check for bank conflicts
	/*for (i = 0; i < banked_regfile; i++)
	{
		if (bank_writes[i] > 8/banked_regfile) // 8 writeports divide by num_banks to get writeports per bank
		{
			printf("warning; bank write conflict; %d writes to bank %d! (Context: %d, PC: 0x%08x)\n", bank_writes[i], i, CID(current), current->pc);
		}
		if (bank_reads[i] > 16/banked_regfile) // 16 readports divide by num_banks to get readports per bank
		{
			printf("warning; bank read conflict; %d reads to bank %d! (Context: %d, PC: 0x%08x)\n", bank_reads[i], i, CID(current), current->pc);
		}
	}*/
	bundle->bank_r1t = 0;
	bundle->bank_r2t = 0;
	bundle->bank_wrt = 0;
	/*for (i=0; i<8; i++)
	{
		bundle->bank_r1[i] = 0;
		bundle->bank_r2[i] = 0;
		bundle->bank_wr[i] = 0;
	}*/
	for (i=0; i<8; i++)
	{
		if (bank_r1[i] > 1)
		{
			printf("warning; bank read1 conflict; %d read1s to port %d! (Context: %d, PC: 0x%08x)\n", bank_r1[i], i, CID(current), current->pc);
			bundle->bank_r1t++;
			//bundle->bank_r1[bank_r1[i]]++;
		}
		if (bank_r2[i] > 1)
		{
			printf("warning; bank read2 conflict; %d read2s to port %d! (Context: %d, PC: 0x%08x)\n", bank_r2[i], i, CID(current), current->pc);
			bundle->bank_r2t++;
			//bundle->bank_r2[bank_r2[i]]++;
		}
		if (bank_wr[i] > 1)
		{
			printf("warning; bank write conflict; %d writes to port %d! (Context: %d, PC: 0x%08x)\n", bank_wr[i], i, CID(current), current->pc);
			bundle->bank_wrt++;
			//bundle->bank_wr[bank_wr[i]]++;
		}
	}

	//H TODO: We need to reorder the bundle so that MEM ops will be last (because of traps: a mem op cannot be reverted)
	return bundle;
}

int rvex_get_instr_index(struct instr_info * ins)
{
	int index = ins - ((struct instr_info *)&itable);
	
	return index;
}

struct instr_info * rvex_get_instr_info(int index)
{
	return  ((struct instr_info *)&itable) + index;
}

void rvex_decode_init(int enable_dcache)
{
	if (enable_dcache) {
		memcpy(itable.load_store_table, load_store_table_dcache,
		       sizeof(load_store_table_dcache));

		//H TODO: xchg
		//H ldl, stl, ldbr, stbr are not in the table, copy them individually
		itable.mul_table[27] = other_load_store_table_dcache[0];
		itable.mul_table[29] = other_load_store_table_dcache[1];
		itable.call_table[14] = other_load_store_table_dcache[2];
		itable.call_table[15] = other_load_store_table_dcache[3];

	}

	//H TODO: Floating point instructions (create a flag to enabled/disable them)
}

