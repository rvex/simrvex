#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

static struct option long_options[] = {
	{"help", 0, 0, 'h'},
	{"symbols", 1, 0, 's'},
	{0, 0, 0, 0}
};

char *cmd;

struct sym {
	unsigned long addr;
	char *name;
};

struct sym *table;
unsigned int table_size;

int sym_cmp(const void *p1, const void *p2)
{
	const struct sym *s1 = (struct sym *)p1;
	const struct sym *s2 = (struct sym *)p2;
	if (s1->addr == s2->addr) {
		return 0;
	} else if (s1->addr > s2->addr) {
		return 1;
	} else {
		return -1;
	}
}

void read_symbols(const char *filename, long offset)
{
	int fd = open(filename, O_RDONLY);
	char *buffer;
	struct stat sbuf;
	int size;
	char *p, *q;
	struct sym *t;
	unsigned long lines;

	if (fd < 0) {
		fprintf(stderr, "%s: Unable to open symbol file %s: %s\n",
			cmd, filename, strerror(errno));
		exit(1);
	}

	if (fstat(fd, &sbuf) != 0) {
		fprintf(stderr, "%s: stat failed on symbol file: %s\n",
			cmd, strerror(errno));
		exit(1);
	}

	size = sbuf.st_size;
	buffer = malloc(size);
	if (buffer == NULL) {
		fprintf(stderr, "%s: Unable to malloc buffer\n", cmd);
		exit(1);
	}

	if (read(fd, buffer, size) != size) {
		fprintf(stderr, "%s: read failed on symbol file: %s\n",
			cmd, strerror(errno));
		exit(1);
	}

	/* Find how many lines */
	lines = 0;
	q = buffer + size;
	for (p = buffer; p < q; p++) {
		if (*p == '\n') {
			lines++;
			*p = '\0';
		}
	}

 	table = realloc(table, sizeof(struct sym) * (table_size + lines + 1));
	if (table == NULL) {
	  fprintf(stderr, "%s: Unable to malloc table\n", cmd);
		exit(1);
	}

	/* Fill in the table */
 	t=table+table_size;
	for (p = buffer; p < q; p += strlen(p) + 1) {
		char *pp;
		unsigned long addr;
		char *section;
		char *name;

		if (strlen(p) < 17)
			continue;
		addr = strtoul(p, &pp, 16);
		if (pp != (p + 8))
			continue;
		section = p + 17;
		name = strchr(section, ' ');
		if (name == NULL)
			continue;
		name++;

		if ((addr == 0) || (*name == '\0') ||
		    (strncmp(section, ".text", 5)
		     && strncmp(section, ".init.text", 10)))
			continue;

		t->addr = addr + offset;
		t->name = name;
		table_size++;
		t++;
	}

	t->addr = 0xffffffff;
	t->name = "Bad symbol";

	close(fd);

	qsort(table, table_size, sizeof(struct sym), sym_cmp);
}

void symbolise(const char *filename)
{
	FILE *file = fopen64(filename, "r");
	char buffer[1024];
	const struct sym *prev;
	int indent = 0;
	char spaces[1024];

	for (indent = 0; indent < sizeof(spaces) - 1; indent++)
		spaces[indent] = ' ';
	spaces[sizeof(spaces) - 1] = '\0';

	if (file == NULL) {
		fprintf(stderr, "%s: Unable to open trace file %s: %s\n",
			cmd, filename, strerror(errno));
		exit(1);
	}

	prev = table;
	indent = 0;
	while (fgets(buffer, sizeof(buffer), file) != NULL) {
		int len = strlen(buffer);
		char *pp;
		unsigned long addr;

		if (buffer[len - 1] == '\n') {
			buffer[len - 1] = '\0';
		}

		addr = strtoul(buffer, &pp, 16);
		if ((pp == (buffer + 8)) && (pp[0] == ':') && (pp[1] == ' ')) {
			if ((addr < prev->addr) || (addr >= (prev + 1)->addr)) {
				const struct sym *first = table;
				const struct sym *last = &table[table_size];
				while (first <= last) {
					prev = (last - first) / 2 + first;
					if ((addr >= prev->addr)
					    && (addr < (prev + 1)->addr)) {
						break;
					}
					if (addr < prev->addr) {
						last = prev - 1;
					} else {
						first = prev + 1;
					}
				}
			}
			if ((addr - prev->addr) > (16 * 1024)) {
				puts(buffer);
				indent = 0;
			} else {
				char buffer2[1024];
				sprintf(buffer2, "%s+%#-6lx", prev->name,
					addr - prev->addr);
				*pp = '\0';
				printf("%s: %s%s\n", buffer, buffer2, pp + 1);
				indent = strlen(buffer2) + 1;
			}
		} else {
			printf("%.*s%s\n", indent, spaces, buffer);
		}
	}

	fclose(file);
}

void usage(void)
{
	fprintf(stderr, "%s: symbolise for DaveSim log files\n", cmd);
	fprintf(stderr, "Usage: %s [OPTION] log\n\n", cmd);
	fprintf(stderr,
		"  -s, --symbols file[=offset]    symbol table (output from objdump --syms)\n");
	fprintf(stderr,
		"                                 (default vmlinux.syms)\n");
	exit(1);
}

int main(int argc, char *argv[])
{
	char *symbol_filename = "vmlinux.syms";
	char c;

	cmd = strchr(argv[0], '/');
	if (cmd == NULL) {
		cmd = argv[0];
	} else {
		cmd++;
	}

	while ((c = getopt_long(argc, argv, "s:", long_options, NULL)) != -1) {
		switch (c) {
		case 'h':
			usage();
		case 's':
                        {
			  long offset = 0;
			  char *p;
			  char *filename = optarg;
			  p = strchr (filename, '=');
			  if (p) {
			    *p = 0;
			    offset = strtol (p + 1, 0, 0);
			  }
			  read_symbols(filename, offset);
			  symbol_filename = 0;
			}
			break;
		default:
			exit(1);
		}
	}

	if (argc - optind != 1) {
		fprintf(stderr, "%s: Missing trace filename\n", cmd);
		exit(1);
	}

	if (symbol_filename) read_symbols (symbol_filename, 0);
	symbolise(argv[optind]);
}
