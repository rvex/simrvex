#ifndef LINK_REGISTER_H
#define LINK_REGISTER_H
#include "simtypes.h"

/**
 * @brief This function should be called whenever the address ea should be watched by the link register
 * @param ea The address line that will be watched
 * @param cid The context ID of the caller
 */
void loadLinked(unsigned ea, u32 cid);

/**
 * @brief Use this function to flag a write to an address
 * @param ea The address line that is being written to
 * @warning You should call this function on every write
 */
void notifyWrite(unsigned ea);

/**
 * @brief returns the current status of address line ea
 * @param ea The address line you want information about
 * @param cid The context ID of the caller
 * @return 1 if the address was load linked in the past and never written to since, 0 otherwise
 */
int isLinked(unsigned ea, u32 cid); 

/**
 * @brief Invalidates every link line related to this specific context id
 *
 * This function should be used if a context is interrupted.
 * The problem that is solved here is that it is impossible for the hardware to know wether or not it is acutally the same process requesting the line.
 * If you leave this out, you could still get a race condition between two threads on the same core (or a thread and a interrupt handler, or...).
 * So, the safe route is to invalidate everything for this context and live happily ever after.
 * @param cid The id of the context that is to be invalidated in the link register.
 */
void invalidateForCid(u32 cid);

/**
 * @brief Does a nice printf of the link registers
 */
void dumpLinkReg(void);

#endif //LINK_REGISTER_H
