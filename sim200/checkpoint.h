#ifndef __ST200_CHECKPOINT_H__
#define __ST200_CHECKPOINT_H__

#include "state.h"

struct checkpoint {
	char header[32];
	char vendor[128];
	unsigned core;
	unsigned version;

	unsigned pc;		// Program counter (virtual address)
	unsigned registers[64];	// Core registers r0-r63
	unsigned branch;	// Branch bits, b0-b7 (1 bit each)

	unsigned ctrl[0x2000];	// Values of all control registers from 0xffff0000 - 0xffffffff (8 bytes apart)
	unsigned intc[0x200];	// Values of INTC/Timer registers from 0x1f000000-0x1f000fff (8 bytes apart)
	unsigned dsu[0x200];	// Values of DSU registers from 0x1f003000-0x1f003fff (8 bytes apart)
//	unsigned tlb[64][4];	// Contents of TLB
	unsigned ram_start;	// Start address of local ram - 0x08000000
	unsigned ram_size;	// Size of local ram - 16MB
	char ram_data[0];	// Contents of local ram - 16MB of data
};

int dump_checkpoint(char *filename, cx_state_t *current);

int restore_checkpoint(char *filename);

#endif
