#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>

#include "state.h"
#include "instr.h"

#include "instr_fields.h"
#include "trace.h"
#include "decode.h"
#include  "ctrlregdef.h"
#include "memory.h"
#include "load.h"

int trace_mode = TRACE_OFF;
int circular_trace = 0;
int trace_context;
int trace_start_cycle = 0;
int trace_start_addr = 0;
int trace_started = 0;

extern glob_state_t global_state;

static struct trace_entry *trace_buffer;
static unsigned trace_buffer_entries;
static unsigned trace_pos;
static int trace_wrapped;

/* A trace entry consists of the following
 * u32 marker - what instruction this is, plus info 
 * u32 pc     - PC of instruction, only valid for 1st entry in bundle
 * 6 data words, word entries will be aligned, byte entries packed.
 * The worst case is ldlc for ST240
 */

struct trace_entry {
	u32 marker;
	u32 pc;
	u32 raw_insn;
	u8 contextId; //H added (other option is to use separate buffers per context)
	u8 data[24];
};

#define RVTRACE

#define PAD_STRING ""
#define PAD_LEN 0

#define TRACE_BUNDLE_STOP            (1<<31)	/* Last entry in the bundle */
#define TRACE_BUNDLE_START           (1<<30)	/* Start of a new bundle - PC is valid */

#define TRACE_FIELD_TYPE_MASK        (0xff<<16)

#define TRACE_BUNDLE_OK              0
#define TRACE_BUNDLE_EXCEPTION       1
#define TRACE_EXCEPTION_INFO         2

#define SET_TRACE_FIELD_TYPE(x,type)  (((x)&TRACE_FIELD_TYPE_MASK)| (type)<<16)
#define GET_TRACE_FIELD_TYPE(x) ( ((x)&TRACE_FIELD_TYPE_MASK)>>16)

#define TRACE_INSTR_INDEX_MASK       0xffff

void allocate_trace_buffer(int size_in_megs)
{

	trace_buffer_entries =
	    (size_in_megs * 1024 * 1024) / sizeof(struct trace_entry);

	/* Allocate space for the trace buffer */
	trace_buffer =
	    malloc(trace_buffer_entries * sizeof(struct trace_entry));

	if (trace_buffer == NULL) {
		fprintf(stderr,
			"Cannot allocate %d Mbytes for trace buffer - bye\n",
			size_in_megs);
		exit(1);
	}

	trace_pos = 0;
	trace_wrapped = 0;
}

static unsigned char *trace_dump_bytes(unsigned char *buffer, int n, ...)
{
	int i;
	va_list ap;

	va_start(ap, n);
	for (i = 0; i < n; i++) {
		*buffer++ = (unsigned char)va_arg(ap, unsigned);
	}
	va_end(ap);
	return buffer;
}

static unsigned char *trace_dump_words(unsigned char *buffer, int n, ...)
{
	int i;
	va_list ap;
	u32 *b = (u32 *) (((size_t) buffer + 3) & ~3);	/* Align */

	va_start(ap, n);
	for (i = 0; i < n; i++) {
		*b++ = va_arg(ap, unsigned);
	}
	va_end(ap);

	return (unsigned char *)b;
}

static unsigned char *trace_extract_bytes(unsigned char *buffer, int n, ...)
{
	int i;
	va_list ap;
	unsigned *p;
	unsigned char *b = buffer;

	va_start(ap, n);
	for (i = 0; i < n; i++) {
		p = va_arg(ap, u32 *);
		*p = (u32) * (b++);
	}
	va_end(ap);

	return b;

}

static unsigned char *trace_extract_words(unsigned char *buffer, int n, ...)
{
	int i;
	va_list ap;
	u32 *b = (unsigned *)(((size_t) buffer + 3) & ~3);	/* Align */
	u32 *p;

	va_start(ap, n);
	for (i = 0; i < n; i++) {
		p = va_arg(ap, u32 *);
		*p = (u32) * (b++);
	}

	va_end(ap);

	return (unsigned char *)b;

}

static char * trace_memory_string(enum instr_type type,int predicate,u32 ea,u32 phys)
{
	static char buffer[64];
	static const char pred_valid[8]= {0,0,1,1,0,0,1,1};
	static const char is_pair[8]={0,1,0,1,0,1,0,1}; /* All odd, but just in case somebody adds another format... */
	char sym= (type>=STORE_TYPE) ? 'S' : 'L'; /* Load or store */
	int offset = 0;
	char* symbolname = NULL;
	
	assert(!((type<LOAD_TYPE) || (type >STORE_CONDITIONAL_PAIR_TYPE)));
	
	if(pred_valid[type-LOAD_TYPE] && !predicate) {
		buffer[0]='\0'; /* print nothing for predications that fail */
	}else if(is_pair[type-LOAD_TYPE]) {
		if(phys == ~0) {
			sprintf(buffer,"%c{%x} %c{%x} P{--------} P{--------}",sym,ea,sym,ea+4);
		} else {
			sprintf(buffer,"%c{%x} %c{%x} P{%x} P{%x}",sym,ea,sym,ea+4,phys-4,phys);
		}
	}else {
		if(phys == ~0) {
			sprintf(buffer,"%c{%x} P{--------}",sym,ea);
		} else {
			if ((phys > MEMORY_START) && (phys < MEMORY_START + MEMORY_SIZE))
			{
				if (find_data_symbol_and_offset_from_addr(ea, &symbolname, &offset))
				{
					if (offset == 0)
						sprintf(buffer,"%c{%x} P{%x} <%s>",sym,ea,phys, symbolname);
					else
						sprintf(buffer,"%c{%x} P{%x} <%s + 0x%x>",sym,ea,phys, symbolname, offset);
				}
				else
					sprintf(buffer,"%c{%x} P{%x}",sym,ea,phys);
			}
			else
				sprintf(buffer,"%c{%x} P{%x}",sym,ea,phys);
		}
	}

	return buffer;
}

static int print_ins(FILE * f, struct trace_entry *entry)
{
	int index;
	struct instr_info *ins;
	u32 dest, src2, src1, r_dest, r_src2,
	    r_src1, idest, r_idest,
	    isrc2;
	u32 bdest, b_bdest, ibdest, b_ibdest, b_scond, scond, btarg;
	u32 bcond, b_bcond, phys,new_ccr, b_bdest2,bdest2,b_bsrc2,bsrc2,bsrc1,b_bsrc1;
	unsigned char *b;
	char *n;
	char *symbol;
	int offset;
	int type;
	enum instr_type itype;
	static int branched[NCONTEXTS];
	
	type = GET_TRACE_FIELD_TYPE(entry->marker);
	index = entry->marker & TRACE_INSTR_INDEX_MASK;
	ins = get_instr_info(index);
	itype=ins->trace.type;

	/* Print out the PC value */
	if (entry->marker & TRACE_BUNDLE_START) {
		if (type == TRACE_BUNDLE_EXCEPTION)
			fputc('\n', f);
		if (trace_context != 1) //H Dont print if only tracing c0
			fprintf(f, "Context %d:\n",entry->contextId);
		if (branched[entry->contextId])
		{
			branched[entry->contextId] = 0;
			if ((entry->pc > MEMORY_START) && (entry->pc < MEMORY_START + MEMORY_SIZE))
			{
				if (find_text_symbol_and_offset_from_addr(entry->pc, &symbol, &offset))
				{
				if (offset == 0)
					fprintf(f,"<%s>:\n",symbol);
				else
					fprintf(f,"<%s + 0x%x>:\n",symbol, offset);
				}
			}
		}
	}
	fprintf(f, "%s", PAD_STRING);
	fprintf(f, "%8X: ", entry->pc);


#ifndef RVTRACE
	fprintf(f, "[%08x] ", entry->raw_insn);
#else
	fprintf(f, "%02x %02x %02x %02x\t", (entry->raw_insn>>24)&0xff, (entry->raw_insn>>16)&0xff, (entry->raw_insn>>8)&0xff, entry->raw_insn&0xff);
#endif
	b = entry->data;
	n = (char *)ins->trace.name;

	switch (itype) {
	case INT3R_TYPE:
		b = trace_extract_bytes(b, 3, &dest, &src2, &src1);
		trace_extract_words(b, 3, &r_dest, &r_src2, &r_src1);

		if (ins->op == nop)
			fprintf(f, "nop");

		else if (ins->op == mtl)
		{
			fprintf(f, "mtl lr(0x%x) = r%d(0x%x)",
								r_src2, src2, r_src2);
		}

		else if (ins->op == add) {
			if (src1 == 0) {
				fprintf(f, "mov r%d(0x%x) = r%d(0x%x)", dest,
					r_dest, src2, r_src2);
			} else {
				fprintf(f, "%s r%d(0x%x) = r%d(0x%x), r%d(0x%x)", n,
					dest, r_dest, src1, r_src1, src2,
					r_src2);
			}
		} else {
			if (ins->op == sub) {
				fprintf(f, "%s r%d(0x%x) = r%d(0x%x), r%d(0x%x)", n,
					dest, r_dest, src2, r_src2, src1,
					r_src1);
			} else {
				fprintf(f, "%s r%d(0x%x) = r%d(0x%x), r%d(0x%x)", n,
					dest, r_dest, src1, r_src1, src2,
					r_src2);
			}
		}
		break;
	case INT3I_TYPE:
		b = trace_extract_bytes(b, 2, &idest, &src1);
		b = trace_extract_words(b, 3, &r_idest, &isrc2, &r_src1);

		if (ins->op == add_immediate && src1 == 0) {
			fprintf(f, "mov r%d(0x%x) = 0x%x", idest, r_idest, isrc2);
		} else if (ins->op != sub_immediate) {
			fprintf(f, "%s r%d(0x%x) = r%d(0x%x), 0x%x", n,
				idest, r_idest, src1, r_src1, isrc2);
		} else {
			fprintf(f, "%s r%d(0x%x) = r%d(0x%x), 0x%x", n,
				idest, r_idest, src1, r_src1, isrc2);
		}
		break;
	case MONADIC_TYPE:
		b = trace_extract_bytes(b, 2, &idest, &src1);
		b = trace_extract_words(b, 2, &r_idest, &r_src1);
		fprintf(f, "%s r%d(0x%x) = r%d(0x%x)",
			n, idest, r_idest, src1, r_src1);
		break;
	case CMP3R_TYPE:
		b = trace_extract_bytes(b, 3, &dest, &src1, &src2);
		b = trace_extract_words(b, 3, &r_dest, &r_src1, &r_src2);
		fprintf(f, "%s r%d(%d) = r%d(0x%x), r%d(0x%x)", n,
			dest, r_dest, src1, r_src1, src2, r_src2);
		break;
	case CMP3R_BR_TYPE:
		b = trace_extract_bytes(b, 4, &bdest, &b_bdest, &src1, &src2);
		b = trace_extract_words(b, 2, &r_src1, &r_src2);

		if (0 && ins->op == orl_bdest && src2 == 0) {
			fprintf(f, "mtb b%d(%d) = r%d(0x%x)", bdest, b_bdest,
				src1, r_src1);
		} else {
			fprintf(f, "%s b%d(%d) = r%d(0x%x), r%d(0x%x)", n,
				bdest, b_bdest, src1, r_src1, src2, r_src2);
		}
		break;
	case CMP3I_TYPE:
		b = trace_extract_bytes(b, 2, &idest, &src1);
		b = trace_extract_words(b, 3, &r_idest, &r_src1, &isrc2);
		fprintf(f, "%s r%d(%d) = r%d(0x%x), 0x%x", n,
			idest, r_idest, src1, r_src1, isrc2);
		break;
	case CMP3I_BR_TYPE:
		b = trace_extract_bytes(b, 3, &ibdest, &b_ibdest, &src1);
		b = trace_extract_words(b, 2, &r_src1, &isrc2);
		fprintf(f, "%s b%d(%d) = r%d(0x%x), 0x%x", n,
			ibdest, b_ibdest, src1, r_src1, isrc2);
		break;
	case BR3R_TYPE:
		b=trace_extract_bytes(b,6,&bdest2,&b_bdest2,&bsrc2,&b_bsrc2,&bsrc1,&b_bsrc1) ;
		fprintf(f,"%s b%d(%d) = b%d(%d), b%d(%d)",n,
				bdest2,b_bdest2,bsrc2,b_bsrc2,bsrc1,b_bsrc1);
		break;
	case SELECTR_TYPE:
		b = trace_extract_bytes(b, 5, &dest, &scond, &b_scond, &src1,
					&src2);
		b = trace_extract_words(b, 3, &r_dest, &r_src1, &r_src2);
		fprintf(f, "%s r%d(0x%x) = b%d(%d), r%d(0x%x), r%d(0x%x)", n,
			dest, r_dest, scond, b_scond, src1, r_src1, src2,
			r_src2);
		break;
	case SELECTI_TYPE:
		b = trace_extract_bytes(b, 4, &idest, &scond, &b_scond, &src1);
		b = trace_extract_words(b, 3, &r_idest, &r_src1, &isrc2);
		if ((ins->op == slctf_immediate) && (isrc2 == 1) && (src1) == 0) {
			fprintf(f, "mfb r%d(0x%x) = b%d(%d)",
				idest, r_idest, scond, b_scond);
		} else {
			fprintf(f, "%s r%d(0x%x) = b%d(%d), r%d(0x%x), 0x%x", n,
				idest, r_idest, scond, b_scond, src1, r_src1,
				isrc2);
		}
		break;
	case CGEN_TYPE:{
			unsigned cgs_bdest, cgs_b_bdest;
			b = trace_extract_bytes(b, 7, &dest, &cgs_bdest,
						&cgs_b_bdest, &src1, &src2,
						&scond, &b_scond);
			b = trace_extract_words(b, 3, &r_dest, &r_src1,
						&r_src2);
			fprintf(f,
				"%s r%d(0x%x), b%d(%d) = r%d(0x%x), r%d(0x%x), b%d(%d)",
				n, dest, r_dest, cgs_bdest, cgs_b_bdest, src1,
				r_src1, src2, r_src2, scond, b_scond);
			break;
		}
	case SYSOP_TYPE:
	case SBREAK_TYPE:
		fprintf(f, "%s", n);
		break;
	case LOAD_TYPE:
		if (ins->op == ldl)
		{
			b = trace_extract_bytes(b, 1, &src1);
			b = trace_extract_words(b, 4, &idest, &isrc2, &r_src1, &phys);
			fprintf(f, "%s l0(0x%x) = 0x%x[r%d(0x%x)] %s", n,
					idest, isrc2, src1, r_src1, trace_memory_string(itype, b_scond,
						r_src1+isrc2, phys));

		}
		else
		{
			b = trace_extract_bytes(b, 2, &idest, &src1);
			b = trace_extract_words(b, 4, &r_idest, &isrc2, &r_src1, &phys);
			fprintf(f, "%s r%d(0x%x) = 0x%x[r%d(0x%x)] %s", n,
					idest, r_idest, isrc2, src1, r_src1, trace_memory_string(itype, b_scond,
						r_src1+isrc2, phys));
		}
		break;
//	case LOAD_PAIR_TYPE:
//		b = trace_extract_bytes(b, 2, &idest, &src1);
//		b = trace_extract_words(b, 5, &r_idest, &r_idestplus1,&isrc2, &r_src1, &phys);
//		fprintf(f, "%s p%d (r%d r%d) 0x%x 0x%x = 0x%x[r%d(0x%x)] %s", n,
//			idest,idest,idest+1, r_idest, r_idestplus1,isrc2, src1, r_src1,
//			trace_memory_string(itype,b_scond,r_src1+isrc2,phys));
//		break;
//  	case LOAD_CONDITIONAL_TYPE:
//    		b=trace_extract_bytes(b,4,&scond,&b_scond,&idest,&src1);
//    		b=trace_extract_words(b,4,&r_idest,&isrc2,&r_src1,&phys);
//      		fprintf(f,"%s r%d(0x%x) = b%u 0x%x, 0x%x[r%d(0x%x)] %s",n,
//	      			idest,r_idest, scond, b_scond, isrc2,src1,r_src1,
//				trace_memory_string(itype,b_scond,r_src1+isrc2,phys));
//    		break;
//	case LOAD_CONDITIONAL_PAIR_TYPE:
//    		b=trace_extract_bytes(b,4,&scond,&b_scond,&idest,&src1);
//    		b=trace_extract_words(b,5,&r_idest,&r_idestplus1,&isrc2,&r_src1,&phys);
//      		fprintf(f,"%s p%d (r%d r%d) 0x%x 0x%x = b%u 0x%x, 0x%x[r%d(0x%x)] %s",n,
//	      			idest,idest,idest+1,r_idest,r_idestplus1, scond, b_scond, isrc2,src1,r_src1,
//				trace_memory_string(itype,b_scond,r_src1+isrc2,phys));
//		break;
	case STORE_TYPE:
		b = trace_extract_bytes(b, 2, &src1, &src2);
		b = trace_extract_words(b, 4, &isrc2, &r_src1, &r_src2, &phys);
		if (ins->op == stl)
		{
			fprintf(f, "%s 0x%x[r%d(0x%x)] = l0 0x%x %s", n,
				isrc2, src1, r_src1, r_src2,trace_memory_string(itype,b_scond,r_src1+isrc2,phys));

		}
		else
		{
			fprintf(f, "%s 0x%x[r%d(0x%x)] = r%d(0x%x) %s", n,
				isrc2, src1, r_src1, src2, r_src2,trace_memory_string(itype,b_scond,r_src1+isrc2,phys));

		}
		break;
//	case STORE_PAIR_TYPE:
//		b = trace_extract_bytes(b, 2, &src1, &src2);
//		b = trace_extract_words(b, 5, &isrc2, &r_src1, &r_src2,&r_src2plus1, &phys);
//		fprintf(f, "%s 0x%x[r%d(0x%x)] = p%d (r%d r%d) 0x%x 0x%x %s", n,
//			isrc2, src1, r_src1, src2,src2,src2+1,r_src2,r_src2plus1,
//			trace_memory_string(itype,b_scond,r_src1+isrc2,phys));
//		break;
//	case STORE_CONDITIONAL_TYPE:
//    		b=trace_extract_bytes(b,4,&scond, &b_scond, &src1,&src2);
//    		b=trace_extract_words(b,4,&isrc2,&r_src1,&r_src2,&phys);
//      		fprintf(f,"%s 0x%x[r%d(0x%x)] = b%u 0x%x, r%d(0x%x) %s",n,
//	      		isrc2,src1,r_src1,scond, b_scond, src2,r_src2,trace_memory_string(itype,b_scond,r_src1+isrc2,phys));
//    		break;
//	case STORE_CONDITIONAL_PAIR_TYPE:
//    		b=trace_extract_bytes(b,4,&scond, &b_scond, &src1,&src2);
//    		b=trace_extract_words(b,5,&isrc2,&r_src1,&r_src2,&r_src2plus1,&phys);
//		fprintf(f,"%s 0x%x[r%d(0x%x)] = b%u 0x%x, p%d (r%d r%d) 0x%x 0x%x %s",n,
//					isrc2,src1,r_src1,scond, b_scond, src2,src2,src2+1,r_src2,r_src2plus1,
//					trace_memory_string(itype,b_scond,r_src1+isrc2,phys));
//		break;
	case STORE_OTHER_TYPE:
		b = trace_extract_bytes(b, 2, &src1, &src2);
		b = trace_extract_words(b, 4, &isrc2, &r_src1, &r_src2, &phys);
		if (ins->op == instr_sync) {
			fprintf(f, "%s", n);
		}
//		else if (ins->op == ccrset || ins->op == ccrclr) {
//			fprintf(f, "%s r%d(0x%x)", n, src2, r_src2);
//		}
		else {
			fprintf(f, "%s 0x%x[r%d(0x%x)] {0x%x}", n, isrc2, src1,
				r_src1, r_src1 + isrc2);
		}
		break;
	case PSW_TYPE:
		b = trace_extract_bytes(b, 2, &idest, &src1);
		b = trace_extract_words(b, 4, &r_idest, &isrc2, &r_src1, &new_ccr);
		fprintf(f, "%s r%d(0x%x) = r%d(0x%x), 0x%x PSW = 0x%x", n,
			idest, r_idest, src1, r_src1,isrc2,new_ccr);
		break;
		
	case CALL_TYPE:{
			unsigned tp, sccr, linkreg;
			branched[entry->contextId] = 1;

			b = trace_extract_words(b, 4, &tp, &sccr, &btarg,
					    &linkreg);

			//I don't mind that the return instructions always print symbol + offset, they will most likely not return to the beginning of a function anyway
			if (ins->op == rfi) {
				trace_extract_words(b, 1, &src1); //the stack pointer before the stack pop value TODO maybe add new r1 value (or compute it here)
				if (find_text_symbol_and_offset_from_addr(tp, &symbol, &offset))
				{
					fprintf(f, "rfi {pc = 0x%x  <%s + 0x%d>, ccr = 0x%x}, r1 (?) = r1 (0x%x), 0x%x", tp, symbol, offset,
					sccr, src1, ((entry->raw_insn>>5)&0x3FFFF));
				}
				else
				{
					fprintf(f, "rfi {pc = 0x%x, ccr = 0x%x}, r1 (?) = r1 (0x%x), 0x%x", tp,
										sccr, src1, ((entry->raw_insn>>5)&0x3FFFF));
				}
			} else if (ins->op == call) {
				fprintf(f, "%s 0x%x", n, btarg);
				if (find_text_symbol_and_offset_from_addr(btarg, &symbol, &offset))
				{
					if (offset)
						fprintf(f, " <%s + 0x%x>", symbol, offset);
					else
						fprintf(f, " <%s>", symbol);
				}
			} else if (ins->op == go_to) {
				fprintf(f, "%s 0x%x", n, btarg);
				if (find_text_symbol_and_offset_from_addr(btarg, &symbol, &offset))
				{
					if (offset)
						fprintf(f, " <%s + 0x%x>", symbol, offset);
					else
						fprintf(f, " <%s>", symbol);
				}
			} else if (ins->op == go_to_lr)	{
				fprintf(f, "%s lr 0x%x", n, linkreg);
				if (find_text_symbol_and_offset_from_addr(linkreg, &symbol, &offset))
				{
					if (offset)
						fprintf(f, " <%s + 0x%x>", symbol, offset);
					else
						fprintf(f, " <%s>", symbol);
				}
			} else if (ins->op == return_lr) {
				trace_extract_words(b, 1, &src1);
				if (find_text_symbol_and_offset_from_addr(linkreg, &symbol, &offset))
				{
					fprintf(f, "%s lr 0x%x <%s + 0x%d>, r1 (0x%x) = r1 (0x%x), 0x%x", n, linkreg, symbol, offset, src1 + ((entry->raw_insn>>5)&0x3FFFF), src1, ((entry->raw_insn>>5)&0x3FFFF));
				}
				else
				{
					fprintf(f, "%s lr 0x%x, r1 (0x%x) = r1 (0x%x), 0x%x", n, linkreg, src1 + ((entry->raw_insn>>5)&0x3FFFF), src1, ((entry->raw_insn>>5)&0x3FFFF));
				}
			} else if (ins->op == call_lr) {
				fprintf(f, "%s lr 0x%x", n, linkreg);
				if (find_text_symbol_and_offset_from_addr(linkreg, &symbol, &offset))
				{
					if (offset)
						fprintf(f, " <%s + 0x%x>", symbol, offset);
					else
						fprintf(f, " <%s>", symbol);
				}
			}
			break;
		}
	case BRANCH_TYPE:
		branched[entry->contextId] = 1;
		b = trace_extract_bytes(b, 2, &bcond, &b_bcond);
		b = trace_extract_words(b, 1, &btarg);

		fprintf(f, "%s b%d(%d), 0x%x", n, bcond, b_bcond, btarg);
		if (find_text_symbol_and_offset_from_addr(btarg, &symbol, &offset))
		{
			if (offset)
				fprintf(f, " <%s + 0x%x>", symbol, offset);
			else
				fprintf(f, " <%s>", symbol);
		}
		break;
	case PSEUDO_TYPE:
		if (ins->op == sim_syscall)
			fprintf(f, "syscall");
		else if (ins->op == limm)
		{
			b = trace_extract_bytes(b, 1, &src1);
			b = trace_extract_words(b, 1, &src2);
			fprintf(f, "limmh 0x%d, 0x%x", src1, src2);
		}
		break;
	default:
		fprintf(f,"UNKNOWN TYPE %d\n",itype);
		break;
	}

	if (type == TRACE_BUNDLE_EXCEPTION) {
		fprintf(f, " <DID NOT COMPLETE>\n");
	} else {
		if (entry->marker & TRACE_BUNDLE_STOP) {
			/* Last syllable in bundle. Print the ;; */
			fprintf(f, ";;\n");
	} else fprintf(f, "\n");
	}


	return 1;
}

#if 0
static char *exception_string_table[] = {
	"STBUS_IC",
	"STBUS_DC",
	"EXTERN_INT",
	"IBREAK",
//	"ITLB",
	"SBREAK",
	"ILL_INST",
	"SYSCALL",
	"DBREAK",
	"MISALIGNED",
	"CREG",
	"CREG",
//	"DTLB",
	"SDI"
};

#define NUM_EXCEPTIONS (sizeof(exception_string)/sizeof(char*))

static char *exception_string(unsigned e)
{
	int i;

	for (i = 0; i < 32; i++) {
		if ((e >> i) & 1)
			break;
	}

	if (i >= 14)
		return "UNKNOWN";

	return exception_string_table[i];

}
#endif

//static char *tlb_cause_string(unsigned e)
//{
//	switch (e & TLB_EXCAUSE_CAUSE_MASK) {
//	case TLB_EXCAUSE_CAUSE_NO_MAPPING:
//		return "TLB_NO_MAPPING";
//		break;
//	case TLB_EXCAUSE_CAUSE_WRITE_TO_CLEAN:
//		return "TLB_WRITE_TO_CLEAN";
//		break;
//	case TLB_EXCAUSE_CAUSE_PROT_VIOLATION:
//		return "TLB_PROT_VIOLATION";
//	case TLB_EXCAUSE_CAUSE_MULTI_MAPPING:
//		return "TLB_MULTI_MAPPING";
//
//	}
//
//	return "UNKNOWN";
//}

#ifdef NOHANDLERS
static void print_exception(FILE * f, struct trace_entry *entry)
{
	char *name;
	unsigned cause, arg, point;

	trace_extract_words(entry->data, 2, &cause, &arg, &point);

	fprintf(f, PAD_STRING);
	fprintf(f, "Trap:\n");


	switch (cause){
		case 1:
			fprintf(f, "Invalid opcode at %p\n", point);
			break;
		case 2:
			fprintf(f, "Misaligned branch at %p to %p\n", point, (void *)arg);
			break;
		case 3:
			fprintf(f, "Fetch fault at %p\n", point);
			break;
		case 4:
			fprintf(f, "Misaligned data access at %p to %p\n", point, (void *)arg);
			break;
		case 5:
			fprintf(f, "Data fault at %p to %p\n", point, (void *)arg);
			break;
		case 6:
			fprintf(f, "Invalid long immediate encoding at %p", point);
			break;
		case 7:
			fprintf(f, "Unhandled interrupt (%x) at %p\n", arg, point);
			break;
		default:
			fprintf(f, "Unknown exception: %x at %p with argument %x\n", cause, point, arg);
			break;
	}

}
#endif


/* Dump from start to end (inclusive) */
static void print_trace_range(int start, int end)
{
	int pos;
	struct trace_entry *p;

	if (end >= trace_buffer_entries)
		return;

	pos = start;
	while (pos <= end) {
		p = trace_buffer + pos;
		switch (GET_TRACE_FIELD_TYPE(p->marker)) {
		case TRACE_BUNDLE_OK:
		case TRACE_BUNDLE_EXCEPTION:
			print_ins(stdout, p);
			break;
		case TRACE_EXCEPTION_INFO:
#ifdef NOHANDLERS
			print_exception(stdout, p);
#else
			break;
#endif
		}
		pos++;
	}
}

void dump_trace_buffer(void)
{
	if (!circular_trace) {
		fflush(stdout);
		return;
	}
	if (trace_wrapped) {
		printf("Dumping %d entries (full buffer)...\n", trace_buffer_entries);
		print_trace_range(trace_pos, trace_buffer_entries - 1);
	}
	else
		printf("Dumping %d entries...\n", trace_buffer_entries);
	print_trace_range(0, trace_pos - (trace_pos != 0));
}

static void dump_ins(struct trace_entry *entry, int pos, int bundle_size,
		cx_state_t *current, cx_state_t *new,
		     struct decoded_ins *ins)
{
	struct decoded_ins_params *p = &(ins->params);
	unsigned char *b;
	struct instr_info *info;
	u32 phys=last_phys;

	info = ins->trace_marker;
	entry->marker = get_instr_index(info);
	entry->contextId = CID(current);

	if (pos == 0) {
		entry->marker |= TRACE_BUNDLE_START;
	}

#ifdef NOHANDLERS
	/* We have to mark this as a special entry */
	if (exception) {
		entry->marker |=
		    SET_TRACE_FIELD_TYPE(entry->marker, TRACE_BUNDLE_EXCEPTION);
		    phys=~0;
	}
#endif

	if (pos == bundle_size - 1) {
		entry->marker |= TRACE_BUNDLE_STOP;
	}

	entry->pc = current->pc + (pos*4);
	entry->raw_insn = ins->raw_ins;

	b = (unsigned char *)entry->data;

	/* We are word aligned in the buffer */
	switch (info->trace.type) {
	case INT3R_TYPE:
		b = trace_dump_bytes(b, 3, DEST(p), SRC2(p), SRC1(p));
		b = trace_dump_words(b, 3, R_DEST, R_SRC2, R_SRC1);
		break;
	case INT3I_TYPE:
		b = trace_dump_bytes(b, 2, IDEST(p), SRC1(p));
		b = trace_dump_words(b, 3, R_IDEST, ISRC2, R_SRC1);
		break;
	case MONADIC_TYPE:
		b = trace_dump_bytes(b, 2, IDEST(p), SRC1(p));
		b = trace_dump_words(b, 2, R_IDEST, R_SRC1);
		break;
	case CMP3R_TYPE:
		b = trace_dump_bytes(b, 3, DEST(p), SRC1(p), SRC2(p));
		b = trace_dump_words(b, 3, R_DEST, R_SRC1, R_SRC2);
		break;
	case CMP3R_BR_TYPE:
		b = trace_dump_bytes(b, 4, BDEST(p), B_BDEST, SRC1(p), SRC2(p));
		b = trace_dump_words(b, 2, R_SRC1, R_SRC2);
		break;
	case CMP3I_TYPE:
		b = trace_dump_bytes(b, 2, IDEST(p), SRC1(p));
		b = trace_dump_words(b, 3, R_IDEST, R_SRC1, ISRC2);
		break;
	case CMP3I_BR_TYPE:
		b = trace_dump_bytes(b, 3, IBDEST(p), B_IBDEST, SRC1(p));
		b = trace_dump_words(b, 2, R_SRC1, ISRC2);
		break;
	case SELECTR_TYPE:
		b = trace_dump_bytes(b, 5, DEST(p), SCOND(p), B_SCOND, SRC1(p),
				     SRC2(p));
		b = trace_dump_words(b, 3, R_DEST, R_SRC1, R_SRC2);
		break;
	case SELECTI_TYPE:
		b = trace_dump_bytes(b, 4, IDEST(p), SCOND(p), B_SCOND,
				     SRC1(p));
		b = trace_dump_words(b, 3, R_IDEST, R_SRC1, ISRC2);
		break;
	case CGEN_TYPE:
		b = trace_dump_bytes(b, 7, DEST(p), CGS_BDEST(p), CGS_B_BDEST,
				     SRC1(p), SRC2(p), SCOND(p), B_SCOND);
		b = trace_dump_words(b, 3, R_DEST, R_SRC1, R_SRC2);
		break;
	case SYSOP_TYPE:
	case SBREAK_TYPE:
		break;
	case LOAD_TYPE:
		if (ins->op == ldl)
		{
			b = trace_dump_bytes(b, 1, SRC1(p));
			b = trace_dump_words(b, 4, L_DEST, ISRC2, R_SRC1, phys);
		}
		else
		{
			b = trace_dump_bytes(b, 2, IDEST(p), SRC1(p));
			b = trace_dump_words(b, 4, R_IDEST, ISRC2, R_SRC1, phys);
		}
		break;
//	case LOAD_PAIR_TYPE:
//		b = trace_dump_bytes(b, 2, IDEST(p), SRC1(p));
//		b = trace_dump_words(b, 5, R_IDEST, R_IDESTPLUS1,ISRC2, R_SRC1, phys);
//		break;
	case LOAD_CONDITIONAL_TYPE:
		b=trace_dump_bytes(b,4,SCOND(p),B_SCOND,IDEST(p),SRC1(p));
    		b=trace_dump_words(b,4,R_IDEST,ISRC2,R_SRC1,phys);
    		break;
//	case LOAD_CONDITIONAL_PAIR_TYPE:
//		b=trace_dump_bytes(b,4,SCOND(p),B_SCOND,IDEST(p),SRC1(p));
//    		b=trace_dump_words(b,5,R_IDEST,R_IDESTPLUS1,ISRC2,R_SRC1,phys);
//    		break;
	case STORE_TYPE:
	case STORE_OTHER_TYPE:
		if (ins->op == stl)
		{
			b = trace_dump_bytes(b, 2, SRC1(p), 0);
			b = trace_dump_words(b, 4, ISRC2, R_SRC1, R_LR, phys);
		}
		else
		{
			b = trace_dump_bytes(b, 2, SRC1(p), DEST(p));
			b = trace_dump_words(b, 4, ISRC2, R_SRC1, R_SRC3, phys);
		}
		break;
//	case STORE_PAIR_TYPE:
//		b = trace_dump_bytes(b, 2, SRC1(p), SRC2(p));
//		b = trace_dump_words(b, 5, ISRC2, R_SRC1, R_SRC2, R_SRC2PLUS1,phys);
//		break;
	case STORE_CONDITIONAL_TYPE:
    		b=trace_dump_bytes(b,4,SCOND(p),B_SCOND,SRC1(p),DEST(p));
    		b=trace_dump_words(b,4,ISRC2,R_SRC1,R_SRC3,phys);
    		break;
//	case STORE_CONDITIONAL_PAIR_TYPE:
//    		b=trace_dump_bytes(b,4,SCOND(p),B_SCOND,SRC1(p),SRC2(p));
//    		b=trace_dump_words(b,5,ISRC2,R_SRC1,R_SRC2,R_SRC2PLUS1,phys);
//		break;
	case PSW_TYPE:
		b = trace_dump_bytes(b, 2, IDEST(p), DEST(p));
		b = trace_dump_words(b, 4, R_IDEST, ISRC2, R_SRC1, current->ccr);
		break;
	case CALL_TYPE:
		if (ins->op == rfi) {
			b = trace_dump_words(b, 2, current->tp,
					     current->sccr);
		} else {
			b = trace_dump_words(b, 2, 0, 0);
		}
		b = trace_dump_words(b, 2, BUNDLE_PC + (bundle_size*4) + (BTARG(p) << 3),
				     current->lr);
		if (ins->op == return_lr || ins->op == rfi){
			b = trace_dump_words(b, 1, current->r[1]);
		}
		break;
	case BRANCH_TYPE:
		b = trace_dump_bytes(b, 2, BCOND(p), B_BCOND);
		b = trace_dump_words(b, 1, BUNDLE_PC + (bundle_size*4) + (BTARG(p) << 3));
		break;
	case BR3R_TYPE:
		b=trace_dump_bytes(b,6,BDEST2(p),B_BDEST2,BSRC2(p),B_SRC2,BSRC1(p),B_SRC1);
		break;
	case PSEUDO_TYPE:
		if (ins->op == limm)
		{
			b = trace_dump_bytes(b, 1, SRC1(p));
			b = trace_dump_words(b, 1, SRC2(p));
		}
		break;
	default: 
		printf("%s UNKNOWN TYPE %d\n",__FUNCTION__,info->trace.type);
		break;
	}

}

#ifdef NOHANDLERS
static void dump_exception(struct trace_entry *entry, cx_state_t *current,
		cx_state_t *new, struct decoded_bundle *bundle)
{
	entry->marker =
	    SET_TRACE_FIELD_TYPE(entry->marker, TRACE_EXCEPTION_INFO);
	trace_dump_words(entry->data, 3, current->tc, current->ta, current->tp);
}
#endif

static inline void check_trace_wrap(void)
{

	trace_pos++;

	if (circular_trace) {
		/* Check to see if we have wrapped circular buffer */
		if (trace_pos == trace_buffer_entries) {
			trace_wrapped = 1;
			trace_pos = 0;
		}
	} else {
		/* Always reset */
		trace_pos = 0;
	}
}

void print_trace_info(cx_state_t *current, struct state *new,
		      struct decoded_bundle *bundle)
{
	int i;
	int print = 0;

	if (trace_start_addr == current->pc)
		trace_start_addr = 0;
	if (trace_start_cycle == global_state.cnt)
		trace_start_cycle = 0;

	if (trace_start_addr == 0 && trace_start_cycle == 0)
		trace_started = 1;

	if ((trace_mode & TRACE_CORE) && trace_started) {
		print = 1;	/* We print absolutely everything */
	}
#if 0 //not supported
	else {		/* Either super or user */
		if (trace_mode & TRACE_USER_MODE) {
			/* If bundle is NULL this means that we have an I exception most likely - so don't print */
			/* if change_ccr, then we have jumped from super to user, so we don't print this bundle */
			print = (ccr & PSW_USER_MODE) && !last_bundle_change_ccr
			    && bundle != NULL;
		} else {
			/* if change_ccr set, we must have been in super mode, so we print */
			print = (!(ccr & PSW_USER_MODE)
				 || last_bundle_change_ccr);
		}
	}

	/* We have to reset this here */
	last_bundle_change_ccr = 0;
#endif

	if (!print)
		return;

	if (bundle && (trace_context & (1<<CID(current)) ) ) {
		for (i = 0; i < bundle->num; i++) {
			dump_ins(trace_buffer + trace_pos, i, bundle->num,
				 current, new, bundle->ins + i);
			if (!circular_trace)
				print_ins(stdout, trace_buffer + trace_pos);
			check_trace_wrap();
		}
	}

#ifdef NOHANDLERS
	if (current->trapcause) {
		dump_exception(trace_buffer + trace_pos, current, new, bundle);
		if (!circular_trace)
			print_exception(stdout, trace_buffer + trace_pos);
		check_trace_wrap();
	}
#endif
}
