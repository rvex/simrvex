// Definitions of SIMD instructions





//H I don't thing we need this for rVEX
#if 0

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "state.h"
#include "instr.h"
#include "simd.h"
#include "icache.h"
#include "checkpoint.h"
#include "trace.h"
#include "dsu.h"

#include "instr_fields.h"

static inline u32 Saturate32(s64 p)
{
  if (p>0x7fffffff) {
    return 0x7fffffff;
  } else if (p < -0x80000000LL) {
    return 0x80000000;
  } else {
    return p;
  }
}

inline int saturate16(int p)
{
  if (p>0x7fff)
  {
    return 0x7fff;
  }
  else if (p<-0x8000)
  {
    return -0x8000;
  }
  else
  {
    return p;
  }
}


inline int overflow16(int p)
{
  if (p>0x7fff)
  {
    return 1;
  }
  else if (p<0xffff8000)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

inline unsigned int unsignedextract8(unsigned int p, unsigned int s)
{
  unsigned char c = (p >> (s*8)) & 0xff;
  return (unsigned int) c;
}

inline int signedextract8(int p, unsigned int s)
{
  int c = (p >> (s*8)) & 0xff;
  if ((c &0x80) == 0)
    return c;
  else
    return c | 0xffffff00;
}

inline unsigned int unsignedextract16(unsigned int p, unsigned int s)
{
  unsigned short c = (p >> (s*16)) & 0xffff;
  return (unsigned int) c;
}

inline int signedextract16(int p, unsigned int s)
{
  int c = (p >> (s*16)) & 0xffff;
  if ((c & 0x8000) == 0)
    return c;
  else
    return c | 0xffff0000;
}

inline unsigned int unsignedsaturate8(int p)
{
  if (p > 0xff)
  {
    return 0xff;
  }
  else if (p<0)
  {
    return 0;
  }
  else
  {
    return p;
  }
}




INS_PROTO(perm_pb)
{
  unsigned result=0;
  int i;
  
  for (i=0; i<4; ++i)
  {
    unsigned byteno = (R_SRC2 >> (2 * i)) & 3;
    
    result |= ((R_SRC1 >> (8 * byteno)) & 0xff) << (8 * i);
  }
  R_DEST = result;
}

INS_PROTO(perm_pb_immediate)
{
  unsigned result=0;
  int i;
  
  for (i=0; i<4; ++i)
  {
    unsigned byteno = (ISRC2 >> (2 * i)) & 3;
    
    result |= ((R_SRC1 >> (8 * byteno)) & 0xff) << (8 * i);
  }
  R_IDEST = result;
}

INS_PROTO(abss_ph)
{
  int j;
  unsigned int result1 = 0;
  for (j=0; j<2; ++j)
  {
    unsigned int unpacked_opd = abs(signedextract16(R_SRC1, j));
    unpacked_opd = saturate16(unpacked_opd);
    result1 |= (unpacked_opd & 0xffff) << (16*j);
  }
  R_DEST = result1;
}

INS_PROTO(absubu_pb)
{
  int j;
  unsigned int result1 = 0;
  for (j=0; j<4; ++j)
  {
    int subresult = unsignedextract8(R_SRC1, j) - unsignedextract8(R_SRC2, j);
    if (subresult > 0)
    {
      result1 |= (subresult & 0xff) << (j*8);
    }
    else
    {
      result1 |= ((-subresult) & 0xff) << (j*8);
    }
  }
  R_DEST = result1;
}

INS_PROTO(add_ph)
{
  int j;
  unsigned int result1=0;
  for (j=0; j<2; ++j)
  {
    int addresult = signedextract16(R_SRC1, j) + signedextract16(R_SRC2, j);
    result1 = result1 | ((addresult & 0xffff) << (16*j));
  }
  R_DEST = result1;
}

INS_PROTO(adds_ph)
{
  int j;
  unsigned int result1=0;
  for (j=0; j<2; ++j)
  {
    int addresult = signedextract16(R_SRC1, j) + signedextract16(R_SRC2, j);
    addresult = saturate16(addresult);
    result1 = result1 | ((addresult & 0xffff) << (16*j));
  }
  R_DEST = result1;
}

INS_PROTO(avg4u_pb)
{
  unsigned int sum1 = B_SCOND & 0x3;
  unsigned int sum2 = sum1;
  int j=0;
  for (j=0; j<2; ++j)
  {
    sum1 += unsignedextract8(R_SRC1, j);
    sum1 += unsignedextract8(R_SRC2, j);
    sum2 += unsignedextract8(R_SRC1, j+2);
    sum2 += unsignedextract8(R_SRC2, j+2);
  }
  R_DEST = (sum1 >> 2) | (((sum2 >> 2) & 0xffff) << 16);
}

INS_PROTO(avgu_pb)
{
  unsigned int sum;
  unsigned int result1 = 0;
  int j=0;
  for (j=0; j<4; ++j)
  {
    sum = (B_SCOND != 0);
    sum += unsignedextract8(R_SRC1, j);
    sum += unsignedextract8(R_SRC2, j);
    result1 |= ((sum >> 1) & 0xff) << (8*j);
  }
  R_DEST = result1;
}

INS_PROTO(cmpeq_pb_bdest)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<4; ++j)
  {
    unpacked_res = unsignedextract8(R_SRC1, j) == unsignedextract8(R_SRC2, j);
    result1 |= unpacked_res << j;
  }
  B_BDEST2 = result1;
}

INS_PROTO(cmpeq_pb)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<4; ++j)
  {
    unpacked_res = unsignedextract8(R_SRC1, j) == unsignedextract8(R_SRC2, j);
    result1 |= unpacked_res << j*8;
  }
  R_DEST = result1;
}

INS_PROTO(cmpeq_ph_bdest)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<2; ++j)
  {
    unpacked_res = unsignedextract16(R_SRC1, j) == unsignedextract16(R_SRC2, j);
    result1 |= (unpacked_res ? 3 : 0) << (j * 2);
  }
  B_BDEST2 = result1;
}

INS_PROTO(cmpeq_ph)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<2; ++j)
  {
    unpacked_res = unsignedextract16(R_SRC1, j) == unsignedextract16(R_SRC2, j);
    result1 |= unpacked_res << (j * 16);
  }
  R_DEST = result1;
}

INS_PROTO(cmpgt_ph_bdest)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<2; ++j)
  {
    unpacked_res = (signedextract16(R_SRC1, j) > signedextract16(R_SRC2, j));
    result1 |= (unpacked_res ? 3 : 0) << (j * 2);
  }
  B_BDEST2 = result1;
}

INS_PROTO(cmpgt_ph)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<2; ++j)
  {
    unpacked_res = (signedextract16(R_SRC1, j) > signedextract16(R_SRC2, j));
    result1 |= unpacked_res << (j * 16);
  }
  R_DEST = result1;
}

INS_PROTO(cmpgtu_pb_bdest)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<4; ++j)
  {
    unpacked_res = (unsignedextract8(R_SRC1, j) > unsignedextract8(R_SRC2, j));
    result1 |= unpacked_res << j;
  }
  B_BDEST2 = result1;
}

INS_PROTO(cmpgtu_pb)
{
  unsigned int result1=0;
  int unpacked_res;
  int j;
  for (j=0; j<4; ++j)
  {
    unpacked_res = (unsignedextract8(R_SRC1, j) > unsignedextract8(R_SRC2, j));
    result1 |= unpacked_res << j*8;
  }
  R_DEST = result1;
}

INS_PROTO(ext1_pb)
{
  R_DEST = ((R_SRC2 & 0xff) << (8*3)) |
           ((R_SRC1 >> 8) & 0xffffff) ;
}

INS_PROTO(ext2_pb)
{
  R_DEST = ((R_SRC1 >> 16) & 0xffff) |
           ((R_SRC2 & 0xffff) << 16);
}

INS_PROTO(ext3_pb)
{
  R_DEST = ((R_SRC1 >> (8*3)) & 0xff) |
           ((R_SRC2 & 0xffffff) << 8);
}

INS_PROTO(extl_pb)
{
  unsigned int result1 = 0;
  int j;
  int index;
  int byte;
  for (j=0; j<4; ++j)
  {
    index = j+(B_SCOND & 3);
    if (index < 4)
    {
      byte = unsignedextract8(R_SRC1, index);
    }
    else
    {
      byte = unsignedextract8(R_SRC2, index - 4);
    }
    result1 |= byte << (j * 8);
  }
  R_DEST = result1;
}


INS_PROTO(extr_pb)
{
  unsigned int result1 = 0;
  int j;
  int index;
  int byte;
  for (j=0; j<4; ++j)
  {
    index = j-(B_SCOND & 3);
    if (index >= 0)
    {
      byte = unsignedextract8(R_SRC1, index);
    }
    else
    {
      byte = unsignedextract8(R_SRC2, index + 4);
    }
    result1 |= byte << (j * 8);
  }
  R_DEST = result1;
}


INS_PROTO(max_ph)
{
  int j;
  unsigned int result1=0;
  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    int unpacked_opd2 = signedextract16(R_SRC2, j);
    unsigned int maxres = (unsigned int)((unpacked_opd1 > unpacked_opd2) ? unpacked_opd1 : unpacked_opd2);
    result1 |= (maxres & 0xffff) << (j * 16);
  }
  R_DEST = result1;
}


INS_PROTO(min_ph)
{
  int j;
  unsigned int result1=0;
  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    int unpacked_opd2 = signedextract16(R_SRC2, j);
    unsigned int minres = (unsigned int)((unpacked_opd1 < unpacked_opd2) ? unpacked_opd1 : unpacked_opd2);
    result1 |= (minres & 0xffff) << (j * 16);
  }
  R_DEST = result1;
}


INS_PROTO(mul_ph)
{
  int j;
  int result1=0;
  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    int unpacked_opd2 = signedextract16(R_SRC2, j);
    result1 |= ((unpacked_opd1 * unpacked_opd2) & 0xffff)
               << (j * 16);
  }
  R_DEST = result1;
}


INS_PROTO(muladd_ph)
{
  int j;
  int result1=0;
  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    int unpacked_opd2 = signedextract16(R_SRC2, j);
    result1 += (unpacked_opd1 * unpacked_opd2);
  }
  R_DEST = Saturate32(result1);
}


INS_PROTO(muladdus_pb)
{
  int j;
  unsigned int result1=0;
  for (j=0; j<4; ++j)
  {
    result1 += unsignedextract16(R_SRC1, j) * unsignedextract16(R_SRC2, j);
  }
  R_DEST = result1;
}

INS_PROTO(mulfracadds_ph)
{
  int result1=0;
  int j;
  int mulresult;
  for (j=0; j<2; ++j)
  {
    int extractedopd1 = signedextract16(R_SRC1, j);
    int extractedopd2 = signedextract16(R_SRC2, j);
    if (((~extractedopd1) == 0x8000) && ((~extractedopd2) == 0x8000))
    {
      mulresult = 0x7fffffff;
    }
    else
    {
      mulresult = (extractedopd1 * extractedopd2) << 1;
    }
    result1 += mulresult;
  }
  R_DEST = Saturate32(result1);
}

INS_PROTO(mulfracrm_ph)
{
  int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    int mulresult = signedextract16(R_SRC1, j) * signedextract16(R_SRC2, j);
    result1 |= saturate16(mulresult >> 15) << (16*j);
  }
  R_DEST = result1;
}


INS_PROTO(mulfracrne_ph)
{
  int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    int mulresult = signedextract16(R_SRC1, j) * signedextract16(R_SRC2, j);
    int rounding = ((1<<14)-1) + ((mulresult >> 15) & 1);
    mulresult = saturate16((mulresult + rounding) >> 15);
    result1 |= (mulresult & 0xffff) << (16*j);
  }
  R_DEST = result1;
}


INS_PROTO(pack_pb)
{
  unsigned int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    int unpacked_opd2 = signedextract16(R_SRC2, j);
    result1 |= ((unpacked_opd1 & 0xff) << (8*j) )
             | ((unpacked_opd2 & 0xff) << (8*(j+2)) );
  }
  R_DEST = result1;
}


INS_PROTO(packrnp_phh)
{
  int rounded_opd1 = (((int)R_SRC1 >> 15) + 1) >> 1;
  int rounded_opd2 = (((int)R_SRC2 >> 15) + 1) >> 1;
  rounded_opd1 = saturate16(rounded_opd1);
  rounded_opd2 = saturate16(rounded_opd2);
  R_DEST = (rounded_opd1 & 0xffff) 
             | ((rounded_opd2 & 0xffff) << 16);
}


INS_PROTO(packs_ph)
{
  R_DEST = (saturate16(R_SRC1) & 0xffff) | ((saturate16(R_SRC2) & 0xffff) << 16);
}


INS_PROTO(packsu_pb)
{
  unsigned int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    unsigned int unpacked_opd1 = unsignedsaturate8(signedextract16(R_SRC1, j));
    unsigned int unpacked_opd2 = unsignedsaturate8(signedextract16(R_SRC2, j));
    result1 |= (unpacked_opd1 << (8*j)) | (unpacked_opd2 << (8*(j+2)) );
  }
  R_DEST = result1;
}


INS_PROTO(sadu_pb)
{
  int result1=0;
  int j;
  for (j=0; j<4; ++j)
  {
    int subresult = unsignedextract8(R_SRC1, j) - unsignedextract8(R_SRC2, j);
    if (subresult > 0)
    {
      result1 += subresult;
    }
    else
    {
      result1 -= subresult;
    }
  }
  R_DEST = result1;
}


INS_PROTO(shl_ph_immediate)
{
  int result1 = 0;
  int j;
  int shiftdistance = ISRC2 & 0xff;
  if (shiftdistance < 16)
  {
    for (j=0; j<2; ++j)
    {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      result1 |= ((unpacked_opd1 << shiftdistance) & 0xffff) << (j*16);
    }
  }
  R_IDEST = result1;
}


INS_PROTO(shl_ph)
{
  int result1=0;
  int j;
  int shiftdistance = R_SRC2 & 0xff;
  if (shiftdistance < 16)
  {
    for (j=0; j<2; ++j)
    {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      result1 |= ((unpacked_opd1 << shiftdistance) & 0xffff) << (j*16);
    }
  }
  R_DEST = result1;
}


INS_PROTO(shls_ph_immediate)
{
  int result1 = 0;
  int j;
  int shiftdistance = ISRC2 & 0xff;
  if (shiftdistance > 16)
  {
    shiftdistance = 16;
  }

  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    result1 |= (saturate16(unpacked_opd1 << shiftdistance) & 0xffff) << (j*16);
  }
  R_IDEST = result1;
}


INS_PROTO(shls_ph)
{
  int result1=0;
  int j;
  int shiftdistance = R_SRC2 & 0xff;
  if (shiftdistance > 16)
  {
    shiftdistance = 16;
  }

  for (j=0; j<2; ++j)
  {
    int unpacked_opd1 = signedextract16(R_SRC1, j);
    result1 |= (saturate16(unpacked_opd1 << shiftdistance) & 0xffff) << (j*16);
  }
  R_DEST = result1;
}

INS_PROTO(shr_ph_immediate)
{
  int result1 = 0;
  int j;
  int shiftdistance = ISRC2 & 0xff;
  if (ISRC2 > 15)
  {
    shiftdistance = 15;
  }
  
  for (j=0; j<2; ++j)
  {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      result1 |= ((unpacked_opd1 >> shiftdistance) & 0xffff) << (j*16);
  }
  R_IDEST = result1;
}


INS_PROTO(shr_ph)
{
  int result1=0;
  int j;
  int shiftdistance = R_SRC2 & 0xff;
  if (shiftdistance > 15)
  {
    shiftdistance = 15;
  }
  
  for (j=0; j<2; ++j)
  {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      result1 |= ((unpacked_opd1 >> shiftdistance) & 0xffff) << (j*16);
  }
  R_DEST = result1;
}


INS_PROTO(shrrne_ph_immediate)
{
  int result1 = 0;
  int j;
  int shiftdistance = ISRC2 & 0xff;
  if (shiftdistance > 16)
  {
    shiftdistance = 16;
  }
  
  if (shiftdistance > 0)
  {
    for (j=0; j<2; ++j)
    {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      int rounding = (1 << (shiftdistance - 1)) + ((unpacked_opd1 >> shiftdistance) & 1) - 1;
      result1 |= (((unpacked_opd1 + rounding) >> shiftdistance) & 0xffff) << (j*16);
    }
  }
  else
  {
    result1 = R_SRC1;
  }
  R_IDEST = result1;
}



INS_PROTO(shrrne_ph)
{
  int result1 = 0;
  int j;
  int shiftdistance = R_SRC2 & 0xff;
  if (shiftdistance > 16)
  {
    shiftdistance = 16;
  }
  
  if (shiftdistance > 0)
  {
    for (j=0; j<2; ++j)
    {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      int rounding = (1 << (shiftdistance - 1)) + ((unpacked_opd1 >> shiftdistance) & 1) - 1;
      result1 |= (((unpacked_opd1 + rounding) >> shiftdistance) & 0xffff) << (j*16);
    }
  }
  else
  {
    result1 = R_SRC1;
  }
  R_DEST = result1;
}


INS_PROTO(shrrnp_ph_immediate)
{
  int result1 = 0;
  int j;
  int shiftdistance = ISRC2 & 0xff;
  if (shiftdistance > 16)
  {
    shiftdistance = 16;
  }
  
  if (shiftdistance > 0)
  {
    for (j=0; j<2; ++j)
    {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      int rounding = (1 << (shiftdistance - 1));
      result1 |= (((unpacked_opd1 + rounding) >> shiftdistance) & 0xffff) << (j*16);
    }
  }
  else
  {
    result1 = R_SRC1;
  }
  R_IDEST = result1;
}


INS_PROTO(shrrnp_ph)
{
  int result1 = 0;
  int j;
  int shiftdistance = R_SRC2 & 0xff;
  if (shiftdistance > 16)
  {
    shiftdistance = 16;
  }
  
  if (shiftdistance > 0)
  {
    for (j=0; j<2; ++j)
    {
      int unpacked_opd1 = signedextract16(R_SRC1, j);
      int rounding = (1 << (shiftdistance - 1));
      result1 |= (((unpacked_opd1 + rounding) >> shiftdistance) & 0xffff) << (j*16);
    }
  }
  else
  {
    result1 = R_SRC1;
  }
  R_DEST = result1;
}


INS_PROTO(shuff_pbh)
{
  int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    unsigned int unpacked_opd1 = unsignedextract8(R_SRC1, j+2);
    unsigned int unpacked_opd2 = unsignedextract8(R_SRC2, j+2);
    result1 |= (unpacked_opd1 << (16 * j)) | (unpacked_opd2 << (16 * j + 8));
  }
  R_DEST = result1;
}


INS_PROTO(shuff_pbl)
{
  int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    unsigned int unpacked_opd1 = unsignedextract8(R_SRC1, j);
    unsigned int unpacked_opd2 = unsignedextract8(R_SRC2, j);
    result1 |= (unpacked_opd1 << (16 * j)) | (unpacked_opd2 << (16 * j + 8));
  }
  R_DEST = result1;
}


INS_PROTO(shuff_phh)
{
  R_DEST = ((R_SRC1 >> 16) & 0xffff) | (R_SRC2 & 0xffff0000);
}


INS_PROTO(shuff_phl)
{
  R_DEST = (R_SRC1 & 0xffff) | ((R_SRC2 & 0xffff) << 16);
}

INS_PROTO(shuffeve_pb)
{
  unsigned int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    unsigned int unpacked_opd1 = unsignedextract8(R_SRC1, 2*j);
    unsigned int unpacked_opd2 = unsignedextract8(R_SRC2, 2*j);
    result1 |= (unpacked_opd1 << (16*j)) | (unpacked_opd2 << (16*j+8));
  }
  R_DEST = result1;
}


INS_PROTO(shuffodd_pb)
{
  unsigned int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    unsigned int unpacked_opd1 = unsignedextract8(R_SRC1, 2*j+1);
    unsigned int unpacked_opd2 = unsignedextract8(R_SRC2, 2*j+1);
    result1 |= (unpacked_opd1 << (16*j)) | (unpacked_opd2 << (16*j+8));
  }
  R_DEST = result1;
}


INS_PROTO(slct_pb_immediate)
{
  unsigned int result1=0;
  int j;
  int byte;
  for (j=0; j<4; ++j)
  {
    if (((B_SCOND >> j) & 1) == 0)
    {
      byte=unsignedextract8(ISRC2, j);
    }
    else
    {
      byte=unsignedextract8(R_SRC1, j);
    }
    result1 |= (byte << (8*j));
  }
  R_IDEST = result1;
}


INS_PROTO(slct_pb)
{
  unsigned int result1=0;
  int j;
  int byte;
  for (j=0; j<4; ++j)
  {
    if (((B_SCOND >> j) & 1) == 0)
    {
      byte=unsignedextract8(R_SRC2, j);
    }
    else
    {
      byte=unsignedextract8(R_SRC1, j);
    }
    result1 |= (byte << (8*j));
  }
  R_DEST = result1;
}


INS_PROTO(slctf_pb_immediate)
{
  unsigned int result1=0;
  int j;
  int byte;
  for (j=0; j<4; ++j)
  {
    if (((B_SCOND >> j) & 1) == 0)
    {
      byte=unsignedextract8(R_SRC1, j);
    }
    else
    {
      byte=unsignedextract8(ISRC2, j);
    }
    result1 |= (byte << (8*j));
  }
  R_IDEST = result1;
}


INS_PROTO(sub_ph)
{
  int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    int subresult = signedextract16(R_SRC2, j) - signedextract16(R_SRC1, j);
    result1 |= (subresult & 0xffff) << (16*j);
  }
  R_DEST = result1;
}


INS_PROTO(subs_ph)
{
  int result1=0;
  int j;
  for (j=0; j<2; ++j)
  {
    int subresult = signedextract16(R_SRC2, j) - signedextract16(R_SRC1, j);
    subresult = saturate16(subresult);
    result1 |= (subresult & 0xffff) << (16*j);
  }
  R_DEST = result1;
}


#endif //0
