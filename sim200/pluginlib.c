#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>

#include <sim200/plugin.h>
#include <sim200/plugin_helper.h>

#include "setup.h"

#include "config.h"


static u32 kernel_params = ~0;	/* Physical address */
static char *kernel_cmdline = "";
static char *initrd_filename = NULL;
static u32 initrd_base = 0;

struct plugin_param {
	char *filename;
	char **argv;
	int argc;
	struct plugin_param *next;
};
static struct plugin_param *plugin_params = NULL;

static struct plugin *plugins;
static struct plugin_callbacks *plugin_callbacks;

/* This is the default system we have */
static struct system_info sim_info = {
	PLUGIN_SYSTEM_INFO_V1,	/* Version  */
	0x08000000,		/* Memory start. Zero page assumed to be here */
	32 * 1024 * 1024	/* 32 Megs of memory */
};

static void expand_variables(char *buffer, int buffer_len, int line)
{
	char *p;

	for (p = buffer; (p = strchr(buffer, '$')) != NULL;) {
		char *env_name;
		char *start = p;
		char *end;
		char *env_value;
		int env_len;
		int rest_len;

		p++;
		if (*p != '{') {
			fprintf(stderr,
				"No '{' after '$' when parsing line %d\n",
				line);
			exit(1);
		}
		env_name = ++p;
		end = strchr(p, '}');
		if (end == NULL) {
			fprintf(stderr, "No '}' when parsing line %d\n", line);
			exit(1);
		}
		*end = '\0';
		env_value = getenv(env_name);
		if (env_value == NULL) {
			fprintf(stderr,
				"Unable to expand variable \"%s\" on line %d\n",
				env_name, line);
			exit(1);
		}
		env_len = strlen(env_value);
		rest_len = strlen(end + 1);
		if (((start - buffer) + env_len + rest_len + 1) > buffer_len) {
			fprintf(stderr, "Line length exceeded on line %d\n",
				line);
			exit(1);
		}
		memmove(start + env_len, end + 1, rest_len + 1);
		memcpy(start, env_value, env_len);
		p += env_len;
	}
}

static int argvify(char *buffer, int line, char *argv[])
{
	int argc;
	char *p;

	for (argc = 0, p = buffer; *p != '\0';) {
		while (isspace(*p))
			p++;
		if ((*p == '\0') || (*p == '#'))
			break;
		if (*p == '"') {
			p++;
			argv[argc++] = p;
			p = strchr(p, '"');
			if (p == NULL) {
				fprintf(stderr,
					"No closing '\"' when parsing line %d\n",
					line);
				exit(1);
			}
			*p = '\0';
			p++;
		} else {
			argv[argc++] = p;
			p++;
			while ((!isspace(*p)) && (*p != '\0'))
				p++;
			if (*p != '\0') {
				*p = '\0';
				p++;
			}
		}
	}
	argv[argc] = NULL;
	return argc;
}

static void parse_conf(void)
{
	char *filename;
	FILE *config;
	int line = 0;
	char **lines =  NULL;
	int allocated = 0;
	char buffer[1024];
	char *argv[128];
	int argc;
	int j;

	filename = getenv("ADAPTOR_CONFIG");
	if (filename == NULL) {
		filename = "plugin.conf";
	}

	allocated = 7;
	lines = malloc(allocated*sizeof(char *));
	if (lines == NULL){
		fprintf(stderr, "Unable to allocate memory\n");
		return;
	}

	config = fopen(filename, "r");
	if (config == NULL) {
		//fprintf(stderr, "Unable to open %s\n", filename);
		fprintf(stderr, "Unable to open %s. Using default configuration.\n", filename);
		lines[line] = malloc(1024 * sizeof(char));
		if (lines[line] == NULL) {
			fprintf(stderr, "Unable to allocate memory\n");
			return;
		}
		strncpy(lines[line], "plugin dummy_uart", 1024);
		line++;
	} else {
		while (fgets(buffer, sizeof(buffer), config)) {
			int len = strlen(buffer);
			if (len > 0 && buffer[len-1] == '\n')
				buffer[len-1] = '\0';
			if (line >= allocated) {
				char **newlines;
				allocated = allocated*2+1;
				newlines = realloc(lines, allocated*sizeof(char *));
				if (newlines == NULL){
					fprintf(stderr, "Unable to allocate memory\n");
					return;
				}
				lines = newlines;
			}
			lines[line] = malloc(1024 * sizeof(char));
			if (lines[line] == NULL) {
				fprintf(stderr, "Unable to allocate memory\n");
				return;
			}
			strncpy(lines[line], buffer, 1024);
			line++;
		}
		fclose(config);
	}
	for (j = 0; j < line; j++) {
		expand_variables(lines[j], 1024, j);
		argc = argvify(lines[j], j, argv);

		if (argc == 0)
			continue;

		if (strcmp(argv[0], "plugin") == 0) {
			struct plugin_param *p;
			int i;
			if (argc < 2) {
				fprintf(stderr,
					"Insufficient parameters to %s at line %d\n",
					argv[0], j);
				exit(1);
			}

			p = malloc(sizeof(*p));
			if (p == NULL) {
				fprintf(stderr, "Malloc failed\n");
				exit(1);
			}
			/* Allocate enough space for potential .so extension */
			p->filename = malloc(strlen(argv[1]) + 3 + 1);
			strcpy(p->filename,argv[1]);
			p->argv = malloc(argc * sizeof(char *));
			p->argv[0] = p->filename;
			for (i = 0; i < argc - 2; i++) {
				p->argv[i + 1] = strdup(argv[i + 2]);
			}
			p->argv[i + 1] = NULL;
			p->argc = argc - 1;

			p->next = plugin_params;
			plugin_params = p;
		} else if (strcmp(argv[0], "initrd") == 0) {
			if (argc != 3) {
				fprintf(stderr,
					"Incorrect number of parameters to %s at line %d\n",
					argv[0], j);
				exit(1);
			}
			initrd_base = strtoul(argv[1], NULL, 0);
			initrd_filename = strdup(argv[2]);
		} else if (strcmp(argv[0], "append") == 0) {
			if (argc != 2) {
				fprintf(stderr,
					"Incorrect number of parameters to %s at line %d\n",
					argv[0], j);
				exit(1);
			}
			kernel_cmdline = strdup(argv[1]);
		} else if (strcmp(argv[0], "params") == 0) {
			if (argc != 2) {
				fprintf(stderr,
					"Incorrect number of parameters to %s at line %d\n",
					argv[0], j);
				exit(1);
			}
			kernel_params = strtoul(argv[1], NULL, 0);
		} else {
			fprintf(stderr, "Unrecognised command at line %d\n",
				j);
			exit(1);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
// Exported interface
//////////////////////////////////////////////////////////////////////////////

struct memory_bank *pluginlib_find_mb(u32 addr)
{
	struct plugin *plugin;
	for (plugin = plugins; plugin != NULL; plugin = plugin->next) {
		int j;
		struct memory_bank *mb;
		for (j = 0, mb = plugin->banks; j < plugin->num_banks;
		     j++, mb++) {
			if ((addr >= mb->base)
			    && (addr < (mb->base + mb->size))) {
				return mb;
			}
		}
	}
	return NULL;
}

struct plugin *pluginlib_init(struct plugin_callbacks *callbacks,
			      struct system_info *info)
{
	struct plugin_param *p;

	/* If we have it override defaults */
	if (info) {
		memcpy(&sim_info, info, sizeof(struct system_info));
	}

	parse_conf();

	plugin_callbacks = callbacks;

	for (p = plugin_params; p != NULL; p = p->next) {
		void *handle;
		struct plugin *(*init) (struct plugin_callbacks *, int argc,
					char *argv[]);
		struct plugin *plugin;
		int len;
		char *full_filename;

//		fprintf(stderr, "Loading : %s\n", p->filename);

		len=strlen(p->filename);
		full_filename=malloc(len+3+1+strlen(SIM200_PLUGIN_DIR)+1);
		
		if(strcmp(".so",p->filename+len-3)) strcat(p->filename,".so");
		
		handle = dlopen(p->filename, RTLD_LAZY);
		if (handle == NULL) {
			/* Failed, so stick on SIM200_PLUGIN_DIR on the front */
			strcpy(full_filename,SIM200_PLUGIN_DIR);
			strcat(full_filename,p->filename);
			handle=dlopen(full_filename,RTLD_LAZY);
		}
		
		if (handle == NULL) {
                        fprintf(stderr, "%s\n", dlerror());
                        exit(1);
                }

		free(full_filename);
		init =
		    (struct plugin *
		     (*)(struct plugin_callbacks *, int, char *[]))dlsym(handle,
									 "init");
		if (init == NULL) {
			fprintf(stderr, "Unable to find init function in %s\n",
				p->filename);
			exit(1);
		}
		plugin = init(callbacks, p->argc, p->argv);
		plugin->handle = handle;
		plugin->next = plugins;
		plugins = plugin;
	}
	return plugins;
}

static unsigned long write_tag(struct tag *tag, unsigned long addr)
{
	int len = stohl(tag->hdr.size) * 4;	/* hdr.size is in words */

	plugin_callbacks->write_memory(tag, addr, len);

	return len;
}

void pluginlib_setup_params(void)
{
	u32 addr;		/* This will be zero page */
	struct tag *tag;
	int cmdlen;

	/* Can override default - be careful */
	addr = (kernel_params == ~0) ? sim_info.memory_base : kernel_params;

	tag = malloc(sizeof(tag) + COMMAND_LINE_SIZE);

	if (tag == NULL) {
		fprintf(stderr, "Run out of memory\n");
		exit(1);
	}

	tag->hdr.tag = htosl(ATAG_CORE);
	tag->hdr.size = htosl(tag_size(tag_core));
	tag->core.magic = htosl(ATAG_CORE_MAGIC);
	tag->core.flags = htosl(1);
	tag->core.rootdev = htosl(0xff);

	addr += write_tag(tag, addr);

	tag->hdr.tag = htosl(ATAG_MEM);
	tag->hdr.size = htosl(tag_size(tag_mem32));
	tag->mem.start = htosl(sim_info.memory_base);
	tag->mem.size = htosl(sim_info.memory_size);

	addr += write_tag(tag, addr);

	if (initrd_filename != NULL) {
		FILE *initrd_file = fopen(initrd_filename, "r");
		unsigned res;
		unsigned char buf[1024];
		unsigned size;

		if (initrd_file == NULL) {
			fprintf(stderr, "Unable to open initrd file: %s: %s\n",
				initrd_filename, strerror(errno));
			exit(1);
		}
		size = 0;
		while ((res = fread(buf, 1, sizeof(buf), initrd_file)) != 0) {
			plugin_callbacks->write_memory(buf, initrd_base + size,
						       sizeof(buf));
			size += res;
		}
		fclose(initrd_file);

		tag->hdr.tag = htosl(ATAG_INITRD);
		tag->hdr.size = htosl(tag_size(tag_initrd));
		tag->initrd.start = htosl(initrd_base);
		tag->initrd.size = htosl(size);

		addr += write_tag(tag, addr);
	}

	tag->hdr.tag = htosl(ATAG_CMDLINE);
	cmdlen = strlen(kernel_cmdline) + 1;

	if (cmdlen > COMMAND_LINE_SIZE)
		cmdlen = COMMAND_LINE_SIZE;

	memcpy(tag->cmdline.cmdline, kernel_cmdline, cmdlen);

	tag->hdr.size = tag_size(tag_cmdline) + ((((cmdlen) + 3) & ~3)) / 4;

	addr += write_tag(tag, addr);

	/* Terminate */
	tag->hdr.tag = htosl(ATAG_NONE);
	tag->hdr.size = htosl(2);

	addr += write_tag(tag, addr);

	free(tag);

}

void pluginlib_finish(void)
{
	struct plugin *plugin;
	for (plugin = plugins; plugin != NULL; plugin = plugin->next) {
		plugin->close_fn();
	}
}
