#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "state.h"
#include "instr.h"
#include "checkpoint.h"
#include "trace.h"
#include "memory.h"

#include "instr_fields.h"
#include "bitops.h"

#include "icache.h"

#include "sysnum.h" //from uclibc
#include <sys/stat.h> //for emulating stat syscall
#include <sys/errno.h> //for emulating stat syscall
#include <fcntl.h>

/* Flag to show if we have potentially gone from super to user */

int last_bundle_change_psw = 0;


//#define EXCEPTION(x) exception|=x
#define SignExtend8(x) ((s8)(x))
#define SignExtend16(x) ((s16)(x))
#define SignExtend(x,n) SignExtend##n(x)
#define ZeroExtend(x,n) ( (x) & ((1<<(n))-1))
#define ZeroExtend8(x) ((u8)(x))
#define ZeroExtend16(x) ((u16)(x))

static inline u32 Saturate32(s64 p)
{
  if (p>0x7fffffff) {
    return 0x7fffffff;
  } else if (p < -0x80000000LL) {
    return 0x80000000;
  } else {
    return p;
  }
}

#if 0
static inline u32 Overflow32(s64 p)
{
  if (p>0x7fffffff) {
    return 1;
  } else if (p<0x80000000) {
    return 1;
  } else {
    return 0;
  }
}
#endif

#define Saturate16(x) ( (x) > 0xffff ? 0xffff : (x) )
#define Overflow16(x) ( (x) > 0xffff ? 1 : 0 )

#define UNIMP(x)  INS_PROTO(x) { printf("Unimp instruction %s\n",#x);}

INS_PROTO(cmpgef)
{
	R_DEST = (R_SRC1_FLOAT >= R_SRC2_FLOAT);
}

INS_PROTO(cmpeqf)
{
	R_DEST = (R_SRC1_FLOAT == R_SRC2_FLOAT);
}

INS_PROTO(cmpgtf)
{
	R_DEST = (R_SRC1_FLOAT > R_SRC2_FLOAT);
}

INS_PROTO(cmpgef_bdest)
{
	B_BDEST = (R_SRC1_FLOAT >= R_SRC2_FLOAT);
}


INS_PROTO(cmpgtf_bdest)
{
	B_BDEST = (R_SRC1_FLOAT > R_SRC2_FLOAT);
}


INS_PROTO(cmpeqf_bdest)
{
	B_BDEST = (R_SRC1_FLOAT == R_SRC2_FLOAT);
}


INS_PROTO(illegal)
{
	printf("Illegal instruction (opcode does not exist)\n");
	triggerTrap(current, TRAP_INVALID_OP, 0);
}

/* This is just a placeholder for more decode */
INS_PROTO(monadic)
{
}

INS_PROTO(add)
{
	R_DEST = R_SRC1 + R_SRC2;

}

INS_PROTO(add_immediate)
{
	R_IDEST = R_SRC1 + ISRC2;
}

INS_PROTO(addpc_immediate) 
{
  	R_IDEST = BUNDLE_PC + ISRC2;
}

//INS_PROTO(adds)
//{
//       	R_DEST=Saturate32(((s64) R_SRC1) + ((s64)R_SRC2));
//}
//
//INS_PROTO(addso)
//{
//       	R_DEST=Overflow32(((s64) R_SRC1) + ((s64)R_SRC2));
//}

INS_PROTO(addf)
{
	R_DEST_FLOAT = R_SRC1_FLOAT + R_SRC2_FLOAT;
}

INS_PROTO(addcg)
{
	u64 adc;

	adc = (s64) R_SRC1 + (s64) R_SRC2 + (s64) (B_SCOND!=0);

	R_DEST = (u32) adc;
	CGS_B_BDEST = ((u32) (adc >> 32)) & 1;
}

INS_PROTO(and)
{
	R_DEST = R_SRC1 & R_SRC2;
}

INS_PROTO(and_immediate)
{
	R_IDEST = R_SRC1 & ISRC2;
}

INS_PROTO(andc)
{
	R_DEST = (~R_SRC1) & R_SRC2;
}

INS_PROTO(andc_immediate)
{
	R_IDEST = (~R_SRC1) & ISRC2;
}

INS_PROTO(andl)
{
	R_DEST = R_SRC1 && R_SRC2;
}

INS_PROTO(andl_bdest)
{
	B_BDEST = R_SRC1 && R_SRC2;
}

INS_PROTO(andl_immediate)
{
	R_IDEST = R_SRC1 && ISRC2;
}

INS_PROTO(andl_ibdest)
{
	B_IBDEST = R_SRC1 && ISRC2;
}

//INS_PROTO(andl_br3r)
//{
//	B_BDEST2= B_SRC1 && B_SRC2;
//}

INS_PROTO(br)
{
	int i;
	unsigned int syl;

	if (B_BCOND) {

		//H branch penalty: we're fetching the next bundle. This really assumes that branch instructions are always last in the bundle
		for (i = 0; i < issueWidth(current); i++)
		{
			syl = fetch(current, PC+(i*4), (i == 0));
			if (syl & 0x2) break; //stop bit
		}
		PC = PC + (BTARG(p)  << 3); //H the rVEX uses nextpc as the reference for branch targets.
		current->cr_iacc++;
		current->branch_delay = 1;
	}
}

//INS_PROTO(ibreak)
//{
//	exception |= ILL_INST_EXCEPTION;
//}

INS_PROTO(brf)
{
	int i;
	unsigned int syl;

	if (B_BCOND == 0) {

		//H branch penalty: we're fetching the next bundle. This assumes that branch instructions are always last in the bundle
		for (i = 0; i < issueWidth(current); i++)
		{
			syl = fetch(current, PC+(i*4), (i == 0));
			if (syl & 0x2) break; //stop bit
		}
		PC = PC + (BTARG(p)  << 3); //H the rVEX uses nextpc as the reference for branch targets.
		current->branch_delay = 1;
	}
}

INS_PROTO(bswap)
{
	u32 b0, b1, b2, b3;
	u32 r = R_SRC1;

	b0 = r & 0xff;
	b1 = (r >> 8) & 0xff;
	b2 = (r >> 16) & 0xff;
	b3 = (r >> 24) & 0xff;

	R_IDEST = (b0 << 24) | (b1 << 16) | (b2 << 8) | b3;
}

INS_PROTO(call)
{
	int i;
	unsigned int syl;

	//H branch penalty: we're fetching the next bundle. This assumes that branch instructions are always last in the bundle
	for (i = 0; i < issueWidth(current); i++)
	{
		syl = fetch(current, PC+(i*4), (i == 0));
		if (syl & 0x2) break; //stop bit
	}
	current->branch_delay = 1;

	new->lr = NEXT_PC;

	PC = PC + (BTARG(p)  << 3); //H the rVEX branches relative to the next PC instead of the current PC

}

INS_PROTO(call_lr)
{
	int i;
	unsigned int syl;

	//H branch penalty: we're fetching the next bundle. This assumes that branch instructions are always last in the bundle
	for (i = 0; i < issueWidth(current); i++)
	{
		syl = fetch(current, PC+(i*4), (i == 0));
		if (syl & 0x2) break; //stop bit
	}
	current->branch_delay = 1;

	new->lr = NEXT_PC;

	/* Check for alignment error   */
	if (current->lr & 7)
		triggerTrap(current, TRAP_MISALIGNED_BRANCH, current->lr);

	PC=current->lr;

}

INS_PROTO(clz)
{
	int i;
	unsigned r = R_SRC1;

	for (i = 31; i >= 0; i--) {
		if (r & (1 << i))
			break;
	}

	R_IDEST = 31 - i;
}

INS_PROTO(cmpeq)
{
	R_DEST = (R_SRC1 == R_SRC2);
}

INS_PROTO(cmpeq_bdest)
{
	B_BDEST = (R_SRC1 == R_SRC2);
}

INS_PROTO(cmpeq_immediate)
{
	R_IDEST = (R_SRC1 == ISRC2);
}

INS_PROTO(cmpeq_ibdest)
{
	B_IBDEST = (R_SRC1 == ISRC2);
}

INS_PROTO(cmpge)
{
	R_DEST = (R_SRC1_SIGNED >= R_SRC2_SIGNED);
}

INS_PROTO(cmpge_bdest)
{
	B_BDEST = (R_SRC1_SIGNED >= R_SRC2_SIGNED);
}

INS_PROTO(cmpge_immediate)
{
	R_IDEST = (R_SRC1_SIGNED >= ISRC2_SIGNED);
}

INS_PROTO(cmpge_ibdest)
{
	B_IBDEST = (R_SRC1_SIGNED >= ISRC2_SIGNED);
}

INS_PROTO(cmpgeu)
{
	R_DEST = (R_SRC1 >= R_SRC2);
}

INS_PROTO(cmpgeu_bdest)
{
	B_BDEST = (R_SRC1 >= R_SRC2);
}

INS_PROTO(cmpgeu_immediate)
{
	R_IDEST = (R_SRC1 >= ISRC2);
}

INS_PROTO(cmpgeu_ibdest)
{
	B_IBDEST = (R_SRC1 >= ISRC2);
}

INS_PROTO(cmpgt)
{
	R_DEST = (R_SRC1_SIGNED > R_SRC2_SIGNED);
}

INS_PROTO(cmpgt_bdest)
{
	B_BDEST = (R_SRC1_SIGNED > R_SRC2_SIGNED);
}

INS_PROTO(cmpgt_immediate)
{
	R_IDEST = (R_SRC1_SIGNED > ISRC2_SIGNED);
}

INS_PROTO(cmpgt_ibdest)
{
	B_IBDEST = (R_SRC1_SIGNED > ISRC2_SIGNED);

}

INS_PROTO(cmpgtu)
{
	R_DEST = (R_SRC1 > R_SRC2);
}

INS_PROTO(cmpgtu_bdest)
{
	B_BDEST = (R_SRC1 > R_SRC2);
}

INS_PROTO(cmpgtu_immediate)
{
	R_IDEST = (R_SRC1 > ISRC2);
}

INS_PROTO(cmpgtu_ibdest)
{
	B_IBDEST = (R_SRC1 > ISRC2);
}

INS_PROTO(cmple)
{
	R_DEST = (R_SRC1_SIGNED <= R_SRC2_SIGNED);
}

INS_PROTO(cmple_bdest)
{
	B_BDEST = (R_SRC1_SIGNED <= R_SRC2_SIGNED);
}

INS_PROTO(cmple_immediate)
{
	R_IDEST = (R_SRC1_SIGNED <= ISRC2_SIGNED);
}

INS_PROTO(cmple_ibdest)
{
	B_IBDEST = (R_SRC1_SIGNED <= ISRC2_SIGNED);

}

INS_PROTO(cmpleu)
{
	R_DEST = (R_SRC1 <= R_SRC2);
}

INS_PROTO(cmpleu_bdest)
{
	B_BDEST = (R_SRC1 <= R_SRC2);
}

INS_PROTO(cmpleu_immediate)
{
	R_IDEST = (R_SRC1 <= ISRC2);
}

INS_PROTO(cmpleu_ibdest)
{
	B_IBDEST = (R_SRC1 <= ISRC2);
}

INS_PROTO(cmplt)
{
	R_DEST = (R_SRC1_SIGNED < R_SRC2_SIGNED);
}

INS_PROTO(cmplt_bdest)
{
	B_BDEST = (R_SRC1_SIGNED < R_SRC2_SIGNED);
}

INS_PROTO(cmplt_immediate)
{
	R_IDEST = (R_SRC1_SIGNED < ISRC2_SIGNED);
}

INS_PROTO(cmplt_ibdest)
{
	B_IBDEST = (R_SRC1_SIGNED < ISRC2_SIGNED);
}

INS_PROTO(cmpltu)
{
	R_DEST = (R_SRC1 < R_SRC2);
}

INS_PROTO(cmpltu_bdest)
{
	B_BDEST = (R_SRC1 < R_SRC2);
}

INS_PROTO(cmpltu_immediate)
{
	R_IDEST = (R_SRC1 < ISRC2);
}

INS_PROTO(cmpltu_ibdest)
{
	B_IBDEST = (R_SRC1 < ISRC2);
}

INS_PROTO(cmpne)
{
	R_DEST = (R_SRC1 != R_SRC2);
}

INS_PROTO(cmpne_bdest)
{
	B_BDEST = (R_SRC1 != R_SRC2);
}

INS_PROTO(cmpne_immediate)
{
	R_IDEST = (R_SRC1 != ISRC2);
}

INS_PROTO(cmpne_ibdest)
{
	B_IBDEST = (R_SRC1 != ISRC2);
}

UNIMP(dbgsbrk);

INS_PROTO(instr_div)
{
 	if (R_SRC2 == 0) {
    		if (R_SRC1_SIGNED >= 0) {
      			R_DEST = 0x7fffffff;
    		} else {
      			R_DEST = 0x80000000;
    		}
  	} else if (R_SRC1 == 0x80000000 && R_SRC2 == 0xffffffff) {
    		R_DEST = 0x80000000;
  	} else {
    		R_DEST = R_SRC1_SIGNED / R_SRC2_SIGNED;
  	}
}

INS_PROTO(divu)
{
  	if (R_SRC2 == 0) {
    		R_DEST = 0xffffffff;
  	} else {
    		R_DEST = R_SRC1 / R_SRC2;
  	}
}

INS_PROTO(divs)
{
	u32 temp;

	temp = (R_SRC1 * 2) | (B_SCOND & 1);

	if (R_SRC1_SIGNED < 0) {
		R_DEST = temp + R_SRC2;
		CGS_B_BDEST = 1;
	} else {
		R_DEST = temp - R_SRC2;
		CGS_B_BDEST = 0;
	}
}


#if 0 //H we do this in decode_rvex
INS_PROTO(extract_immediate)
{
  unsigned int position = ISRC2 & 31;
  unsigned int bitCount = BITMASK_EXTRACT(8:5,ISRC2) + 1;
  if (bitCount + position > 32)
  {
    EXCEPTION(ILL_INST_EXCEPTION);
  }
  else
  {
    unsigned int sign = 1 << (bitCount - 1);
    unsigned int mask = (1 << (bitCount - 1)) - 1;
    unsigned int result1 = R_SRC1 >> position;
    if (R_SRC1 & sign)
    {
      result1 |= ~mask;
    }
    else
    {
      result1 &= mask;
    }
    R_IDEST = result1;
  }
}

INS_PROTO(extractu_immediate)
{
  unsigned int position = ISRC2 & 31;
  unsigned int bitCount = BITMASK_EXTRACT(8:5,ISRC2) + 1;
  if (bitCount + position > 32)
  {
    EXCEPTION(ILL_INST_EXCEPTION);
  }
  else
  {
    unsigned int mask = (1 << (bitCount)) - 1;
    R_IDEST = (R_SRC1 >> position) & mask;
  }
}

INS_PROTO(extractl_immediate)
{
  unsigned int position = ISRC2 & 31;
  unsigned int bitCount = BITMASK_EXTRACT(8:5,ISRC2) + 17;
  if (bitCount + position > 32)
  {
    EXCEPTION(ILL_INST_EXCEPTION);
  }
  else
  {
    unsigned int sign = 1 << (bitCount - 1);
    unsigned int mask = (1 << (bitCount - 1)) - 1;
    unsigned int result1 = R_SRC1 >> position;
    if (R_SRC1 & sign)
    {
      result1 |= ~mask;
    }
    else
    {
      result1 &= mask;
    }
    R_IDEST = result1;
  }
}

INS_PROTO(extractlu_immediate)
{
  unsigned int position = ISRC2 & 31;
  unsigned int bitCount = BITMASK_EXTRACT(8:5,ISRC2) + 17;
  if (bitCount + position > 32)
  {
    EXCEPTION(ILL_INST_EXCEPTION);
  }
  else
  {
    unsigned int mask = (1 << (bitCount)) - 1;
    R_IDEST = (R_SRC1 >> position) & mask;
  }
}
#endif

INS_PROTO(go_to)
{
	int i;
	unsigned int syl;

	PC = PC + (BTARG(p)  << 3); //H the rVEX uses nextpc as the reference for branch targets.

	//H branch penalty: we're fetching the next bundle. This assumes that branch instructions are always last in the bundle
	for (i = 0; i < issueWidth(current); i++)
	{
		syl = fetch(current, PC+(i*4), (i == 0));
		if (syl & 0x2) break; //stop bit
	}
	current->branch_delay = 1;

//	if (new->pc == current->pc) sleep_cpu(); //H not supported
}

INS_PROTO(go_to_lr)
{
	int i;
	unsigned int syl;

	  /* Check for alignment error   */
	  if (current->lr & 7)
		  triggerTrap(current, TRAP_MISALIGNED_BRANCH, current->lr);

	  PC=current->lr;

	//H branch penalty: we're fetching the next bundle. This assumes that branch instructions are always last in the bundle
	for (i = 0; i < issueWidth(current); i++)
	{
		syl = fetch(current, PC+(i*4), (i == 0));
		if (syl & 0x2) break; //stop bit
	}
	current->branch_delay = 1;
}


/* All the loads are basically the same - the only difference is 
 * the size of the load and the dissmissable rubbish
 */

#define ENABLE_DCACHE
#include "load_store.c"
#undef ENABLE_DCACHE
#include "load_store.c"

INS_PROTO(limm)
{
	//handled in decode_rvex
}

INS_PROTO(max)
{

	R_DEST = (R_SRC1_SIGNED > R_SRC2_SIGNED) ? R_SRC1 : R_SRC2;

}

INS_PROTO(max_immediate)
{

	R_IDEST = (R_SRC1_SIGNED > ISRC2_SIGNED) ? R_SRC1 : ISRC2;

}

INS_PROTO(maxu)
{

	R_DEST = (R_SRC1 > R_SRC2) ? R_SRC1 : R_SRC2;

}

INS_PROTO(maxu_immediate)
{

	R_IDEST = (R_SRC1 > ISRC2) ? R_SRC1 : ISRC2;
}

INS_PROTO(min)
{

	R_DEST = (R_SRC1_SIGNED < R_SRC2_SIGNED) ? R_SRC1 : R_SRC2;

}

INS_PROTO(min_immediate)
{

	R_IDEST = (R_SRC1_SIGNED < ISRC2_SIGNED) ? R_SRC1 : ISRC2;

}

INS_PROTO(minu)
{

	R_DEST = (R_SRC1 < R_SRC2) ? R_SRC1 : R_SRC2;

}

INS_PROTO(minu_immediate)
{

	R_IDEST = (R_SRC1 < ISRC2) ? R_SRC1 : ISRC2;

}

INS_PROTO(mov_bdest)
{
  	B_BDEST2 = R_SRC1 & 0x7;
}


//INS_PROTO(mov_br3r)
//{
// 	B_BDEST2 = B_SRC1 & 0x7;
//}

INS_PROTO(mov_slct)
{
	R_DEST = B_SCOND & 0x7 ;
}

INS_PROTO(mfl)
{
	R_DEST = L_SRC;
}

INS_PROTO(mtl)
{
	L_DEST = R_SRC2; //This is actually wrong in the html documentation file
}

INS_PROTO(mtl_immediate)
{
	L_DEST = ISRC2;
}

INS_PROTO(mpyf)
{
	R_DEST_FLOAT = R_SRC1_FLOAT * R_SRC2_FLOAT;
}

INS_PROTO(convif)
{
	R_DEST_FLOAT = R_SRC1_SIGNED;
}

INS_PROTO(convfi)
{
	R_DEST_SIGNED = R_SRC1_FLOAT;
}


INS_PROTO(mpy32)
{
	R_DEST = R_SRC1_SIGNED * R_SRC2_SIGNED;
}

INS_PROTO(mpy32_immediate)
{
	R_IDEST = R_SRC1_SIGNED * ISRC2_SIGNED;
}

INS_PROTO(mpy64h)
{
	R_DEST = (((s64) R_SRC1_SIGNED * (s64) R_SRC2_SIGNED)) >> 32;
}

INS_PROTO(mpy64h_immediate)
{
	R_IDEST = (((s64) R_SRC1_SIGNED * (s64) ISRC2_SIGNED)) >> 32;
}

INS_PROTO(mpy64hu)
{
	R_DEST = ((u64) R_SRC1 * (u64) R_SRC2) >> 32;
}

INS_PROTO(mpy64hu_immediate)
{
	R_IDEST = ((u64) R_SRC1 * (u64) ISRC2) >> 32;
}

INS_PROTO(mpyfrac)
{
	s64 result;

	if ((R_SRC1_SIGNED == 0x80000000) && (R_SRC2_SIGNED == 0x80000000)) {
		result = 0x7fffffff;
	} else {
		result = (s64) R_SRC1_SIGNED *(s64) R_SRC2_SIGNED;
		result += 1 << 30;
		result >>= 31;
	}

	R_DEST = result;
}

INS_PROTO(mpyfrac_immediate)
{
	s64 result;

	if ((R_SRC1_SIGNED == 0x80000000) && (ISRC2_SIGNED == 0x80000000)) {
		result = 0x7fffffff;
	} else {
		result = (s64) R_SRC1_SIGNED *(s64) ISRC2_SIGNED;
		result += 1 << 30;
		result >>= 31;
	}

	R_IDEST = result;
}

INS_PROTO(mpyh)
{
	R_DEST = R_SRC1_SIGNED * (R_SRC2_SIGNED >> 16);
}

INS_PROTO(mpyh_immediate)
{
	R_IDEST = R_SRC1_SIGNED * (ISRC2_SIGNED >> 16);
}

INS_PROTO(mpyhh)
{
	R_DEST = (R_SRC1_SIGNED >> 16) * (R_SRC2_SIGNED >> 16);
}

INS_PROTO(mpyhh_immediate)
{
	R_IDEST = (R_SRC1_SIGNED >> 16) * (ISRC2_SIGNED >> 16);
}

INS_PROTO(mpyhhs)
{
	s64 op1 = R_SRC1_SIGNED, op2 = R_SRC2_SIGNED, result;

	result = (op1 * (op2 >> 16)) >> 16;

	R_DEST = (u32) result;
}

INS_PROTO(mpyhhs_immediate)
{
	s64 op1 = R_SRC1_SIGNED, op2 = ISRC2_SIGNED, result;

	result = (op1 * (op2 >> 16)) >> 16;

	R_IDEST = (u32) result;
}

INS_PROTO(mpyhhu)
{
	R_DEST = ((u16) (R_SRC1_SIGNED >> 16)) * ((u16) (R_SRC2_SIGNED >> 16));
}

INS_PROTO(mpyhhu_immediate)
{

	R_IDEST = ((u16) (R_SRC1_SIGNED >> 16)) * ((u16) (ISRC2_SIGNED >> 16));
}

INS_PROTO(mpyhs)
{
	u32 result;

	result = (R_SRC1_SIGNED * ((u16) (R_SRC2_SIGNED >> 16))) << 16;
	R_DEST = (u32) result;
}

INS_PROTO(mpyhs_immediate)
{
	u32 result;

	result = (R_SRC1_SIGNED * ((u16) (ISRC2_SIGNED >> 16))) << 16;
	R_IDEST = (u32) result;
}

INS_PROTO(mpyhu)
{
	R_DEST = R_SRC1 * ((u16) ((R_SRC2_SIGNED) >> 16));
}

INS_PROTO(mpyhu_immediate)
{
	R_IDEST = R_SRC1 * ((u16) ((ISRC2_SIGNED) >> 16));
}

INS_PROTO(mpyl)
{
	R_DEST = R_SRC1_SIGNED * ((s16) (R_SRC2_SIGNED));
}

INS_PROTO(mpyl_immediate)
{
	R_IDEST = R_SRC1_SIGNED * ((s16) (ISRC2_SIGNED));
}

INS_PROTO(mpylh)
{
	R_DEST = ((s16) (R_SRC1)) * (R_SRC2_SIGNED >> 16);
}

INS_PROTO(mpylh_immediate)
{
	R_IDEST = ((s16) (R_SRC1)) * (ISRC2_SIGNED >> 16);
}

INS_PROTO(mpylhu)
{
	R_DEST = ((u16) (R_SRC1)) * ((u16) (R_SRC2_SIGNED >> 16));
}

INS_PROTO(mpylhu_immediate)
{
	R_IDEST = ((u16) (R_SRC1)) * ((u16) (ISRC2_SIGNED >> 16));
}

INS_PROTO(mpylhus)
{
	s64 result;

	result = ((s64) R_SRC1_SIGNED * ((u16) R_SRC2)) >> 32;

	R_DEST = (u32) result;
}

INS_PROTO(mpylhus_immediate)
{
	s64 result;

	result = ((s64) R_SRC1_SIGNED * ((u16) ISRC2)) >> 32;

	R_IDEST = (u32) result;
}

INS_PROTO(mpyll)
{
	R_DEST = ((s16) R_SRC1) * ((s16) R_SRC2);
}

INS_PROTO(mpyll_immediate)
{
	R_IDEST = ((s16) R_SRC1) * ((s16) ISRC2);
}

INS_PROTO(mpyllu)
{
	R_DEST = ((u16) R_SRC1) * ((u16) R_SRC2);
}

INS_PROTO(mpyllu_immediate)
{
	R_IDEST = ((u16) R_SRC1) * ((u16) ISRC2);
}

INS_PROTO(mpylu)
{
	R_DEST = R_SRC1 * ((u16) R_SRC2);
}

INS_PROTO(mpylu_immediate)
{
	R_IDEST = R_SRC1 * ((u16) ISRC2);
}

INS_PROTO(nandl)
{
	R_DEST = !((R_SRC1 != 0) && (R_SRC2 != 0));
}

INS_PROTO(nandl_bdest)
{
	B_BDEST = !((R_SRC1 != 0) && (R_SRC2 != 0));
}

INS_PROTO(nandl_immediate)
{

	R_IDEST = !((R_SRC1 != 0) && (ISRC2 != 0));
}

INS_PROTO(nandl_ibdest)
{
	B_IBDEST = !((R_SRC1 != 0) && (ISRC2 != 0));

}

//INS_PROTO(nandl_br3r)
//{
//	B_BDEST2 = !( (B_SRC1!=0) && (B_SRC2!=0));
//
//}
INS_PROTO(norl)
{
	R_DEST = !((R_SRC1 != 0) || (R_SRC2 != 0));
}

INS_PROTO(norl_bdest)
{
	B_BDEST = !((R_SRC1 != 0) || (R_SRC2 != 0));
}

INS_PROTO(norl_immediate)
{

	R_IDEST = !((R_SRC1 != 0) || (ISRC2 != 0));
}

INS_PROTO(norl_ibdest)
{
	B_IBDEST = !((R_SRC1 != 0) || (ISRC2 != 0));

}

INS_PROTO(norl_br3r) 
{
	B_BDEST2 = (!( (B_SRC1!=0) || (B_SRC2!=0)));
}

INS_PROTO(nop)
{
	current->cr_nop++; //H This counts the number of NOPs encoded in the binary
}


INS_PROTO(or)
{
	R_DEST = R_SRC1 | R_SRC2;
}

INS_PROTO(or_immediate)
{
	R_IDEST = R_SRC1 | ISRC2;
}

INS_PROTO(orc)
{
	R_DEST = ~R_SRC1 | R_SRC2;
}

INS_PROTO(orc_immediate)
{
	R_IDEST = ~R_SRC1 | ISRC2;
}

INS_PROTO(orl)
{
	R_DEST = (R_SRC1 != 0) || (R_SRC2 != 0);
}

INS_PROTO(orl_bdest)
{
	B_BDEST = (R_SRC1 != 0) || (R_SRC2 != 0);
}

INS_PROTO(orl_immediate)
{
	R_IDEST = (R_SRC1 != 0) || (ISRC2 != 0);
}

INS_PROTO(orl_ibdest)
{
	B_IBDEST = (R_SRC1 != 0) || (ISRC2 != 0);
}


//INS_PROTO(orl_br3r)
//{
//	B_BDEST2 = (B_SRC1 != 0) || (B_SRC2 != 0);
//}

//INS_PROTO(pft)
//{
//	/* DO NOTHING */
//}
//
//
//INS_PROTO(pftc)
//{
//	/* DO NOTHING */
//}
//
//
//
//INS_PROTO(pswset)
//{
//	psw |= R_SRC2;
//	/* Could potentially do a super/user transition */
//	invalidate_last_tlb_cache();
//	last_bundle_change_psw = 1;
//
//}
//
//INS_PROTO(pswclr)
//{
//	psw &= ~R_SRC2;
//	/* Could potentially do a super/user transition */
//	invalidate_last_tlb_cache();
//	last_bundle_change_psw = 1;
//
//}
//
//INS_PROTO(pswmask)
//{
//  	u32 old_psw = psw;
//  	u32 mask = ISRC2 ;
//
//  	if ((psw & PSW_USER_MODE) && !(psw & PSW_DEBUG_MODE)) {
//    		exception|=ILL_INST_EXCEPTION;
//    		return;
//  	}
//
//  	psw = (R_SRC1 & mask) | (psw & ~mask);
//
//  	R_IDEST = old_psw;
//
//	last_bundle_change_psw = 1;
//	invalidate_last_tlb_cache();
//}
//
//
//
//#define PRGINS_MASK (~((1<<13)-1))
//
//INS_PROTO(prginspg)
//{
//	u32 ea = R_SRC1 + ISRC2;
//
//	/* This gives us the physical mask */
//	ea &= PRGINS_MASK;
//
//	invalidate_icache_address(ea, 8192);
//}
//
//INS_PROTO(prgins)
//{
//	invalidate_icache();
//}
//
//
//#define PRGINSADD_MASK BITMASK(31:6)
//
//INS_PROTO(prginsadd)
//{
//  u32 ea = (R_SRC1 + ISRC2);
//  struct tlb_entry *t;
//
//  /* This models what the hardware actually does, it uses the dside
//   * to lookup
//   */
//  t=dside_lookup_tlb(ea,1,0);
//
//  /* We should check for execute permission here as well */
//
//  if(exception) return;
//
//  ea = TRANSLATE(t,ea);
//
//  //invalidate_icache();
//  invalidate_icache_address((ea & PRGINSADD_MASK), 64);
//
//}
//
//
//INS_PROTO(prginsset)
//{
//  // TODO: IMPLEMENT 4-way ICache - THEN CORRECT THIS!
//  //u32 ea = (R_SRC1 + ISRC2);
//  //invalidate_icache_set((ea & PRGINSADD_MASK), 64);
//  invalidate_icache();
//}



//INS_PROTO(rem)
//{
//  if (R_SRC2 == 0)
//  {
//    R_DEST = 0;
//  }
//  else if (R_SRC1 == 0x80000000 && R_SRC2 == 0xffffffff)
//  {
//    R_DEST = 0;
//  }
//  else
//  {
//    R_DEST = (R_SRC1_SIGNED % R_SRC2_SIGNED);
//  }
//}
//
//INS_PROTO(remu)
//{
//  //R_DEST = UIRemIeee(R_SRC1, R_SRC2);
//  // i.e......
//  if (R_SRC2 == 0)
//  {
//    R_DEST = 0;
//  }
//  else
//  {
//    R_DEST = R_SRC1 % R_SRC2;
//  }
//}
//
//INS_PROTO(retention)
//{
//  /* Do nothing */
//}

/* VEX can also pop the stack using this insn */
INS_PROTO(return_lr)
{
  /* Check for alignment error   */
  if (current->lr & 7)
	  triggerTrap(current, TRAP_MISALIGNED_BRANCH, current->lr);

  PC=current->lr;

  /* pop stack */
  new->r[1] = current->r[1] + BTARG(p);
  current->branch_delay = 1;
}

INS_PROTO(rfi)
{

	if (current->ccr & CR_CCR_KME_C) //not running in system mode
	{
		printf("Error; return from interrupt while not running in system mode.\n");
		triggerTrap(current, TRAP_INVALID_OP, 0);
	}

  if (current->tp & 7)
	  triggerTrap(current, TRAP_MISALIGNED_BRANCH, current->tp);

	new->pc = (current->tp);

	//H Note that we're changing the current CCR and not new->ccr. This is not correct if this bundle also generates a trap.
	//Or, for example, after this syllable a write is performed to a creg (which needs the context to run in super mode)
	current->ccr = current->sccr;

	/* Possible super/user transition  */
//	invalidate_last_tlb_cache();

	last_bundle_change_psw = 1;

	/* pop stack */
	new->r[1] = current->r[1] + BTARG(p);
	current->branch_delay = 1;
}

INS_PROTO(rotl)
{
  	R_IDEST = (R_SRC1 << R_SRC2) | (R_SRC1 >> (32 - R_SRC2));
}

INS_PROTO(rotl_immediate)
{
  	R_IDEST = (R_SRC1 << ISRC2) | (R_SRC1 >> (32 - ISRC2));
}

INS_PROTO(sats)
{
  	R_DEST = Saturate16(R_SRC1);
}

INS_PROTO(satso)
{
  	R_DEST = Overflow16(R_SRC1);
}

INS_PROTO(sbit)
{
  	R_DEST = R_SRC1 | (1 << ISRC2);
}

INS_PROTO(sbitf)
{
	R_DEST = R_SRC1 & (~(1 << ISRC2));
}

//INS_PROTO(instr_sbrk)
//{
//	exception |= SBREAK_EXCEPTION;
//}

INS_PROTO(sh1add)
{

	R_DEST = (R_SRC1_SIGNED << 1) + R_SRC2;

}

INS_PROTO(sh1add_immediate)
{

	R_IDEST = (R_SRC1_SIGNED << 1) + ISRC2;

}


//INS_PROTO(sh1adds)
//{
//  	u64 tmp1 = R_SRC1_SIGNED;
//  	u64 shiftresult = tmp1 << 1;
//  	u64 addresult = Saturate32(shiftresult) + R_SRC2_SIGNED;
//
//  	R_DEST = Saturate32(addresult);
//}
//
//
//INS_PROTO(sh1addso)
//{
//  	u64 tmp1 = R_SRC1_SIGNED;
//  	u64 shiftresult = tmp1 << 1;
//  	u64 addresult = shiftresult + R_SRC2_SIGNED;
//
//  	R_DEST = Overflow32(shiftresult) | Overflow32(addresult);
//}
//
//INS_PROTO(sh1subs)
//{
//  	u64 tmp1 = R_SRC1_SIGNED;
//  	u64 shiftresult = tmp1 << 1;
//  	u64 subresult = R_SRC2 - Saturate32(shiftresult);
//
//  	R_DEST = Saturate32(subresult);
//}
//
//INS_PROTO(sh1subso)
//{
//  u64 tmp1 = R_SRC1_SIGNED;
//  u64 shiftresult = tmp1 << 1;
//  u64 subresult = R_SRC2 - shiftresult;
//
//  R_DEST = Overflow32(shiftresult) || Overflow32(subresult);
//}


INS_PROTO(sh2add)
{

	R_DEST = (R_SRC1_SIGNED << 2) + R_SRC2;

}

INS_PROTO(sh2add_immediate)
{

	R_IDEST = (R_SRC1_SIGNED << 2) + ISRC2;

}

INS_PROTO(sh3add)
{

	R_DEST = (R_SRC1_SIGNED << 3) + R_SRC2;

}

INS_PROTO(sh3add_immediate)
{

	R_IDEST = (R_SRC1_SIGNED << 3) + ISRC2;

}

INS_PROTO(sh4add)
{

	R_DEST = (R_SRC1_SIGNED << 4) + R_SRC2;

}

INS_PROTO(sh4add_immediate)
{

	R_IDEST = (R_SRC1_SIGNED << 4) + ISRC2;

}

INS_PROTO(shl)
{
	u32 op2;

	op2 = (u8) R_SRC2;
	if (op2 > 31) {
		R_DEST = 0;
	} else {
		R_DEST = R_SRC1_SIGNED << op2;
	}
}

INS_PROTO(shl_immediate)
{
	u32 op2;

	op2 = (u8) ISRC2;
	if (op2 > 31) {
		R_IDEST = 0;
	} else {
		R_IDEST = R_SRC1_SIGNED << op2;
	}
}


INS_PROTO(shls)
{
  u32 op2;
  s64 tmp1 = R_SRC1_SIGNED;
 
  op2= R_SRC2 & 0xff;
 R_DEST = Saturate32(tmp1 << ((op2 > 32) ? 32 : op2) );
}



INS_PROTO(shls_immediate)
{
  u32 op2;
  s64 tmp1 = R_SRC1_SIGNED;

  op2= ISRC2 & 0xff;
  R_IDEST = Saturate32(tmp1 << ((op2 > 32) ? 32 : op2) );
}

//INS_PROTO(shlso)
//{
//  u32 op2;
//  s64 tmp1 = R_SRC1_SIGNED;
//
//  op2= (u8)R_SRC2;
//  R_DEST = Overflow32(tmp1 << ((op2 > 32) ? 32 : op2) );
//}
//
//
//
//INS_PROTO(shlso_immediate)
//{
//  u32 op2;
//  s64 tmp1 = R_SRC1_SIGNED;
//
//  op2= (u8)ISRC2;
//  R_DEST = Overflow32(tmp1 << ((op2 > 32) ? 32 : op2) );
//}


INS_PROTO(shr)
{
	int shift = ZeroExtend8(R_SRC2);

	if (shift > 31) {
		R_DEST = (R_SRC1_SIGNED < 0) ? -1 : 0;
	} else {
		R_DEST = R_SRC1_SIGNED >> shift;
	}

}

INS_PROTO(shr_immediate)
{
	int shift = ZeroExtend8(ISRC2);

	if (shift > 31) {
		R_IDEST = (R_SRC1_SIGNED < 0) ? -1 : 0;
	} else {
		R_IDEST = R_SRC1_SIGNED >> shift;
	}

}

INS_PROTO(shru)
{
	int shift = ZeroExtend8(R_SRC2);

	if (shift > 31) {
		R_DEST = 0;
	} else {
		R_DEST = R_SRC1 >> shift;
	}

}

INS_PROTO(shru_immediate)
{
	int shift = ZeroExtend8(ISRC2);

	if (shift > 31) {
		R_IDEST = 0;
	} else {
		R_IDEST = R_SRC1 >> shift;
	}
}

//INS_PROTO(shrrnp_immediate)
//{
//  s64 operand1 = (R_SRC1_SIGNED);
//  u32 shift=ZeroExtend8(ISRC2);
//
//  if (shift>32)
//  {
//    shift=32;
//  }
//
//  if (shift>0)
//  {
//    s64 rounding = 1;
//    rounding <<= (shift - 1);
//    s64 result = (operand1 + rounding) >> shift;
//    R_IDEST = result;
//  }
//  else
//  {
//    R_IDEST = R_SRC1;
//  }
//}

INS_PROTO(slct)
{

	R_DEST = (B_SCOND != 0) ? R_SRC1 : R_SRC2;
}

INS_PROTO(slct_immediate)
{

	R_IDEST = (B_SCOND != 0) ? R_SRC1 : ISRC2;
}

INS_PROTO(slctf)
{

	R_DEST = (B_SCOND == 0) ? R_SRC1 : R_SRC2;
}

INS_PROTO(slctf_immediate)
{

	R_IDEST = (B_SCOND == 0) ? R_SRC1 : ISRC2;
}

INS_PROTO(stop)
{
	//H Note that we're changing the current DCR and not new->dcr. This is not correct if this bundle also generates a trap.
	current->dcr = current->dcr | (1<<CR_DCR_D_BIT); //H TODO: There's probably more
	printf("Context %d: Halting execution because of stop instruction\n", CID(current));
}

INS_PROTO(sub)
{
	R_DEST = R_SRC2_SIGNED - R_SRC1_SIGNED;
}

INS_PROTO(sub_immediate)
{
	R_IDEST = ISRC2_SIGNED - R_SRC1_SIGNED;
}

INS_PROTO(subf)
{
	R_DEST_FLOAT = R_SRC1_FLOAT - R_SRC2_FLOAT;
}

//INS_PROTO(subs)
//{
//	R_DEST=Saturate32((s64)R_SRC1 - (s64)R_SRC2);
//}

//INS_PROTO(subso)
//{
//	R_DEST=Overflow32((s64)R_SRC1 - (s64)R_SRC2);
//}

INS_PROTO(sxt)
{
  int op2 = ZeroExtend8(R_SRC2);
  if (op2 == 0)
  {
    R_DEST = 0;
  }
  else if (op2 > 31)
  {
    R_DEST = R_SRC1;
  }
  else
  {
    int sign = 1 << (op2 - 1);
    int mask = (1 << (op2 - 1)) - 1;
    if ((R_SRC1 & sign) != 0)
    {
      R_DEST = R_SRC1 | (~mask);
    }
    else
    {
      R_DEST = R_SRC1 & mask;
    }
  }
}

INS_PROTO(sxtb)
{
	R_IDEST = SignExtend(R_SRC1, 8);
}

INS_PROTO(sxth)
{
	R_IDEST = SignExtend(R_SRC1, 16);
}


INS_PROTO(sxt_immediate)
{
  int op2 = ZeroExtend8(ISRC2);
  if (op2 == 0)
  {
    R_IDEST = 0;
  }
  else if (op2 > 31)
  {
    R_IDEST = R_SRC1;
  }
  else
  {
    int sign = 1 << (op2 - 1);
    int mask = (1 << (op2 - 1)) - 1;
    if ((R_SRC1 & sign) != 0)
    {
      R_IDEST = R_SRC1 | (~mask);
    }
    else
    {
      R_IDEST = R_SRC1 & mask;
    }
  }
}

INS_PROTO(instr_sync)
{
	/* Do nothing */
}

//INS_PROTO(syncins)
//{
//	/* Do nothing */
//}

//INS_PROTO(tbit)
//{
//	if (R_SRC1 & (1 << SRC2))
//		B_BDEST = 1;
//	else
//		B_BDEST = 0;
//}

INS_PROTO(tbit_immediate)
{
	if (R_SRC1 & (1 << ISRC2))
		R_IDEST = 1;
	else
		R_IDEST = 0;
}

INS_PROTO(tbit_ibdest)
{
	if (R_SRC1 & (1 << ISRC2))
		B_BDEST = 1;
	else
		B_BDEST = 0;
}

//INS_PROTO(tbitf)
//{
//	if (R_SRC1 & (1 << R_SRC2))
//		B_BDEST = 0;
//	else
//		B_BDEST = 1;
//}

INS_PROTO(tbitf_immediate)
{
	if (R_SRC1 & (1 << ISRC2))
		R_IDEST = 1;
	else
		R_IDEST = 0;
}

INS_PROTO(tbitf_ibdest)
{
	if (R_SRC1 & (1 << ISRC2))
		B_BDEST = 1;
	else
		B_BDEST = 0;
}


/*
rVEX uCLibc:
sizeof(dev_t) is 8
sizeof(ino_t) is 4
sizeof(mode_t) is 4
sizeof(nlink_t) is 4
sizeof(uid_t) is 4
sizeof(gid_t) is 4
sizeof(dev_t) is 8
sizeof(off_t) is 4

rVEX new newlib:
sizeof(dev_t) is 2
sizeof(ino_t) is 2
sizeof(mode_t) is 4
sizeof(nlink_t) is 2
sizeof(uid_t) is 2
sizeof(gid_t) is 2
sizeof(dev_t) is 2
sizeof(off_t) is 4

amd64:
sizeof(dev_t) is 8
sizeof(ino_t) is 8
sizeof(mode_t) is 4
sizeof(nlink_t) is 8
sizeof(uid_t) is 4
sizeof(gid_t) is 4
sizeof(dev_t) is 8
sizeof(off_t) is 8
*/

typedef unsigned long long 	uclibc_dev_t;
typedef unsigned long 		uclibc_ino_t;
typedef unsigned long		uclibc_mode_t;
typedef unsigned long		uclibc_nlink_t;
typedef unsigned long		uclibc_uid_t;
typedef unsigned long		uclibc_gid_t;
typedef long				uclibc_off_t;

typedef unsigned short	 	newlib_dev_t;
typedef unsigned short 		newlib_ino_t;
typedef unsigned long		newlib_mode_t;
typedef unsigned short		newlib_nlink_t;
typedef unsigned short		newlib_uid_t;
typedef unsigned short		newlib_gid_t;
typedef long				newlib_off_t;

/*
 * Great, we need to translate the struct stat that we get from our host into what the st200 libc expects.
 * These are the first 8 records from the struct stat in newlib.
 * The rest seems to be obscure so I'm assume that the programs we want to emulate syscalls for don't need them.
 */
struct	uclibc_stat
{
	uclibc_dev_t		st_dev;
	uclibc_ino_t		st_ino;
	uclibc_mode_t	st_mode;
	uclibc_nlink_t	st_nlink;
	uclibc_uid_t		st_uid;
	uclibc_gid_t		st_gid;
	uclibc_dev_t		st_rdev;
	uclibc_off_t		st_size;
};

struct	newlib_stat
{
	newlib_dev_t		st_dev;
	newlib_ino_t		st_ino;
	newlib_mode_t	st_mode;
	newlib_nlink_t	st_nlink;
	newlib_uid_t		st_uid;
	newlib_gid_t		st_gid;
	newlib_dev_t		st_rdev;
	newlib_off_t		st_size;
};

INS_PROTO(instr_trap)
{
	/*
	 * This function can emulate system calls on the host (replacing __dotsyscall).
	 * Applications that were built for Linux, using uCLinux, use trap number 0x90.
	 * Applications that were built with newlib use trap number 0x91, as it has a different syscall table.
	 */
	u32 *arg = current->r + 3;
	u32 syscall_num = current->r[2];
	char buf[128];

	if (ISRC2 == 0x90 && syscall_emulation)
	{
		switch (syscall_num) {
		case __NR_read:
		{
			char *s;
			int i;
//			printf("Attempting to read from fd %d, len %d, to buffer at addres 0x%x: ", arg[0], arg[2], arg[1]);
			new->r[3] = read(arg[0], get_memory_string(arg[1]), arg[2]);
//			s = get_memory_string(arg[1]);
//			for (i = 0; i < new->r[3] && i < 16; i++) {
//				printf("%02hhX ", *s++);
//			}
//			printf("\n");
			if (new->r[3] == -1)
				new->r[3] = -errno;
			break;
		}
		case __NR_write:
		{
			char *s;
			int i;
//			printf("Trying to write to fd %d, length %d, to addr 0x%x: ", arg[0], arg[2], arg[1], get_memory_string(arg[1]));
//			s = get_memory_string(arg[1]);
//			for (i = 0; i < arg[2] && i < 16; i++) {
//				printf("%02hhX ", *s++);
//			}
//			printf("\n");
			new->r[3] = write(arg[0], get_memory_string(arg[1]), arg[2]);
			if (new->r[3] == -1)
				new->r[3] = -errno;
			break;
		}
		case __NR_open:
		{
			//we want everything to work relative to the current directory. so if there's a full path, we need to prefix it.
			int fd;
			char *path;
			path = get_memory_string(arg[0]);
			if (path[0] == '/')
			{
				char fullpath[512];
				getcwd(fullpath, sizeof(fullpath));
				strcat(fullpath, path);
				path = fullpath;
			}
			
			//host creation flags are different from newlib because we can't have nice things, so convert
			static unsigned const int open_flag_convert_table[] = {
				O_APPEND,     0x000008,
				O_ASYNC,      0x000040,
				O_CLOEXEC,    0x040000,
				O_CREAT,      0x000200,
				//O_DIRECT,     0x080000,
				O_DIRECTORY,  0x200000,
				O_DSYNC,      0x002000,
				O_EXCL,       0x000800,
				O_NOCTTY,     0x008000,
				O_NOFOLLOW,   0x100000,
				O_NONBLOCK,   0x004000,
				O_SYNC,       0x002000,
				O_TRUNC,      0x000400,
				0,            0
			};
			unsigned const int *open_flag_convert = open_flag_convert_table;
			
			//copy mode, which is the same
			unsigned int host_flags = arg[1] & 3;
			unsigned int sim_flags_matched = 3;
			
			while (open_flag_convert[1])
			{
				if (arg[1] & open_flag_convert[1])
				{
					host_flags |= open_flag_convert[0];
					sim_flags_matched |= open_flag_convert[1];
				}
				open_flag_convert += 2;
			}
			
			//return EINVAL when unknown flags were found
			if (arg[1] & ~sim_flags_matched)
			{
				new->r[3] = -EINVAL;
			}
			else
			{
				printf("Trying to open(%s, 0x%x, 0x%x)\n", path, host_flags, arg[2]);
				fd = open(path, host_flags, arg[2]);
				//printf("open returned %d\n", fd);
				if (fd < 0)
					perror("open failed");
				new->r[3] = (fd == -1) ? -errno : fd;
			}
			break;

		}
		case __NR_close:
		{
			if (arg[0] < 3) //let's not ask the OS to close our own stdin/out/err
			{
				new->r[3] = 0;
				break;
			}
			new->r[3] = close(arg[0]);
			if (new->r[3] == -1)
				new->r[3] = -errno;
			break;
		}
		case __NR_lseek:
		{
			new->r[3] = lseek(arg[0], arg[1], arg[2]);
			if (new->r[3] == -1)
				new->r[3] = -errno;
			break;
		}
		case __NR_ioctl:
		{
			new->r[3] = ioctl(arg[0], arg[1], arg[2]);
			if (new->r[3] == -1)
				new->r[3] = -errno;
			break;
		}
		case __NR_fcntl:
		{
			new->r[3] = fcntl(arg[0], arg[1], arg[2]);
			if (new->r[3] == -1)
				new->r[3] = -errno;
			break;
		}
		case __NR_stat:
		{
			struct stat hoststat;
			struct uclibc_stat *rvexstat = (struct uclibc_stat*)get_memory_string(arg[1]);
			int retval = stat(get_memory_string(arg[0]), &hoststat);

			//the layout of the hosts stat struct is different from st200s (64bit, etc), so we need to translate
			rvexstat->st_dev = hoststat.st_dev; //TODO: should the 2 subwords of this value be swapped?
			rvexstat->st_ino = hoststat.st_ino;
			rvexstat->st_mode = hoststat.st_mode;
			rvexstat->st_nlink = hoststat.st_nlink;
			rvexstat->st_uid = hoststat.st_uid;
			rvexstat->st_gid = hoststat.st_gid;
			rvexstat->st_rdev = hoststat.st_rdev; //same as above
			rvexstat->st_size = hoststat.st_size;
			
			printf("Warning: stat syscall is bullshit right now because endianness.\n");

			new->r[3] = (retval == -1) ? -errno : retval;
//			new->r[3] = -88;
			break;
		}
		case __NR_fstat:
		{
			struct stat hoststat;
			struct uclibc_stat *rvexstat = (struct uclibc_stat*)get_memory_string(arg[1]);
			int retval = fstat(arg[0], &hoststat);

			//the layout of the hosts stat struct is different from st200s (64bit, etc), so we need to translate
			rvexstat->st_dev = hoststat.st_dev;//same as above
			rvexstat->st_ino = hoststat.st_ino;
			rvexstat->st_mode = hoststat.st_mode;
			rvexstat->st_nlink = hoststat.st_nlink;
			rvexstat->st_uid = hoststat.st_uid;
			rvexstat->st_gid = hoststat.st_gid;
			rvexstat->st_rdev = hoststat.st_rdev;//same as above
			rvexstat->st_size = hoststat.st_size;

			new->r[3] = (retval == -1) ? -errno : retval;
			break;
		}
		case __NR_mmap:
		{
			new->r[3] = sim_mmap(arg[0], arg[1], arg[2], arg[3], arg[4], arg[5]); //sim_mmap directly returns MAP_FAILED
			break;
		}
		case __NR_time:
		{
			u32 t = (u32)time(0);
			if (arg[0]) {
				write_memory_32(arg[0], t);
			}
			new->r[3] = t;
			break;
		}
		default:
			printf("unimplemented uCLibc syscall - number %d\n", syscall_num);
			new->r[3] = -ENOSYS;
			break;
		}
		/*if (new->r[3] > (unsigned int)-4096)
		{
			new->br[0] = 1;
		}
		else
			new->br[0] = 0;*/
	}
	else if (ISRC2 == 0x91 && syscall_emulation)
	{
		switch (syscall_num) {
		case __NEWLIB_NR_exit:
		{
			simulator_exit(arg[0]);
			break;
		}
		case __NEWLIB_NR_sbrk:
		{
//			new->r[3] = sim_sbrk(arg[0]);
//			if (new->r[3] < 0)
//				new->r[5] = new->r[3]; //sim_sbrk directly returns MAP_FAILED if it fails
//			else
//				new->r[5] = 0;
//			break;
			triggerTrap(current, ISRC2, R_SRC1);
			break;
		}
		case __NEWLIB_NR_setup:
		{
			//nothing to do for the simulator
			new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_write:
		{
			char *s;
			int i;
//			printf("Trying to write to fd %d, length %d, to addr 0x%x: ", arg[0], arg[2], arg[1], get_memory_string(arg[1]));
//			s = get_memory_string(arg[1]);
//			for (i = 0; i < arg[2] && i < 16; i++) {
//				printf("%02hhX ", *s++);
//			}
//			printf("\n");
			new->r[3] = write(arg[0], get_memory_string(arg[1]), arg[2]);
			if (new->r[3] == -1)
				new->r[5] = errno;
			else
				new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_read:
		{
			char *s;
			int i;
//			printf("Attempting to read from fd %d, len %d, to buffer at addres 0x%x: ", arg[0], arg[2], arg[1]);
			new->r[3] = read(arg[0], get_memory_string(arg[1]), arg[2]);
//			s = get_memory_string(arg[1]);
//			for (i = 0; i < new->r[3] && i < 16; i++) {
//				printf("%02hhX ", *s++);
//			}
//			printf("\n");
			if (new->r[3] == -1)
				new->r[5] = errno;
			else
				new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_open:
		{
			//we want everything to work relative to the current directory. so if there's a full path, we need to prefix it.
			int fd;
			char *path;
			path = get_memory_string(arg[0]);
			if (path[0] == '/')
			{
				char fullpath[512];
				getcwd(fullpath, sizeof(fullpath));
				strcat(fullpath, path);
				path = fullpath;
			}

			//host creation flags are different from newlib because we can't have nice things, so convert
			static unsigned const int open_flag_convert_table[] = {
				O_APPEND,     0x000008,
				O_ASYNC,      0x000040,
				O_CLOEXEC,    0x040000,
				O_CREAT,      0x000200,
				//O_DIRECT,     0x080000,
				O_DIRECTORY,  0x200000,
				O_DSYNC,      0x002000,
				O_EXCL,       0x000800,
				O_NOCTTY,     0x008000,
				O_NOFOLLOW,   0x100000,
				O_NONBLOCK,   0x004000,
				O_SYNC,       0x002000,
				O_TRUNC,      0x000400,
				0,            0
			};
			unsigned const int *open_flag_convert = open_flag_convert_table;

			//copy mode, which is the same
			unsigned int host_flags = arg[1] & 3;
			unsigned int sim_flags_matched = 3;

			while (open_flag_convert[1])
			{
				if (arg[1] & open_flag_convert[1])
				{
					host_flags |= open_flag_convert[0];
					sim_flags_matched |= open_flag_convert[1];
				}
				open_flag_convert += 2;
			}

			//return EINVAL when unknown flags were found
			if (arg[1] & ~sim_flags_matched)
			{
				new->r[5] = EINVAL;
			}
			else
			{
				printf("Trying to open(%s, 0x%x, 0x%x)\n", path, host_flags, arg[2]);
				fd = open(path, host_flags, arg[2]);
				//printf("open returned %d\n", fd);
				if (fd < 0)
					perror("open failed");
				new->r[3] = fd;
				new->r[5] = (fd == -1) ? errno : 0;
			}
			break;
		}
		case __NEWLIB_NR_close:
		{
			if (arg[0] < 3) //let's not ask the OS to close our own stdin/out/err
			{
				new->r[5] = 0;
				break;
			}
			new->r[3] = close(arg[0]);
			if (new->r[3] == -1)
				new->r[5] = errno;
			else
				new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_lseek:
		{
			new->r[3] = lseek(arg[0], arg[1], arg[2]);
			if (new->r[3] == -1)
				new->r[5] = errno;
			else
				new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_ioctl:
		{
			new->r[3] = ioctl(arg[0], arg[1], arg[2]);
			if (new->r[3] == -1)
				new->r[5] = errno;
			else
				new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_fcntl:
		{
			new->r[3] = fcntl(arg[0], arg[1], arg[2]);
			if (new->r[3] == -1)
				new->r[5] = errno;
			else
				new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_stat:
		{
			struct stat hoststat;
			struct newlib_stat *rvexstat = (struct newlib_stat*)get_memory_string(arg[1]);
			int retval = stat(get_memory_string(arg[0]), &hoststat);

			//the layout of the hosts stat struct is different from st200s (64bit, etc), so we need to translate
			rvexstat->st_dev = hoststat.st_dev;
			rvexstat->st_ino = hoststat.st_ino;
			rvexstat->st_mode = hoststat.st_mode;
			rvexstat->st_nlink = hoststat.st_nlink;
			rvexstat->st_uid = hoststat.st_uid;
			rvexstat->st_gid = hoststat.st_gid;
			rvexstat->st_rdev = hoststat.st_rdev;
			rvexstat->st_size = hoststat.st_size;

			printf("Warning: stat syscall is not tested.\n");

			new->r[3] = retval;
			new->r[5] = (retval == -1) ? errno : 0;
			break;
		}
		case __NEWLIB_NR_fstat:
		{
			struct stat hoststat;
			struct newlib_stat *rvexstat = (struct newlib_stat*)get_memory_string(arg[1]);
			int retval = fstat(arg[0], &hoststat);

			//the layout of the hosts stat struct is different from st200s (64bit, etc), so we need to translate
			rvexstat->st_dev = hoststat.st_dev;
			rvexstat->st_ino = hoststat.st_ino;
			rvexstat->st_mode = hoststat.st_mode;
			rvexstat->st_nlink = hoststat.st_nlink;
			rvexstat->st_uid = hoststat.st_uid;
			rvexstat->st_gid = hoststat.st_gid;
			rvexstat->st_rdev = hoststat.st_rdev;
			rvexstat->st_size = hoststat.st_size;

			printf("Warning: stat syscall is not tested.\n");

			new->r[3] = retval;
			new->r[5] = (retval == -1) ? errno : 0;
			break;
		}
		case __NEWLIB_NR_gettimeofday:
		{
			u32 t = (u32)time(0);
			if (arg[0]) {
				write_memory_32(arg[0], t);
			}
			new->r[3] = t;
			new->r[5] = 0;
			break;
		}
		case __NEWLIB_NR_link:
		case __NEWLIB_NR_unlink:
		case __NEWLIB_NR_mkdir:
		{
			printf("unimplemented  newlib syscall - number %d. Returning ENOSYS.\n", syscall_num);
			new->r[5] = ENOSYS;
			break;
		}
		case __NEWLIB_NR_fork:
		case __NEWLIB_NR_execve:
		case __NEWLIB_NR_waitpid:
		case __NEWLIB_NR_kill:
		case __NEWLIB_NR_times:
		default:
			printf("unimplemented  newlib syscall - number %d. Sending to trap handler.\n", syscall_num);
			triggerTrap(current, ISRC2, R_SRC1);
			break;
		}
	}
	else
	{
		switch (ISRC2)
		{
			case __NR_cacheflush:
				flush_icache();
				flush_dcache();
				break;
			case __NR_exit:
				simulator_exit(arg[0]);
				break;
			case __NR_Linux_syscalls + 0:
				snprintf(buf, sizeof(buf), "checkpoint-%llu", current->cr_cyc);
				dump_checkpoint(buf, current);
				break;
			case __NR_Linux_syscalls + 1:
				new->r[3] = set_trace_mode(current->r[4]);
				break;
			case __NR_Linux_syscalls + 2:
				new->r[3] = set_trace_context(current->r[4]);
				break;
			default:
				triggerTrap(current, ISRC2, R_SRC1);
				break;
		}
	}

}

INS_PROTO(xor)
{
	R_DEST = R_SRC1 ^ R_SRC2;
}

INS_PROTO(xor_immediate)
{
	R_IDEST = R_SRC1 ^ ISRC2;
}

INS_PROTO(zxt)
{
  int op2 = ZeroExtend8(R_SRC2);

  if (op2 > 31)
  {
    R_DEST = R_SRC1;
  }
  else
  {
    int mask = (1 << op2) - 1;
    R_DEST = R_SRC1 & mask;
  }
    
}

INS_PROTO(zxt_immediate)
{
  int op2 = ZeroExtend8(ISRC2);

  if (op2 > 31)
  {
    R_IDEST = R_SRC1;
  }
  else
  {
    int mask = (1 << op2) - 1;
    R_IDEST = R_SRC1 & mask;
  }
    
}

INS_PROTO(zxtb)
{
	R_IDEST = ZeroExtend(R_SRC1, 8);
}


INS_PROTO(zxth)
{
	R_IDEST = ZeroExtend(R_SRC1, 16);
}


/* This is the magic instruction that is inserted when the instruction
 * is decoded, it is of zero size. The instruction that is actually at 
 * the magic PC value must not have 4 syls in the bundle.
 * Note that it is never printed out in the 
 * the trace. Doing it this way avoid having to check for the PC in the 
 * main loop, and if it is cached then there will be no overhead.
 */

INS_PROTO(sim_syscall)
{
	//H moved into instr_trap
}
