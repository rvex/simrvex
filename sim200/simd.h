#ifndef __SIMD_H_
#define __SIMD_H_

/* Declarations of SIMD instructions */

#if 0

INS_PROTO(perm_pb);
INS_PROTO(perm_pb_immediate);
INS_PROTO(abss_ph);
INS_PROTO(absubu_pb);
INS_PROTO(add_ph);
INS_PROTO(adds_ph);
INS_PROTO(avg4u_pb);
INS_PROTO(avgu_pb);
INS_PROTO(cmpeq_pb_bdest);
INS_PROTO(cmpeq_pb);
INS_PROTO(cmpeq_ph_bdest);
INS_PROTO(cmpeq_ph);
INS_PROTO(cmpgt_ph_bdest);
INS_PROTO(cmpgt_ph);
INS_PROTO(cmpgtu_pb_bdest);
INS_PROTO(cmpgtu_pb);
INS_PROTO(ext1_pb);
INS_PROTO(ext2_pb);
INS_PROTO(ext3_pb);
INS_PROTO(extl_pb);
INS_PROTO(extr_pb);
INS_PROTO(max_ph);
INS_PROTO(min_ph);
INS_PROTO(mul_ph);
INS_PROTO(muladd_ph);
INS_PROTO(muladdus_pb);
INS_PROTO(mulfracadds_ph);
INS_PROTO(mulfracrm_ph);
INS_PROTO(mulfracrne_ph);
INS_PROTO(pack_pb);
INS_PROTO(packrnp_phh);
INS_PROTO(packs_ph);
INS_PROTO(packsu_pb);
INS_PROTO(sadu_pb);
INS_PROTO(shl_ph_immediate);
INS_PROTO(shl_ph);
INS_PROTO(shls_ph_immediate);
INS_PROTO(shls_ph);
INS_PROTO(shr_ph_immediate);
INS_PROTO(shr_ph);
INS_PROTO(shrrne_ph_immediate);
INS_PROTO(shrrne_ph);
INS_PROTO(shrrnp_ph_immediate);
INS_PROTO(shrrnp_ph);
INS_PROTO(shuff_pbh);
INS_PROTO(shuff_pbl);
INS_PROTO(shuff_phh);
INS_PROTO(shuff_phl);
INS_PROTO(shuffeve_pb);
INS_PROTO(shuffodd_pb);
INS_PROTO(slct_pb_immediate);
INS_PROTO(slct_pb);
INS_PROTO(slctf_pb_immediate);
INS_PROTO(sub_ph);
INS_PROTO(subs_ph);

#endif //0

#endif /* __SIMD_H__ */

