#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <termios.h>
#include <errno.h>
#include <libgen.h>
#include <wordexp.h>

#include "state.h"

#define EXTERN
#include "load.h"
#undef EXTERN
#include "memory.h"

#include "ctrlregdef.h"

#include "irq.h"
#include "instr.h"

#include "memory.h"
#include "tlb.h"
#include "icache.h"
#include "pluginp.h"
#include "decode.h"
#include "trace.h"
#include "dsu.h"
#include "conditional_logic.h"

/*
Joost TODO

Stuff to implement:
*option to load more files
-rvd connection?
*Add more system calls that can be handled by the host
*implement a timer (like GRLIB)
-multi-core
-local memories
-Maybe a DMA unit
-Make all of this stuff configurable (maybe also with an option to read in VHDL options)
-instruction buffer, long immediate prefetching, etc.
-Redo the bus & cache system, decouple contexts <-> cache blocks
-Check for trap arguments
-fix mfl instruction trace print
*/

#define TIMER_INT 	8

#define TIMER_EN 	(1<<0)
#define TIMER_RS 	(1<<1)
#define TIMER_LOAD 	(1<<2)
#define TIMER_IE 	(1<<3)

struct timeval tv;


/*
 * Global control registers (should be a runtime option)
 */
glob_state_t global_state;

//There are 2 states per context: current and new.
cx_state_t context_state[NCONTEXTS][2];

//peripherals will assert a bit in this "vector"
int interrupt_line;
static pthread_mutex_t interrupt_mutex;

//The contexts will assert their flag (1<<contextid) when they have written to the configuration request register
int reconfigurationRequested;

//Reconfiguration and cache miss penalties (cycles)
int reconf_penalty = 8; //9
int drmiss_penalty = 12; //not completely sure, lets say 8
int dwmiss_penalty = 8; //2
int imiss_penalty = 16; //14

int init_conf = 0;
int nLaneGroups = 4;

int banked_regfile = 1;

// start addresses for each context provided on command line.
u32 ctxt_start[NCONTEXTS] = {0, 0, 0, 0};

struct program {
	int num_ctxts;
	int ctxts[NCONTEXTS];
	wordexp_t w;
};

int programs_size = 0;
int programs_count = 0;
struct program *programs = NULL;

#if 0
int  cpu_asleep = 0;
#endif

u32 last_phys;

int enable_dcache = 0;
int unified_cache = 0; //1 = unified, non-blocking, 2 = 2 blocks with both 2 ports

//Flags if we're running in interactive mode, where ctrl-c invokes a small menu and the process will not exit when the core halts
int interactive = 1;

//If enabled, syscalls (traps with argument 0x90) will be passed to the host OS by the simulator.
int syscall_emulation = 0;
u32 sim_brk = 0;

//name and address to load an auxiliary file into memory
char auxfile_name[64];
u32 auxfile_address;


//dump mem global variables
long int dump_start;
long int dump_end;
int dump_enable = 0;

typedef struct {
	//Number of items in data
	int count;
	//Allocated size of data (in items)
	int size;
	//Pointer to array of pointers to items.
	char **data;
} extra_elf_t;

extra_elf_t extra_elf_files = {0, 0, NULL};

static char *load_file_name;

long run_cycles = 0;

static void (*sim_loop) (void);

//int tlb_index;			/*which tlb we are talking to */


#if 0
static int cpu_is_asleep = 0;

static sem_t cpu_idle_sem;
#endif

/*
 * Returns 1 if the context is not halted and currently assigned to one or more LaneGroups
 * (in other words, it is currently executing), 0 otherwise.
 */
int contextActive(cx_state_t *context)
{
	int i, found, cid;
	u32 current_config = global_state.cc;
	cid = context->sccr>>24;

	//Check if context is not halted for some reason
	if (context->dcr
			& ((1<<CR_DCR_D_BIT) | (1<<CR_DCR_J_BIT) | (1<<CR_DCR_S_BIT) | (1<<CR_DCR_B_BIT)))
		return 0;

	found = -1;
	for (i = 0; i < NCONTEXTS; i++)
	{
		if ((current_config & 0xf) == cid)
		{
			found = cid;
			break;
		}
		current_config >>= 4;
	}
	if (found == -1)
		return 0;
	else return 1;
}

/*
 * Returns 1 if the context is assigned to one or more lanegroups in the new configuration
 * and was not assigned to any lanes in the old configuration, 0 otherwise.
 * In other words, it flags whether the context has just been started.
 */
int contextActivated(int contextID, int oldConfig, int newConfig)
{
	int i, foundInOld, foundInNew;
	foundInOld = 0;
	foundInNew = 0;
	for (i = 0; i < NCONTEXTS; i++)
	{
		if (((oldConfig >> (i*4)) & 0xf) == contextID)
			foundInOld = 1;
		if (((newConfig >> (i*4)) & 0xf) == contextID)
			foundInNew = 1;
	}
	return (foundInOld == 0 && foundInNew == 1);
}

/*
 * Returns 1 if the context is not assigned to any lanes in the new configuration
 * and was assigned to one or more lanegroups in the old configuration, 0 otherwise.
 * In other words, it flags whether the context has just been stopped.
 */
int contextDeactivated(int contextID, int oldConfig, int newConfig)
{
	int i, foundInOld, foundInNew;
	foundInOld = 0;
	foundInNew = 0;
	for (i = 0; i < NCONTEXTS; i++)
	{
		if (((oldConfig >> (i*4)) & 0xf) == contextID)
			foundInOld = 1;
		if (((newConfig >> (i*4)) & 0xf) == contextID)
			foundInNew = 1;
	}
	return (foundInOld == 1 && foundInNew == 0);
}



/*
 * Returns the issue width of the given context.
 * This function assumes the config is valid.
 */
int issueWidth(cx_state_t *context)
{
	int i;
	int iw = 0;
	int cid = CID(context);
	u32 current_config = global_state.cc;
	for (i = 0; i < NCONTEXTS; i++)
	{
		if ((current_config & 0xf) == cid)
		{
			iw += 2;
		}
		current_config >>= 4;
	}
	return iw;
}

/*
 * Returns 1 if the given config (value that is written to the configuration request register)
 * represents a valid configuration, 0 otherwise.
 */
int configIsValid(int config)
{
	/*
	 * The characteristics of a valid configuration are:
	 * - Each lanegroup must be assigned to an existing context
	 * - Lanegroups that are assigned to the same context must be adjacent
	 * - The number of lanegroups that can be assigned to a context is 2, 4, 8 or 16
	 * (depending on the issue width of the core)
	 * - The lanegroup assignments must be aligned to the number of assigned lanes
	 * (e.g. a 4-issue core must be assigned to lanes 0-4 or 4-8).
	 *
	 * After creating a bitmap of the selected lanes per context, the valid values are (assuming an 8-issue core):
	 * 0           -- disabled
	 * 1, 2, 4, 8, -- 2-issue core
	 * 3, 12       -- 4-issue core
	 * 15          -- 8-issue core
	 */

	int i, laneSel;
	int contextMap[NCONTEXTS];
	for (i = 0; i < 4; i++)
		contextMap[i] = 0;

	for (i = 0; i < nLaneGroups; i++)
	{
		laneSel = ((config >> (i*4)) & 0xf);
		//does the context exist?
		if (laneSel >= NCONTEXTS && laneSel != 8)
			return 0;
		contextMap[laneSel] |= (1<<i);
	}

	for (i = 0; i < NCONTEXTS; i++)
	{
		switch (contextMap[i])
		{
		case 0:
		case 1:
		case 2:
		case 4:
		case 8:
		case 3:
		case 12:
		case 15:
			continue;
		default:
			return 0;
		}
	}
	return 1;
}
/*
 * Converts a value that encodes a configuration into flags;
 * each nibble (4 bits) represents a context id.
 * For every id in the value, it enables a bit in the result to flag that this context is enabled
 */
int convertToFlags(int val)
{
	int i, id;
	int res = 0;
	for (i = 0; i < NCONTEXTS; i++)
	{
		id = (val&0xf);
		if (id || (i == 0)) //Don't enable context 0 if it is not the first one we find
			res |= (1 << id);
		val >>= 4;
	}
	return res;
}

int getCoupledLaneGroups(cx_state_t *context)
{
	int i;
	int cid = CID(context);
	int current_config = global_state.cc;
	int mask = 0;

	for (i = 0; i < NCONTEXTS; i++)
	{
		if ((current_config & 0xf) == cid)
		{
			mask |= (1<<i);
		}
		current_config >>= 4;
	}
	return mask;
}

void set_interrupt(int line)
{
	pthread_mutex_lock(&interrupt_mutex);
	interrupt_line |= (1<<line);
	pthread_mutex_unlock(&interrupt_mutex);
}

void clear_interrupt(int line)
{
	//not supported
}

#if 0 //H not supported
void sleep_cpu(void)
{
	cpu_is_asleep = 1;
	sem_wait(&cpu_idle_sem);
	cpu_is_asleep = 0;
}

void wake_cpu(void)
{
	if(cpu_is_asleep) {
		sem_post(&cpu_idle_sem);
	}
}
#endif

void update_timer()
{
	int i;
	global_state.timer_scaler--;
	if (global_state.timer_scaler == 0)
	{
		global_state.timer_scaler = global_state.timer_scaler_reload;
		for (i = 0; i < NTIMERS; i++)
		{
			if (global_state.timer_tim_config[i] & TIMER_EN)
			{
				global_state.timer_tim_ctr[i]--;
			}
		}
	}
	for (i = 0; i < NTIMERS; i++)
	{
		if (global_state.timer_tim_config[i] & TIMER_LOAD)
		{
			global_state.timer_tim_config[i] &= ~TIMER_LOAD;
			global_state.timer_tim_ctr[i] = global_state.timer_tim_reload[i];
		}
		if (global_state.timer_tim_ctr[i] == 0)
		{
			if (global_state.timer_tim_config[i] & TIMER_RS)
			{
				global_state.timer_tim_ctr[i] = global_state.timer_tim_reload[i];
			}
			if (global_state.timer_tim_config[i] & TIMER_IE)
			{
				set_interrupt(TIMER_INT+i);
			}
		}
	}
}

static void init_state(int context_index, cx_state_t *s)
{
	int i;

	for (i = 0; i < 64; i++) {
		s->r[i] = 0;
	}
	for (i = 0; i < 8; i++) {
		s->br[i] = 0;
	}

	s->pc = ctxt_start[context_index];
	printf("context %d starting at PC = 0x%08x\n", context_index, s->pc);
	s->lr = 0;

	//H TODO: initialize all control registers (need to generate reset vectors)
	s->ccr = 0x1aa;
	s->sccr = (context_index<<24 | 0x2aa);
	s->dcr = (1<<CR_DCR_I_BIT);

	s->trapcause = 0;
	s->traparg = 0;

	s->busWaitCycles = 0;
	s->branch_delay = 0;

	s->int_force = 0;
	s->int_mask = 0;

	clearCounters(s);
}


void prioritizeAndTriggerInterrupt(cx_state_t *context)
{
	int i, int_id;
	int_id = 16;
	for (i = 15; i >= 0; i--) //lower ints have higher prio
	{
		if (((interrupt_line | context->int_force) & context->int_mask) & (1<<i))
			int_id = i;
	}
	if (int_id != 16)
	{
//		printf("context %d; triggering interrupt %d (interrupt_line 0x%x, int_force 0x%x, int_mask 0x%x).\n", CID(context), int_id, interrupt_line, context->int_force, context->int_mask);
		context->trapcause = TRAP_EXT_INTERRUPT;
		context->traparg = int_id;
		interrupt_line &= ~(1<<int_id);
		context->int_force &= ~(1<<int_id); //we're not sure if this is what actually happens in the GRLIB IRQMP
	}
//	else
//	{
//		printf("Interrupt was triggered or forced but is masked (interrupt_line = 0x%x, int_force = 0x%x, int_mask = 0x%x)\n", interrupt_line, context->int_force, context->int_mask);
//	}
}

/*
 * Trigger a trap if there isn't already a higher priority trap pending
 */
void triggerTrap(cx_state_t *current, int cause, int argument)
{
//	printf("context %d; triggering trap (cause %d, argument 0x%x, point 0x%x.\n", CID(current), cause, argument, current->pc);
	if (current->trapcause != 0)
		printf("Warning; multiple traps were triggered (current trapcause %d, new %d) in a cycle (priorities not modeled accurately)\n", current->trapcause, cause);
	if ((current->trapcause == 0) || current->trapcause > cause)
	{
		current->trapcause = cause;
		current->traparg = argument;
	}
}

/*
 * If there has been a trap, the bundle will not be committed, so this function will be called before state is updated.
 * Otherwise if there is an interrupt, the bundle will be committed and state will be updated first.
 * So that's why it only operates on current state.
 */
void trap(cx_state_t *current)
{
//	printf("Context %d handling trap cause %d, arg 0x%x, point 0x%08x\n", CID(current), current->trapcause, current->traparg, current->pc);

	current->sccr = (current->sccr & CR_SCCR_ID_MASK) | (current->ccr & ~(CR_SCCR_ID_MASK));
	//Set cause
	current->ccr = (current->ccr & ~CR_CCR_CAUSE_MASK) | (current->trapcause << CR_CCR_CAUSE_BIT);
	current->ta = current->traparg;
	current->tp = current->pc;

	//This how the hardware actually behaves: if ints are enabled but traps are not, it goes into panic (which seems weird to me)
	if (current->ccr & CR_CCR_RFT)
		current->pc = current->th;
	else
		current->pc = current->ph;

	//If there are no panic/trap handlers configured, pc is now 0. In that case, stop the core
	if (current->pc == 0)
	{
		current->dcr |= (1<<CR_DCR_B_BIT); //assert break bit
		printf("Context %d caught Trap without handlers configured. Halting context.\n", CID(current));
	}

	//Disable ints and traps, go into kernel mode
	current->ccr = (current->ccr & ~(CR_CCR_IEN | CR_CCR_RFT | CR_CCR_KME_C))
			| CR_CCR_KME | CR_CCR_IEN_C | CR_CCR_RFT_C;


#ifdef NOHANDLERS
	//If we have caught a trap and do not use the handlers, jut crash
	if(current->trapcause != TRAP_EXT_INTERRUPT)
	{
//		dump_utlb();
		printf("Illegal instruction at 0x%08x\n",except_addr);
		simulator_exit(1);
	}
#endif

	current->trapcause = 0;
	current->traparg = 0;
}

void updateGlobalState()
{
	int i, oldConfig, newConfig, valid, selectedContext;
	newConfig = -1;
	selectedContext = -1;

	//cycle counter
	global_state.cnt++;

	update_timer();

	//Check for reconfiguration requests. Context 0 has highest priority.
	oldConfig = global_state.cc;
	for (i = NCONTEXTS-1; i >= 0; i--)
	{
		if (reconfigurationRequested & (1<<i)) //this context has requested a new config
		{
//			printf("updateGlobalState: context %d requested config 0x%08x\n", i, context_state[i][0].crr);
			newConfig = context_state[i][0].crr;
			selectedContext = i;
		}
	}

	if (newConfig == -1)
		return;

	valid = configIsValid(newConfig);
	if (valid && (global_state.cc != newConfig))
	{
		global_state.cc = newConfig;
		global_state.gsr = (global_state.gsr & ~CR_GSR_E_MASK); //clear reconfiguration error bit
//		invalidate_icache(); //Not needed anymore since we have a separate buffer for each possible issue width
		for (i = 0; i < NCONTEXTS; i++)
		{
			context_state[i][0].busWaitCycles += reconf_penalty;
			if (contextActivated(i, oldConfig, newConfig))
			{
				printf("Context %d: resuming execution at PC 0x%08x\n", i, context_state[i][0].pc);
			}
			if (contextDeactivated(i, oldConfig, newConfig))
			{
				printf("Context %d: suspending execution at PC 0x%08x\n", i, context_state[i][0].pc);
			}
		}
	}
	if (!valid)
	{
		global_state.gsr = (global_state.gsr & ~CR_GSR_E_MASK) | (1 << CR_GSR_E_BIT); //set reconfiguration error bit
	}

	//set requester ID regardless of whether it was valid
	global_state.gsr = (global_state.gsr & ~CR_GSR_RID_MASK) | (selectedContext << CR_GSR_RID_BIT);

	//reset requests (contexts that lost arbitration must check for this and perform another request themselves)
	reconfigurationRequested = 0;

//	printf("updateGlobalState: processed %s request by context %d\n", valid ? "valid" : "invalid", selectedContext);
	global_state.nReconfigurations++;
}

void update_state(cx_state_t *current, cx_state_t *new,
		  struct decoded_bundle *bundle)
{
	int i;
	int reg;

	/* Update registers */
	for (i = 0; i < bundle->num_r_updates; i++) {
		reg = bundle->r_index[i];
		current->r[reg] = new->r[reg];
	}
	current->r[0] = 0;	/* Instructions don't special case r0 so we do */

	/* Update branch bits */
	for (i = 0; i < bundle->num_b_updates; i++) {
		reg = bundle->b_index[i];
		current->br[reg] = new->br[reg];
	}
	if (bundle->update_lr)
		current->lr = new->lr;
	if (bundle->stop_bit)
		current->cr_stop++;
	/* Change to new pc */
	current->pc = new->pc;

	current->cr_bun++;
	current->cr_syl += bundle->num - bundle->numNOPs;

}

void store_child_args_main(int argc, char **argv, const char *filename)
{
	unsigned int argc_addr, argv_addr, argv_end, argv_size;
	unsigned int offset;
	int i, j;
	int size;
	// find the addresses of argc and argv and the size of argv
	if (!find_sym_addr_in_file(filename, "__argc", &argc_addr) ||
		!find_sym_addr_in_file(filename, "__argv", &argv_addr) ||
		!find_sym_addr_in_file(filename, "__argv_end", &argv_end))
	{
		printf("Unable to locate __argc, __argv, or __argv_end symbol, cannot initialize argc and argv for simulated program.\n");
		return;
	}

	// check that the size of argv does not exceed the allocated space
	argv_size = argv_end - argv_addr;
	size = 4*(argc+1);
	for (i = 0; i < argc; ++i)
		size += strlen(argv[i]) + 1;
	if (size > argv_size) {
		printf("Child args exceed MAX_ARG (%d).\n", argv_size);
		return;
	}
	// write argc to address in child memory
	write_memory_32(argc_addr, argc);
	// skip argc + 2 address size spaces (1 for 0-terminating the argv pointer array, 1 for envp)
	offset = 4*(argc+2);
	for (i = 0; i < argc; i++) {
		// write the address of the start of each string into the list of string pointers
		write_memory_32(argv_addr + i*4, argv_addr + offset);
		// copy each string to into memory at the correct offset
		for (j = 0; argv[i][j]; j++, offset++)
			write_memory_8(argv_addr+offset, argv[i][j]);
		// null terminate the string
		write_memory_8(argv_addr+offset, argv[i][j]);
		offset++;
	}
	// null terminate the array of pointers
	write_memory_32(argv_addr+i*4, 0);
	//envp
	write_memory_32(argv_addr+i*4+4, 0);
}

void store_child_args(const struct program *p)
{
	int argc = p->w.we_wordc;
	char **argv = p->w.we_wordv;
	const char *filename = p->w.we_wordv[0];
	store_child_args_main(argc, argv, filename);
}

static void init_machine_state()
{
	int i;
	cx_state_t *current;

	for (i = 0; i < NCONTEXTS; i++)
	{
		current = context_state[i];

		init_state(i, current);

		/* This is where we start executing from */
		//current->pc = initial_pc;
	}

	//H TODO: initialize global registers using generated reset vectors?
	global_state.gsr  = 0;
	global_state.cc   = init_conf;
	global_state.aff  = 0;
	global_state.cnt  = 0;
	global_state.dcfg = 0x1337;

	//H initialize timer
	global_state.timer_scaler = ~0;
	global_state.timer_scaler_reload = ~0;
	global_state.timer_config = NTIMERS | (1<<8); //last 3 bits is nr of timers, bit 8 signals separate interrupts (not implemented)
	for (i = 0; i < NTIMERS; i++)
	{
		global_state.timer_tim_ctr[i] = ~0;
		global_state.timer_tim_reload[i] = ~0;
		global_state.timer_tim_config[i] = 0;
	}

	//H TODO: initialize LIMC, SYLC etc. (they are never written to)
#ifdef ALLREGS
	global_state.cver1 = ('3' << CR_CVER1_VER_BIT |'s' << CR_CVER1_CTAG0_BIT | 'i' << CR_CVER1_CTAG1_BIT |
			'm' << CR_CVER1_CTAG2_BIT);
	global_state.cver1 = ('r' << CR_CVER0_CTAG3_BIT |'v' << CR_CVER0_CTAG4_BIT | 'e' << CR_CVER0_CTAG5_BIT |
				'x' << CR_CVER0_CTAG6_BIT);
#endif
}

/* Default loop */
#define SIM_LOOP_NAME rvex_sim_loop
#define FAST_PRGINS
#include "simloop.c"
#undef SIM_LOOP_NAME

//H Let's not do this for now (We'll keep it a compile-time option)
/* Loop with ibreak checking */
//#define SIM_LOOP_NAME rvex_sim_loop_check_ibreak
//#define CHECK_IBREAK
//#include "simloop.c"
//#undef CHECK_IBREAK


static void usage(void)
{
	printf("Usage: %s [options] [elf file] [--args [ARG]...]\n", program_name);
	printf("Options:\n");
	printf("-h, --help\n"
			"\t\tPrint this help message\n");
	printf("--args [ARG]...\n"
				"\t\tAll arguments after this flag are passed to the program.\n");
	printf("-t, --trace <mode>,<context>\n"
			"\t\tEnable tracing. Can select one or more contexts to trace\n"
			"\t\t(default: all. Place context 0 last)\n"
			"\t\tmode: 1 - core, 2 - cache, 3 - all\n");
	printf("-c, --circ <Mb>\n"
			"\t\tTrace goes to circular buffer. 32Mb default\n");
	printf("--lanegroups <n>\n"
				"\t\tSets the number of LaneGroups of the processor.\n"
				"\t\tThis can have influence on the initial configuration.\n"
				"\t\tIf you want to change the default configuration with\n"
				"\t\ta non-standard number of LaneGroups, please set the \n"
				"\t\tnumber of lanegroups first.\n");
	printf("-i, --init <config>\n"
			"\t\tInitial core configuration in hex\n");
	printf("-d, --dcache\n"
			"\t\tEnable dcache\n");
	printf("-b, --batch\n"
				"\t\tEnable Batch (non-interactive) execution.\n"
				"\t\tBy default, the simulator can be interrupted interactively\n"
				"\t\tusing ctrl-c. This way, the user can dump register state \n"
				"\t\tand change trace mode at any time. Using batch mode, simrvex\n"
				"\t\tdoes not catch ctrl-c so it can be killed and will behave \n"
				"\t\tproperly when called from a script.\n");
	printf("-e, --emulate-syscalls\n"
				"\t\tEnable Syscall emulation.\n"
				"\t\tWill pass system calls to the host machine.\n"
				"\t\tAny file paths will be accessed relative from the working \n"
				"\t\tdirectory.\n"
				"\t\tKeep in mind that this mode may overwrite files.\n");
	printf("-p, --penalty <Reconfiguration>,<Data Read>,<Data Write>,<Instruction>\n"
			"\t\tReconfiguration and Cache miss penalties in cycles\n");
	printf("--ram <address>,<file>\n"
			"\t\tLoad ram with file contents\n");
	printf("--ramelf <file>\n"
			"\t\tLoad additional elf files into memory\n");
	printf("--dump <start_address>,<end_address>\n"
			"\t\tDump memory region after execution finishes into a file with\n"
			"\t\tthe prefix memdump_\n");
	printf("--trace-start-cycle <start_cycle> \n"
			"\t\tStart tracing from given cycle.\n");
	printf("--trace-start-addr <start_addr> \n"
			"\t\tStart tracing from given Program Counter address.\n");
	printf("--context-exec \"<contexts> <command>\"\n"
			"\t\tSpecify the command for contexts to run.\n"
			"\t\tcontexts should be a comma seperated list of context IDs.\n"
			"\t\tCommand should be the path to the binary, followed by any\n"
			"\t\targuments to the program.\n");
	printf("--run-cycles <cycles>\n"
			"\t\tStop the simulation after the specified number of cycles.\n"
			"\t\tThe default value (0) means no limit.\n");
	printf("--banks <banks>\n"
				"\t\tDivide the register file in to a specified number of banks.\n"
				"\t\tThe simulator gives warnings for bank conflicts.\n");
	exit(0);

}

#define DEFAULT_CIRCULAR_BUFFER_SIZE (32)

#define BREAK_OPTION (0xff+1)
#define ROM_OPTION (0xff+2)

enum option_ids {
	OPT_DUMP = 512,
	OPT_RAMELF,
	OPT_TRACE_START_CYCLE,
	OPT_TRACE_START_ADDR,
	OPT_CTXT_EXEC,
	OPT_RUN_CYCLES,
	OPT_LANEGROUPS,
	OPT_BANKS,
};

static struct option long_opts[] = {
	{"trace", optional_argument, NULL, 't'},
	{"circ", optional_argument, NULL, 'c'},
	{"init", required_argument, NULL, 'i'},
	{"batch", no_argument, NULL, 'b'},
	{"help", no_argument, NULL, 'h'},
	{"dcache", no_argument, NULL, 'd'},
	{"penalty", required_argument, NULL, 'p'},
	{"ram", required_argument, NULL, 'r'},
	{"ramelf", required_argument, NULL, OPT_RAMELF},
	{"dump", required_argument, NULL, OPT_DUMP},
	{"trace-start-cycle", required_argument, NULL, OPT_TRACE_START_CYCLE},
	{"trace-start-addr", required_argument, NULL, OPT_TRACE_START_ADDR},
	{"context-exec", required_argument, NULL, OPT_CTXT_EXEC},
	{"run-cycles", required_argument, NULL, OPT_RUN_CYCLES},
	{"emulate-syscalls", no_argument, NULL, 'e'},
	{"lanegroups", required_argument, NULL, OPT_LANEGROUPS},
	{"banks", required_argument, NULL, OPT_BANKS},
	{0, 0, 0, 0}
};

/**
 * Parse str into a long and store at valid
 * Store the pointer to the end of the parses value at endp.
 * return 1 for success and 0 for error.
 */
static int read_long(char *str, char **endp, long *val)
{
	*val = strtol(str, endp, 0);
	if (errno == EINVAL || errno == ERANGE) {
		perror(str);
		return 0;
	} else if (str == *endp) {
		return 0;
	}
	return 1;
}

/**
 * Parse a string specifying a number of contexts and a program to run in those
 * contexts. Here is an example of such a string:
 * "0,1,2 program arg1 arg2 arg3"
 */
int parse_program_option(char *string, int max, struct program *p)
{
	char *startp = string;
	char *endp = NULL;
	int error = 0;
	long val = 0;
	int num_vals = 0;

	while (num_vals < NCONTEXTS) {
		if (!read_long(startp, &endp, &val) || val >= NCONTEXTS)
		{
			error = 1;
			break;
		}
		p->ctxts[num_vals] = val;
		num_vals++;
		if (*endp == ',') {
			startp = endp + 1;
		} else {
			break;
		}
	}
	p->num_ctxts = num_vals;
	if (*endp != '\0') {
		if (wordexp(endp+1, &p->w, 0)) {
			error = 1;
		}
	} else {
		error = 1;
	}
	if (error)
		printf("Error parsing context-exec option.");
	return error;
}

static int parse_cmd_line(int argc, char *argv[])
{
	int c;
	int circ_buffer_size = DEFAULT_CIRCULAR_BUFFER_SIZE;
	int error = 0;
	int i,j;
	int new_argc = argc;

	trace_context = ALLCONTEXTS;

	program_name = strrchr(argv[0], '/');
	if (program_name == NULL)
		program_name = argv[0];
	else
		program_name++;

	//locate the name of the binary, all options after that should be passed to simulation
	for (i = 1; i < argc; i++)
	{
		if (strncmp(argv[i], "--args", 6) == 0)
		{
			new_argc = i;

			//remove the --args flag from the list by shifting all arguments after it one position to the left
			for (j = i; j < argc-1; j++)
			{
				argv[j] = argv[j+1];
			}
			argv[argc-1] = NULL;
			argc--;
		}
	}

	sim_loop = rvex_sim_loop;

	while ((c = getopt_long(new_argc, argv, "r:p:du:t::bhc::i:e", long_opts,
			    NULL)) != EOF) {
		switch (c) {
		case 't':
			if (optarg) {
				sscanf(optarg, "%d,%x", &trace_mode, &trace_context);
				trace_context = convertToFlags(trace_context);
			}
			else
			{
				trace_mode = TRACE_CORE; //trace core, not cache
				trace_context = 1; //only trace context 0
			}
			break;
		case 'r':
			sscanf(optarg, "0x%x,%63s", &auxfile_address, auxfile_name);
			load_binfile(auxfile_address, auxfile_name);
			break;
		case 'b':
			interactive = 0;
			break;
		case 'e':
			printf("Enabling syscall emulation mode\n");
			syscall_emulation = 1;
			break;
		case 'c':
			if (optarg) {
				circ_buffer_size = strtoul(optarg, 0, 0);
			}
			circular_trace = 1;
//			trace_mode = TRACE_ON;
			break;
		case 'i':
			sscanf(optarg, "0x%x", &init_conf);
			if (!configIsValid(init_conf))
			{
				printf("Invalid initial core configuration (0x%x) given. Falling back to default.\n", init_conf);
				init_conf = 0;
			}
			printf("Initial core configuration: %x.\n", init_conf);
			break;
		case OPT_LANEGROUPS:
			sscanf(optarg, "%d", &nLaneGroups);
			if ((nLaneGroups != 1) && (nLaneGroups != 2) && (nLaneGroups != 4))
			{
				printf("The number of lanegroups must be 1, 2, or 4. Falling back to default");
				nLaneGroups = 4;
			}
			if (nLaneGroups == 1)
			{
				init_conf = 0x8880;
			}
			else if (nLaneGroups == 2)
			{
				init_conf = 0x8800;
			}
			else
			{
				init_conf = 0;
			}
			printf("Number of LaneGroups: %d. Initial core configuration: %x.\n", nLaneGroups, init_conf);
			break;
		case OPT_BANKS:
			sscanf(optarg, "%d", &banked_regfile);
			printf("Register file is divided into %d banks\n", banked_regfile);
			break;
		case 'd':
			enable_dcache = 1;
			break;
		case 'u':
			sscanf(optarg, "%d", &unified_cache);
			printf("Unified cache configuration: %d\n", unified_cache);
			break;
		case 'p':
			sscanf(optarg, "%d,%d,%d,%d", &reconf_penalty, &drmiss_penalty, &dwmiss_penalty, &imiss_penalty);
			break;

		case 'h':
			usage();
			break;
		case OPT_DUMP:
		{
			char *startp = NULL;
			char *endp = NULL;
			startp = optarg;
			if (!read_long(startp, &endp, &dump_start))
			{
				error = 1;
				break;
			}
			startp = endp;
			endp = NULL;
			if (*startp == ',') {
				startp++;
			} else {
				printf("Error parsing dump command.\n");
				error = 1;
				break;
			}
			if (!read_long(startp, &endp, &dump_end))
			{
				error = 1;
				break;
			}
			dump_enable = 1;
			break;
		}
		case OPT_RAMELF:
		{
			if (extra_elf_files.count >= extra_elf_files.size) {
				int newsize = extra_elf_files.size*2 + 1;
				char **temp = realloc(extra_elf_files.data, 
						      newsize*sizeof(char *));
				if (!temp) {
					printf("Error allocating memory\n");
					error = 1;
					break;
				}
				extra_elf_files.data = temp;
				extra_elf_files.size = newsize;
			}
			extra_elf_files.data[extra_elf_files.count] = strdup(
				optarg);
			extra_elf_files.count++;
			break;
		}
		case OPT_TRACE_START_CYCLE:
		{
			sscanf(optarg, "%d", &trace_start_cycle);
			break;
		}
		case OPT_TRACE_START_ADDR:
		{
			sscanf(optarg, "0x%x", &trace_start_addr);
			break;
		}
		case OPT_CTXT_EXEC:
		{
			if (programs_count > 1 && syscall_emulation)
			{
				printf("Multiple programs in combination with syscall emulation is not supported.\n");
				error = 1;
			}
			if (programs_count >= programs_size) {
				int newsize = programs_size*2 + 1;
				struct program *temp = realloc(
					programs,
					newsize*sizeof(struct program));
				if (!temp) {
					printf("Error allocating memory\n");
					error = 1;
					break;
				}
				programs = temp;
				programs_size = newsize;
			}
			if (parse_program_option(optarg,
						 NCONTEXTS,
						 &programs[programs_count]))
				error = 1;
			else
				programs_count++;
			break;
		}
		case OPT_RUN_CYCLES:
		{
			char *endp = NULL;
			if(!read_long(optarg, &endp, &run_cycles))
				error = 1;
			break;
		}
		default:
			printf("unknown option!\n");
			error = 1;
			break;
		}
	}
	if (error) {
		printf("Error parsing command line options, exiting.\n");
		exit(EXIT_FAILURE);
	}

	if (argv[optind])
		load_file_name = argv[optind];
	else
		load_file_name = NULL;
	
	if (load_file_name == NULL && programs_count == 0)
	{
		usage();
		exit(-1);
	}
	allocate_trace_buffer(circ_buffer_size);
	return argc; //argc may have been changed because of a --arg parameter
}

/**
 * Print the memory range provided in command line arguments to a file.
 */
void dump_mem(void)
{
	char *filename = NULL;
	FILE *f = NULL;
	int i;
	if (!dump_enable)
		return;
	if (asprintf(&filename, "memdump_%lx_%lx.dump", dump_start, dump_end) == -1) {
		return;
	}
	f = fopen(filename, "w");
	if (!f) {
		return;
	}
	for (i = dump_start; i < dump_end; ++i) {
		fputc(access_memory_8(i), f);
	}
	fclose(f);
}

void print_global_state()
{
	printf("\nGlobal state:\n\n");
	printf("\tGSR: 0x%08x\n", global_state.gsr);
	printf("\t CC: 0x%08x\n", global_state.cc);
	printf("\tCNT: %llu\n", global_state.cnt);
	printf("\n");
}

void print_register_state(int context)
{
	int i,j;

	printf("\nContext %d - %d Lanes assigned, %s\n\n", context, issueWidth(&context_state[context][0]), contextActive(&context_state[context][0]) ? "Running" : "Stopped");

	printf("\tPC: 0x%08x\n", context_state[context][0].pc);
	printf("\tLR: 0x%08x\n\n", context_state[context][0].lr);

	printf("General Purpose regs:\n");

	for (j = 0; j < 16; j++)
	{
		for (i = 0; i < 4; i++)
		{
			printf("$r%02d = 0x%08x  ", (j*4)+i, context_state[context][0].r[(j*4)+i]);
		}
		printf("\n");
	}

	printf("\nBranch regs:\n");
	for (j = 0; j < 2; j++)
	{
		for (i = 0; i < 4; i++)
		{
			//printf("$b%d = %s  ", (j*4)+i, machine_state[context][0].br[(j*4)+i] ? "true" : "false");
			printf("$b%d = %d  ", (j*4)+i, context_state[context][0].br[(j*4)+i]);
		}
		printf("\n");
	}

	printf("\n CCR: 0x%08x\n", context_state[context][0].ccr);
	printf("SCCR: 0x%08x\n", context_state[context][0].sccr);
	printf("  TH: 0x%08x\n", context_state[context][0].th);
	printf("  PH: 0x%08x\n", context_state[context][0].ph);
	printf("  TP: 0x%08x\n", context_state[context][0].tp);
	printf("  TA: 0x%08x\n", context_state[context][0].ta);
	for (i = 0; i < NBREAKPOINTS; i++)
	{
		printf(" BR%d: 0x%08x\n", i, context_state[context][0].brk[i]);
	}
	printf(" RET: %d\n", (char)((context_state[context][0].dcr2>>24)&0xff));
	printf("\n\n");
}

void print_counters(int context)
{
//	printf("Active cycles: %u\n", context_state[context][0].cr_cyc);
//	printf("Stalled cycles: %u\n", context_state[context][0].cr_stall);
//	printf("Committed bundles: %u\n", context_state[context][0].cr_bun);
//	printf("Committed syllables: %u\n", context_state[context][0].cr_syl);
//	printf("Committed NOPs: %u\n", context_state[context][0].cr_nop);
//
//	printf("Instruction cache accesses: %u\n", context_state[context][0].cr_iacc);
//	printf("Instruction cache misses: %u\n", context_state[context][0].cr_imiss);
//	printf("Data cache read accesses: %u\n", context_state[context][0].cr_dracc);
//	printf("Data cache read misses: %u\n", context_state[context][0].cr_drmiss);
//	printf("Data cache write accesses: %u\n", context_state[context][0].cr_dwacc);
//	printf("Data cache write misses: %u\n", context_state[context][0].cr_dwmiss);
//	printf("Data cache bypass accesses: %u\n", context_state[context][0].cr_dbypass);
//	printf("Data waits on write buf: %u\n", context_state[context][0].cr_dwbuf);
	printf(",Active cycles,%llu\n", context_state[context][0].cr_cyc);
	printf(",Stalled cycles,%llu\n", context_state[context][0].cr_stall);
	printf(",Committed bundles,%llu\n", context_state[context][0].cr_bun);
	printf(",Committed syllables,%llu\n", context_state[context][0].cr_syl);
	printf(",Committed NOPs,%llu\n", context_state[context][0].cr_nop);
	printf(",Committed STOPs,%llu\n", context_state[context][0].cr_stop);

	printf(",Instruction cache accesses,%llu\n", context_state[context][0].cr_iacc);
	printf(",Instruction cache misses,%llu\n", context_state[context][0].cr_imiss);
	printf(",Data cache read accesses,%llu\n", context_state[context][0].cr_dracc);
	printf(",Data cache read misses,%llu\n", context_state[context][0].cr_drmiss);
	printf(",Data cache write accesses,%llu\n", context_state[context][0].cr_dwacc);
	printf(",Data cache write misses,%llu\n", context_state[context][0].cr_dwmiss);
	printf(",Data cache bypass accesses,%llu\n", context_state[context][0].cr_dbypass);
	printf(",Data waits on write buf,%llu\n", context_state[context][0].cr_dwbuf);

	printf(",Bank read1 conflicts,%llu\n", context_state[context][0].bank_r1);
	printf(",Bank read2 conflicts,%llu\n", context_state[context][0].bank_r2);
	printf(",Bank write conflicts,%llu\n", context_state[context][0].bank_wr);
}

void print_all_state(int contexts)
{
	unsigned int context;
	struct timeval end_tv;
	gettimeofday(&end_tv, NULL);
	unsigned int extime = (int)(end_tv.tv_sec - tv.tv_sec);
	unsigned int hours = extime/3600;
	unsigned int minutes = (extime - (hours*3600))/60;
	unsigned int seconds = extime - (hours*3600) - (minutes*60);

	print_global_state();

	for (context = 0; context < NCONTEXTS; context++)
	{
		if ((contexts & (1<<context)) && (context_state[context][0].cr_bun != 0)) //print only if selected and context has committed bundles
		{
			print_register_state(context);
			print_counters(context);
		}
	}

	printf(",Total number of processed configuration requests,%u\n", global_state.nReconfigurations);
	printf(",Processor cycle counter,%llu\n", global_state.cnt);
	printf(",Simulation time (simrvex execution time),%d,seconds,(%d hours, %d minutes, %d seconds)\n\n", extime, hours, minutes, seconds);
    dumpLinkReg();
	printf("\n");

}

void configure_terminal()
{
	struct termios settings;
	int result;

	result = tcgetattr(STDIN_FILENO, &settings);
	if (result < 0)
	{
		perror("error in tcgetattr");
		return;
	}
	settings.c_iflag &= ~ICANON; //H if you want to run linux, you'd also want to disable echo
	result = tcsetattr(STDIN_FILENO, TCSANOW, &settings);
	if (result < 0)
	{
		perror("error in tcgetattr");
		return;
	}
	return;
}

void reset_terminal()
{
	struct termios settings;
	int result;

	result = tcgetattr(STDIN_FILENO, &settings);
	if (result < 0)
	{
		perror("error in tcgetattr");
		return;
	}
	settings.c_iflag |= ICANON;
	result = tcsetattr(STDIN_FILENO, TCSANOW, &settings);
	if (result < 0)
	{
		perror("error in tcgetattr");
		return;
	}
	return;
}

void simulator_exit(int code)
{
	dump_trace_buffer();

	print_all_state(trace_context);
	plugin_close();
	reset_terminal();

	if (interactive)
	{
		printf("Simulation finished.\n");
	}
	exit(code);
}

int set_trace_mode(int new_mode)
{
	int old_mode = trace_mode;

	trace_mode = new_mode;

	return old_mode;
}

int set_trace_context(int new_context)
{
	int old_context = trace_context;

	trace_context = new_context;

	return old_context;
}

/**
 * Load extra elf files into memory.
 */
static void load_extra_files(void)
{
	int i;
	for (i = 0; i < extra_elf_files.count; i++) {
		load_elf_file(extra_elf_files.data[i]);
	}
}

/*
 * Interactive Interrupt; wait for a keypress and depending on the key either halt the simulator, print state and/or continue
 */
void int_int()
{
	fflush(0); //Flush file and output buffers so you can view the simulation output
	printf("simrvex interrupted\n");
	printf("Press q to exit or \"e\" to dump state and exit, \n"
					"\t\"s\" to dump context states, \"g\" to dump global state, \n"
					"\t\"t\" to modify trace mode and context (t\\nMODE,CONTEXT) or \"c\" to continue...\n");

	char c;
	while(c = getchar())
	{
		printf("Press q to exit or \"e\" to dump state and exit, \n"
						"\t\"s\" to dump context states, \"g\" to dump global state, \n"
						"\t\"t\" to modify trace mode and context (t\\nMODE,CONTEXT) or \"c\" to continue...\n");
		if (c == 'c')
			break;
		if (c == 'q')
		{
			printf("Quit...\n");
			exit(0);
		}
		if (c == 's')
		{
			printf("current trace_context: 0x%x\n", trace_context);
			print_all_state(trace_context != 0 ? trace_context : ALLCONTEXTS);
		}
		if (c == 'g')
		{
			print_global_state();
		}
		if (c == 't')
		{
			//H TODO: this doesn't work
			fscanf(stdin, "%d,%x", &trace_mode, &trace_context);
			trace_context = convertToFlags(trace_context);
			printf("New trace_context: 0x%x, new trace_mode: 0x%x\n", trace_context, trace_mode);
		}
		if (c == 'e')
			simulator_exit(0);
	}
	printf("simrvex continuing...\n");
}

static struct sigaction old_sigaction;
static void install_signal_handler(void);

static void signal_handler(int sig)
{
	sigset_t set;

	/* Now re-enable SIGINT. This allows you to CTRL-C
	 * when you forget to redirect the trace output to 
	 * a file
	 */
	sigaction(SIGINT, &old_sigaction, NULL);
	sigemptyset(&set);
	sigaddset(&set, SIGINT);
	sigprocmask(SIG_UNBLOCK, &set, NULL);

	if (interactive)
	{
		int_int();
	}
	else
	{
		printf("Received interrupt signal; exiting...\n");
		simulator_exit(1);
	}

	install_signal_handler();
	return;
}

static void install_signal_handler(void)
{
	struct sigaction action;

	action.sa_handler = signal_handler;
	action.sa_flags = 0;
	sigemptyset(&action.sa_mask);
	sigaction(SIGINT, &action, &old_sigaction);
}

int main(int argc, char *argv[])
{
	u32 entry;
	int i;

	gettimeofday(&tv, NULL);

	pthread_mutex_init(&interrupt_mutex, NULL);
//	sem_init(&cpu_idle_sem, 0, 0);

    // Initialize the random number generator
    srand((unsigned)tv.tv_sec);

	sim_init_mem();

	argc = parse_cmd_line(argc, argv);

	load_extra_files();

	entry = load_elf_file(load_file_name);	/* Entry is what the elf files says is the start addr */

	if (load_file_name != NULL)
		store_child_args_main(argc - optind, argv + optind, load_file_name);

	for (i = 0; i < NCONTEXTS; i++) {
		ctxt_start[i] = entry;
	}
	for (i = 0; i < programs_count; i++) {
		int j;
		entry = load_elf_file(programs[i].w.we_wordv[0]);
		for (j = 0; j < programs[i].num_ctxts; j++) {
			ctxt_start[programs[i].ctxts[j]] = entry;
		}
		store_child_args(&programs[i]);
	}

	install_signal_handler();

	configure_terminal();
	plugin_open(0);

	/* Just zeros all the registers and sets 
	 * pc to sane value
	 */
	init_machine_state();

	decode_init(enable_dcache);

	sim_loop();

	dump_mem();
	simulator_exit(0);

	return 0;
}
