#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "simtypes.h"

struct writebuf_entry{
//	u32 address; //H we can't keep track of this, because a new write can occur while we are waiting. Also, we don't need to because both our cache and memory are updated immediately at the time of access
	u32 waitCycles;
	u32 requestWaiting;
};

extern int enable_dcache;
extern int unified_cache;
extern int cache_broadcasting;
extern int cache_broadcast_penalty;

extern int drmiss_penalty;
extern int dwmiss_penalty;
extern int imiss_penalty;

void clearCounters(cx_state_t *current);

u32 access_8(cx_state_t *current, u32 phys);
u32 access_16(cx_state_t *current, u32 phys);
u32 access_32(cx_state_t *current, u32 phys);

u32 access_memory_8(u32 phys);
u32 access_memory_16(u32 phys);
u32 access_memory_32(u32 phys);

u32 fetch(cx_state_t *current, u32 phys, int first_syl);

void write_8(cx_state_t *current, u32 phys, u8 value);
void write_16(cx_state_t *current, u32 phys, u16 value);
void write_32(cx_state_t *current, u32 phys, u32 value, int debug_bus);

void write_memory_8(u32 phys, u8 value);
void write_memory_16(u32 phys, u16 value);
void write_memory_32(u32 phys, u32 value);

u32 dcache_access_8(cx_state_t *current, u32 phys);
u32 dcache_access_16(cx_state_t *current, u32 phys);
u32 dcache_access_32(cx_state_t *current, u32 phys);

void dcache_write_8(cx_state_t *current, u32 phys, u8 value);
void dcache_write_16(cx_state_t *current, u32 phys, u16 value);
void dcache_write_32(cx_state_t *current, u32 phys, u32 value);

void sim_load_write(const unsigned char *data, int len, unsigned long phys);

unsigned char *get_memory_string(u32 phys);

void *sim_mmap(void *addr, u32 length, int prot, int flags,
                  int fd, s32 offset);

void sim_init_mem(void);

//void purge_set(u32 phys);
//void purge_addr(u32 phys);
//void flush_addr(u32 phys);
//void invalidate_addr(u32 phys);

void flush_dcache();
void flush_icache();


/* These represent the control registers */
#define CTRL_REG_START CREG_BASE
#define CTRL_REG_SIZE CREG_SIZE //H TODO: get this from generated rvex.h (needs to be added there)

#define MEMORY_START 0x00000000
#define MEMORY_SIZE (512*1024*1024)

#endif
