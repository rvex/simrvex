#ifndef __SIMTYPES_H__
#define __SIMTYPES_H__

typedef unsigned long long u64;
typedef long long s64;

typedef unsigned u32;
typedef int s32;

typedef unsigned short u16;
typedef short s16;

typedef unsigned char u8;
typedef signed char s8;

#endif
