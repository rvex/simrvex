#ifdef DISABLE_PLUGINS
#define plugin_open(x) do { } while(0)
#define plugin_close() do { } while(0)
#define plugin_read(contextID, dst, address, numBytes) 0
#define plugin_write(contextID, src, address, numBytes) 0
#else
/* if linux_mode is set, then command line and plugin info is poked 
 * in at 0x08002000. This will bugger up any programs loaded with 
 * the HP toolset with strange results.
 */
void plugin_open(int linux_mode);
void plugin_close(void);
int plugin_read(int contextID, void *dst, unsigned int address, unsigned int numBytes);
int plugin_write(int contextID, const void *src, unsigned int address, unsigned int numBytes);
#endif
