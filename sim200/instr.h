
#define INS_PROTO(name) void __attribute__((regparm(3))) \
                             name(cx_state_t *current,cx_state_t *new,\
                                  struct decoded_ins_params *p)

#define INS_PROTO_SPECIAL(name,param) void __attribute__((regparm(3))) \
                                           name##_##param(cx_state_t *current,cx_state_t *new,\
                                                          struct decoded_ins_params *p)

#define INS_PROTO_DCACHE(name)  INS_PROTO_SPECIAL(name,dcache)

/* Prints out the trace data for the bundle just executed */
void print_trace_info(cx_state_t *current, cx_state_t *new,
		      struct decoded_bundle *bundle);

/* Used to indicate this decoding is illegal */
INS_PROTO(illegal);
/* Placeholder for sub decode */
INS_PROTO(monadic);

/* simulator backdoor, effectively a "magic" pc value */
INS_PROTO(sim_syscall);

INS_PROTO(instr_trap);
INS_PROTO(limm);
INS_PROTO(sbit);
INS_PROTO(sbitf);
INS_PROTO(tbit_immediate);
INS_PROTO(tbitf_immediate);
INS_PROTO(tbit_ibdest);
INS_PROTO(tbitf_ibdest);
INS_PROTO(nop);
INS_PROTO(mfl);
INS_PROTO(mtl);
INS_PROTO(mtl_immediate);
INS_PROTO(ldl);
INS_PROTO(stl);
INS_PROTO(stop);
INS_PROTO(zxtb);


INS_PROTO(add);
INS_PROTO(sub);
INS_PROTO(shl);
INS_PROTO(shr);
INS_PROTO(shru);
INS_PROTO(sh1add);
INS_PROTO(sh2add);
INS_PROTO(sh3add);
INS_PROTO(sh4add);
INS_PROTO(and);
INS_PROTO(andc);
INS_PROTO(or);
INS_PROTO(orc);
INS_PROTO(xor);
INS_PROTO(mpylhus);
INS_PROTO(max);
INS_PROTO(maxu);
INS_PROTO(min);
INS_PROTO(minu);
INS_PROTO(mpy32);
INS_PROTO(mpy64h);
INS_PROTO(mpy64hu);
INS_PROTO(mpyfrac);
INS_PROTO(mpyhhs);
INS_PROTO(mpyl);
INS_PROTO(mpylu);
INS_PROTO(mpyh);
INS_PROTO(mpyhu);
INS_PROTO(mpyll);
INS_PROTO(mpyllu);
INS_PROTO(mpylh);
INS_PROTO(mpylhu);
INS_PROTO(mpyhh);
INS_PROTO(mpyhhu);
INS_PROTO(mpyhs);
INS_PROTO(cmpeq);
INS_PROTO(cmpne);
INS_PROTO(cmpge);
INS_PROTO(cmpgeu);
INS_PROTO(cmpgt);
INS_PROTO(cmpgtu);
INS_PROTO(cmple);
INS_PROTO(cmpleu);
INS_PROTO(cmplt);
INS_PROTO(cmpltu);
INS_PROTO(andl);
INS_PROTO(nandl);
INS_PROTO(orl);
INS_PROTO(norl);
INS_PROTO(illegal);
INS_PROTO(cmpeq_bdest);
INS_PROTO(cmpne_bdest);
INS_PROTO(cmpge_bdest);
INS_PROTO(cmpgeu_bdest);
INS_PROTO(cmpgt_bdest);
INS_PROTO(cmpgtu_bdest);
INS_PROTO(cmple_bdest);
INS_PROTO(cmpleu_bdest);
INS_PROTO(cmplt_bdest);
INS_PROTO(cmpltu_bdest);
INS_PROTO(andl_bdest);
INS_PROTO(nandl_bdest);
INS_PROTO(orl_bdest);
INS_PROTO(norl_bdest);
INS_PROTO(add_immediate);
INS_PROTO(sub_immediate);
INS_PROTO(shl_immediate);
INS_PROTO(shr_immediate);
INS_PROTO(shru_immediate);
INS_PROTO(sh1add_immediate);
INS_PROTO(sh2add_immediate);
INS_PROTO(sh3add_immediate);
INS_PROTO(sh4add_immediate);
INS_PROTO(and_immediate);
INS_PROTO(andc_immediate);
INS_PROTO(or_immediate);
INS_PROTO(orc_immediate);
INS_PROTO(xor_immediate);
INS_PROTO(mpy32_immediate);
INS_PROTO(mpy64h_immediate);
INS_PROTO(mpy64hu_immediate);
INS_PROTO(mpyfrac_immediate);
INS_PROTO(mpylhus_immediate);
INS_PROTO(max_immediate);
INS_PROTO(maxu_immediate);
INS_PROTO(min_immediate);
INS_PROTO(minu_immediate);
INS_PROTO(mpyhhs_immediate);
INS_PROTO(mpyl_immediate);
INS_PROTO(mpylu_immediate);
INS_PROTO(mpyh_immediate);
INS_PROTO(mpyhu_immediate);
INS_PROTO(mpyll_immediate);
INS_PROTO(mpyllu_immediate);
INS_PROTO(mpylh_immediate);
INS_PROTO(mpylhu_immediate);
INS_PROTO(mpyhh_immediate);
INS_PROTO(mpyhhu_immediate);
INS_PROTO(mpyhs_immediate);
INS_PROTO(cmpeq_immediate);
INS_PROTO(cmpne_immediate);
INS_PROTO(cmpge_immediate);
INS_PROTO(cmpgeu_immediate);
INS_PROTO(cmpgt_immediate);
INS_PROTO(cmpgtu_immediate);
INS_PROTO(cmple_immediate);
INS_PROTO(cmpleu_immediate);
INS_PROTO(cmplt_immediate);
INS_PROTO(cmpltu_immediate);
INS_PROTO(andl_immediate);
INS_PROTO(nandl_immediate);
INS_PROTO(orl_immediate);
INS_PROTO(norl_immediate);
INS_PROTO(cmpeq_ibdest);
INS_PROTO(cmpne_ibdest);
INS_PROTO(cmpge_ibdest);
INS_PROTO(cmpgeu_ibdest);
INS_PROTO(cmpgt_ibdest);
INS_PROTO(cmpgtu_ibdest);
INS_PROTO(cmple_ibdest);
INS_PROTO(cmpleu_ibdest);
INS_PROTO(cmplt_ibdest);
INS_PROTO(cmpltu_ibdest);
INS_PROTO(andl_ibdest);
INS_PROTO(nandl_ibdest);
INS_PROTO(orl_ibdest);
INS_PROTO(norl_ibdest);
INS_PROTO(sxtb);
INS_PROTO(sxth);
//INS_PROTO(bswap);
INS_PROTO(zxth);
INS_PROTO(clz);
INS_PROTO(slct);
INS_PROTO(slctf);
INS_PROTO(slct_immediate);
INS_PROTO(slctf_immediate);
INS_PROTO(addcg);
INS_PROTO(divs);
//INS_PROTO(prgins);
//INS_PROTO(prginspg);
//INS_PROTO(instr_sbrk);
//INS_PROTO(instr_syscall);
//INS_PROTO(ibreak);

INS_PROTO(ldw);
INS_PROTO(ldh);
INS_PROTO(ldhu);
INS_PROTO(ldb);
INS_PROTO(ldbr);
INS_PROTO(ldbu);
INS_PROTO(stw);
INS_PROTO(sth);
INS_PROTO(stb);
INS_PROTO(ldl);
INS_PROTO(stl);
INS_PROTO(ldbr);
INS_PROTO(stbr);
INS_PROTO(xchg);
//INS_PROTO(pft);
//INS_PROTO(prgadd);
//INS_PROTO(prgset);

INS_PROTO_DCACHE(ldw);
INS_PROTO_DCACHE(ldh);
INS_PROTO_DCACHE(ldhu);
INS_PROTO_DCACHE(ldb);
INS_PROTO_DCACHE(ldbu);
INS_PROTO_DCACHE(stw);
INS_PROTO_DCACHE(sth);
INS_PROTO_DCACHE(stb);
INS_PROTO_DCACHE(ldl);
INS_PROTO_DCACHE(stl);
INS_PROTO_DCACHE(ldbr);
INS_PROTO_DCACHE(stbr);
//INS_PROTO_DCACHE(prgadd);
//INS_PROTO_DCACHE(prgset);

//INS_PROTO(pswset);
//INS_PROTO(pswclr);
INS_PROTO(instr_sync);
INS_PROTO(call);
INS_PROTO(call_lr);
INS_PROTO(go_to);
INS_PROTO(go_to_lr);
INS_PROTO(rfi);

INS_PROTO(br);
INS_PROTO(brf);

//H is a st240 insn, but we need it for VEX
INS_PROTO(return_lr);

//floating point instructions
INS_PROTO(addf);
INS_PROTO(subf);
INS_PROTO(mpyf);
INS_PROTO(convif);
INS_PROTO(convfi);
INS_PROTO(cmpgef);
INS_PROTO(cmpeqf);
INS_PROTO(cmpgtf);
INS_PROTO(cmpgef_bdest);
INS_PROTO(cmpeqf_bdest);
INS_PROTO(cmpgtf_bdest);

// Load-linked store-conditional instructions
INS_PROTO(ldwl);
INS_PROTO(stwl);

/* New instructions added to st240 */
/*
INS_PROTO(adds);
INS_PROTO(subs);
INS_PROTO(sh1adds);
INS_PROTO(sh1subs);
INS_PROTO(sats);
INS_PROTO(adds_ph);
INS_PROTO(subs_ph);
INS_PROTO(addso);
INS_PROTO(rem);
INS_PROTO(subso);
INS_PROTO(divu);
INS_PROTO(sh1addso);
INS_PROTO(remu);
INS_PROTO(sh1subso);
INS_PROTO(satso);
INS_PROTO(cmpeq_pb);
INS_PROTO(cmpgtu_pb);
INS_PROTO(cmpeq_ph);
INS_PROTO(cmpgt_ph);
INS_PROTO(mov_bdest);
INS_PROTO(cmpeq_pb_bdest);
INS_PROTO(cmpgtu_pb_bdest);
INS_PROTO(cmpeq_ph_bdest);
INS_PROTO(cmpgt_ph_bdest);
INS_PROTO(addpc_immediate);
INS_PROTO(clz_immediate);
INS_PROTO(shls);
INS_PROTO(shlso);
INS_PROTO(sxt);
INS_PROTO(zxt);
INS_PROTO(rotl);
INS_PROTO(rotl_immediate);
INS_PROTO(perm_pb);
INS_PROTO(shuff_pbh);
INS_PROTO(sadu_pb);
INS_PROTO(shuff_pbl);
INS_PROTO(absubu_pb);
INS_PROTO(shuffodd_pb);
INS_PROTO(mpyaddus_pb);
INS_PROTO(shuffeve_pb);
INS_PROTO(pack_pb);
INS_PROTO(ext1_pb);
INS_PROTO(ext2_pb);
INS_PROTO(ext3_pb);
INS_PROTO(packsu_pb);
INS_PROTO(shl_ph);
INS_PROTO(add_ph);
INS_PROTO(shuff_phh);
INS_PROTO(shls_ph);
INS_PROTO(abss_ph);
INS_PROTO(shuff_phl);
INS_PROTO(shr_ph);
INS_PROTO(max_ph);
INS_PROTO(sub_ph);
INS_PROTO(shrrnp_ph);
INS_PROTO(min_ph);
INS_PROTO(mpyfracadds_ph);
INS_PROTO(shrrne_ph);
INS_PROTO(mpy_ph);
INS_PROTO(mpyadd_ph);
INS_PROTO(mpyfracrm_ph);
INS_PROTO(packrnp_phh);
INS_PROTO(mpyfracrne_ph);
INS_PROTO(packs_ph);
INS_PROTO(ldlc);
INS_PROTO(syncins);
INS_PROTO(ldl);
INS_PROTO(pswmask);
INS_PROTO(instr_div);
INS_PROTO(mov_slct);
INS_PROTO(shls_immediate);
INS_PROTO(shlso_immediate);
INS_PROTO(shrrnp_immediate);
INS_PROTO(extract_immediate);
INS_PROTO(extractl_immediate);
INS_PROTO(extractu_immediate);
INS_PROTO(extractlu_immediate);
INS_PROTO(sxt_immediate);
INS_PROTO(zxt_immediate);
INS_PROTO(ldwc);
INS_PROTO(ldhc);
INS_PROTO(ldhuc);
INS_PROTO(ldbc);
INS_PROTO(ldbuc);
INS_PROTO(stlc);
INS_PROTO(stl);
INS_PROTO(stwc);
INS_PROTO(sthc);
INS_PROTO(stbc);
INS_PROTO(pftc);
INS_PROTO(prginsadd);
INS_PROTO(prginsset);
INS_PROTO(flushadd);
INS_PROTO(invadd);

INS_PROTO(syncins);
INS_PROTO(wmb);
INS_PROTO(waitl);
INS_PROTO(ldwl);
INS_PROTO(stwl);
INS_PROTO(dbgsbrk);
INS_PROTO(retention);

INS_PROTO(prgadd_l1);
INS_PROTO(flushadd_l1);
INS_PROTO(invadd_l1);
INS_PROTO(return_lr);

INS_PROTO(andl_br3r);
INS_PROTO(mov_br3r);
INS_PROTO(nandl_br3r);
INS_PROTO(orl_br3r);
INS_PROTO(norl_br3r);
*/
