#ifndef __INSTR_FIELDS_H__
#define __INSTR_FIELDS_H__

#include "simtypes.h"

/* These macros are used during executing the decoded instructions. 
 * note that they are different from the ones used to decode, even
 * though some of them have the same name 
 */

#define SRC1(p) ((p)->op1)
#define SRC2(p) ((p)->op2)
#define DEST(p) ((p)->op3)
#define IDEST(p) ((p)->op3)

#define BSRC1(p) (((p)->op1)&7)
#define BSRC2(p) (((p)->op2)&7)

#define R_SRC1 (current->r[SRC1(p)])
#define R_SRC2 (current->r[SRC2(p)])
#define R_SRC3 (current->r[DEST(p)])
#define R_SRC2PLUS1 (current->r[SRC2(p)+1])
#define R_SRC2_IS_R0 (SRC2(p)==0)

#define ISRC2  ((p)->op2)

#define L_SRC (current->lr)

/* As above, but with implicit cast 
 * I think the default should be the other
 * way round, since it is more common to 
 * operands as signed rather than signed
 */
#define R_SRC1_SIGNED ((s32)R_SRC1)
#define R_SRC2_SIGNED ((s32)R_SRC2)
#define ISRC2_SIGNED ((s32)ISRC2)

/* Ditto for float */
#define R_SRC1_FLOAT (current->r_float[SRC1(p)])
#define R_SRC2_FLOAT (current->r_float[SRC2(p)])

#define L_DEST (new->lr)

#define R_DEST (new->r[DEST(p)])
#define R_IDEST (new->r[IDEST(p)])
// the next register, suitable for use in ldl
//#define R_IDESTPLUS1 (new->r[IDEST(p)+1])
#define R_LR (current->lr)
/* Same but for floating point */
#define R_DEST_FLOAT (new->r_float[DEST(p)])
#define R_DEST_SIGNED (new->r_signed[DEST(p)])

//TODO: fix
#define BDEST(p) ((p)->op3)
#define BDEST2(p) ((p)->op3)
#define IBDEST(p) ((p)->op3)

#define GET_BIT(number,bit) ( ((number) & (1<<(bit)))>>(bit))

#define SCOND(p) ((p)->cgs.scond)
#define PCOND(p) ((p)->cgs.scond)
#define CGS_BDEST(p) ((p)->cgs.bdest)

#define BTARG(p) ((p)->branch.btarg)
#define BCOND(p) ((p)->branch.bcond)

#define B_SCOND (current->br[SCOND(p)])
#define B_PCOND (current->br[PCOND(p)])
#define B_BCOND ( current->br[BCOND(p)])


#define B_SRC1 (current->br[BSRC1(p)])
#define B_SRC2 (current->br[BSRC2(p)])

#define B_BDEST (new->br[BDEST(p)])
#define B_BDEST2 (new->br[BDEST2(p)])
#define B_IBDEST (new->br[IBDEST(p)])

#define CGS_B_BDEST (new->br[CGS_BDEST(p)])

#define PC (new->pc)
#define NEXT_PC (new->pc)
#define BUNDLE_PC (current->pc)

#endif
