#include "icache.h"
#include "memory.h"

#ifndef SIM_LOOP_NAME
#error SIM_LOOP_NAME not defined
#endif

#ifndef MCAT
#define MCAT_SUB(x,y) x##y
#define MCAT(x,y) MCAT_SUB(x,y)
#endif

struct decoded_bundle *rvex_slow_get_next_decoded_bundle(cx_state_t *current,
						    struct icache_entry
						    **icache_slot);
extern void fill_icache_line(cx_state_t *current, u32 phys);
extern int lookup_icache(cx_state_t *current, u32 phys, int first_syl);

extern cx_state_t context_state[NCONTEXTS][2];
extern struct writebuf_entry writebuf[NCONTEXTS];

static inline struct decoded_bundle * rvex_get_next_decoded_bundle(cx_state_t *current)
{

//  struct tlb_entry *t;
  unsigned pc=current->pc;
  unsigned icache_index; /* Must be unsigned, for test to work */
  int iw_idx;
  int iw = issueWidth(current);
  if (iw == 2)
	  iw_idx = 0;
  else if (iw == 4)
	  iw_idx = 1;
  else
	  iw_idx = 2;


  current->cr_iacc++;

//  t=iside_lookup_tlb(pc);
//  if(exception) return NULL;
//  phys_pc=TRANSLATE(t,pc);

  icache_index=(pc-MEMORY_START)>>3; //H was phys_pc

  /* We are no longer in main memory! */
  if(icache_index>=MEMORY_SIZE/8) {
    return rvex_slow_get_next_decoded_bundle(current,NULL);
  }

  /* Is this bundle already present ?? */
  /* Note we have to check that the prgins number matches as well */
  if(icache[iw_idx][icache_index]
#ifdef FAST_PRGINS
		  && icache[iw_idx][icache_index]->invalidate_cycle==invalidate_cycle_count
#endif
    ) {
	  //H just because this bundle is in our simulator's icache doesn't mean we don't have to check the simulated icache for a hit/miss
	  if (enable_dcache && !lookup_icache(current, pc, 1))
		{
			fill_icache_line(current, pc);
		}
	  return &(icache[iw_idx][icache_index]->bundle);
  }
  return rvex_slow_get_next_decoded_bundle(current, icache[iw_idx]+icache_index);

}

//#ifndef CHECK_IBREAK
static void rvex_sim_loop(void)
//#else
//static void rvex_sim_loop_check_ibreak(void)
//#endif
{
	int context;
	int i;
	int busGrant = 0;
	// is there any context still active to be running?
	int running = 1;
	struct decoded_bundle *bundle;
	struct decoded_ins *ins;
	cx_state_t *new;
	cx_state_t *current;

	while ((!run_cycles || global_state.cnt < run_cycles) &&  running){
	running = 0;

	if (enable_dcache)
	{
		if (context_state[busGrant][0].busWaitCycles == 0)
		{
			for (i = 0; (i < NCONTEXTS) && (context_state[busGrant][0].busWaitCycles == 0); i++)
			{
				busGrant = (busGrant+1) & (NCONTEXTS-1); //Round Robin
			}
		}
	} //enable_dcache

	for (context = 0; context < NCONTEXTS; context++)
	{
		int active;

		current = &context_state[context][0];
		new = &context_state[context][1];
		active = contextActive(current);
		if (active)
		{
			running = 1;
			current->cr_cyc++;
			/* Handle branch delay slot.
			 * If we executed a branch instruction in the previous cycle,
			 * do nothing on this cycle.
			 */
			if (current->branch_delay)
			{
				current->branch_delay = 0;
				continue;
			}
		}

		if (enable_dcache && current->busWaitCycles) //stall context if it is waiting for a bus access
		{
			current->cr_stall++;
			if (busGrant == context)
				current->busWaitCycles--; //the bus keeps running also if context is disabled
		}
		else if (active)
		{
			bundle=rvex_get_next_decoded_bundle(current);
#ifdef CHECK_IBREAK
			check_ibreak(current->pc);
#endif

			if (likely(bundle != NULL)) {
				/* Assume we do not jump, but just got to next */
				new->pc = current->pc + bundle->size;
				for (i = 0, ins = bundle->ins; i < bundle->num;
					 i++, ins++) {
					ins->op(current, new, &ins->params);
				}
				current->bank_r1 += bundle->bank_r1t;
				current->bank_r2 += bundle->bank_r2t;
				current->bank_wr += bundle->bank_wrt;
			}
			if (unlikely(trace_mode))
				print_trace_info(current, new, bundle);

			if (unlikely(current->trapcause)) {
				/* Multiple exceptions may be thrown, prioritize them */
				trap(current);
				print_trace_info(current, new, NULL);
				continue;
			} else {
				update_state(current, new, bundle);
			}


			/* Check for interrupt */
			if (unlikely((interrupt_line || current->int_force) && (current->ccr & CR_CCR_IEN))) {
				prioritizeAndTriggerInterrupt(current);

				/* So we see the interrupt exception in the trace */
				if (unlikely(trace_mode))
					print_trace_info(current, new, NULL);

				if (current->trapcause) //we need to check for this again here, because the interrupt may have been masked in prioritizeAndTriggerInterrupt()
					trap(current);
			}
		}
	}
	//update global state such as control registers (handle reconf. requests)
	updateGlobalState();
	}
}
