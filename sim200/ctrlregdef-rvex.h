/*
 * include/asm-st200/ctrlregdef-230.h
 *
 * Copyright (C) 2002 STMicroelectronics Limited
 *	Author: Stuart Menefy <stuart.menefy@st.com>
 *
 * Control register defintions for ST230
 */

#ifndef _ASM_ST200_CTRLREGDEFS_231_H
#define _ASM_ST200_CTRLREGDEFS_231_H



#if 0
/* Exception cause as an integer, indicating the cause of the last trap. */
#define EXCAUSENO	CTRL_ADDR(0xff88)

/************ PSW bit definitions ************/
#define PSW_USER_MODE			(1<< 0)	/* When 1 the core is in user mode, otherwise supervisor mode. */
#define PSW_INT_ENABLE			(1<< 1)	/* When 1 external interrupts are enabled. */
#define PSW_TLB_ENABLE			(1<< 2)	/* When 1 address translation is enabled. */
#define PSW_TLB_DYNAMIC			(1<< 3)	/* When 1 purges and speculative loads which miss the TLB cause TLB_NO_MAPPING exceptions. */
#define PSW_SPECLOAD_MALIGNTRAP_EN	(1<< 4)	/* When 1 enables exceptions on speculative load misalignment errors. */
#define PSW_DBREAK_ENABLE		(1<< 8)	/* When 1 data breakpoints are enabled. */
#define PSW_IBREAK_ENABLE		(1<< 9)	/* When 1 instruction breakpoints are enabled. */
#define PSW_DEBUG_MODE			(1<<12)	/* When 1 the core is in debug mode. */

/* These are the default things we always stick in the PSW, separated out for
 * user and kernel. Done like this for uclinux, which doesn't appreciate 
 * having the TLB turned on!
 */
#define PSW_KERNEL_NOSPEC	(PSW_TLB_ENABLE|PSW_SPECLOAD_MALIGNTRAP_EN)
#define PSW_KERNEL_DEFAULT	(PSW_TLB_ENABLE|PSW_SPECLOAD_MALIGNTRAP_EN|PSW_TLB_DYNAMIC)
#define PSW_USER_DEFAULT	(PSW_TLB_ENABLE|PSW_TLB_DYNAMIC|PSW_USER_MODE)


/* EXCAUSENO definitions */
#define EXCAUSENO_STBUS_IC_ERROR	 0	/* The Instruction Cache caused a bus error. */
#define EXCAUSENO_STBUS_DC_ERROR	 1	/* The Data Cache caused a bus error. */
#define EXCAUSENO_EXTERN_INT		 2	/* There was an external interrupt. */
#define EXCAUSENO_IBREAK		 3	/* An instruction address breakpoint has occured. */
#define EXCAUSENO_ITLB			 4	/* Instruction side TLB exception */
#define EXCAUSENO_SBREAK		 5	/* A software breakpoint was found. */
#define EXCAUSENO_ILL_INST		 6	/* The bundle could not be decoded into legal sequence of operations or a privileged operation is being issued in user mode. */
#define EXCAUSENO_SYSCALL		 7	/* A syscall instruction was found */
#define EXCAUSENO_DBREAK		 8	/* The DPU has triggered a breakpoint on a data address. */
#define EXCAUSENO_MISALIGNED_TRAP	 9	/* The address is misaligned and misaligned accesses are not supported. */
#define EXCAUSENO_CREG_NO_MAPPING	10	/* The load or store address was in control register space, but no control register exists at that exact address. */
#define EXCAUSENO_CREG_ACCESS_VIOLATION	11	/* A store to a control register was attempted whilst in user mode. */
#define EXCAUSENO_DTLB			12	/* Data side TLB exception */
#define EXCAUSENO_SDI_TIMEOUT		14	/* One of the SDI interfaces timed out while being accessed. */

/* EXCAUSE bit definitions */
#define EXCAUSE_STBUS_IC_ERROR		(1<<EXCAUSENO_STBUS_IC_ERROR)
#define EXCAUSE_STBUS_DC_ERROR		(1<<EXCAUSENO_STBUS_DC_ERROR)
#define EXCAUSE_EXTERN_INT		(1<<EXCAUSENO_EXTERN_INT)
#define EXCAUSE_IBREAK			(1<<EXCAUSENO_IBREAK)
#define EXCAUSE_ITLB			(1<<EXCAUSENO_ITLB)
#define EXCAUSE_SBREAK			(1<<EXCAUSENO_SBREAK)
#define EXCAUSE_ILL_INST		(1<<EXCAUSENO_ILL_INST)
#define EXCAUSE_SYSCALL			(1<<EXCAUSENO_SYSCALL)
#define EXCAUSE_DBREAK			(1<<EXCAUSENO_DBREAK)
#define EXCAUSE_MISALIGNED_TRAP		(1<<EXCAUSENO_MISALIGNED_TRAP)
#define EXCAUSE_CREG_NO_MAPPING		(1<<EXCAUSENO_CREG_NO_MAPPING)
#define EXCAUSE_CREG_ACCESS_VIOLATION	(1<<EXCAUSENO_CREG_ACCESS_VIOLATION)
#define EXCAUSE_DTLB			(1<<EXCAUSENO_DTLB)
#define EXCAUSE_SDI_TIMEOUT		(1<<EXCAUSENO_SDI_TIMEOUT)

#endif

#endif				/* _ASM_ST200_CTRLREGDEFS_231_H */
