#ifndef __SIMTIME_H__
#define __SIMTIME_H__

#include "simtypes.h"

/* Sets timer reset value */
void set_timer_const(int timer, u32 value);

/* Gets timer reset value */
u32 get_timer_const(int timer);

void set_timer_count(int timer, u32 value);

u32 get_timer_count(int timer);

/* Switches em on and off */
void enable_timer(int timer);
void disable_timer(int timer);

int is_timer_enabled(int timer);

void enable_interrupt(int timer);
void disable_interrupt(int timer);
int is_interrupt_enabled(int timer);

u32 read_timer(int timer);
void write_timer(int timer, u32 value);


int get_interrupt_status(int timer);

void clear_interrupt_status(int timer);

void sim_time_init(u32 clock_mhz);

#endif






