#ifndef __LOAD_H__
#define __LOAD_H__

void load_binfile(unsigned int address, char *filename);
unsigned long load_elf_file(const char *name);

int compare_syms(const void *a, const void *b);
int find_text_symbol_and_offset_from_addr(unsigned addr, char **name, int *offset);
int find_text_symbol_from_addr(unsigned addr, char **name);
int find_data_symbol_and_offset_from_addr(unsigned addr, char **name, int *offset);
int find_data_symbol_from_addr(unsigned addr, char **name);
int find_addr_from_data_symbol(const char *name, unsigned *addr);
int find_sym_addr_in_file(const char *filename, const char *symname,
			  unsigned *addr);

#ifndef EXTERN
#define EXTERN extern
#endif
EXTERN int verbose;
EXTERN char *program_name;

#endif
