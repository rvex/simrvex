#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include <sim200/simtypes.h>

struct memory_bank {
	u32 base, size;
	u32 connection; //mask where the context that can access this port are flagged. Bus is bit 31.
	int (*read_fn) (int contextID, void *dst, u32 addr, u32 bytes);
	int (*write_fn) (int contextID, const void *dst, u32 addr, u32 bytes);
};

struct plugin {
	int num_banks;
	struct memory_bank *banks;
	void (*close_fn) (void);

/* Used by plugin manager */
	void *handle;
	struct plugin *next;
};

#define PLUGIN_SYSTEM_INFO_V1 1

struct system_info {
	int version;		/* Version number of struct */
	u32 memory_base;	/* Base of main memory */
	u32 memory_size;	/* How much memory we have */
	u32 pad[32];		/* Room for future expansion */
};

struct plugin_callbacks {
	/* These can be called from any thread */
	void (*set_interrupt) (int irq);
	void (*clear_interrupt) (int irq);

	/* These can only be called from the 'simulator' thread */
	int (*read_memory) (void *dst, u32 addr, u32 size);
	int (*write_memory) (const void *src, u32 addr, u32 size);
	int (*set_trace_mode) (int mode);
};

struct memory_bank *pluginlib_find_mb(u32 addr);
/* Pass NULL for info if your simulator doesn't support this */
struct plugin *pluginlib_init(struct plugin_callbacks *plugin_callbacks,
			      struct system_info *info);
void pluginlib_setup_params(void);
void pluginlib_finish(void);

#endif
