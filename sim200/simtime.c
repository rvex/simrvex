#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/signal.h>
#include <semaphore.h>
#include <sys/errno.h>
#include <pthread.h>


#include "state.h"
#include "simtypes.h"
#include "simtime.h"


struct sim_timer {
	u32 timer_const;
	u32 timer_count;
	int enabled;
	int int_enabled;
	int status;
	timer_t timer_id; /* associated timer */
};

#define NUM_TIMERS 3

static struct sim_timer timer[NUM_TIMERS];

static u32 timer_clock_mhz;

void set_timer_const(int t, u32 value)
{
	if (t > NUM_TIMERS) return;

	timer[t].timer_const = value;

}

u32 get_timer_const(int t)
{
	return timer[t].timer_const;
}

static u32 timespec_to_clock(struct timespec *t)
{
	return (t->tv_sec * timer_clock_mhz) + (  ((long long )t->tv_nsec * (long long)((timer_clock_mhz/1000000L))) / 1000L );
}

void clock_to_timespec(unsigned clock, struct timespec *t)
{
        t->tv_sec = clock/timer_clock_mhz;
        t->tv_nsec = ((long long) ((clock - (t->tv_sec * timer_clock_mhz))) * 1000L) / (timer_clock_mhz/1000000L);
}


void enable_timer(int t)
{
	struct itimerspec ts;
	int ret;

	if (timer[t].enabled) return;

	clock_to_timespec(timer[t].timer_count, &ts.it_value);
	clock_to_timespec(timer[t].timer_const, &ts.it_interval);

	/* Set up the timer, note we take a callback even if interrupts are
	 * not enabled
	 */
	ret = timer_settime(timer[t].timer_id, 0, &ts, NULL);
	if (ret) {
		printf("timer settime failed %d\n",ret);
	}

	timer[t].enabled = 1;
}


u32 get_timer_count(int t)
{
	struct itimerspec ts;

	if(timer[t].enabled) {
		timer_gettime(timer[t].timer_id, &ts);
		timer[t].timer_count = timespec_to_clock(&ts.it_value);
	}

	return timer[t].timer_count;
}

void disable_timer(int t)
{
	struct itimerspec ts;

	if (!timer[t].enabled) return;

	get_timer_count(t);

	/* Disable the timer */
	memset(&ts, 0, sizeof(ts));
	timer_settime(timer[t].timer_id, 0, &ts, NULL);

	timer[t].enabled = 0;
}

void set_timer_count(int t, u32 value)
{
	int enabled = timer[t].enabled;

	if(enabled) disable_timer(t);

	timer[t].timer_count = value;

	if(enabled) enable_timer(t);
}

void enable_interrupt(int t)
{
	timer[t].int_enabled = 1;

	if(timer[t].status) set_interrupt(t);
}

void disable_interrupt(int t)
{
	timer[t].int_enabled = 0;

	clear_interrupt(t);
}

int is_interrupt_enabled(int t)
{
	return timer[t].int_enabled;
}

int is_timer_enabled(int t)
{
	return timer[t].enabled;
}

int get_interrupt_status(int t)
{
	return timer[t].status;
}

void clear_interrupt_status(int t)
{
	timer[t].status = 0;

	clear_interrupt(t);
}

static void timer_callback(union sigval sigev_value)
{
	struct sim_timer * t = (struct sim_timer *) sigev_value.sival_ptr;
	int timer_num = t - timer;

	t->status = 1 ;

	if(t->int_enabled)
		set_interrupt(timer_num);
}


void sim_time_init(u32 clock_mhz)
{
	int i;
	struct sigevent evp;
	int ret;

	timer_clock_mhz = clock_mhz;

	evp.sigev_notify = SIGEV_THREAD;
	evp.sigev_notify_function = timer_callback;
	evp.sigev_notify_attributes = NULL;

	for (i = 0; i < NUM_TIMERS; i++) {
		evp.sigev_value.sival_ptr = timer + i;
		ret = timer_create(CLOCK_MONOTONIC, &evp, &(timer[i].timer_id));
		if (ret) {
			printf("Timer create failed code ret %d\n",ret);
		}
	}
}
