#include "dsu.h"
#include "conditional_logic.h"

#define SAVE_LOAD_TRANSLATE(ea,read,diss)

#define LOAD_TRANSLATE(ea) SAVE_LOAD_TRANSLATE(ea,1,0)
#define SAVE_TRANSLATE(ea) SAVE_LOAD_TRANSLATE((ea),0,0)

#define VALIDATE_ALIGNED_MASK(ea,mask,diss) \
  if((ea)&(mask)) {\
    last_phys=~0; \
	triggerTrap(current, TRAP_MISALIGNED_ACCESS, ea);\
    return;\
  }

#define VALIDATE_ALIGNED_32(ea) VALIDATE_ALIGNED_MASK(ea,3,0)
#define VALIDATE_ALIGNED_16(ea) VALIDATE_ALIGNED_MASK(ea,1,0)
#define VALIDATE_ALIGNED_64(ea) VALIDATE_ALIGNED_MASK(ea,7,0)

#undef CACHED
#undef CACHED_INS_PROTO

#ifndef ENABLE_DCACHE
#define CACHED(ea) 0
#define CACHED_INS_PROTO(name) INS_PROTO(name)
#else
#define CACHED(ea) ((ea >= MEMORY_START) && (ea < MEMORY_START + MEMORY_SIZE))
#define CACHED_INS_PROTO(name) INS_PROTO_DCACHE(name)
#endif

#undef read_8
#undef read_16
#undef read_32
#undef store_8
#undef store_16
#undef store_32

#define read_8(ea) CACHED(ea) ? dcache_access_8(current, ea) : access_8(current, ea)
#define read_16(ea) CACHED(ea) ? dcache_access_16(current, ea) : access_16(current, ea)
#define read_32(ea) CACHED(ea) ? dcache_access_32(current, ea) : access_32(current, ea)

#define store_8(ea,value)   notifyWrite(ea); if(CACHED(ea)) dcache_write_8(current, ea,value); else write_8(current, ea,value)
#define store_16(ea,value)  notifyWrite(ea); if(CACHED(ea)) dcache_write_16(current, ea,value); else write_16(current, ea,value)
#define store_32(ea,value)  notifyWrite(ea); if(CACHED(ea)) dcache_write_32(current, ea,value); else write_32(current, ea,value, 0)

//#define CHECK_DBREAK(ea) if(check_dbreak(ea)) {triggerTrap(current, TRAP_DMEM_FAULT, ea) ;return;}
#define CHECK_DRBREAK(ea) check_drbreak(current, ea)
#define CHECK_DWBREAK(ea) check_dwbreak(current, ea)

CACHED_INS_PROTO(ldb)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DRBREAK(ea);

	R_IDEST = SignExtend(read_8(ea), 8);

}

//CACHED_INS_PROTO(ldbc)
//{
//  	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea=R_SRC1 + ISRC2;
//
//	CHECK_DRBREAK(ea);
//
//	R_IDEST=SignExtend(read_8(ea), 8);
//}

CACHED_INS_PROTO(ldbr)
{
	unsigned ea;
	unsigned char tmp;

	ea = R_SRC1 + ISRC2;

	CHECK_DRBREAK(ea);

	tmp = read_8(ea);
	new->br[0] = (tmp >> 0) & 1;
	new->br[1] = (tmp >> 1) & 1;
	new->br[2] = (tmp >> 2) & 1;
	new->br[3] = (tmp >> 3) & 1;
	new->br[4] = (tmp >> 4) & 1;
	new->br[5] = (tmp >> 5) & 1;
	new->br[6] = (tmp >> 6) & 1;
	new->br[7] = (tmp >> 7) & 1;

}

CACHED_INS_PROTO(ldbu)
{
	unsigned ea;
	//struct tlb_entry *t;

	ea = R_SRC1 + ISRC2;

	CHECK_DRBREAK(ea);

	R_IDEST = read_8(ea);
}

//CACHED_INS_PROTO(ldbuc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DRBREAK(ea);
//
//	R_IDEST = read_8(ea);
//}

CACHED_INS_PROTO(ldh)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DRBREAK(ea);

	VALIDATE_ALIGNED_16(ea);

	R_IDEST = SignExtend(read_16(ea), 16);
}


//CACHED_INS_PROTO(ldhc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DRBREAK(ea);
//
//	VALIDATE_ALIGNED_16(ea);
//
//	R_IDEST = SignExtend(read_16(ea), 16);
//}

CACHED_INS_PROTO(ldhu)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DRBREAK(ea);

	VALIDATE_ALIGNED_16(ea);

	R_IDEST = ZeroExtend(read_16(ea), 16);
}

//CACHED_INS_PROTO(ldhuc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DRBREAK(ea);
//
//	VALIDATE_ALIGNED_16(ea);
//
//	R_IDEST = ZeroExtend(read_16(ea), 16);
//}

CACHED_INS_PROTO(ldw)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DRBREAK(ea);

	VALIDATE_ALIGNED_32(ea);

	R_IDEST = read_32(ea);
}

//CACHED_INS_PROTO(ldwc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DRBREAK(ea);
//
//	VALIDATE_ALIGNED_32(ea);
//
//	R_IDEST = read_32(ea);
//}

//load link register
CACHED_INS_PROTO(ldl)
{
  	unsigned ea;

  	ea=R_SRC1 + ISRC2;

  	CHECK_DRBREAK(ea);

  	VALIDATE_ALIGNED_32(ea);

  	L_DEST=read_32(ea);
}
/* st200 orig
CACHED_INS_PROTO(ldl)
{
  	unsigned ea;
  	//struct tlb_entry *t;

  	ea=R_SRC1 + ISRC2;

  	CHECK_DBREAK(ea);

  	VALIDATE_ALIGNED_64(ea);

  	LOAD_TRANSLATE(ea);

  	R_IDEST=read_32(0,ea);
  	R_IDESTPLUS1 = read_32(t, ea+4);

}
*/
/*
CACHED_INS_PROTO(ldlc)
{
  	unsigned ea;
  	//struct tlb_entry *t;

	if(B_SCOND == 0) return;

  	ea=R_SRC1 + ISRC2;

  	CHECK_DBREAK(ea);

  	VALIDATE_ALIGNED_64(ea);

  	LOAD_TRANSLATE(ea);

  	R_IDEST=read_32(0,ea);
  	R_IDESTPLUS1 = read_32(t, ea+4);
}   
*/

CACHED_INS_PROTO(ldwl)
{
	unsigned ea;
	ea = R_SRC1 + ISRC2;
	CHECK_DRBREAK(ea);
	VALIDATE_ALIGNED_32(ea);
    loadLinked(ea, CID(current));
	R_IDEST = read_32(ea);
}   

CACHED_INS_PROTO(stwl)
{
	unsigned ea;
	ea = R_SRC1 + ISRC2;
	CHECK_DWBREAK(ea);
	VALIDATE_ALIGNED_32(ea);
    if (isLinked(ea, CID(current))) {
        B_SCOND = 1;
        store_32(ea, R_SRC3);
    } else {
        B_SCOND = 0;
    }
}

CACHED_INS_PROTO(stb)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DWBREAK(ea);

	store_8(ea, R_SRC3);
}

//CACHED_INS_PROTO(stbc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DWBREAK(ea);
//
//	store_8(ea, R_SRC3);
//}

CACHED_INS_PROTO(stbr)
{
	unsigned ea;
	unsigned char tmp;

	ea = R_SRC1 + ISRC2;

	CHECK_DWBREAK(ea);

	tmp = (current->br[0] | (current->br[1] << 1) | (current->br[2] << 2) |
			(current->br[3] << 3) | (current->br[4] << 4) | (current->br[5] << 5) |
			(current->br[6] << 6) | (current->br[7] << 7));

	store_8(ea, tmp);
}

CACHED_INS_PROTO(sth)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DWBREAK(ea);

	VALIDATE_ALIGNED_16(ea);

	store_16(ea, R_SRC3);
}


//CACHED_INS_PROTO(sthc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DWBREAK(ea);
//
//	VALIDATE_ALIGNED_16(ea);
//
//	store_16(ea, R_SRC3);
//}


CACHED_INS_PROTO(stw)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DWBREAK(ea);

	VALIDATE_ALIGNED_32(ea);

	store_32(ea, R_SRC3);
}

//Atomically exchange value in register and memory
CACHED_INS_PROTO(xchg)
{
	unsigned ea;
	unsigned tmp;

	ea = R_SRC1 + ISRC2;

	CHECK_DWBREAK(ea);

	VALIDATE_ALIGNED_32(ea);

	tmp = read_32(ea);
	store_32(ea, R_SRC3);
	R_IDEST = tmp;
}


//CACHED_INS_PROTO(stwc)
//{
//	unsigned ea;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DWBREAK(ea);
//
//	VALIDATE_ALIGNED_32(ea);
//
//	store_32(ea, R_SRC3);
//}

CACHED_INS_PROTO(stl)
{
	unsigned ea;

	ea = R_SRC1 + ISRC2;

	CHECK_DWBREAK(ea);

	VALIDATE_ALIGNED_32(ea);

	store_32(ea, L_SRC);
}

/* st200 orig */
//CACHED_INS_PROTO(stl)
//{
//	unsigned ea;
//	//struct tlb_entry *t;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DBREAK(ea);
//
//	VALIDATE_ALIGNED_64(ea);
//
//	/* Because it is 64 bit aligned, cannot cross page boundary */
//	SAVE_TRANSLATE(ea);
//
//	if(R_SRC2_IS_R0) {
//		store_32(t, ea, 0);
//		store_32(t,ea+4,0);
//	} else {
//		store_32(t, ea, R_SRC2);
//		store_32(t,ea+4,R_SRC2PLUS1);
//	}
//}



//CACHED_INS_PROTO(stlc)
//{
//	unsigned ea;
//	//struct tlb_entry *t;
//
//	if(B_SCOND == 0) return;
//
//	ea = R_SRC1 + ISRC2;
//
//	CHECK_DBREAK(ea);
//
//	VALIDATE_ALIGNED_64(ea);
//
//	/* Because it is 64 bit aligned, cannot cross page boundary */
//	SAVE_TRANSLATE(ea);
//
//	if(R_SRC2_IS_R0) {
//		store_32(t, ea, 0);
//		store_32(t,ea+4,0);
//	} else {
//		store_32(t, ea, R_SRC2);
//		store_32(t,ea+4,R_SRC2PLUS1);
//	}
//}
//
//
//CACHED_INS_PROTO(prgadd)
//{
//	//struct tlb_entry *t;
//	unsigned ea;
//
//	ea = R_SRC1 + ISRC2;
//
//	t = dside_lookup_tlb(ea, 1, 0);
//
//	if (t == NULL && !(psw & PSW_TLB_DYNAMIC))
//		return;
//
//	if (exception) {
//		if ((exception & DTLB_EXCEPTION)
//		    && (tlb_cause == TLB_EXCAUSE_CAUSE_PROT_VIOLATION)) {
//			exception = 0;
//			return;
//		}
//		/* We must have missed */
//		return;
//	}
//
//	ea = TRANSLATE(t, ea);
//
//#ifdef ENABLE_DCACHE
//	purge_addr(ea);
//#endif
//
//}
//
//
//CACHED_INS_PROTO(prgadd_l1)
//{
//	//struct tlb_entry *t;
//	unsigned ea;
//
//	ea = R_SRC1 + ISRC2;
//
//	t = dside_lookup_tlb(ea, 1, 0);
//
//	if (t == NULL && !(psw & PSW_TLB_DYNAMIC))
//		return;
//
//	if (exception) {
//		if ((exception & DTLB_EXCEPTION)
//		    && (tlb_cause == TLB_EXCAUSE_CAUSE_PROT_VIOLATION)) {
//			exception = 0;
//			return;
//		}
//		/* We must have missed */
//		return;
//	}
//
//	ea = TRANSLATE(t, ea);
//
//#ifdef ENABLE_DCACHE
//	purge_addr(ea);
//#endif
//
//}
//
//
//CACHED_INS_PROTO(prgset)
//{
//#ifdef ENABLE_DCACHE
//	unsigned ea;
//
//	ea = R_SRC1 + ISRC2;
//
//	purge_set(ea);
//#endif
//
//}
//
//
//CACHED_INS_PROTO(flushadd)
//{
//  //struct tlb_entry *t;
//  unsigned ea;
//
//  ea=R_SRC1 + ISRC2;
//
//  t=dside_lookup_tlb(ea,1,0);
//
//  ea=TRANSLATE(t,ea);
//
//#ifdef ENABLE_DCACHE
//  flush_addr(ea);
//#endif
//
//
//}
//
//
//CACHED_INS_PROTO(flushadd_l1)
//{
//  //struct tlb_entry *t;
//  unsigned ea;
//
//  ea=R_SRC1 + ISRC2;
//
//  t=dside_lookup_tlb(ea,1,0);
//
//  ea=TRANSLATE(t,ea);
//
//#ifdef ENABLE_DCACHE
//// TODO: strictly should be e.g. flush_addr_l1
//  flush_addr(ea);
//#endif
//
//
//}
//
//
//CACHED_INS_PROTO(invadd)
//{
//  /* INvalidate the address given in the L1 data memory subsystem and L2 cache
//   * (if present) */
//  //struct tlb_entry *t;
//  unsigned ea;
//
//  ea=R_SRC1 + ISRC2;
//
//  t=dside_lookup_tlb(ea,1,0);
//
//  ea=TRANSLATE(t,ea);
//
//#ifdef ENABLE_DCACHE
//  invalidate_addr(ea);
//#endif
//}
//
//CACHED_INS_PROTO(invadd_l1)
//{
//  /* INvalidate the address given in the L1 data memory subsystem only */
//  //struct tlb_entry *t;
//  unsigned ea;
//
//  ea=R_SRC1 + ISRC2;
//
//  t=dside_lookup_tlb(ea,1,0);
//
//  ea=TRANSLATE(t,ea);
//
//#ifdef ENABLE_DCACHE
//  invalidate_addr(ea);
//#endif
//}
//
//
//CACHED_INS_PROTO(wmb)
//{
//	/* Does nothing */
//}
//
//CACHED_INS_PROTO(waitl)
//{
//	/* Does nothing */
//}



