#ifndef __DSU_H__
#define __DSU_H__

#include "state.h"

//H When handling a breakpoint hit, we can choose to either cause a trap and let software handle it or ask the user what to do
//#define TRAP_ON_BREAKPOINT


//H TODO: This code is a lot bigger than the original, I would rather not inline this. Maybe use 1 common printf string could simplify it somewhat?
static void check_ibreak(cx_state_t *current, unsigned pc)
{
	//H is there a breakpoint enabled at all?
	if (likely((current->dcr & (CR_DCR_BR0_MASK | CR_DCR_BR1_MASK | CR_DCR_BR2_MASK | CR_DCR_BR3_MASK)) == 0))
		return;

#if (NBREAKPOINTS > 0)
	if (((current->dcr & CR_DCR_BR0_MASK) == ((0x1) << CR_DCR_BR0_BIT)) && (pc == current->brk[0]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_0, 0);
#else
		printf("Context %d: Breakpoint 0 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 1)
	if (((current->dcr & CR_DCR_BR1_MASK) == ((0x1) << CR_DCR_BR1_BIT)) && (pc == current->brk[1]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_1, 0);
#else
		printf("Context %d: Breakpoint 1 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 2)
	if (((current->dcr & CR_DCR_BR2_MASK) == ((0x1) << CR_DCR_BR2_BIT)) && (pc == current->brk[2]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_2, 0);
#else
		printf("Context %d: Breakpoint 2 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 3)
	if (((current->dcr & CR_DCR_BR3_MASK) == ((0x1) << CR_DCR_BR3_BIT)) && (pc == current->brk[3]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_3, 0);
#else
		printf("Context %d: Breakpoint 3 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
	return;
}

static void check_drbreak(cx_state_t *current, unsigned ea)
{
	//H is there a breakpoint enabled at all?
	if (likely((current->dcr & (CR_DCR_BR0_MASK | CR_DCR_BR1_MASK | CR_DCR_BR2_MASK | CR_DCR_BR3_MASK)) == 0))
		return;

#if (NBREAKPOINTS > 0)
	if (((current->dcr & CR_DCR_BR0_MASK) == ((0x3) << CR_DCR_BR0_BIT)) && (ea == current->brk[0]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_0, 0);
#else
		printf("Context %d: Watchpoint (read) 0 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 1)
	if (((current->dcr & CR_DCR_BR1_MASK) == ((0x3) << CR_DCR_BR1_BIT)) && (ea == current->brk[1]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_1, 0);
#else
		printf("Context %d: Watchpoint (read) 1 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 2)
	if (((current->dcr & CR_DCR_BR2_MASK) == ((0x3) << CR_DCR_BR2_BIT)) && (ea == current->brk[2]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_2, 0);
#else
		printf("Context %d: Watchpoint (read) 2 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 3)
	if (((current->dcr & CR_DCR_BR3_MASK) == ((0x3) << CR_DCR_BR3_BIT)) && (ea == current->brk[3]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_3, 0);
#else
		printf("Context %d: Watchpoint (read) 3 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
	return;
}

static void check_dwbreak(cx_state_t *current, unsigned ea)
{
	//H is there a breakpoint enabled at all?
	if (likely((current->dcr & (CR_DCR_BR0_MASK | CR_DCR_BR1_MASK | CR_DCR_BR2_MASK | CR_DCR_BR3_MASK)) == 0))
		return;

#if (NBREAKPOINTS > 0)
	if ((current->dcr & (0x2 << CR_DCR_BR0_BIT)) && (ea == current->brk[0]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_0, 0);
#else
		printf("Context %d: Watchpoint (write) 0 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 1)
	if ((current->dcr & (0x2 << CR_DCR_BR1_BIT)) && (ea == current->brk[0]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_1, 0);
#else
		printf("Context %d: Watchpoint (write) 1 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 2)
	if ((current->dcr & (0x2 << CR_DCR_BR2_BIT)) && (ea == current->brk[0]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_2, 0);
#else
		printf("Context %d: Watchpoint (write) 2 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
#if (NBREAKPOINTS > 3)
	if ((current->dcr & (0x2 << CR_DCR_BR3_BIT)) && (ea == current->brk[0]))
	{
#ifdef TRAP_ON_BREAKPOINT
			triggerTrap(current, TRAP_HW_BREAKPOINT_3, 0);
#else
		printf("Context %d: Watchpoint (write) 3 hit, interrupting simulator...\n\n", CID(current));
		int_int();
#endif
	}
#endif
	return;
}


#endif
