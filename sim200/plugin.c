#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "state.h" //for cx_state_t
#include "memory.h"
#include "state.h"
#include <sim200/plugin.h>
#include "pluginp.h"
#include "trace.h"

static int read_memory(void *dst, unsigned addr, unsigned size)
{
	unsigned char *p = get_memory_string(addr);
	if (p == NULL)
		return 0;
	memcpy(dst, p, size);
	return 1;
}

static int write_memory(const void *src, unsigned addr, unsigned size)
{
	unsigned char *p = get_memory_string(addr);
	if (p == NULL)
		return 0;
	memcpy(p, src, size);
	return 1;
}

static struct plugin_callbacks plugin_callbacks = {
	set_interrupt,
	clear_interrupt,
	read_memory,
	write_memory,
	set_trace_mode
};

//////////////////////////////////////////////////////////////////////////////
// Exported interface
//////////////////////////////////////////////////////////////////////////////

void plugin_open(int doparams)
{
	struct system_info info = {
		PLUGIN_SYSTEM_INFO_V1, MEMORY_START, MEMORY_SIZE
	};

	pluginlib_init(&plugin_callbacks, &info);
	if (doparams) {
		pluginlib_setup_params();
	}
}

void plugin_close(void)
{
	pluginlib_finish();
}

int plugin_read(int contextID, void *dst, unsigned int address, unsigned int numBytes)
{
	struct memory_bank *mb = pluginlib_find_mb(address);
	if (mb == NULL)
		return 0;

	return mb->read_fn(contextID, dst, address, numBytes);
}

int plugin_write(int contextID, const void *src, unsigned int address, unsigned int numBytes)
{
	struct memory_bank *mb = pluginlib_find_mb(address);
	if (mb == NULL)
		return 0;

	return mb->write_fn(contextID, src, address, numBytes);
}
