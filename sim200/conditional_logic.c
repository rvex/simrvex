#include <stdint.h>     // uint_fast8_t
#include <unistd.h>     // ssize_t
#include <limits.h>     // SSIZE_MAX
#include <stdio.h>      // printf

#include "conditional_logic.h"
#include "state.h"

#define PATCH_EA(ea) (ea >>= 2)

struct LinkCacheLine{
    uint_fast8_t isValid;
    uint_fast32_t addr;
};

// A little known fact is that this variable will end up in the BSS section, which HAS TO BE zero on program start.
// So, on program start, lines is set to all zeros.
//
// "Smokey, this isn't Nam, this is bowling. There are rules."
// There can never be two valid lines containing the same address.
static struct LinkCacheLine lines[NCONTEXTS];

void notifyWrite(unsigned ea){
    PATCH_EA(ea);
    size_t it;
    for (it = 0; it < NCONTEXTS; ++it){
        if (lines[it].addr == ea) lines[it].isValid = 0;
    }
}

void loadLinked(unsigned ea, u32 cid){
    PATCH_EA(ea);
    notifyWrite(ea);
    lines[cid].isValid = 1;
    lines[cid].addr = ea;
}

void invalidateForCid(u32 cid){
    lines[cid].isValid = 0;
}

int isLinked(unsigned ea, u32 cid){
    PATCH_EA(ea);
    if (lines[cid].addr == ea && lines[cid].isValid){
        return 1;
    }
    return 0;
}

void dumpLinkReg(void){
    printf("Dumping link register, %d lines in total (1 per context)\n", NCONTEXTS);
    printf("\t|context ID\t| address\t| is valid\t|\n");
    ssize_t it;
    for (it = 0; it < NCONTEXTS; ++it){
        printf("\t| %zd\t\t| 0x%.8lx\t| %.1u\t\t|\n", it, lines[it].addr, lines[it].isValid);
    }

}
