#ifndef __PLUGINI_H__
#define __PLUGINI_H__

/* Little endian only */
#define  htoss(x)	(x)
#define  stohs(x)	(x)
#define  htosl(x)	(x)
#define  stohl(x)	(x)

/* Helper function to create threads. All plugins should use this, rather
 * than calling pthread_create() directly
 */
void pluginlib_thread_create(void (*thread) (void *arg), void *arg);

#endif
