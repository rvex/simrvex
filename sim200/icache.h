#ifndef __ICACHE_H__
#define __ICACHE_H__

#include "state.h"
#include "trace.h"
#include "memory.h"
#include "ctrlregdef.h"
#include "tlb.h"

struct icache_entry {
	struct decoded_bundle bundle;	/* The decoded bundle */
	struct icache_entry **slot;	/* Pointer back to the entry in the table */
	unsigned invalidate_cycle; /* If used */
};

/* How big the icache can possibly be */
#define ICACHE_MAX_SIZE (32*1024*1024)
#define ICACHE_NUM_ENTRIES (ICACHE_MAX_SIZE/sizeof(struct icache_entry))

extern struct icache_entry icache_buffer[3][ICACHE_NUM_ENTRIES];

/* Enormous array of pointers. Each word of memory is present, so that
 * lookup is *really* fast, and we don't have to deal with the unpleasant
 * problems of decoded bundles crossing cache lines. Memory is cheap!!
 */
extern struct icache_entry **icache[3];
//extern struct icache_entry *icache[MEMORY_SIZE / 4];

extern unsigned long invalidate_cycle_count;

extern int icache_next; /* Next entry in icache array */

void invalidate_icache(void);

void invalidate_icache_address(unsigned start, unsigned size);

#endif
