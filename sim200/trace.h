#ifndef __TRACE_H__
#define __TRACE_H__

enum instr_type { INT3R_TYPE, 		/* 0 */
		  INT3I_TYPE, 		/* 1 */
		  MONADIC_TYPE, 	/* 2 */
		  CMP3R_TYPE,		/* 3 */
		  CMP3R_BR_TYPE,	/* 4 */
		  CMP3I_TYPE, 		/* 5 */
		  CMP3I_BR_TYPE, 	/* 6 */
		  SELECTR_TYPE,		/* 7 */
		  SELECTI_TYPE,		/* 8 */
		  CGEN_TYPE, 		/* 9 */
		  SYSOP_TYPE,		/* 10 */
		  SBREAK_TYPE, 		/* 11 */
		  LOAD_TYPE,		/* 12 */	/* Do not add anything in the load/store group, look at trace_memory_string */
		  LOAD_PAIR_TYPE,	/* 13 */
		  LOAD_CONDITIONAL_TYPE,/* 14 */
		  LOAD_CONDITIONAL_PAIR_TYPE, /* 15 */
		  STORE_TYPE,		/* 16 */
		  STORE_PAIR_TYPE,	/* 17 */
		  STORE_CONDITIONAL_TYPE,	/* 18 */
		  STORE_CONDITIONAL_PAIR_TYPE,	/* 19 */
		  STORE_OTHER_TYPE,		/* 20 */
		  PSW_TYPE,			/* 21 */
		  BR2R_TYPE,			/* 22 */
		  BR3R_TYPE,			/* 23 */
		  BSRC_TYPE,			/* 24 */
		  STWL_TYPE,			/* 25 */
		  LDWL_TYPE,			/* 26 */
	    	  CALL_TYPE,			/* 27 */
		  BRANCH_TYPE,			/* 28 */
		  PSEUDO_TYPE
};

struct trace_info {
	const char *name;	/* mnenmonic name */
	enum instr_type type;	/* How to print it */
};

#define TRACE_OFF             0
#define TRACE_CORE            1
#define TRACE_CACHE           2
#define TRACE_ALL  (TRACE_CORE|TRACE_CACHE)

extern int trace_mode;
extern int trace_context;
extern int trace_start_cycle;
extern int trace_start_addr;

int set_trace_mode(int new_mode);
int set_trace_context(int new_context);

/* If set, just wrap round. Only output at program exit 
 * Otherwise we just dump 'n go 
 */

extern int circular_trace;
extern int trace_context;

void dump_trace_buffer(void);

void print_trace_info(cx_state_t *current, cx_state_t *new,
		      struct decoded_bundle *bundle);

void allocate_trace_buffer(int size_in_megs);

#endif
