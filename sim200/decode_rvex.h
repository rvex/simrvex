#ifndef __DECODE_RVEX_H__
#define __DECODE_RVEX_H__

/* Given an instruction index number, gets all the information about it */
int rvex_get_instr_index(struct instr_info *ins);

/* Reverse of above */
struct instr_info * rvex_get_instr_info(int index);


/* Initialisation function for rvex */
void rvex_decode_init(int enable_dcache);

/* Main decode loop */
struct decoded_bundle *rvex_slow_get_next_decoded_bundle(cx_state_t *current,
						    struct icache_entry
						    **icache_slot);
#endif

