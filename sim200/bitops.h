
#ifndef __BITOPS_H__
#define __BITOPS_H__

/* Macros to create bitfields using upper:lower syntax, ie,
 * BITMASK(27:23) will give a mask to pull out bits 27 -> 23 in 
 * the word. MSB must go first !!!
 */
#define CREATE_BITMASK(t,b) (((unsigned)(1U << (((t)-(b)+1)))-1)  << (b))
#define BITMASK_LOWER_BIT(mask) (0?mask)
#define BITMASK_UPPER_BIT(mask) (1?mask)
#define BITMASK(mask) CREATE_BITMASK(BITMASK_UPPER_BIT(mask),BITMASK_LOWER_BIT(mask))
#define BITMASK_SIZE(mask)( (BITMASK(mask) >> BITMASK_LOWER_BIT(mask)) + 1) /* Size of the bitfield, power of 2 */
#define BITMASK_EXTRACT(mask,x) ( ((x) & BITMASK(mask)) >> BITMASK_LOWER_BIT(mask)) /* Suck out the bits */

#endif

