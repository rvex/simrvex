#ifndef __TLB_H__
#define __TLB_H__

#if 0

#define PROT_SUPER_EXECUTE 1
#define PROT_SUPER_READ 2
#define PROT_SUPER_WRITE 4

#define PROT_USER_EXECUTE 8
#define PROT_USER_READ 16
#define PROT_USER_WRITE 32

#define CACHE_POLICY_UNCACHED 0
#define CACHE_POLICY_CACHED 1
#define CACHE_POLICY_WRITE_COMBINE 2

#define PAGE_SIZE_DISABLED 0
#define PAGE_SIZE_8K 1
#define PAGE_SIZE_4MB 2
#define PAGE_SIZE_256MB 3
#define PAGE_SIZE_4K 4

#define NUM_UTLB_ENTRIES 64
#define NUM_DTLB_ENTRIES 8
#define NUM_ITLB_ENTRIES 4

#define PAGE_MASK_4K   (0xfffff000)
#define PAGE_MASK_8K   (0xffffe000)
#define PAGE_MASK_4M   (0xffc00000)
#define PAGE_MASK_256M (0xf0000000)

/* Expanded TLB entry */
/* We expand these for speed of access. Question, is it better to use
 * more cache, or unpack on the fly?
 */
struct tlb_entry {
	u32 vaddr;
	u32 paddr;
	u32 page_mask;		/* Mask to get translated bits. Precomputed */
	unsigned asid;
	unsigned prot;		/* 6 bits of protection */
	int shared;
	int dirty;
	struct tlb_entry **micro_i;
	struct tlb_entry **micro_d;
	int size;		/* zero means disabled */
	int cache_policy;
	unsigned entry[4];	/* the "unexpanded entry, ie what the machine holds */
};

/* When adding new entries to the utlb, we have to be careful to check if 
 * the entry that was there is held in the micro tlb. If so, we have 
 * to copy it and update the pointer 
 */

/* Gives physical address assuming t points at the correct TLB entry */

#define TRANSLATE(t,virtual) ((t)->paddr+(virtual&(~((t)->page_mask))))

#define TLB_IGNORE_ADDRESS_BITS 13

/* Blats the ITLB */
void invalidate_itlb(void);
/* Ditto for DTLB */
void invalidate_dtlb(void);

void invalidate_last_tlb_cache(void);

extern struct tlb_entry *last_itlb;

struct tlb_entry *slow_iside_lookup_tlb(u32 virtual);

static inline struct tlb_entry *iside_lookup_tlb(u32 virtual)
{

	/* We do not need to check PSW, since last will point at a dummy
	 * zero entry if the TLB is not enabled.
	 */

	/* last_itlb always points at either a valid shared entry, or 
	 * the ASID matches the current ASID. No need to check these 
	 * We also don't have to check permissions, as changing from super 
	 * to user clears this cache. Also it is never updated if you don't 
	 * have execute in the first place
	 */
	if ((virtual & last_itlb->page_mask) == last_itlb->vaddr)
		return last_itlb;

	/* Damn we are on the slow path, either a micro or a full UTLB lookup */

	return slow_iside_lookup_tlb(virtual);

}

struct tlb_entry *dside_lookup_tlb(u32 virtual, int read, int diss);

struct tlb_entry *get_tlb_entry(int index);

/* Which tlb we are currently talking to */
extern int tlb_index;

extern u32 tlb_replace;

/* Changing any value will cause the TLB's to be updated */
void write_tlb_register(int offset, u32 value);

/* Gets current value */
u32 read_tlb_register(int offset);

void dump_utlb(void);

#endif

#endif
