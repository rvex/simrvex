#include <stdio.h>
#include <stdlib.h>
#include "state.h"
#include "trace.h"
#include "decode.h"
#include "icache.h"
//#include "decode_st231.h"
//#include "decode_st240.h"
#include "decode_rvex.h"


int (*get_instr_index)(struct instr_info *ins);
struct instr_info * (*get_instr_info)(int index);


void decode_init(int enable_dcache)
{
	rvex_decode_init(enable_dcache);
	get_instr_index=rvex_get_instr_index;
	get_instr_info=rvex_get_instr_info;

}
		


