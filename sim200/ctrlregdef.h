/*
 * include/asm-st200/ctrlregdef.h
 *
 * Copyright (C) 2002 STMicroelectronics Limited
 *	Author: Stuart Menefy <stuart.menefy@st.com>
 *
 * Control register defintions for generic ST200
 */

#ifndef _ASM_ST200_CTRLREGDEFS_H
#define _ASM_ST200_CTRLREGDEFS_H

#include "rvex.h"

/*
 * Most of the definitions are already in the generated rvex.h.
 * Additional definitions can be added here if necessary.
 */

#if 0
#define CTRL_REG_BASE 0xffff0000

#define CTRL_ADDR(offset) (CTRL_REG_BASE+(offset))

/* Saved Program Counter, written by hardware on exception. */
#define SAVED_PC	CTRL_ADDR(0xffe8)

/* Saved PSW, written by hardware on exception. */
#define SAVED_PSW	CTRL_ADDR(0xfff0)

/* The Program Status Word. */
#define PSW		CTRL_ADDR(0xfff8)

/* Exception handler defines */

/* The address of the exception handler code. */
#define HANDLER_PC CTRL_ADDR(0xffe0)

/* A one hot vector of trap (exception/interrupt) types
   indicating the cause of the last trap. Written by the hardware on a trap. */
#define EXCAUSE CTRL_ADDR(0xffd8)

/* This will be the data effective address in the case of 
    either a DPU, CREG, DBREAK, or MISALIGNED_TRAP exception. 
    For other exception types this register will be zero. */
#define EXADDRESS CTRL_ADDR(0xffd0)

/* Debug related registers */

#define DBREAK_LOWER	CTRL_ADDR(0xfe80)	/* Data breakpoint lower address. */
#define DBREAK_UPPER	CTRL_ADDR(0xfe78)	/* Data breakpoint upper address. */
#define DBREAK_CONTROL	CTRL_ADDR(0xfe70)	/* Data breakpoint control. */

#define IBREAK_LOWER	CTRL_ADDR(0xfdd0)	/* Instruction breakpoint lower address. */
#define IBREAK_UPPER	CTRL_ADDR(0xfdc8)	/* Instruction breakpoint upper address. */
#define IBREAK_CONTROL	CTRL_ADDR(0xfdc0)	/* Instruction breakpoint control. */

#define BREAK_CONTROL_BRK_IN_RANGE	(1<<0)	/* Break if address >= lower && address <=upper. */
#define BREAK_CONTROL_BRK_OUT_RANGE	(1<<1)	/* Break if address < lower || address > upper. */
#define BREAK_CONTROL_BRK_EITHER	(1<<2)	/* Break if address == lower || address == upper. */
#define BREAK_CONTROL_BRK_MASKED	(1<<3)	/* Break if address & upper == lower. */

#define SAVED_SAVED_PSW	CTRL_ADDR(0xffc0)	/* PSW saved by debug unit interrupt. */
#define SAVED_SAVED_PC	CTRL_ADDR(0xffb8)	/* PC saved by debug unit interrupt. */

#define PERIPHERAL_BASE_ADDRESS CTRL_ADDR(0xffb0)

#endif

#endif				/* _ASM_ST200_CTRLREGDEFS_H */
