#ifndef __DECODE_H__
#define __DECODE_H__


/* The main tables are built around this */

struct instr_info {
	instr_func op;		 /* Code to execute */
	struct trace_info trace; /* Information for trace output */
};


void decode_init(int enable_dcache);

/* These functions allow mapping from instruction number to data and vice
 * versa. Needed as the binary trace format obviously cannot use pointers!
 */
extern int (*get_instr_index)(struct instr_info *ins);
extern struct instr_info * (*get_instr_info)(int index);

#endif

