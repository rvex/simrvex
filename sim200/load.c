/* $Id: load.c,v 1.5 1999/08/02 17:40:45 stuart Exp $
 *
 * sh3mon/load.c
 *
 * Copyright (C) 1999 Stuart Menefy
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <config.h>

#include <stdbool.h>
#include <bfd.h>
#include "load.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <limits.h>

#include "rvex.h"
#include "plugin.h"

#define TARGET "elf32-rvex"

#define MAX_SYMBOL_NAME_LENGTH 32

typedef struct {
	void (*fn) (const unsigned char *buffer, int len, unsigned long addr);
} iterator_data_t;

typedef struct{
	char *name;
	unsigned int address;
} symbol;

symbol *textsymlist = NULL;
int nTextSymbols = 0;

symbol *datasymlist  = NULL;
int nDataSymbols = 0;

char cregstr[] = "CREG";
char globregstr[] = "GLOBALREG";
char pluginstr[] = "PLUGIN";
char grlibstr[] = "GRLIB_PERIPH";


static void fatal(const char *string);
static void load_iterator(bfd * abfd, asection * section, PTR data);
void sim_load_write(const unsigned char *data, int len, unsigned long phys); // in memory.c
static void load_symbols(bfd *abfd);

void load_binfile(unsigned int address, char *filename)
{
	int fd;
	struct stat info;
	unsigned char* mapAddr;
	if (stat(filename, &info))
	{
		perror("Error opening file");
		exit(-1);
	}

	fd = open(filename, O_RDONLY);

	mapAddr = (unsigned char*)mmap(0, info.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	sim_load_write(mapAddr, info.st_size, address);

}

static void load_iterator(bfd * abfd, asection * section, PTR ptr)
{
	bfd_size_type datasize;
	bfd_byte *data;
	iterator_data_t *iterator_data = (iterator_data_t *) ptr;
	int load = 0;

	datasize = bfd_section_size(abfd, section);

//	printf("Section: %s (at 0x%08lx loaded at 0x%08lx) 0x%08lx bytes\n",
//	       section->name, section->vma, section->lma, datasize);

	if (datasize == 0) {
		return;
	}
#if 0
	for (a = 0; a < (sizeof(load_sections) / sizeof(char *)); a++) {

		if (strcmp(section->name, load_sections[a]) == 0) {
			load = 1;
			break;
		}
	}
#endif

	load = section->flags & SEC_LOAD;

	if (!load)
		return;

	data = (bfd_byte *) malloc((size_t) datasize);
	if (data == NULL) {
		fprintf(stderr, "%s: Out of memory\n", program_name);
		exit(1);
	}
	bfd_get_section_contents(abfd, section, data, 0, datasize);

	iterator_data->fn(data, datasize, section->lma);

	free(data);

}

static void fatal(const char *string)
{
	const char *errmsg = bfd_errmsg(bfd_get_error());

	if (string)
		fprintf(stderr, "%s: %s: %s\n", program_name, string, errmsg);
	else
		fprintf(stderr, "%s: %s\n", program_name, errmsg);
	exit(1);
}

/**
 * Load elf file into memory.
 *
 * @param name Filename of file to load into memory.
 * @return The address where the program starts.
 */
unsigned long load_elf_file(const char *name)
{
	iterator_data_t data;
	bfd *abfd;

	if (!name)
		return 0;
	bfd_init();
	abfd = bfd_openr(name, NULL);
	if (abfd == NULL) {
		fatal(name);
	}
	if (!bfd_check_format(abfd, bfd_object)) {
		fatal(bfd_get_filename(abfd));
	}
	data.fn = sim_load_write;
	bfd_map_over_sections(abfd, load_iterator, &data);
	load_symbols(abfd);
	if (bfd_close(abfd) == false) {
		fatal(bfd_get_filename(abfd));
	}
	return (unsigned long) bfd_get_start_address(abfd);
}

#define valueof(x) ((x)->section->vma + (x)->value)

/*
 * Compare symbols for sorting. First use the address. If they are equal, use name.
 */
int compare_syms(const void *a, const void *b)
{
	int i;
	const symbol *asym = (const symbol*)a;
	const symbol *bsym = (const symbol*)b;
	if (asym->address > bsym->address) return 1;
	else if (asym->address < bsym->address) return -1;
	else
	{
		for (i = 0; i < strnlen(asym->name, MAX_SYMBOL_NAME_LENGTH) && i < strnlen(bsym->name, MAX_SYMBOL_NAME_LENGTH); i++)
		{
				if (asym->name[i] > bsym->name[i]) return 1;
				if (asym->name[i] < bsym->name[i]) return 1;
		}
	}
	return 0;
}

/**
 * Return 1 if the symbol is in the text section else 0.
 */
static int is_text_symbol(bfd *abfd, asymbol *sym)
{
	if (!strcmp(".text", bfd_section_name(abfd, sym->section)))
		return 1;
	return 0;
}

/**
 * Return 1 if the symbol is in the data or bss section else 0.
 */
static int is_data_symbol(bfd *abfd, asymbol *sym)
{
	const char *section_name = bfd_section_name(abfd, sym->section);
	if (!strcmp(".data", section_name) || !strcmp(".bss", section_name))
		return 1;
	return 0;
}

static void load_symbols(bfd *abfd)
{
	PTR minisyms;
	unsigned size;
	int i, j, k, count, len;
	asymbol *sym;
	asymbol *store;
	bfd_byte *from;
	int data_sym_count = 0;
	int text_sym_count = 0;
	symbol *temp_text;
	symbol *temp_data;
	int new_data_sym_size = 0;
	int new_text_sym_size = 0;

	count = bfd_read_minisymbols(abfd, 0, &minisyms, &size);

	if (count <= 0) {
		return;
	}
	store = bfd_make_empty_symbol(abfd);

	from = (bfd_byte *) minisyms;
	//Count the number of symbols that are relevant for us
	for (i = 0; i < count; i++, from += size) {
		sym = bfd_minisymbol_to_symbol(abfd, 0, (const PTR) from, store);
		if (is_text_symbol(abfd, sym))
			text_sym_count++;
		else if (is_data_symbol(abfd, sym))
			data_sym_count++;
	}
	new_data_sym_size = nDataSymbols + data_sym_count;
	new_text_sym_size = nTextSymbols + text_sym_count;

	temp_text = (symbol*) realloc(textsymlist, new_text_sym_size*sizeof(symbol));
	if (!temp_text)
	{
		printf("Error allocating memory for textsymlist\n");
		exit(1);
	}
	textsymlist = temp_text;
	temp_data = (symbol*) realloc(datasymlist, new_data_sym_size*sizeof(symbol));
	if (!temp_data)
	{
		printf("Error allocating memory for textsymlist\n");
		exit(1);
	}
	datasymlist = temp_data;

	from = (bfd_byte *) minisyms;
	//write the text symbols into the array. we need 2 indices, i for the original (all) symbols, j for the array with the text symbols.
	for (i = j = 0; (i < count) && (j < text_sym_count) ; i++, from += size) {
		sym = bfd_minisymbol_to_symbol(abfd, 0, (const PTR)(from), store);
		if (is_text_symbol(abfd, sym))
		{
			len = strnlen(bfd_asymbol_name(sym), MAX_SYMBOL_NAME_LENGTH);
			textsymlist[nTextSymbols + j].name = strndup(bfd_asymbol_name(sym), len);
			if (textsymlist[nTextSymbols + j].name == NULL)
			{
				printf("Error allocating memory for symbol name\n");
				exit(-1);
			}
			textsymlist[nTextSymbols + j].address = valueof(sym);
			j++;
		}
	}
	nTextSymbols += text_sym_count;

	from = (bfd_byte *) minisyms;
	//write the data symbols into the array. we need 2 indices, i for the original (all) symbols, k for the array with the data symbols.
	for (i = k = 0; (i < count) && (k < data_sym_count) ; i++, from += size) {
		sym = bfd_minisymbol_to_symbol(abfd, 0, (const PTR)(from), store);
		if (is_data_symbol(abfd, sym))
		{
			len = strnlen(bfd_asymbol_name(sym), MAX_SYMBOL_NAME_LENGTH);
			datasymlist[nDataSymbols + k].name = strndup(bfd_asymbol_name(sym), len);
			if (datasymlist[nDataSymbols + k].name == NULL)
			{
				printf("Error allocating memory for symbol name\n");
				exit(-1);
			}
			datasymlist[nDataSymbols + k].address = valueof(sym);
			k++;
		}
	}
	nDataSymbols += data_sym_count;

	qsort(textsymlist, nTextSymbols, sizeof(symbol), compare_syms);
	qsort(datasymlist, nDataSymbols, sizeof(symbol), compare_syms);

	return;
}

/*
 * Given an address, this function will return the name and offset to the closest text symbol
 */
int find_text_symbol_and_offset_from_addr(unsigned addr, char **name, int *offset)
{
	int i, diff, smallestDiff;
	int foundone = 0;
	if (nTextSymbols <= 0) {
		return 0;
	}

	smallestDiff = INT_MAX;
	for (i = 0; i < nTextSymbols; i++) {
		diff = addr - textsymlist[i].address;
		if ((diff >= 0) && (diff < smallestDiff)) {
			smallestDiff = diff;
			*name = textsymlist[i].name;
			foundone = 1;
		}
	}
	*offset = smallestDiff;
	return foundone;
}

/*
 * Given an address, this function will return the name and offset to the closest text symbol
 */
int find_text_symbol_from_addr(unsigned addr, char **name)
{
	int i;

	if (nTextSymbols <= 0) {
		return 0;
	}

	for (i = 0; i < nTextSymbols; i++) {
		if (addr == textsymlist[i].address) {
			*name = textsymlist[i].name;
			return 1;
		}
	}
	return 0;
}


/*
 * Given an address, this function will return the name and offset to the closest data symbol
 */
int find_data_symbol_and_offset_from_addr(unsigned addr, char **name, int *offset)
{
	int i;
	unsigned diff, smallestDiff;
	int foundone = 0;
	struct memory_bank *pluginaddr;
	if (nDataSymbols <= 0) {
		*name = NULL;
		*offset = 0;
		return 0;
	}

	pluginaddr = pluginlib_find_mb(addr);
	if (pluginaddr != NULL)
	{
		*name = pluginstr;
		*offset = addr - pluginaddr->base;
		return 1;
	}

	if ((addr >> 28) == 0x8)
	{
		*name = grlibstr;
		*offset = addr - 0x80000000;
		return 1;
	}

	if (addr >= CREG_BASE && addr < CREG_BASE + CREG_SIZE)
	{
		*name = cregstr;
		*offset = addr - CREG_BASE;
		return 1;
	}
	if (addr >= 0xd0000000 && addr < 0xe0000000)
	{
		*name = globregstr;
		*offset = addr - 0xd0000000;
		return 1;
	}


	*name = NULL;
	smallestDiff = addr;
	for (i = 0; i < nDataSymbols; i++) {
		if (addr < datasymlist[i].address)
			break;
		diff = addr - datasymlist[i].address;
		if (diff < smallestDiff) {
			smallestDiff = diff;
			*name = datasymlist[i].name;
			foundone = 1;
		}
	}
	*offset = smallestDiff;
	return foundone;
}

/*
 * Given an address, this function will return the name and offset to the closest data symbol
 */
int find_data_symbol_from_addr(unsigned addr, char **name)
{
	int i;
	struct memory_bank *pluginaddr;

	if (nDataSymbols <= 0) {
		return 0;
	}

	pluginaddr = pluginlib_find_mb(addr);
	if (pluginaddr != NULL)
	{
		*name = pluginstr;
	}

	if (addr >= CREG_BASE && addr < CREG_BASE + CREG_SIZE)
	{
		*name = cregstr;
		return 1;
	}
	if (addr >= 0xd0000000 && addr < 0xe0000000)
	{
		*name = globregstr;
		return 1;
	}

	for (i = 0; i < nDataSymbols; i++) {
		if (addr == datasymlist[i].address) {
			*name = datasymlist[i].name;
			return 1;
		}
	}
	return 0;
}
/**
 * Store the address of the data symbol matching name in addr.
 * Returns 0 if the symbol is not found, otherwise 1.
 */
int find_addr_from_data_symbol(const char *name, unsigned *addr)
{
	int i;
	for (i = 0; i < nDataSymbols; i++) {
		if (!strcmp(name, datasymlist[i].name)) {
			*addr = datasymlist[i].address;
			return 1;
		}
	}
	return 0;
}

/**
 * Find the address of a symbol in executable called filename.
 *
 * @param filename The file to search in.
 * @param symname The symbol to search for.
 * @param addr The location to store the address of the symbol.
 *
 * @return 0 if failed, 1 for success.
 */
int find_sym_addr_in_file(const char *filename, const char *symname,
			  unsigned *addr)
{
	bfd *abfd;
	int res = 0;
	PTR minisyms;
	unsigned size;
	int i, count;
	asymbol *sym;
	asymbol *store;
	bfd_byte *from;

	bfd_init();
	abfd = bfd_openr(filename, NULL);
	if (abfd == NULL) {
		fatal(filename);
	}
	if (!bfd_check_format(abfd, bfd_object)) {
		fatal(bfd_get_filename(abfd));
	}

	count = bfd_read_minisymbols(abfd, 0, &minisyms, &size);
	store = bfd_make_empty_symbol(abfd);
	from = (bfd_byte *) minisyms;
	for (i = 0; i < count; i++, from += size) {
		sym = bfd_minisymbol_to_symbol(abfd, 0, (const PTR)(from), store);
		if (!strcmp(bfd_asymbol_name(sym), symname)) {
			*addr = valueof(sym);
			res = 1;
			break;
		}
	}
	bfd_close(abfd);
	return res;
}

