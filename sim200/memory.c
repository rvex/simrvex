#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "state.h"
#include "ctrlregdef.h"
#include "memory.h"
#include "tlb.h"
#include "irq.h"
#include "pluginp.h"
#include "simtypes.h"
#include "trace.h"

#include "simtime.h"
#include "icache.h"

#include <sys/mman.h>

#define ENABLE_CACHE_TRACE

#define SWAP32(x) ((((x) & 0xff) << 24) | (((x) & 0xff00) << 8) | (((x) & 0xff0000) >> 8) | (((x) >> 24) & 0xff))
#define SWAP16(x) ((((x) & 0xff) << 8) | ((x) & 0xff00) >> 8)

#define TOGGLEBITS(value, mask) (value & ~mask) | ((value^mask)&mask)

extern glob_state_t global_state;
extern cx_state_t context_state[NCONTEXTS][2];
extern int reconfigurationRequested;

static union {
	u8 byte_array[MEMORY_SIZE];
	u16 short_array[MEMORY_SIZE / 2];
	u32 word_array[MEMORY_SIZE / 4];
} memory;

//H TODO: move these somewhere more appropriate
#define TIMER_SCALER_ADDR 0x80000300
#define TIMER_SCALER_RELOAD_ADDR 0x80000304
#define TIMER_CONFIG_ADDR 0x80000308
#define TIMER_TIM_CTR_ADDR(x) (0x80000310 + (x*0x10))
#define TIMER_TIM_RELOAD_ADDR(x) (0x80000314 + (x*0x10))
#define TIMER_TIM_CONFIG_ADDR(x) (0x80000318 + (x*0x10))

#define IRQ_IRL 0x80000200 //Interrupt Level Register
#define IRQ_PENDING 0x80000204
//#define IRQ_FORCE 0x80000208 //only exists if NCPU=1
#define IRQ_CLEAR 0x8000020c
#define IRQ_MPSTAT 0x80000210 //Multi-processor status register
#define IRQ_BROADCAST 0x80000214
#define IRQ_MASK 0x80000240
#define IRQ_FORCE 0x80000280

#define DCACHE_NUM_WAYS 4
#define DCACHE_SETS_LOG2 8
#define DCACHE_NUM_SETS (1<<DCACHE_SETS_LOG2)
#define DCACHE_LINESIZE_LOG2 3 //8 bytes (64 bits)
#define DCACHE_LINE_SIZE (1<<DCACHE_LINESIZE_LOG2)

#define DCACHE_SET_MASK ((1<<(DCACHE_SETS_LOG2+DCACHE_LINESIZE_LOG2))-1)
#define DCACHE_LINE_MASK ((1<<DCACHE_LINESIZE_LOG2)-1)
#define DCACHE_TAG_MASK (~DCACHE_LINE_MASK)

#define ICACHE_NUM_WAYS 4
#define ICACHE_SETS_LOG2 6
#define ICACHE_NUM_SETS (1<<ICACHE_SETS_LOG2)
#define ICACHE_LINESIZE_LOG2 5 //256 bits or a full 8-issue instruction bundle
#define ICACHE_LINE_SIZE (1<<ICACHE_LINESIZE_LOG2)

#define ICACHE_SET_MASK ((1<<(ICACHE_SETS_LOG2+ICACHE_LINESIZE_LOG2))-1)
#define ICACHE_LINE_MASK ((1<<ICACHE_LINESIZE_LOG2)-1)
#define ICACHE_TAG_MASK (~ICACHE_LINE_MASK)

#define INVALID_TAG (~0)

struct dcache_entry {
	u32 tag;		/* Which address is present */
//	int dirty;		/* Has line been written to */
//	union {
//		u8 line_8[DCACHE_LINE_SIZE];
//		u16 line_16[DCACHE_LINE_SIZE / 2];
//		u32 line_32[DCACHE_LINE_SIZE / 4];
//	};
};
static struct dcache_entry dcache[NCONTEXTS][DCACHE_NUM_WAYS][DCACHE_NUM_SETS];

//We only need the tags
static u32 icache_tags[NCONTEXTS][ICACHE_NUM_WAYS][ICACHE_NUM_SETS];

u32 updateFlags(u32 writeValue, u32 mask, u32 currentReg)
{
	if ((writeValue & mask) == mask) //Toggle flag
	{
		return ((writeValue & ~mask) | TOGGLEBITS(currentReg, mask));
	}
	else if ((writeValue & mask) == 0) //writing 2 0s should do nothing
	{
		return (writeValue & ~mask) | (currentReg & mask);
	}
	else return writeValue;
}

void clearCounters(cx_state_t *current)
{
	//Core performance counters
	current->cr_bun = 0;
	current->cr_cyc = 0;
	current->cr_nop = 0;
	current->cr_stall = 0;
	current->cr_syl = 0;
	current->cr_stop = 0;

	//Cache performance counters
	current->cr_dbypass = 0;
	current->cr_dracc = 0;
	current->cr_drmiss = 0;
	current->cr_dwacc = 0;
	current->cr_dwbuf = 0;
	current->cr_dwmiss = 0;
	current->cr_iacc = 0;
	current->cr_imiss = 0;

	//Bank conflict counters
	current->bank_r1 = 0;
	current->bank_r2 = 0;
	current->bank_wr = 0;
}

void flush_dcache()
{
	int i, j, k;

	for (i = 0; i < NCONTEXTS; i++) {
		for (j = 0; j < DCACHE_NUM_SETS; j++)
			for (k = 0; k < DCACHE_NUM_SETS; k++) {
				dcache[i][j][k].tag = INVALID_TAG;
	//			dcache[j][i].dirty = 0;
			}
	}
}

static void init_dcache(void)
{
	flush_dcache();
}

void flush_icache()
{
	int i, j, k;
	for (i = 0; i < NCONTEXTS; i++) {
		for (j = 0; j < ICACHE_NUM_WAYS; j++)
		for (k = 0; k < ICACHE_NUM_SETS; k++) {
			icache_tags[i][j][k] = INVALID_TAG;
		}
	}
}

static void init_icache(void)
{
	int i,j;
//	in case you want to run the simulator in GDB, you'll need to dynamically allocate this array here
	for (i = 0; i < 3; i++)
		icache[i] = (struct icache_entry**) malloc(sizeof(struct icache_entry*) * (MEMORY_SIZE / 8));

	flush_icache();
}

/*
 * Invalidate an address in any block except the ones in the mask
 */
void invalidate_addr(u32 phys, int exceptMask)
{
	struct dcache_entry *d;
	int dset_index = (phys & DCACHE_SET_MASK) >> DCACHE_LINESIZE_LOG2;
	int iset_index = (phys & ICACHE_SET_MASK) >> ICACHE_LINESIZE_LOG2;
	unsigned dtag = phys & DCACHE_TAG_MASK;
	unsigned itag = phys & ICACHE_TAG_MASK;
	int i,j;

	for (i = 0; i < NCONTEXTS; i++)
	{
		if ((exceptMask & (1<<i)) == 0)
		{
			for (j = 0; j < DCACHE_NUM_WAYS; j++)
			{
				d = &dcache[i][j][dset_index];
				if (d->tag == dtag)
					d->tag = INVALID_TAG;
			}
			for (j = 0; j < ICACHE_NUM_WAYS; j++)
			{
				if (icache_tags[i][j][iset_index] == itag)
					icache_tags[i][j][iset_index] = INVALID_TAG;
			}
		}
	}
}

/*
 * Returns 1 if the address is cached, 0 otherwise
 */
int lookup_icache(cx_state_t *current, u32 phys, int first_syl)
{
	int set_index = (phys & ICACHE_SET_MASK) >> ICACHE_LINESIZE_LOG2;
	unsigned tag = phys & ICACHE_TAG_MASK;
	int i, j, coupledContextMask;

	if (unified_cache == 1)
		coupledContextMask = ALLCONTEXTS;
	else if (unified_cache == 2)
	{
		coupledContextMask = getCoupledLaneGroups(current);
		if ((coupledContextMask & 0x3) && !(coupledContextMask & 0xc))
			coupledContextMask = 0x3;
		else if ((coupledContextMask & 0xc) && !(coupledContextMask & 0x3))
			coupledContextMask = 0xc;
		//else it's 0x1111 so don't need to change it

	}
	else
		coupledContextMask = getCoupledLaneGroups(current);

	for (i = NCONTEXTS-1; i >= 0; i--) //H when multiple contexts hit, the highest indexed block must service the request
	{
		if (coupledContextMask & (1<<i)) //This block is currently coupled to this context, search it
		{
			for (j = 0; j < ICACHE_NUM_WAYS; j++)
			{
			if (icache_tags[i][j][set_index] == tag)
				{
#ifdef ENABLE_CACHE_TRACE
					if (first_syl)
					{
						if (trace_mode & TRACE_CACHE)
						{
							if (trace_context & (1 << CID(current)))
							{
								if (trace_context != 1)
									printf("Context %d: ", CID(current));
								printf("# fetch for next bundle (address 0x%08X) serviced by icache block %d: hit\n", current->pc, i);
							}
						}
					}
#endif
					return 1;
				}
			}
		}
	}

	return 0;
}

void fill_icache_line(cx_state_t *current, u32 phys)
{
	int set_index = (phys & ICACHE_SET_MASK) >> ICACHE_LINESIZE_LOG2;
	unsigned tag = phys & ICACHE_TAG_MASK;
	int block_index, start_block;
	int i, coupledContextMask;
	int way_index = global_state.cnt & (ICACHE_NUM_WAYS - 1);
	int iw;
	if (unified_cache == 1)
	{
		iw = 8;
		coupledContextMask = ALLCONTEXTS;
	}
	else if (unified_cache == 2)
	{
		coupledContextMask = getCoupledLaneGroups(current);
		if ((coupledContextMask & 0x3) && !(coupledContextMask & 0xc))
		{
			coupledContextMask = 0x3;
			iw = 4;
		}
		else if ((coupledContextMask & 0xc) && !(coupledContextMask & 0x3))
		{
			coupledContextMask = 0xc;
			iw = 4;
		}
		else
			iw = 8;
	}
	else
	{
		iw = issueWidth(current);
		coupledContextMask = getCoupledLaneGroups(current);
	}

	//find the start block; the lowest indexed block assigned to this context
	for (i = NCONTEXTS-1; i >= 0; i--)
	{
		if (coupledContextMask&(1<<i))
			start_block = i;
	}

	/*
	 * The the (iw/2)-1 bits above the set mask determine the block that services this miss;
	 *
	 */
	block_index = (current->pc >> (ICACHE_SETS_LOG2+ICACHE_LINESIZE_LOG2)) & ((iw/2)-1);

	//we need to add the block index to the start block
	block_index += start_block;

	icache_tags[block_index][way_index][set_index] = tag;

#ifdef ENABLE_CACHE_TRACE
	//H TODO: sometimes it prints an old pc. Why?
	if (trace_mode & TRACE_CACHE)
	{
		if (trace_context & (1 << CID(current)))
		{
			if (trace_context != 1)
				printf("Context %d: ", CID(current));
			printf("# fetch for next bundle (address 0x%08X) serviced by icache block %d: miss\n", current->pc, block_index);
	//		printf("Context %d: Adding %d cycles penalty\n", CID(current), imiss_penalty);
		}
	}
#endif
	current->busWaitCycles += imiss_penalty;
	current->cr_imiss++;
}

/*
 * Returns the cache entry if the address is cached, 0 otherwise
 */
static void dcache_access(cx_state_t *current, u32 phys, int fullword, int write)
{
	int set_index = (phys & DCACHE_SET_MASK) >> DCACHE_LINESIZE_LOG2;
	unsigned tag = phys & DCACHE_TAG_MASK;
	struct dcache_entry *d;
	int i, j, coupledContextMask;
	int way_index = global_state.cnt & (DCACHE_NUM_WAYS - 1);
	int block_index, start_block;
	int foundEmptyWritebuf = 1;
	int iw;
	if (unified_cache == 1)
	{
		iw = 8;
		coupledContextMask = ALLCONTEXTS;
	}
	else if (unified_cache == 2)
	{
		coupledContextMask = getCoupledLaneGroups(current);
		if ((coupledContextMask & 0x3) && !(coupledContextMask & 0xc))
		{
			coupledContextMask = 0x3;
			iw = 4;
		}
		else if ((coupledContextMask & 0xc) && !(coupledContextMask & 0x3))
		{
			coupledContextMask = 0xc;
			iw = 4;
		}
		else
			iw = 8;
	}
	else
	{
		iw = issueWidth(current);
		coupledContextMask = getCoupledLaneGroups(current);
	}

	if (write)
		current->cr_dwacc++;
	else
		current->cr_dracc++;


	for (i = NCONTEXTS-1; i >= 0; i--) //H when multiple blocks hit, the highest indexed block must service the request
	{
		if (coupledContextMask & (1<<i)) //This block is currently coupled to this context, search it
		{
			for (j = 0; j < DCACHE_NUM_WAYS; j++)
			{
				d = &dcache[i][j][set_index];
				if (d->tag == tag) //hit
				{
					if (write)
					{
						current->busWaitCycles += 1; //write accesses always have 1 penalty cycle
						invalidate_addr(phys, (1<<i)); //H invalidate the address in all other blocks, because we are going to write to it
					}
#ifdef ENABLE_CACHE_TRACE
					if (trace_mode & TRACE_CACHE)
					{
						if (trace_context & (1 << CID(current)))
						{
							if (trace_context != 1)
								printf("Context %d: ", CID(current));
							if (write)
								printf("# write (%s line) (address 0x%08X) serviced by dcache block %d%s: hit\n", (fullword ? "full" : "partial"), phys, i, (write && !foundEmptyWritebuf) ? " after buffered write was completed" : "");
							else
								printf("# read (address 0x%08X) serviced by dcache block %d: hit\n", phys, i);
						}
					}
#endif
					return;
				}
			}
		}
	}

	//find the start block; the lowest indexed block assigned to this context
	for (i = NCONTEXTS-1; i >= 0; i--)
	{
		if (coupledContextMask&(1<<i))
			start_block = i;
	}

	/*
	 * In case of a read: the (iw/2)-1 bits above the set mask determine the block that services this miss;
	 *
	 */
	block_index = (phys >> (DCACHE_SETS_LOG2+DCACHE_LINESIZE_LOG2)) & ((iw/2)-1);
	block_index += start_block;

	if (!write)
	{
		current->cr_drmiss++;
		current->busWaitCycles += drmiss_penalty;
	}
	else //write
	{
		current->cr_dwmiss++;
		invalidate_addr(phys, (1<<block_index)); //H invalidate the address in all other blocks, because we are going to write to it

		if (!fullword) //in case of a partial line write, we need to fetch the line first
			current->busWaitCycles += drmiss_penalty;

		current->busWaitCycles += dwmiss_penalty;
	}

	d = &dcache[block_index][way_index][set_index];
	d->tag = tag;

#ifdef ENABLE_CACHE_TRACE
	if (trace_mode & TRACE_CACHE)
	{
		if (trace_context & (1 << CID(current)))
		{
			if (trace_context != 1)
				printf("Context %d: ", CID(current));
			if (write)
				printf("# write (%s line) (address 0x%08X) serviced by dcache block %d%s: miss\n", (fullword ? "full" : "partial"), phys, block_index, (write && !foundEmptyWritebuf) ? " after buffered write was completed" : "");
			else
				printf("# read (address 0x%08X) serviced by dcache block %d: miss\n", phys, block_index);
		}
	}
#endif


	return;
}


u32 dcache_access_32(cx_state_t *current, u32 phys)
{
	dcache_access(current, phys, 1, 0);
	last_phys = phys;
	return access_memory_32(phys);

}

u32 dcache_access_16(cx_state_t *current, u32 phys)
{
	dcache_access(current, phys, 0, 0);
	last_phys = phys;
	return access_memory_16(phys);

}

u32 dcache_access_8(cx_state_t *current, u32 phys)
{
	dcache_access(current, phys, 0, 0);
	last_phys = phys;
	return access_memory_8(phys);

}

void dcache_write_32(cx_state_t *current, u32 phys, u32 value)
{
	dcache_access(current, phys, 1, 1);
	last_phys = phys;
	write_memory_32(phys, value); //always write the value to memory
}

void dcache_write_16(cx_state_t *current, u32 phys, u16 value)
{
	dcache_access(current, phys, 0, 1);
	last_phys = phys;
	write_memory_16(phys, value); //always write the value to memory
}

void dcache_write_8(cx_state_t *current, u32 phys, u8 value)
{
	dcache_access(current, phys, 0, 1);
	last_phys = phys;
	write_memory_8(phys, value); //always write the value to memory
}

u32 access_memory_8(u32 phys)
{
	last_phys = phys;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		return memory.byte_array[(phys - MEMORY_START)];
	}
	return 0;
}

u32 access_8(cx_state_t *current, u32 phys)
{
	unsigned char result;
	int rshift;
	u32 word;
	last_phys = phys;
	int retval;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		return memory.byte_array[(phys - MEMORY_START)];
	}

	/*
	 * Access to control registers. First do an aligned word access. Return the byte we want
	 * We don't want to handle the ctrl register stuff everywhere separately.
	 */
	if (phys >= CTRL_REG_START && phys <= (CTRL_REG_START + CTRL_REG_SIZE-1)) {
		word = access_32(current, (phys & ~0x3));
		rshift = (3 - (phys & 0x3)) * 8; //rVEX is big-endian
		return ((word >> rshift) & 0xff);
	}

	retval = plugin_read(CID(current), &result, phys, 1);
	if (retval)
	{
		if (enable_dcache && (retval != 2))
		{
			current->cr_dbypass++;
			current->busWaitCycles += drmiss_penalty;
		}
		return result;
	}

	triggerTrap(current, TRAP_DMEM_FAULT, phys);

	return 0;

}

u32 access_memory_16(u32 phys)
{
	last_phys = phys;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		return SWAP16(memory.short_array[(phys - MEMORY_START) >> 1]);
	}
	return 0;
}

u32 access_16(cx_state_t *current, u32 phys)
{
	unsigned short result;
	int rshift;
	u32 word;
	last_phys = phys;
	int retval;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		return SWAP16(memory.short_array[(phys - MEMORY_START) >> 1]);
	}

	/*
	 * Access to control registers. First do an aligned word access. Return the byte we want
	 * We don't want to handle the ctrl register stuff everywhere separately.
	 */
	if (phys >= CTRL_REG_START && phys <= (CTRL_REG_START + CTRL_REG_SIZE-1)) {
		word = access_32(current, (phys & ~0x3));
		rshift = (3 - (phys & 0x3)) * 16; //rVEX is big-endian
		return ((word >> rshift) & 0xffff);
	}

	retval = plugin_read(CID(current), &result, phys, 2);
	if (retval)
	{
		if (enable_dcache && (retval != 2))
		{
			current->cr_dbypass++;
			current->busWaitCycles += drmiss_penalty;
		}
		return result;
	}

	triggerTrap(current, TRAP_DMEM_FAULT, phys);

	return 0;

}

u32 fetch(cx_state_t *current, u32 phys, int first_syl)
{
	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		if (enable_dcache)
		{
			if (!lookup_icache(current, phys, first_syl))
			{
				fill_icache_line(current, phys);
			}
		}
		return SWAP32(memory.word_array[(phys - MEMORY_START) >> 2]);
	}

	triggerTrap(current, TRAP_FETCH_FAULT, phys);
	return 0;
}

u32 access_memory_32(u32 phys)
{
	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE)))
	{
		return SWAP32(memory.word_array[(phys - MEMORY_START) >> 2]);
	}
	else return 0;
}

u32 access_32(cx_state_t *current, u32 phys)
{
	u32 result;
	last_phys = phys;
	int retval;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		return SWAP32(memory.word_array[(phys - MEMORY_START) >> 2]);
	}
	if (phys >= 0xd0000000 && phys <= 0xe0000000) { //H this is a rather wide check, also it actually depends on the nr of cores in the system
		int context = ((phys>>10)&0xf);

		//TODO: System global registers (keep in mind they are platform dependent!)
		//These should be caught before we go into cregs of other contexts

		//case 0xD0000800:
		//CACHE register: flush and bypass

		//printf("context %d accessing cregs of context %d through debug bus orig addr 0x%x, context creg addr 0x%x\n", CID(current), context, phys, CREG_BASE + (phys-0xd0000000-(context<<10)));
		if (enable_dcache)
		{
			current->cr_dbypass++;
			current->busWaitCycles += drmiss_penalty;
		}
		return access_32(context_state[context], CREG_BASE + (phys-0xd0000000-(context<<10)));

	}
	if (phys >= CTRL_REG_START && phys <= (CTRL_REG_START + CTRL_REG_SIZE-1)) {

		switch (phys) {

		//Global control registers
		case CR_GSR_ADDR:
			return global_state.gsr;
		case CR_CC_ADDR:
			return global_state.cc;
		case CR_AFF_ADDR:
			return global_state.aff;
		case CR_CNT_ADDR:
			return global_state.cnt;
		case CR_CNTH_ADDR:
			return (global_state.cnt >> 24);

#ifdef ALLREGS

	//Long immediate capability registers
		case CR_LIMC0_ADDR:
			return global_state.limc0;
		case CR_LIMC1_ADDR:
			return global_state.limc1;
		case CR_LIMC2_ADDR:
			return global_state.limc2;
		case CR_LIMC3_ADDR:
			return global_state.limc3;
		case CR_LIMC4_ADDR:
			return global_state.limc4;
		case CR_LIMC5_ADDR:
			return global_state.limc5;
		case CR_LIMC6_ADDR:
			return global_state.limc6;
		case CR_LIMC7_ADDR:
			return global_state.limc7;

	//Syllable index capability registers
		case CR_SIC0_ADDR:
			return global_state.sic0;
		case CR_SIC1_ADDR:
			return global_state.sic1;
		case CR_SIC2_ADDR:
			return global_state.sic2;
		case CR_SIC3_ADDR:
			return global_state.sic3;

	//General purpose register delay registers
		case CR_GPS0_ADDR:
			return global_state.gps0;
		case CR_GPS1_ADDR:
			return global_state.gps1;

	//Special delay registers
		case CR_SPS0_ADDR:
			return global_state.sps0;
		case CR_SPS1_ADDR:
			return global_state.sps1;

	//Extension registers
		case CR_EXT0_ADDR:
			return global_state.ext0;
		case CR_EXT1_ADDR:
			return global_state.ext1;
		case CR_EXT2_ADDR:
			return global_state.ext2;

	//Design-time Configuration registers
		case CR_DCFG_ADDR:
			return global_state.dcfg;

	//Core version registers
		case CR_CVER0_ADDR:
			return global_state.cver0;
		case CR_CVER1_ADDR:
			return global_state.cver1;

	//platform version registers
		case CR_PVER0_ADDR:
			return global_state.pver0;
		case CR_PVER1_ADDR:
			return global_state.pver1;

#endif


		//Context control registers
		case CR_LR_ADDR:
			return current->lr;
		case CR_PC_ADDR:
			return current->pc; //H This is actually not supported in hardware (reading PC by the core)
			break;

		case CR_CCR_ADDR:
			return (current->ccr & ~CR_CCR_BRANCH_MASK) | //H place the branch registers into the value
			(current->br[0]&1) << (CR_CCR_BRANCH_BIT + 0) |
			(current->br[1]&1) << (CR_CCR_BRANCH_BIT + 1) |
			(current->br[2]&1) << (CR_CCR_BRANCH_BIT + 2) |
			(current->br[3]&1) << (CR_CCR_BRANCH_BIT + 3) |
			(current->br[4]&1) << (CR_CCR_BRANCH_BIT + 4) |
			(current->br[5]&1) << (CR_CCR_BRANCH_BIT + 5) |
			(current->br[6]&1) << (CR_CCR_BRANCH_BIT + 6) |
			(current->br[7]&1) << (CR_CCR_BRANCH_BIT + 7);
		case CR_SCCR_ADDR:
			return (current->sccr);

		case CR_TH_ADDR:
			return current->th;
		case CR_PH_ADDR:
			return current->ph;

		case CR_TP_ADDR:
			return current->tp;
		case CR_TA_ADDR:
			return current->ta;

		case CR_BR0_ADDR:
			return current->brk[0];
		case CR_BR1_ADDR:
			return current->brk[1];
		case CR_BR2_ADDR:
			return current->brk[2];
		case CR_BR3_ADDR:
			return current->brk[3];

		case CR_DCR_ADDR:
			return current->dcr;  //debug ctrl reg
		case CR_DCR2_ADDR:
			return current->dcr2; //debug ctrl reg 2
		case CR_CRR_ADDR:
			return current->crr; //reconf request reg

			//scratchpad regs
		case CR_SCRP1_ADDR:
			return current->scrp1;
		case CR_SCRP2_ADDR:
			return current->scrp2;
		case CR_SCRP3_ADDR:
			return current->scrp3;
		case CR_SCRP4_ADDR:
			return current->scrp4;

			//Context counters
		case CR_CYC_ADDR:
			return current->cr_cyc;
		case CR_STALL_ADDR:
			return current->cr_stall;
		case CR_BUN_ADDR:
			return current->cr_bun;
		case CR_SYL_ADDR:
			return current->cr_syl;
		case CR_NOP_ADDR:
			return current->cr_nop;
		case CR_STOP_ADDR:
			return current->cr_stop;

			//Cache counters
		case CR_IACC_ADDR:
			return current->cr_iacc;
		case CR_IMISS_ADDR:
			return current->cr_imiss;
		case CR_DRACC_ADDR:
			return current->cr_dracc;
		case CR_DRMISS_ADDR:
			return current->cr_drmiss;
		case CR_DWACC_ADDR:
			return current->cr_dwacc;
		case CR_DWMISS_ADDR:
			return current->cr_dwmiss;
		case CR_DBYPASS_ADDR:
			return current->cr_dbypass;
		case CR_DWBUF_ADDR:
			return current->cr_dwbuf;


			/*
			 * From here it's all bullshit registers you would normally not need
			 */
		#ifdef ALLREGS
		case CR_WCFG_ADDR:
			if (CID(current) == 0) //Only context 0 has a WCFG register
				return current->wcfg; //wake-up config register
			else return 0;
		case CR_SAWC_ADDR:
			if (CID(current) == 0) //Only context 0 has a WCFG register
				return current->sawc; //sleep and wake-up reg
			else return 0;

		case CR_RSC_ADDR:
			if (CID(current) != 0) //Context 0 doesn't have a RSC register
				return current->rsc; //requested software context
			else return 0;
		case CR_CSC_ADDR:
			if (CID(current) != 0) //Context 0 doesn't have a CSC register
				return current->csc; //current software context
			else return 0;

			//H TODO: All of these need to be wired to the registers of the corresponding context

			//Requested/Current swctxt on hwctxt $n$ (normally only needs 4)
#if NCONTEXTS > 1
		case CR_RSC1_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc1;
			else return 0;
		case CR_CSC1_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc1;
			else return 0;
#endif
#if NCONTEXTS > 2
		case CR_RSC2_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc2;
			else return 0;
		case CR_CSC2_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc2;
			else return 0;
#endif
#if NCONTEXTS > 3
		case CR_RSC3_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc3;
			else return 0;
		case CR_CSC3_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc3;
			else return 0;
#endif
#if NCONTEXTS > 4
		case CR_RSC4_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc4;
			else return 0;
		case CR_CSC4_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc4;
			else return 0;
#endif
#if NCONTEXTS > 5
		case CR_RSC5_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc5;
			else return 0;
		case CR_CSC5_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc5;
			else return 0;
#endif
#if NCONTEXTS > 6
		case CR_RSC6_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc6;
			else return 0;
		case CR_CSC6_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc6;
			else return 0;
#endif
#if NCONTEXTS > 7
		case CR_RSC7_ADDR:
			if (CID(current) == 0) //Only context 0 has an RSCn register
				return current->rsc7;
			else return 0;
		case CR_CSC7_ADDR:
			if (CID(current) == 0) //Only context 0 has a CSCn register
				return current->csc7;
			else return 0;
#endif

#endif


		default:
//			triggerTrap(current, TRAP_DMEM_FAULT, phys);
			printf("Warning; read from non-existing (or not implemented) control register\n");
			return 0;
		}
	}
	if ((phys >> 8) == 0x800003) //H GRLIB APB timer peripheral
	{
		switch(phys)
		{
		case TIMER_SCALER_ADDR:
			return global_state.timer_scaler;
		case TIMER_SCALER_RELOAD_ADDR:
			return global_state.timer_scaler_reload;
		case TIMER_CONFIG_ADDR:
			return global_state.timer_config;
		case TIMER_TIM_CTR_ADDR(0):
			return global_state.timer_tim_ctr[0];
		case TIMER_TIM_RELOAD_ADDR(0):
			return global_state.timer_tim_reload[0];
		case TIMER_TIM_CONFIG_ADDR(0):
			return global_state.timer_tim_config[0];
#if NTIMERS >1
		case TIMER_TIM_CTR_ADDR(1):
			return global_state.timer_tim_ctr[1];
		case TIMER_TIM_RELOAD_ADDR(1):
			return global_state.timer_tim_reload[1];
		case TIMER_TIM_CONFIG_ADDR(1):
			return global_state.timer_tim_config[1];
#endif
#if NTIMERS > 2
		case TIMER_TIM_CTR_ADDR(2):
			return global_state.timer_tim_ctr[2];
		case TIMER_TIM_RELOAD_ADDR(2):
			return global_state.timer_tim_reload[2];
		case TIMER_TIM_CONFIG_ADDR(2):
			return global_state.timer_tim_config[2];
#endif
#if NTIMERS > 3
		case TIMER_TIM_CTR_ADDR(3):
			return global_state.timer_tim_ctr[3];
		case TIMER_TIM_RELOAD_ADDR(3):
			return global_state.timer_tim_reload[3];
		case TIMER_TIM_CONFIG_ADDR(3):
			return global_state.timer_tim_config[3];
#endif
		default:
			return 0; //H not sure if this is the correct behavior
		}
	}
	if ((phys >> 8) == 0x800002) //H GRLIB APB IRQ controller peripheral
	{
		switch(phys)
		{
			case IRQ_IRL: //Interrupt Level Register
				break;
			case IRQ_PENDING:
				break;
	//		case IRQ_FORCE: //only exists if NCPU=1
			case IRQ_CLEAR:
				break;
			case IRQ_MPSTAT: //Multi-processor status register
				break;
			case IRQ_BROADCAST:
				break;
			case IRQ_MASK:
				return current->int_mask;
				break;
			case IRQ_FORCE:
				return current->int_force;
				break;
		}
		return 0;
	}
	
	retval = plugin_read(CID(current), &result, phys, 4);
	if (retval)
	{
		if (enable_dcache && (retval != 2))
		{
			current->cr_dbypass++;
			current->busWaitCycles += drmiss_penalty;
		}

		return result;
	}

	triggerTrap(current, TRAP_DMEM_FAULT, phys);

	return 0;
}

void write_memory_8(u32 phys, u8 value)
{
	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE)))
	{
		memory.byte_array[(phys - MEMORY_START)] = value;
		//H invalidate the entry in the simulator's decoded instruction buffer
		invalidate_icache_address(phys, 1);
	}
	else return;
}

void write_8(cx_state_t *current, u32 phys, u8 value)
{
	int lshift;
	u32 word;
	last_phys = phys;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		memory.byte_array[(phys - MEMORY_START)] = value;
		//H invalidate the entry in the simulator's decoded instruction buffer
		invalidate_icache_address(phys, 1);
	}
	/*
	 * Access to control registers. First do an aligned word access. Return the byte we want
	 * We don't want to handle the ctrl register stuff everywhere separately.
	 */
	else if (phys >= CTRL_REG_START && phys <= CTRL_REG_START + CTRL_REG_SIZE-1) {
		word = access_32(current, (phys & ~0x3));
		lshift = (3 - (phys & 0x3)) * 8; //rVEX is big-endian
		write_32(current, phys, ((value << lshift) | (word & ~(0xff << lshift))), 0);
//		if (enable_dcache)
//			current->cr_dbypass--; //H ugly, but the access_32 and write_32 pair will both increment the counter

	}
	else
	{
		int retval = plugin_write(CID(current), &value, phys, 1);
		if (!retval) {
			triggerTrap(current, TRAP_DMEM_FAULT, phys);
		}
		else
		{
			if (enable_dcache && (retval != 2))
			{
				current->cr_dbypass++;
				current->busWaitCycles += dwmiss_penalty;
			}
		}
	}
}

void write_memory_16(u32 phys, u16 value)
{
	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE)))
	{
		memory.short_array[(phys - MEMORY_START) >> 1] = SWAP16(value);
		//H invalidate the entry in the simulator's decoded instruction buffer
		invalidate_icache_address(phys, 2);
	}
	else return;
}

void write_16(cx_state_t *current, u32 phys, u16 value)
{
	int lshift;
	u32 word;
	last_phys = phys;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		memory.short_array[(phys - MEMORY_START) >> 1] = SWAP16(value);
		//H invalidate the entry in the simulator's decoded instruction buffer
		invalidate_icache_address(phys, 2);
	}
	/*
	 * Access to control registers. First do an aligned word access. Return the byte we want
	 * We don't want to handle the ctrl register stuff everywhere separately.
	 */
	else if (phys >= CTRL_REG_START && phys <= CTRL_REG_START + CTRL_REG_SIZE-1) {
		word = access_32(current, (phys & ~0x3));
		lshift = (3 - (phys & 0x3)) * 16; //rVEX is big-endian
		write_32(current, phys, ((value << lshift) | (word & ~(0xffff << lshift))), 0);
//		if (enable_dcache)
//			current->cr_dbypass--; //H ugly, but the access_32 and write_32 pair will both increment the counter
	}
	//TODO: this code does not support accessing global cregs using non-word accesses
	else
	{
		int retval = plugin_write(CID(current), &value, phys, 2);
		if (!retval) {
			triggerTrap(current, TRAP_DMEM_FAULT, phys);
		}
		else
		{
			if (enable_dcache && (retval != 2))
			{
				current->cr_dbypass++;
				current->busWaitCycles += dwmiss_penalty;
			}
		}
	}
}

void write_memory_32(u32 phys, u32 value)
{
	value = SWAP32(value);

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE)))
	{
		memory.word_array[(phys - MEMORY_START) >> 2] = value;
		//H invalidate the entry in the simulator's decoded instruction buffer
		invalidate_icache_address(phys, 4);
	}
	else return;
}

void write_32(cx_state_t *current, u32 phys, u32 value, int debug_bus)
{
	last_phys = phys;

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		memory.word_array[(phys - MEMORY_START) >> 2] = SWAP32(value);
		//H invalidate the entry in the simulator's decoded instruction buffer
		invalidate_icache_address(phys, 4);
	} else if (phys >= 0xd0000000 && phys <= 0xe0000000) { //H debug bus. This is a rather wide check, also it actually depends on the nr of cores in the system
		int context = ((phys>>10)&0xf);

		//TODO: System global registers (keep in mind they are platform dependent!)
		//These should be caught before we go into cregs of other contexts


			//case 0xD0000800:
				//CACHE register: flush and bypass

		//printf("context %d writing cregs of context %d through debug bus orig addr 0x%x, context creg addr 0x%x\n", CID(current), context, phys, CREG_BASE + (phys-0xd0000000-(context<<10)));
		if (enable_dcache)
		{
			current->cr_dbypass++;
			current->busWaitCycles += dwmiss_penalty;
		}
		write_32(context_state[context], CREG_BASE + (phys-0xd0000000-(context<<10)), value, 1); //H TODO: This is the bypass to the debug bus but it is ugly right now. If something goes wrong, the target context gets a trap instead of us.

	} else if (phys >= CTRL_REG_START && phys <= (CTRL_REG_START + CTRL_REG_SIZE-1)) {

//		if (!debug_bus && (current->ccr & CR_CCR_KME_C)) //Kernel mode not enabled
//		{
//			triggerTrap(current, TRAP_DMEM_FAULT, phys);
//			return;
//		}

		switch (phys) {

		//Global control registers (these are read-only for the core)
			case CR_GSR_ADDR:
			case CR_CC_ADDR:
			case CR_AFF_ADDR:
			case CR_CNT_ADDR:
			case CR_CNTH_ADDR:
				break;

	#ifdef ALLREGS

		//Long immediate capability registers
			case CR_LIMC0_ADDR:
			case CR_LIMC1_ADDR:
			case CR_LIMC2_ADDR:
			case CR_LIMC3_ADDR:
			case CR_LIMC4_ADDR:
			case CR_LIMC5_ADDR:
			case CR_LIMC6_ADDR:
			case CR_LIMC7_ADDR:

		//Syllable index capability registers
			case CR_SIC0_ADDR:
			case CR_SIC1_ADDR:
			case CR_SIC2_ADDR:
			case CR_SIC3_ADDR:

		//General purpose register delay registers
			case CR_GPS0_ADDR:
			case CR_GPS1_ADDR:

		//Special delay registers
			case CR_SPS0_ADDR:
			case CR_SPS1_ADDR:

		//Extension registers
			case CR_EXT0_ADDR:
			case CR_EXT1_ADDR:
			case CR_EXT2_ADDR:

		//Design-time Configuration registers
			case CR_DCFG_ADDR:

		//Core version registers
			case CR_CVER0_ADDR:
			case CR_CVER1_ADDR:

		//platform version registers
			case CR_PVER0_ADDR:
			case CR_PVER1_ADDR:
				break;
	#endif //ALLREGS


		//Context control registers

			case CR_CCR_ADDR:
			{
				int nonwrite_bits = current->ccr & 0xffffff00;
				value &= 0xff;
				value = updateFlags(value, CR_CCR_C_MASK, current->ccr);
				value = updateFlags(value, CR_CCR_B_MASK, current->ccr);
				value = updateFlags(value, CR_CCR_R_MASK, current->ccr);
				value = updateFlags(value, CR_CCR_I_MASK, current->ccr);
				current->ccr = nonwrite_bits | value;
				break;
			}
			case CR_SCCR_ADDR:
			{
				int nonwrite_bits = current->sccr & 0xfffffc00;
				value &= 0x3ff;
				value = updateFlags(value, CR_CCR_K_MASK, current->sccr);
				value = updateFlags(value, CR_CCR_C_MASK, current->sccr);
				value = updateFlags(value, CR_CCR_B_MASK, current->sccr);
				value = updateFlags(value, CR_CCR_R_MASK, current->sccr);
				value = updateFlags(value, CR_CCR_I_MASK, current->sccr);
				current->sccr = nonwrite_bits | value;
				break;
			}
			case CR_LR_ADDR:
			case CR_PC_ADDR:
				break;

			case CR_TH_ADDR:
				current->th = value;
				break;
			case CR_PH_ADDR:
				current->ph = value;
				break;

			case CR_TP_ADDR:
				current->tp = value;
				break;
			case CR_TA_ADDR: //not writable by the core
				break;

			case CR_BR0_ADDR:
				current->brk[0] = value;
				break;
			case CR_BR1_ADDR:
				current->brk[1] = value;
				break;
			case CR_BR2_ADDR:
				current->brk[2] = value;
				break;
			case CR_BR3_ADDR:
				current->brk[3] = value;
				break;

			case CR_DCR_ADDR:
				value &= ( //CR_DCR_S_MASK | //H I'm not going to implement stepping in a simulator
						CR_DCR_BR0_MASK | CR_DCR_BR1_MASK |
						CR_DCR_BR2_MASK | CR_DCR_BR3_MASK);
				current->dcr = value;  //debug ctrl reg
				break;
			case CR_DCR2_ADDR:
				value &= (CR_DCR2_RESULT_MASK); //I'm not going to implement tracing in a simulator
//							| CR_DCR2_T_MASK | CR_DCR2_M_MASK | CR_DCR2_R_MASK |
//							CR_DCR2_C_MASK | CR_DCR2_I_MASK);
				current->dcr2 = value; //debug ctrl reg 2
				break;
			case CR_CRR_ADDR:
				current->crr = value; //reconf request reg
				reconfigurationRequested |= (1<<CID(current));
				break;

				//scratchpad regs
			case CR_SCRP1_ADDR:
				current->scrp1 = value;
				break;
			case CR_SCRP2_ADDR:
				current->scrp2 = value;
				break;
			case CR_SCRP3_ADDR:
				current->scrp3 = value;
				break;
			case CR_SCRP4_ADDR:
				current->scrp4 = value;
				break;

				//Context counters
			case CR_CYC_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_cyc = 0;
				else
					clearCounters(current);
				break;
			case CR_STALL_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_stall = 0;
				else
					clearCounters(current);
				break;
			case CR_BUN_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_bun = 0;
				else
					clearCounters(current);
				break;
			case CR_SYL_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_syl = 0;
				else
					clearCounters(current);
				break;
			case CR_NOP_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_nop = 0;
				else
					clearCounters(current);
				break;
			case CR_STOP_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_stop = 0;
				else
					clearCounters(current);
				break;

				//Cache counters
			case CR_IACC_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_iacc = 0;
				else
					clearCounters(current);
				break;
			case CR_IMISS_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_imiss = 0;
				else
					clearCounters(current);
				break;
			case CR_DRACC_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_dracc = 0;
				else
					clearCounters(current);
				break;
			case CR_DRMISS_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_drmiss = 0;
				else
					clearCounters(current);
				break;
			case CR_DWACC_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_dwacc = 0;
				else
					clearCounters(current);
				break;
			case CR_DWMISS_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_dwmiss = 0;
				else
					clearCounters(current);
				break;
			case CR_DBYPASS_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_dbypass = 0;
				else
					clearCounters(current);
				break;
			case CR_DWBUF_ADDR:
				if ((value & 1) == 0) //writing even value; reset this counter
					current->cr_dwbuf = 0;
				else
					clearCounters(current);
				break;


				/*
				 * From here it's all bullshit registers you would normally not need
				 */
#ifdef ALLREGS
			case CR_WCFG_ADDR:
				if (CID(current) == 0) //Only context 0 has aWCFG register
					current->wcfg = value; //wake-up config reg (note: only context 0 has one)
				break;
			case CR_SAWC_ADDR:
				if (CID(current) == 0) //Only context 0 has a SAWC register
					current->sawc = value; //sleep and wake-up reg (note: only context 0 has one)
				break;

			case CR_RSC_ADDR:
				if (CID(current) != 0) //Context 0 doesn't have an RSC register
						current->rsc = value; //requested software context
				break;
			case CR_CSC_ADDR:
				if (CID(current) != 0) //Context 0 doesn't have a CSC register
					current->csc = value; //current software context
				break;

				//Requested swctxt on hwctxt $n$ (normally only needs 4)
#if NCONTEXTS > 1
			case CR_RSC1_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc1 = value;
				break;
			case CR_CSC1_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc1 = value;
				break;
#endif
#if NCONTEXTS > 2
			case CR_RSC2_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc2 = value;
				break;
			case CR_CSC2_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc2 = value;
				break;
#endif
#if NCONTEXTS > 3
			case CR_RSC3_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc3 = value;
				break;
			case CR_CSC3_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc3 = value;
				break;
#endif
#if NCONTEXTS > 4
			case CR_RSC4_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc4 = value;
				break;
			case CR_CSC4_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc4 = value;
				break;
#endif
#if NCONTEXTS > 5
			case CR_RSC5_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc5 = value;
				break;
			case CR_CSC5_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc5 = value;
				break;
#endif
#if NCONTEXTS > 6
			case CR_RSC6_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc6 = value;
				break;
			case CR_CSC6_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc6 = value;
				break;
#endif
#if NCONTEXTS > 7
			case CR_RSC7_ADDR:
				if (CID(current) == 0) //Only context 0 has an RSCn register
					current->rsc7 = value;
				break;
			case CR_CSC7_ADDR:
				if (CID(current) == 0) //Only context 0 has a CSCn register
					current->csc7 = value;
				break;
#endif

#endif

			default:
//				triggerTrap(current, TRAP_DMEM_FAULT, phys);
				printf("Warning; ignoring write to non-existing (or not implemented) Control Register\n");
				break;
		}
		return;
	}
	else if ((phys >> 8) == 0x800005) //H measurement trigger
	{
		return;
	}
	else if ((phys >> 8) == 0x800003) //H GRLIB APB timer peripheral
	{
		switch(phys)
		{
			case TIMER_SCALER_ADDR:
				global_state.timer_scaler = value&0xffff;
				break;
			case TIMER_SCALER_RELOAD_ADDR:
				global_state.timer_scaler_reload = value&0xffff;
				break;
			case TIMER_CONFIG_ADDR:
				//none of these are supported
				break;
			case TIMER_TIM_CTR_ADDR(0):
				global_state.timer_tim_ctr[0] = value;
				break;
			case TIMER_TIM_RELOAD_ADDR(0):
				global_state.timer_tim_reload[0] = value;
				break;
			case TIMER_TIM_CONFIG_ADDR(0):
				global_state.timer_tim_config[0] = value&0x1f;
				break;
#if NTIMERS >1
			case TIMER_TIM_CTR_ADDR(1):
				global_state.timer_tim_ctr[1] = value;
				break;
			case TIMER_TIM_RELOAD_ADDR(1):
				global_state.timer_tim_reload[1] = value;
				break;
			case TIMER_TIM_CONFIG_ADDR(1):
				global_state.timer_tim_config[1] = value&0x1f;
				break;
#endif
#if NTIMERS > 2
			case TIMER_TIM_CTR_ADDR(2):
				global_state.timer_tim_ctr[2] = value;
				break;
			case TIMER_TIM_RELOAD_ADDR(2):
				global_state.timer_tim_reload[2] = value;
				break;
			case TIMER_TIM_CONFIG_ADDR(2):
				global_state.timer_tim_config[2] = value&0x1f;
				break;
#endif
#if NTIMERS > 3
			case TIMER_TIM_CTR_ADDR(3):
				global_state.timer_tim_ctr[3] = value;
				break;
			case TIMER_TIM_RELOAD_ADDR(3):
				global_state.timer_tim_reload[3] = value;
				break;
			case TIMER_TIM_CONFIG_ADDR(3):
				global_state.timer_tim_config[3] = value&0x1f;
				break;
#endif
			default:
				break;
		}
		return;
	}
	else if ((phys >> 8) == 0x800002) //H GRLIB APB IRQ controller peripheral
	{
		//Incmomplete - right now only implementing force and mask registers
		if (phys >= IRQ_FORCE && phys < IRQ_FORCE+(4*NCONTEXTS))
		{
			context_state[(phys-IRQ_FORCE)/4][0].int_force |= (value & 0xfffe);
			context_state[(phys-IRQ_FORCE)/4][0].int_force &= ~((value >>16) & 0xfffe);
//			printf("context %d; IRQ_FORCE of context %d changed to 0x%04x\n", CID(current), (phys-IRQ_FORCE)/4, context_state[(phys-IRQ_FORCE)/4][0].int_force);
		}
		if (phys >= IRQ_MASK && phys < IRQ_MASK+(4*NCONTEXTS))
		{
			context_state[(phys-IRQ_MASK)/4][0].int_mask = (value & 0xfffffffe);
//			printf("context %d; IRQ_MASK of context %d changed to 0x%04x\n", CID(current), (phys-IRQ_MASK)/4, context_state[(phys-IRQ_MASK)/4][0].int_mask);
		}
		/*
		switch(phys)
		{
			case IRQ_IRL: //Interrupt Level Register
				break;
			case IRQ_PENDING:
				break;
	//		case IRQ_FORCE: //only exists if NCPU=1
			case IRQ_CLEAR:
				break;
			case IRQ_MPSTAT: //Multi-processor status register
				break;
			case IRQ_BROADCAST:
				break;
			case IRQ_MASK:
				break;
			case IRQ_FORCE:
				break;
		}*/
		return;
	}
	else 
	{
		int retval = plugin_write(CID(current), &value, phys, 4);
		if (!retval) {
			triggerTrap(current, TRAP_DMEM_FAULT, phys);
		}
		else
		{
			if (enable_dcache && (retval != 2))
			{
				current->cr_dbypass++;
				current->busWaitCycles += dwmiss_penalty;
			}
		}
	}
}

void sim_load_write(const unsigned char *data, int len, unsigned long phys_addr)
{
	u32 phys = (u32) phys_addr;

	if (len <= 0)
		return;

	if ((phys >= MEMORY_START
	     && (phys + len) <= (MEMORY_START + MEMORY_SIZE))) {
		memcpy(memory.byte_array + (phys - MEMORY_START), data, len);
	} else {
		fprintf(stderr,
			"Out of bound memory write at address 0x%x size %d\n",
			phys, len);
	}
}

unsigned char *get_memory_string(u32 phys)
{

	if ((phys >= MEMORY_START && phys < (MEMORY_START + MEMORY_SIZE))) {
		return memory.byte_array + (phys - MEMORY_START);
	}

	return 0;
}

void *sim_mmap(void *addr, u32 length, int prot, int flags,
                  int fd, s32 offset)
{
	u32 oldbrk;
	if (!(flags & MAP_ANONYMOUS))
	{
		printf("unimplemented - mmap without MAP_ANONYMOUS (mapping a file)\n");
		return MAP_FAILED;
	}
	if (sim_brk == 0)
	{
		printf("Program has called mmap, but simrvex was unable to find an \"_end\" symbol (or it is 0)\n");
		return MAP_FAILED;
	}
	oldbrk = sim_brk;
	sim_brk += length;
	return oldbrk;
}

void *sim_sbrk(u32 length)
{
	u32 oldbrk;
	if (sim_brk == 0)
	{
		printf("Program has called mmap, but simrvex was unable to find an \"_end\" symbol (or it is 0)\n");
		return MAP_FAILED;
	}
	oldbrk = sim_brk;
	sim_brk += length;
	return oldbrk;
}

void sim_init_mem(void)
{
	int i;

	init_dcache();
	init_icache();

	for (i = 0; i < MEMORY_SIZE / 4; i++) {
		memory.word_array[i] = 0xbaddbabe;
	}
}
