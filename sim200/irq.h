#ifndef __IRQ_H_
#define __IRQ_H_

void triggerInterrupt(cx_state_t *context, int int_id);

#endif
