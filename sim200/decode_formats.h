#ifndef __DECODE_FORMATS_H__
#define __DECODE_FORMATS_H__

#include "bitops.h"


#define MAIN_FORMAT(x) BITMASK_EXTRACT(24:23,x)
#define STOP_BIT BITMASK(1:1)
#define CLUSTER_BIT BITMASK(0:0)

#define OPCODE(x) BITMASK_EXTRACT(31:24,x)


#define SRC1(x) BITMASK_EXTRACT(16:11,x)
#define SRC2(x) BITMASK_EXTRACT(10:5,x)
#define IDEST(x) BITMASK_EXTRACT(22:17,x)
#define ISRC2(x) BITMASK_EXTRACT(10:2,x)
#define DEST(x) IDEST(x)
#define BDEST(x) BITMASK_EXTRACT(19:17,x) /* rVEX cmp etc. */
#define BDEST2(x) BCOND(x) /* rVEX addcg and divs */
#define SETS_BDEST(x) BITMASK_EXTRACT(24:24,x)
#define IMM(x) BITMASK_EXTRACT(24:2,x)
#define SCOND(x) BITMASK_EXTRACT(26:24,x)
#define BCOND(x) BITMASK_EXTRACT(4:2,x)
#define BTARG(x) BITMASK_EXTRACT(23:5,x)

#define LIMM_MASK ((0x8<<28))
#define LIMM_SEL_MASK(x) ((x) & (0xf<<28))
#define LIMM_INDEX(x) BITMASK_EXTRACT(27:25,x)
#define EXTEND_BIT(x) BITMASK_EXTRACT(23:23,x)
#define BR(x) (BITMASK_EXTRACT(26:25,x) == 0x2)

#define OP1(x) SRC1(x)
#define OP2(x) (EXTEND_BIT(x) ? ISRC2(x) : SRC2(x))
#define OP3(x) IDEST(x)

#define SIGN_EXTEND(x,n) (  ((x)&1<<n) ? ((x)| (~((1<<n)-1))) : (x) )

#define INT3R_ENTRY(x)           { x,            {#x,    INT3R_TYPE}}
#define INT3R_ENTRY_NAME(x,name) { x,            {#name,    INT3R_TYPE}}
#define INT3R_N_ENTRY(x)         { x##_n,        {#x".n",    INT3R_TYPE}}
#define INT3R_PH_ENTRY(x)        { x##_ph,       {#x".ph",    INT3R_TYPE}}
#define INT3R_PB_ENTRY(x)        { x##_pb,       {#x".pb",    INT3R_TYPE}}
#define INT3R_PBH_ENTRY(x)       { x##_pbh,      {#x".pbh",    INT3R_TYPE}}
#define INT3R_PHH_ENTRY(x)       { x##_phh,      {#x".phh",    INT3R_TYPE}}
#define INT3R_PHL_ENTRY(x)       { x##_phh,      {#x".phl",    INT3R_TYPE}}
#define INT3R_PBL_ENTRY(x)       { x##_pbl,      {#x".pbl",    INT3R_TYPE}}
								 
//#define INT3R_ENTRY_CPU(x,cpu)   { x,            {#x,    INT3R_TYPE},cpu}
#define INT3I_ENTRY(x)           { x##_immediate,{#x,    INT3I_TYPE}}
#define INT3I_PB_ENTRY(x)        { x##_pb_immediate,{#x".pb",    INT3I_TYPE}}
#define INT3I_PH_ENTRY(x)        { x##_ph_immediate,{#x".ph",    INT3I_TYPE}}
//#define INT3I_ENTRY_CPU(x,cpu)   { x##_immediate,{#x,    INT3I_TYPE},cpu}

#define BR3R_ENTRY(x)		 { x##_br3r,{#x,    BR3R_TYPE}, CPU_ALL}
						 
#define MONADIC_ENTRY(x)         { x,            {#x,    MONADIC_TYPE}}
#define CMP3R_ENTRY(x)           { x,            {#x,    CMP3R_TYPE}}
#define CMP3R_N_ENTRY(x)         { x##_n,        {#x".n",    CMP3R_TYPE}}
#define CMP3R_PH_ENTRY(x)        { x##_ph,       {#x".ph",    CMP3R_TYPE}}
#define CMP3R_PB_ENTRY(x)        { x##_pb,       {#x".pb",    CMP3R_TYPE}}
#define CMP3R_BR_ENTRY(x)        { x##_bdest,    {#x,    CMP3R_BR_TYPE}}
#define CMP3I_ENTRY(x)           { x##_immediate,    {#x,    CMP3I_TYPE}}
#define CMP3I_BR_ENTRY(x)        { x##_ibdest,   {#x,    CMP3I_BR_TYPE}}
#define SELECTR_ENTRY(x)         { x,            {#x,    SELECTR_TYPE}}
#define SELECTR_ENTRY_NAME(x,name){ x,            {#name,    SELECTR_TYPE}}
#define SELECTI_ENTRY(x)         { x##_immediate,{#x,    SELECTI_TYPE}}
#define CGEN_ENTRY(x)            { x,            {#x,    CGEN_TYPE}}
#define SYSOP_ENTRY(x)           { x,            {#x,    SYSOP_TYPE}}
#define SYSOP_ENTRY_NAME(x,name) { x,            {#name,    SYSOP_TYPE}}
#define SBREAK_ENTRY(x)          { x,            {#x,    SBREAK_TYPE}}
#define LOAD_ENTRY(x)            { x,            {#x,    LOAD_TYPE}}
#define LOAD_PAIR_ENTRY(x)       { x,            {#x,    LOAD_PAIR_TYPE}}
#define LOAD_CONDITIONAL_ENTRY(x){ x,            {#x,    LOAD_CONDITIONAL_TYPE}}
#define LOAD_CONDITIONAL_PAIR_ENTRY(x){ x,            {#x,    LOAD_CONDITIONAL_PAIR_TYPE}}
#define LOAD_ENTRY_DCACHE(x)     { x##_dcache,            {#x,    LOAD_TYPE}}
#define LOAD_DISS_ENTRY(x)       { x##_diss,     {#x".d",LOAD_TYPE}}
#define LOAD_DISS_ENTRY_DCACHE(x){ x##_diss_dcache,     {#x".d",LOAD_TYPE}}
#define LDWL_ENTRY(x)	 { x,            {#x,LDWL_TYPE}}
#define STWL_ENTRY(x)	 { x,            {#x,STWL_TYPE}}
#define STORE_ENTRY(x)           { x,            {#x,    STORE_TYPE}}
#define STORE_PAIR_ENTRY(x)      { x,            {#x,    STORE_PAIR_TYPE}}
#define STORE_CONDITIONAL_ENTRY(x) { x,            {#x,    STORE_CONDITIONAL_TYPE}}
#define STORE_CONDITIONAL_PAIR_ENTRY(x) { x,            {#x,    STORE_CONDITIONAL_PAIR_TYPE}}
#define STORE_ENTRY_DCACHE(x)    { x##_dcache,   {#x,    STORE_TYPE}}
#define STORE_ENTRY_NAME(x,name) { x,            {#name,    STORE_TYPE}}
#define STORE_OTHER_ENTRY(x)           { x,            {#x,    STORE_OTHER_TYPE}}
#define STORE_OTHER_ENTRY_DCACHE(x)    { x##_dcache,   {#x,    STORE_OTHER_TYPE}}
#define STORE_OTHER_ENTRY_NAME(x,name) { x,            {#name, STORE_OTHER_TYPE}}
#define PSW_ENTRY(x)           { x,            {#x,    PSW_TYPE}}
								 
#define CALL_ENTRY(x)            { x,            {#x,    CALL_TYPE}}
#define CALL_ENTRY_NAME(x,name)  { x,            {#name, CALL_TYPE}}
#define BRANCH_ENTRY(x)          { x,            {#x,    BRANCH_TYPE}}

#define PSEUDO_ENTRY(x)          { x,            {#x,    PSEUDO_TYPE}}

#define ILLEGAL_ENTRY            PSEUDO_ENTRY(illegal)
#define ILLEGAL2_ENTRY           ILLEGAL_ENTRY,ILLEGAL_ENTRY
#define ILLEGAL3_ENTRY           ILLEGAL2_ENTRY,ILLEGAL_ENTRY
#define ILLEGAL4_ENTRY           ILLEGAL3_ENTRY,ILLEGAL_ENTRY
#define ILLEGAL5_ENTRY           ILLEGAL4_ENTRY,ILLEGAL_ENTRY
#define ILLEGAL6_ENTRY           ILLEGAL5_ENTRY,ILLEGAL_ENTRY
#define ILLEGAL7_ENTRY           ILLEGAL6_ENTRY,ILLEGAL_ENTRY
#define ILLEGAL8_ENTRY           ILLEGAL7_ENTRY,ILLEGAL_ENTRY
								
/* Defines used for the RVEX decode.
 * As we don't have those nice format bits, these tables will not be as neat as for st200 */
#define RVEX_FORMAT_INTEGER1_ENTRIES  BITMASK_SIZE(27:23)
#define RVEX_FORMAT_INTEGER1_OPC(x) BITMASK_EXTRACT(27:23,x)
#define RVEX_FORMAT_INTEGER2_ENTRIES  BITMASK_SIZE(26:23)
#define RVEX_FORMAT_INTEGER2_OPC(x) BITMASK_EXTRACT(26:23,x)

#define RVEX_FORMAT_MUL_ENTRIES  BITMASK_SIZE(27:23)
#define RVEX_FORMAT_MUL_OPC(x) BITMASK_EXTRACT(27:23,x)

#define RVEX_FORMAT_LOGIC_ENTRIES  BITMASK_SIZE(28:23)
#define RVEX_FORMAT_LOGIC_OPC(x) BITMASK_EXTRACT(28:23,x)

#define RVEX_FORMAT_SYSOP_ENTRIES BITMASK_SIZE(27:23)
#define RVEX_FORMAT_SYSOP_OPC(x) BITMASK_EXTRACT(27:23,x)

#define RVEX_FORMAT_SYSOP2_ENTRIES BITMASK_SIZE(27:23)
#define RVEX_FORMAT_SYSOP2_OPC(x) BITMASK_EXTRACT(27:23,x)


//#define RVEX_FORMAT_CMP_BIT(x) BITMASK_EXTRACT(25:25,x)
#define RVEX_FORMAT_SLCT_ENTRIES 6
//#define RVEX_FORMAT_SLCT_OPC(x) BITMASK_EXTRACT(27:24,x)

#define RVEX_FORMAT_LOAD_STORE_ENTRIES BITMASK_SIZE(26:24)
#define RVEX_FORMAT_LOAD_STORE_OPC(x) BITMASK_EXTRACT(26:24,x)

#define RVEX_FORMAT_CALL_ENTRIES BITMASK_SIZE(27:24)
#define RVEX_FORMAT_CALL_OPC(x) BITMASK_EXTRACT(27:24,x)
//#define RVEX_FORMAT_BRANCH_ENTRIES 2
//#define RVEX_FORMAT_BRANCH_OPC(x) BITMASK_EXTRACT(26:26,x)

#define RVEX_FORMAT_PSEUDO_ENTRIES 3
#define RVEX_FORMAT_LOAD_STORE_FIRST_STORE 0x5
#define RVEX_SIM_SYSCALL_INDEX 1

#endif

