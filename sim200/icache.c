#include <stdlib.h>
#include "icache.h"

/* Enormous array of pointers. Each word of memory is present, so that
 * lookup is *really* fast, and we don't have to deal with the unpleasant
 * problems of decoded bundles crossing cache lines. Memory is cheap!!
 */

//H If you want to run the simulator in GDB, you'll need to dynamically allocate this (init_icache in memory.c)
//struct icache_entry *icache[MEMORY_SIZE / 4];
struct icache_entry **icache[3];

/*
 * Each possible issuewidth needs its own buffer of decoded instructions (could do it per context but that costs more memory and doesn't offer re-use between contexts)
 */
struct icache_entry icache_buffer[3][ICACHE_NUM_ENTRIES];

int icache_next = 0;		/* Holds next entry */

unsigned long invalidate_cycle_count = 0;

void invalidate_icache(void)
{
	invalidate_cycle_count++;

	/* Afer 4billion or so prgins, we need to trash the 
	 * the whole thing the slow way
	 */
	//H after converting to 64bit, it seems unlikely this will be needed
//	if (invalidate_cycle_count == 0) {
//		memset(icache, 0, MEMORY_SIZE);
//	}
//	/* Reset pointer to start of block */
//	icache_next = 0;
}

void invalidate_icache_address(unsigned start, unsigned size)
{
	int i, iw_idx;
	unsigned icache_index_start;
	unsigned icache_index_end;
	unsigned end = start + size - 1;

	icache_index_start = (start - MEMORY_START) >> 3;
	icache_index_end = (end - MEMORY_START) >> 3;

	if (icache_index_start >= MEMORY_SIZE / 8)
		return;

	if (icache_index_end >= MEMORY_SIZE / 8)
		icache_index_end = (MEMORY_SIZE / 8) - 1;

	/* Blat the entries */
	for (iw_idx = 0; iw_idx < 3; iw_idx++) {
		for (i = icache_index_start; i <= icache_index_end; i++) {
			icache[iw_idx][i] = NULL;
		}
	}
}

