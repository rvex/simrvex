#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "state.h"
#include "memory.h"
#include "tlb.h"

#include "checkpoint.h"

#include "rvex.h"

int dump_checkpoint(char *filename, cx_state_t *current)
{
	FILE *f;
	struct checkpoint *check;
	int i;
//	struct tlb_entry *t;

	f = fopen(filename, "wb");

	if (f == NULL) {
		fprintf(stderr, "Cannot open file %s\n", filename);
		return 0;
	}

	check = calloc(1, sizeof(struct checkpoint));

	if (check == NULL) {
		fprintf(stderr, "out of memory");
		return 0;
	}

	strncpy(check->header, "simrvexcheckpoint", 32);

	strncpy(check->vendor, "TUDelft rVEX simulator", 128);
	check->core = 4;
	check->version = 0;

	check->pc = current->pc;

	memcpy(check->registers, current->r, sizeof(unsigned) * 64);

	for (i = 0; i < 8; i++) {
		check->branch |= current->br[i] << i;
	}

	for (i = 0; i < CREG_SIZE; i++) {
		check->ctrl[i] = access_32(current, CREG_BASE + (i * 8));
	}

//	for (i = 0; i < 0x200; i++) {
//		check->intc[i] = access_32(0x1f000000 + (i * 8));
//	}

//	for (i = 0; i < 0x200; i++) {
//		check->dsu[i] = access_32(0x1f003000 + (i * 8));
//	}

//	for (i = 0; i < 64; i++) {
//		t = get_tlb_entry(i);
//		for (j = 0; j < 4; j++) {
//			check->tlb[i][j] = t->entry[j];
//		}
//	}

	check->ram_start = MEMORY_START;
	check->ram_size = MEMORY_SIZE;

	if (fwrite(check, sizeof(struct checkpoint), 1, f) != 1) {
		fprintf(stderr, "Failed to write to checkpoint file\n");
		return 0;
	}

	if (fwrite(get_memory_string(MEMORY_START), 1, MEMORY_SIZE, f) !=
	    MEMORY_SIZE) {
		fprintf(stderr, "Failed to write to checkpoint file\n");
		return 0;
	}

	fclose(f);

	return 1;
}
