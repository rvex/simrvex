Looking at the pluginlibc file, the syntax of the plugin.conf file is something like:

Comments start with #

There are 4 commands;

plugin FILENAME ARGS
	loads a shared object file with some plugin and passes it the args that you give it.

initrd BASE FILENAME
	loads an initrd file at a certain memory location

append ARGS
	adds a string with parameters to the Linux kernel command line parameters

params
	Not sure what this does, I think you can give it a memory address where it can find/should write kernel commandline parameters