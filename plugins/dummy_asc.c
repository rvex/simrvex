#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>

#include <sim200/plugin.h>
#include <sim200/state.h>

/*
 * Let's abuse this file for the local memories for now.
 * I'm thinking of using 1 instance of the plugin for all the memories.
 * The plugin calls will get the context ID as an additional argument.
 * The plugin.conf file will contain additional params;
 * the total number of memories (easier for parsing), and then for each memory;
 * size,
 * the write port connection (bus or context ID) and address mapping,
 * the read ports connections (bus or context IDs) and address mapping
 *
 * I will need to do some more thinking about how to structure this if the cache sharing (nr of read ports)
 * is going to be reconfigurable.
 *
 */

#define LM_READONLY (1<<24)

static struct plugin_callbacks *plugin_callbacks;

static struct plugin dummy_asc_plugin;
static struct memory_bank *memBanks;


unsigned int **memories;

/* COPIED FROM SIM.C
 * Converts a value that encodes a configuration into flags;
 * each nibble (4 bits) represents a context id.
 * For every id in the value, it enables a bit in the result to flag that this context is enabled
 */
inline int convertToFlags(int val)
{
	int i, id;
	int res = 0;
	int count = 0;
	int origval = val;
	for (i = 0; i < NCONTEXTS; i++)
	{
		id = (val&0xf);
		if (id || (i == 0)) //Don't enable context 0 if it is not the first one we find
		{
			res |= (1 << id);
			count++;
		}
		val >>= 4;
	}

	if (count > 1)
		val |= LM_READONLY;

	if (val)
	{
		printf("Error; too many contexts are specified (%x)\n", origval);
		exit(-1);
	}
	return res;
}

void dummy_asc_open(void)
{
}

void dummy_asc_close(void)
{
}

int dummy_asc_read(int contextID, void *dst, u32 address, u32 bytes)
{
	int i;
	unsigned int wordIndex, wordVal, shiftedRead, shift, retval;

	for (i = 0; i < dummy_asc_plugin.num_banks; i++)
	{
		if (address >= memBanks[i].base && address < (memBanks[i].base + memBanks[i].size))
		{
			wordIndex = (address>>2) & (memBanks[i].size-1);
			wordVal = memories[i/2][wordIndex];
			shift = ((address&3)*8);
			shiftedRead = wordVal >> shift; //this value is already shifted. If the access is unaligned, it will generate an error below.

			//Check if the access goes through the bus or if this context is connected to this port
			if (memBanks[i].connection == (1<<31))
			{
				retval = 1;
			}
			else if (memBanks[i].connection & (1<<contextID)) //we are connected to this port
			{
				retval = 2;
			}
			else //we are not connected to this port
			{
				continue;
			}

			if (bytes == 4)
			{
				if (address & 0x3) //misaligned
					return 0;
				*((unsigned *)dst) = wordVal;
				return retval;
			}
			else if (bytes == 2)
			{
				if (address & 0x1) //misaligned
					return 0;
				*((unsigned *)dst) = shiftedRead & 0xffff;
				return retval;
			}
			else if (bytes == 1)
			{
				*((unsigned *)dst) = shiftedRead & 0xff;
				return retval;
			}
		}
	}
	return 0;
}

int dummy_asc_write(int contextID, const void *src, u32 address, u32 bytes)
{
	int i;
	unsigned int wordIndex, mask, shift, retval;

	for (i = 0; i < dummy_asc_plugin.num_banks; i++)
	{
		if (address >= memBanks[i].base && address < (memBanks[i].base + memBanks[i].size))
		{
			wordIndex = (address>>2) & (memBanks[i].size-1);
			shift = ((address&3)*8);
			mask = (((1<<(bytes*8))-1) << shift);

			//Check if the access goes through the bus or if this context is connected to this port
			if (memBanks[i].connection == (1<<31))
			{
				retval = 1;
			}
			else if (memBanks[i].connection & (1<<contextID)) //we are connected to this port
			{
				retval = 2;
			}
			else //we are not connected to this port
			{
				continue;
			}

			//we have found a port. Check if it is a read-only port
			if (memBanks[i].connection & LM_READONLY)
				return 0;

			if (bytes == 4)
			{
				if (address & 0x3) //misaligned
					return 0;
				memories[i/2][wordIndex] = *(unsigned int*)src;
				return retval;
			}
			else if (bytes == 2)
			{
				if (address & 0x1) //misaligned
					return 0;
				memories[i/2][wordIndex] = ((memories[i/2][wordIndex] & ~mask) | ((*(unsigned int*)src << shift) & mask));
				return retval;
			}
			else if (bytes == 1)
			{
				memories[i/2][wordIndex] = ((memories[i/2][wordIndex] & ~mask) | ((*(unsigned int*)src << shift) & mask));
				return retval;
			}
		}
	}
	return 0;
}
/*
static struct memory_bank dummy_asc_mb = {
	.base = DUMMY_ASC_BASE,
	.size = DUMMY_ASC_SIZE,
	.read_fn = dummy_asc_read,
	.write_fn = dummy_asc_write,
};

static struct plugin dummy_asc_plugin = {
	.num_banks = 1,
	.banks = &dummy_asc_mb,
	.close_fn = dummy_asc_close,
};
*/

void check_overlaps(int nports)
{
	int i,j;
	//check for overlaps for each context and the bus
	for (i = 0; i < nports; i++)
	for (j = i; j < nports; j++)
	{
		if (memBanks[i].connection & memBanks[j].connection)
		{
			if (((memBanks[i].base > memBanks[j].base) && (memBanks[i].base < (memBanks[j].base + memBanks[j].size)))
				||
				(((memBanks[i].base + memBanks[i].size) > memBanks[j].base) && ((memBanks[i].base + memBanks[i].size) < (memBanks[j].base + memBanks[j].size))))
			{
				printf("Error; overlapping address mappings in local memories plugin\n"
						"(violating ports: %d and %d, shared connection mask 0x%x)\n",
						i, j, (memBanks[i].connection & memBanks[j].connection));
				exit(-1);
			}
		}
	}
}

struct plugin *init(struct plugin_callbacks *cb, int argc, char *argv[])
{
	int i;
	char **nextarg;
	int nMems;
	int tempval, size;

	nextarg = &argv[2];
	sscanf(argv[1], "%d", &nMems);

	printf("Parsing %d Local Mems\n", nMems);

	memBanks = (struct memory_bank*)malloc(sizeof(struct memory_bank) * (nMems*2)); //2 membanks per memory; 1 for local access, 1 for global
	memories = (unsigned int**)malloc(sizeof(unsigned int*) * nMems);
	dummy_asc_plugin.num_banks = nMems*2;
	dummy_asc_plugin.close_fn = dummy_asc_close;
	dummy_asc_plugin.banks = memBanks;
	for (i = 0; i < nMems*2; i+=2) //nr of memBanks to parse
	{
		memBanks[i].read_fn = dummy_asc_read;
		memBanks[i].write_fn = dummy_asc_write;
		memBanks[i+1].read_fn = dummy_asc_read;
		memBanks[i+1].write_fn = dummy_asc_write;

		//memory size
		sscanf(nextarg[0], "%d", &size);
		memBanks[i].size = size;
		memBanks[i+1].size = size;

		//write port connection and mapping
		if (!strcmp(nextarg[1], "bus"))
		{
			memBanks[i].connection = (1<<31);
		}
		else
		{
			sscanf(nextarg[1], "%x", &tempval);
			if (tempval >= NCONTEXTS)
			{
				printf("Error; no such context or multiple contexts assigned to a write port (%x)\n", tempval);
				exit(-1);
			}
			memBanks[i].connection = convertToFlags(tempval);
		}
		sscanf(nextarg[2], "0x%x", &memBanks[i].base);

		//read ports connection and mapping
		if (!strcmp(nextarg[3], "bus"))
		{
			memBanks[i+1].connection = (1<<31);
		}
		else
		{
			sscanf(nextarg[3], "%x", &tempval);
			memBanks[i+1].connection = convertToFlags(tempval);
		}
		sscanf(nextarg[4], "0x%x", &memBanks[i+1].base);

		memories[i/2] = (unsigned int*)malloc(sizeof(unsigned int) * memBanks[i].size);

		printf("Memory %d (size 0x%x): \n", i/2, memBanks[i].size);
		printf("  Port 1 - connection 0x%x mapped to address 0x%08x,\n", memBanks[i].connection, memBanks[i].base);
		printf("  Port 2 - connection 0x%x mapped to address 0x%08x,\n", memBanks[i+1].connection, memBanks[i+1].base);
		nextarg += 5;
	}
	check_overlaps(nMems*2);

	plugin_callbacks = cb;
	dummy_asc_open();

	printf("Dummy ASC initialized\n");

	return &dummy_asc_plugin;
}
