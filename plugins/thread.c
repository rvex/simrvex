#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>

#include <sim200/plugin_helper.h>

struct dummy_helper_info {
	void (*fn) (void *arg);
	void *arg;
};

static void *dummy_thread_helper(void *arg)
{
	struct dummy_helper_info *d = (struct dummy_helper_info *)arg;
	sigset_t sigset;

	/* We want to ignore the SIGINT, as it should be handled by
	 * the main program only
	 */

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);
	pthread_sigmask(SIG_BLOCK, &sigset, NULL);
	/* Now invoke the program. We leak here but I don't care */
	d->fn(d->arg);
	return NULL;
}

void pluginlib_thread_create(void (*thread) (void *arg), void *arg)
{
	int result;
	static pthread_t tid;
	struct dummy_helper_info *d;

	d = malloc(sizeof(struct dummy_helper_info));
	if (d == NULL) {
		perror("out of memory");
		exit(1);
	}

	d->fn = thread;
	d->arg = arg;

	result = pthread_create(&tid, NULL, dummy_thread_helper, (void *)d);
	if (result != 0) {
		perror("pthread_create");
		exit(1);
	}
}
