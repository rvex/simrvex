/*
 * This code is heavily borrowed from the Solaris PEX AnswerBook
 * available on the web at:
 *   http://docs.sun.com/db/doc/802-1994/6i60663ku?a=view
 *
 * We only support 24 bit true color and 8 bit pseudo color.
 * Anything else you will have to implement yourself.
 */

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <stdlib.h>
#include <stdio.h>

#include "fb.h"

static XStandardColormap *fb_truecolor_map(Display * display,
					   XVisualInfo * visual_info)
{
	XStandardColormap *stdcmap;

	stdcmap = malloc(sizeof(*stdcmap));
	if (stdcmap == NULL)
		return NULL;

	stdcmap->colormap = DefaultColormap(display, DefaultScreen(display));
	stdcmap->visualid = visual_info->visualid;
	stdcmap->killid = ReleaseByFreeingColormap;

	stdcmap->base_pixel = 0;

	stdcmap->red_max = visual_info->red_mask;
	stdcmap->red_mult = 1;
	while (!(stdcmap->red_max & 0x01)) {
		stdcmap->red_max >>= 1;
		stdcmap->red_mult <<= 1;
	}

	stdcmap->green_max = visual_info->green_mask;
	stdcmap->green_mult = 1;
	while (!(stdcmap->green_max & 0x01)) {
		stdcmap->green_max >>= 1;
		stdcmap->green_mult <<= 1;
	}

	stdcmap->blue_max = visual_info->blue_mask;
	stdcmap->blue_mult = 1;
	while (!(stdcmap->blue_max & 0x01)) {
		stdcmap->blue_max >>= 1;
		stdcmap->blue_mult <<= 1;
	}

	return stdcmap;
}

static XStandardColormap *fb_pseudo_stdcmap(Display * display,
					    XVisualInfo * visual_info)
{
	XStandardColormap *stdcmaps, *stdcmap;
	int count;
	int result;

	result = XGetRGBColormaps(display, DefaultRootWindow(display),
				  &stdcmaps, &count, XA_RGB_DEFAULT_MAP);

	if (result != 0) {

		/* Find the matching visual */
		for (stdcmap = stdcmaps; count > 0; stdcmap++, count--) {
			if (stdcmap->visualid == visual_info->visualid)

				/* Success */
				return stdcmap;
		}

		XFree(stdcmaps);
	}
	return NULL;
}

/* Dimensions of the colorcube */
#define NUM_RED		5
#define NUM_GREEN	5
#define NUM_BLUE	5

static XStandardColormap *fb_pseudo_map(Display * display,
					XVisualInfo * visual_info)
{
	int num_colors;
	int red, green, blue;
	unsigned long *pixels;
	unsigned long base_pixel;
	XColor *colors, *color;
	int result;
	Colormap cmap;
	XStandardColormap *stdcmap;

	stdcmap = malloc(sizeof(*stdcmap));
	if (stdcmap == NULL)
		return NULL;

	num_colors = NUM_RED * NUM_GREEN * NUM_BLUE;
	pixels = (unsigned long *)malloc(num_colors * sizeof(*pixels));

	/* First try and get cells in the shared colormap */
	cmap = DefaultColormap(display, DefaultScreen(display));
	result = XAllocColorCells(display, cmap, True,
				  NULL, 0, pixels, num_colors);
	if (result == 0) {
		/* Use a private colormap */
		cmap =
		    XCreateColormap(display,
				    RootWindow(display, DefaultScreen(display)),
				    visual_info->visual, AllocNone);
		result =
		    XAllocColorCells(display, cmap, True, NULL, 0, pixels,
				     num_colors);
		if (result == 0) {
			free(pixels);
			return NULL;
		}
	}

	base_pixel = pixels[0];
	free(pixels);

	stdcmap->colormap = cmap;
	stdcmap->visualid = visual_info->visualid;
	stdcmap->killid = ReleaseByFreeingColormap;

	stdcmap->base_pixel = base_pixel;
	stdcmap->red_max = NUM_RED - 1;
	stdcmap->red_mult = 1;
	stdcmap->green_max = NUM_GREEN - 1;
	stdcmap->green_mult = NUM_RED;
	stdcmap->blue_max = NUM_BLUE - 1;
	stdcmap->blue_mult = NUM_RED * NUM_GREEN;

	colors = (XColor *) malloc(num_colors * sizeof(*colors));
	color = colors;

	for (red = 0; red < NUM_RED; red++) {
		for (green = 0; green < NUM_GREEN; green++) {
			for (blue = 0; blue < NUM_BLUE; blue++) {
				color->flags = DoRed | DoGreen | DoBlue;
				color->pixel = base_pixel +
				    red * stdcmap->red_mult +
				    green * stdcmap->green_mult +
				    blue * stdcmap->blue_mult;
				color->red = red * 65535 / stdcmap->red_max;
				color->green =
				    green * 65535 / stdcmap->green_max;
				color->blue = blue * 65535 / stdcmap->blue_max;
				++color;
			}
		}
	}

	XStoreColors(display, stdcmap->colormap, colors, num_colors);
	free(colors);

	return stdcmap;
}

void
fb_color_setup(Display * display, const char *appname,
	       Visual ** visual_ret, XStandardColormap ** stdcmap_ret)
{

	XVisualInfo visual_info;
	int screen_num;
	XStandardColormap *stdcmap;

	screen_num = DefaultScreen(display);

	if (XMatchVisualInfo(display, screen_num, 24, TrueColor, &visual_info)
	    || XMatchVisualInfo(display, screen_num, 16, TrueColor,
				&visual_info)) {
		/* Great, TrueColor. */

		stdcmap = fb_truecolor_map(display, &visual_info);

	} else
	    if (XMatchVisualInfo
		(display, screen_num, 8, PseudoColor, &visual_info)) {
		/* Boo, PseudoColor. */

		stdcmap = fb_pseudo_stdcmap(display, &visual_info);

		if (!stdcmap) {
			stdcmap = fb_pseudo_map(display, &visual_info);
		}

	} else {
		/* Ahhh, give up */

		stdcmap = NULL;
	}

	if (stdcmap == NULL) {
		fprintf(stderr, "%s: Cannot find a suitable visual\n", appname);
		exit(1);
	}

	*visual_ret = visual_info.visual;
	*stdcmap_ret = stdcmap;
}
