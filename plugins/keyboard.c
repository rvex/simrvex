/* -*- c-basic-offset:2 -*- */

#include <stdlib.h>
#include <stdio.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>

#include <sim200/plugin.h>
#include <sim200/plugin_helper.h>
#include "keyboard.h"

static unsigned char keyboard_buffer[KEYBOARD_BUFFER_SIZE];
static int read_pos;		/* position in buffer */
static int write_pos;
static int m_enabled;		/* Is device switched on ? */
static struct termios otios;
static struct plugin_callbacks *plugin_callbacks;

static void init_state(void)
{
	m_enabled = read_pos = write_pos = 0;;
	plugin_callbacks->clear_interrupt(KEYBOARD_IRQ);
}

static void keyboard_thread(void *arg)
{
	unsigned char c;
	int result;

	do {
		result = read(0, &c, 1);
		if (result < 0) {
			if (errno != EINTR) {
				perror("keyboard read");
				break;
			}
		} else if (result > 0) {
			if (!m_enabled)
				continue;
			/* Discard if no room in buffer */
			if (((write_pos + 1) % KEYBOARD_BUFFER_SIZE) !=
			    read_pos) {
				keyboard_buffer[write_pos] = c;
				write_pos =
				    ((write_pos + 1) % KEYBOARD_BUFFER_SIZE);
			}
		}
		if (read_pos != write_pos) {
			plugin_callbacks->set_interrupt(KEYBOARD_IRQ);
		}
	} while (1);

}

void keyboard_open(void)
{
	struct termios ntios;
	int result;

	init_state();

	result = tcgetattr(0, &otios);
	if (result) {
		perror("tcgetattr");
		return;
	}
	ntios = otios;
	ntios.c_cc[VMIN] = 1;
	ntios.c_cc[VTIME] = 0;
	ntios.c_lflag &= ~ICANON;
	ntios.c_lflag &= ~ECHO;

	result = tcsetattr(0, TCSAFLUSH, &ntios);
	if (result) {
		perror("tcsetattr");
		return;
	}

	pluginlib_thread_create(keyboard_thread, NULL);

	printf("Simulated keyboard initialized\n");

}

void keyboard_close(void)
{
	int result;

	/* Restore terminal to sane mode */
	result = tcsetattr(0, TCSAFLUSH, &otios);
	if (result) {
		perror("tcsetattr");
	}
	printf("Simulated keyboard shutdown\n");
}

int keyboard_read(int contextID, void *dst, u32 address, u32 bytes)
{
	u32 value = 0;

	switch (address) {
	case KEYBOARD_READ_REG:
		if (!m_enabled)
			break;
		value = keyboard_buffer[read_pos];
		read_pos = (read_pos + 1) % KEYBOARD_BUFFER_SIZE;
		if (read_pos == write_pos)
			plugin_callbacks->clear_interrupt(KEYBOARD_IRQ);
		break;
	case KEYBOARD_ENABLE_REG:
		value = m_enabled;
		break;
	default:
		return 0;
	}

	*(unsigned *)dst = htosl(value);
	return 1;
}

int keyboard_write(int contextID, const void *src, u32 address, u32 bytes)
{
	switch (address) {
	case KEYBOARD_READ_REG:
		break;
	case KEYBOARD_ENABLE_REG:
		if (!m_enabled) {
			init_state();
		}
		m_enabled = 1;
		break;
	default:
		return 0;
	}
	return 1;
}

static struct memory_bank keyboard_mb = {
	.base = KEYBOARD_PHYSICAL,
	.size = 8,
	.read_fn = keyboard_read,
	.write_fn = keyboard_write,
};
static struct plugin keyboard_plugin = {
	.num_banks = 1,
	.banks = &keyboard_mb,
	.close_fn = keyboard_close,
};

struct plugin *init(struct plugin_callbacks *cb)
{
	plugin_callbacks = cb;
	keyboard_open();
	return &keyboard_plugin;
}
