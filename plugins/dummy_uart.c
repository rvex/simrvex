#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <pthread.h>
#include <semaphore.h>

#include <sim200/plugin.h>
#include <sim200/plugin_helper.h>

/*
 * To use the terminal without line buffering, do stty -echo -icanon.
 * The following doc is copied from periph_uart_busIface.vhd:
 */

    // The UART has three available registers, mapped at the following
    // addresses.
    //
    // Address                                 Name
    // =====================================   ====
    // 0b--------_--------_--------_----0000   data
    // 0b--------_--------_--------_----0100   stat
    // 0b--------_--------_--------_----1000   ctrl
    //
    // All registers are 8 bit. They have the following fields.
    //
    //            |--7--|--6--|--5--|--4--|--3--|--2--|--1--|--0--|
    //   data (r) |                      RXD                      |
    //            |-----|-----|-----|-----|-----|-----|-----|-----|
    //   data (w) |                      TXD                      |
    //            |-----|-----|-----|-----|-----|-----|-----|-----|
    //   stat (r) |  -  |  -  | ROV | CTI |RXDR | TOV |TXDR |TXDE |
    //            |-----|-----|-----|-----|-----|-----|-----|-----|
    //   stat (w) |  -  |  -  |ROVC |CTIC |  -  |TOVC |  -  |  -  |
    //            |-----|-----|-----|-----|-----|-----|-----|-----|
    // ctrl (r/w) |   RXTL    |ROVE |CTIE |RXDRE|TOVE |TXDRE|TXDEE|
    //            |-----|-----|-----|-----|-----|-----|-----|-----|
    //
    //   RXD - RX data. When read, the topmost byte is popped off of the
    //         receive buffer and is returned. When the buffer is empty, the
    //         returned value is undefined.
    //
    //   TXD - TX data. When written, the written byte is pushed onto the
    //         transmit buffer if room is available. If the buffer is full, the
    //         byte is discarded.
    //
    //  TXDE - TX data register empty. When this flag is set, the transmit FIFO
    // TXDEE   is empty. When TXDEE is set, an interrupt is requested when this
    //         is the case.
    //
    //  TXDR - TX data register ready. When this flag is set, the transmit FIFO
    // TXDRE   is not full and thus ready for new data. When TXDRE is set, an
    //         interrupt is requested when this is the case.
    //
    //   TOV - This flag is set when the application wrote to TXD while the
    //  TOVC   buffer was full. Writing a 1 to TOVC clears this flag. When TOVE
    //  TOVE   is set, an interrupt is requested when TOV is high.
    //
    //  RXDR - RX data ready. This flag is set when there are at least as much
    // RXDRE   characters in the receive buffer as specified by RXTL. When
    //         RXDRE is set, an interrupt is requested when this is the case.
    //
    //   CTI - Character timeout interrupt. This flag is set when the receive
    //  CTIC   FIFO is nonempty while the UART RX line has been idle for at
    //  CTIE   least one character time.  The flag is cleared by writing a 1 to
    //         CTIC. When CTIE is set, an interrupt is requested when CTI is
    //         high. It may be used in conjunction with RXDR and RXTL to
    //         correctly finish reading incoming data packets which are not a
    //         multiple of the value specified by RXTL.
    //
    //   ROV - This flag is set when an incoming byte is discarded because the
    //  ROVC   receive buffer is full. Writing a 1 to ROVC clears this flag.
    //  ROVE   When TOVE is set, an interrupt is requested when ROV is high.
    //
    //  RXTL - RX trigger level. This controls when RXDR is set. The following
    //         encoding is used.
    //           00 => At least 1 character is present in the RX FIFO.
    //           01 => At least 4 characters are present in the RX FIFO.
    //           10 => At least 8 characters are present in the RX FIFO.
    //           11 => At least 14 characters are present in the RX FIFO.
    //
    // NOTE: all of the above registers operate on the UART bytestream as made
    // visible to the application, which is NOT the actual raw UART stream.
    // Characters sent by the application may be escaped to allow unique
    // control characters to be sent over the UART, and debug packets are
    // injected into the stream as well. All this is done transparently
    // however; the receiver performs the inverse operation.

static struct plugin_callbacks *plugin_callbacks;

/*
 * rVEX debug UART peripheral.
 * It is mapped at different addresses for the GRLIB and standalone platform,
 * so I added them both here
 */
#define UART_GRLIB_BASE 0xD1000000
#define UART_GRLIB_DATA_ADDR (UART_GRLIB_BASE)
#define UART_GRLIB_STAT_ADDR (UART_GRLIB_BASE+4)
#define UART_GRLIB_CTRL_ADDR (UART_GRLIB_BASE+8)

#define UART_STANDALONE_BASE 0xF0000000
#define UART_STANDALONE_DATA_ADDR (UART_STANDALONE_BASE)
#define UART_STANDALONE_STAT_ADDR (UART_STANDALONE_BASE+4)
#define UART_STANDALONE_CTRL_ADDR (UART_STANDALONE_BASE+8)

#define UART_INT 	2

//status register (r)
#define TXDE 	(1<<0)
#define TXDR 	(1<<1)
#define TOV 	(1<<2)
#define RXDR 	(1<<3)
#define CTI		(1<<4)
#define ROV 	(1<<5)

//status register (w)
#define TOVC 	(1<<2)
#define CTIC	(1<<4)
#define ROVC 	(1<<5)

//ctrl register (r/w)
#define TXDEE 	(1<<0)
#define TXDRE 	(1<<1)
#define TOVE 	(1<<2)
#define RXDRE 	(1<<3)
#define CTIE	(1<<4)
#define ROVE 	(1<<5)
#define TXTL_BIT 	(6)

int running;

u8 status_reg;
u8 ctrl_reg;
int trigger_level;

u8 rx_fifo[16];
int rx_fifo_ridx;
int rx_fifo_widx;
//H not modeling tx fifo, because we're outputting immediately

//fwd decl
static void uart_thread(void *dummy);

int mod(int a, int b)
{
	int r = a%b;
	if (r < 0)
		r+=b;
	return r;
}

void dummy_uart_open(void)
{
	status_reg = 3; //TXDE and TXDR
	ctrl_reg = 0;
	trigger_level = 0;

	rx_fifo_ridx = 0;
	rx_fifo_widx = 1;

	running = 1;
	pluginlib_thread_create(uart_thread, NULL);
}

void dummy_uart_close(void)
{
	running = 0;
}

void update_trigger_level(int new_level)
{
	switch(new_level){
	case 0:
		trigger_level = 0;
		break;
	case 1:
		trigger_level = 3;
		break;
	case 2:
		trigger_level = 7;
		break;
	case 3:
		trigger_level = 13;
		break;
	default:
		trigger_level = 0;
		break;
	}

}

int dummy_uart_read(int contextID, void *dst, u32 address, u32 bytes)
{
	switch (address){
	case UART_GRLIB_DATA_ADDR:
	case UART_STANDALONE_DATA_ADDR:
		//check if empty
		if (((rx_fifo_ridx+1) & (15)) == rx_fifo_widx)
		{
			status_reg &= ~(RXDR);
		}
		else
		{
			rx_fifo_ridx = ((rx_fifo_ridx+1) & (15));
		}

		//check if dropped below trigger level
		//check if trigger level has been reached
		if (mod((rx_fifo_widx - rx_fifo_ridx),16) <= trigger_level+1)
		{
			status_reg &= ~(RXDR);
		}
		switch (bytes) {
			case 1:
				*(u8*)dst = rx_fifo[rx_fifo_ridx];
				break;
			case 2:
				*(u16*)dst = rx_fifo[rx_fifo_ridx];
				break;
			case 4:
				*(u32*)dst = rx_fifo[rx_fifo_ridx];
				break;
			default:
				return 0;
		}
		return 1;
	case UART_GRLIB_STAT_ADDR:
	case UART_STANDALONE_STAT_ADDR:
		switch (bytes) {
			case 1:
				*(u8*)dst = status_reg;
				break;
			case 2:
				*(u16*)dst = status_reg;
				break;
			case 4:
				*(u32*)dst = status_reg;
				break;
			default:
				return 0;
		}
		return 1;
	case UART_GRLIB_CTRL_ADDR:
	case UART_STANDALONE_CTRL_ADDR:
		switch (bytes) {
			case 1:
				*(u8*)dst = ctrl_reg;
				break;
			case 2:
				*(u16*)dst = ctrl_reg;
				break;
			case 4:
				*(u32*)dst = ctrl_reg;
				break;
			default:
				return 0;
		}
		return 1;
	default:
		return 0;
	}
}

int dummy_uart_write(int contextID, const void *src, u32 address, u32 bytes)
{
	switch (address){
	case UART_GRLIB_DATA_ADDR:
	case UART_STANDALONE_DATA_ADDR:
		fputc(*((unsigned char *)src), stderr);
		return 1;
	case UART_GRLIB_STAT_ADDR:
	case UART_STANDALONE_STAT_ADDR:
		status_reg &= ~(*((unsigned char *)src) & (TOVC|CTIC|ROVC));
		return 1;
	case UART_GRLIB_CTRL_ADDR:
	case UART_STANDALONE_CTRL_ADDR:
		ctrl_reg = *((unsigned char *)src);
		update_trigger_level((ctrl_reg>>TXTL_BIT));
		return 1;
	default:
		return 0;
	}
}

static void uart_thread(void *dummy)
{
	int c;
	struct timeval t;
	fd_set fdp;
	int res;
	while (running)
	{
		FD_ZERO(&fdp);
		FD_SET(0,&fdp);
		t.tv_sec = 0;
		t.tv_usec = 10000; //10 ms
		res = select(1, &fdp, 0, 0, &t);
		if (res == -1)
		{
			perror("select");
			break;
		} else if (res)
		{
			c = getchar();

			if (c == EOF)
				break;

			rx_fifo[rx_fifo_widx] = (u8)c;
			//check for overflow
			if (((rx_fifo_widx+1) & (15)) == rx_fifo_ridx)
			{
				status_reg |= ROV;
				if (ctrl_reg & ROVE)
				{
					plugin_callbacks->set_interrupt(UART_INT);
				}
			}
			else
			{
				rx_fifo_widx = ((rx_fifo_widx+1) & (15));
			}

			//check if trigger level has been reached
			if (mod((rx_fifo_widx - rx_fifo_ridx),16) > trigger_level+1)
			{
				status_reg |= RXDR;
				if (ctrl_reg & RXDRE)
				{
					plugin_callbacks->set_interrupt(UART_INT);
				}
			}
		}
	}
}

static struct memory_bank dummy_uart_mb[2] = {{
	.base = UART_GRLIB_BASE,
	.size = 0xc,
	.read_fn = dummy_uart_read,
	.write_fn = dummy_uart_write,
},
{
	.base = UART_STANDALONE_BASE,
	.size = 0xc,
	.read_fn = dummy_uart_read,
	.write_fn = dummy_uart_write,
}};
static struct plugin dummy_uart_plugin = {
	.num_banks = 2,
	.banks = dummy_uart_mb,
	.close_fn = dummy_uart_close,
};

struct plugin *init(struct plugin_callbacks *cb)
{
	plugin_callbacks = cb;
	dummy_uart_open();

//	printf("rVEX UART Plugin initialized\n");

	return &dummy_uart_plugin;
}
