#ifndef __ETHER_H__
#define __ETHER_H__

#include <semaphore.h>

#define ETHER_BUFFER_SIZE 1600
#define ETHER_Q_LEN 128		/* How many packets we can Q at a time */

struct ether_q {
	u8 buff[ETHER_BUFFER_SIZE];
	int len;		/* Size of data in buffer */
};

struct ether_device {
	int fd;			/* fd for tap device */
	pthread_t tid;		/* Thread Id of reading thread */
	char dev_name[32];	/* Name of tap device */
	struct ether_q q;
	u32 rx_addr;		/* Where to stick incoming packets */
	u32 rx_len;		/* How long the just RX packet was */

	u32 tx_addr;		/* Where in memory to pull packet from */
	u32 tx_len;		/* How long it is */
	u32 enabled;		/* is device switched on ?? */
	int init_ok;		/* Did we manage to set up OK ?? */
	u8 tx_buff[ETHER_BUFFER_SIZE];
	sem_t sync_sem;		/* synchronises data transfer */
};

void ether_open(void);
void ether_close(void);

int ether_read(int contextID, void *dst, u32 address, u32 bytes);
int ether_write(int contextID, const void *src, u32 address, u32 bytes);

#define TAP_ETHER_BASE  0x1f200000
#define TAP_ETHER_TX_ADDR (TAP_ETHER_BASE+0x0)
#define TAP_ETHER_TX_LEN  (TAP_ETHER_BASE+0x4)
#define TAP_ETHER_RX_ADDR (TAP_ETHER_BASE+0x8)
#define TAP_ETHER_RX_LEN  (TAP_ETHER_BASE+0xc)
#define TAP_ETHER_ENABLE  (TAP_ETHER_BASE+0x10)
#define TAP_ETHER_IRQ 4

#endif
