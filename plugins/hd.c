/* -*- c-basic-offset:4 -*- */
/*******************************************************************************
 *                            (C) STMicroelectronics
 *    Reproduction and Communication of this document is strictly prohibited 
 *      unless specifically authorized in writing by STMicroelectronics.
 *------------------------------------------------------------------------------
 * 	                   Advanced System Technology
 *------------------------------------------------------------------------------
 * Created by Thierry Strudel on Wed Apr 23 13:37:20 2003
 ******************************************************************************/

/** 
 * @file dev_hdsim.c 
 * @brief Simulated hard drive device
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <alloca.h>
#include <stdlib.h>
#ifdef __linux__
#include <stdint.h>
#endif

#include <sim200/plugin.h>
#include <sim200/plugin_helper.h>

#define LX_HDSIM_REGION_BASE	0x1fa00000
#define LX_HDSIM_REGION_SIZE	0x00001000

/* the register holding the requested sector of the transfer */
#define LX_HDSIM_RW_SECT	0
/* Write to this reg triggers the read/write to/from the buffer */
#define LX_HDSIM_DO_RW		4
/* The address buffer to transfert to/from */
#define LX_HDSIM_BUFF		8
/* The number of bytes to transfer */
#define LX_HDSIM_RW_LEN		12
/* The disk size (in sectors) */
#define LX_HDSIM_DISK_SIZE	16

#define LX_HDSIM_RW_SECT_REG	(LX_HDSIM_REGION_BASE + LX_HDSIM_RW_SECT)
#define LX_HDSIM_DO_RW_REG	(LX_HDSIM_REGION_BASE + LX_HDSIM_DO_RW)
#define LX_HDSIM_BUFF_REG	(LX_HDSIM_REGION_BASE + LX_HDSIM_BUFF)
#define LX_HDSIM_RW_LEN_REG	(LX_HDSIM_REGION_BASE + LX_HDSIM_RW_LEN)
#define LX_HDSIM_DISK_SIZE_REG	(LX_HDSIM_REGION_BASE + LX_HDSIM_DISK_SIZE)

#define LX_HDSIM_LOG2_HARDSECT 9	/* 512 bytes */
#define LX_HDSIM_HARDSECT (1 << LX_HDSIM_LOG2_HARDSECT)

typedef uint32_t lx_uint32;
typedef uint32_t lx_ptr_ty;

/**
 * The internal state of the hdsim 
 */
/* The sector to read/write from/to */
lx_uint32 sector;
/* The length of the transfer */
lx_uint32 length;
/* The memory address to read/write data from/to */
lx_ptr_ty buff_address;

/* The host file holding the acceded FS */
FILE *fs_file;
char *fs_filename;

int lx_hdsim_debug = 0;
int lx_hdsim_debug_data = 0;

char *lx_hdsim_debug_output_filename;
FILE *lx_hdsim_debug_output_file;

static struct plugin_callbacks *plugin_callbacks;

static int lx_hdsim_data_check_address(unsigned address, int numBytes)
{
	/* See if it is an address we recognize */
	switch (address) {
	case LX_HDSIM_RW_SECT_REG:
	case LX_HDSIM_DO_RW_REG:
	case LX_HDSIM_BUFF_REG:
	case LX_HDSIM_RW_LEN_REG:
	case LX_HDSIM_DISK_SIZE_REG:
		/* It is a known register. Check for full-word operation. */
		if (numBytes != 4) {
			return 0;
		}
		return 1;
	}

	/* Non-existent device register. */
	return 0;
}

static void lx_hdsim_debug_dump_buf(char *tbuf, int length)
{
	int i = 0;
	for (i = 0; i < length; i++) {
		if ((i != 0) && ((i % 2) == 0))
			fprintf(lx_hdsim_debug_output_file, " ");
		if ((i != 0) && ((i % 16) == 0))
			fprintf(lx_hdsim_debug_output_file, "\n");
		fprintf(lx_hdsim_debug_output_file, "%02x",
			(unsigned char)tbuf[i]);
	}
	fprintf(lx_hdsim_debug_output_file, "\n");
}

static int lx_hdsim_data_read(int contextID, void *dst, u32 address, u32 numBytes)
{
	/* Set any methods that are not default. */
	if (!lx_hdsim_data_check_address(address, numBytes)) {
		return 0;
	}

	switch (address) {
	case LX_HDSIM_DISK_SIZE_REG:
		{
			struct stat sbuf;
			if (fstat(fileno(fs_file), &sbuf) != 0) {
				fprintf(stderr,
					"hdsim device: unable to stat file: %s\n",
					strerror(errno));
				return 0;
			}

			*(unsigned *)dst =
			    htosl(sbuf.st_size / LX_HDSIM_HARDSECT);
			return 1;
		}
	}

	return 0;
}

static int lx_hdsim_data_write(int contextID, const void *src, u32 address, u32 numBytes)
{
	unsigned data;

	/* Set any methods that are not default. */
	if (!lx_hdsim_data_check_address(address, numBytes)) {
		return 0;
	}

	data = stohl(*(unsigned *)src);

	switch (address) {
	case LX_HDSIM_RW_SECT_REG:
		sector = data;
		break;
	case LX_HDSIM_BUFF_REG:
		buff_address = data;
		break;
	case LX_HDSIM_RW_LEN_REG:
		length = data;
		break;
	case LX_HDSIM_DO_RW_REG:
		if (lx_hdsim_debug) {
			fprintf(lx_hdsim_debug_output_file,
				"hdsim device: %s transfer, addr 0x%08x, len %d, sec %d\n",
				(data ? "write" : "read"), buff_address, length,
				sector);
		}
		if (fseek(fs_file, sector * LX_HDSIM_HARDSECT, SEEK_SET)) {
			fprintf(stderr, "hdsim device: ERROR - fseek failed\n");
			exit(1);
		} else {
			if (data) {
				/* write transfer */
				char *tbuf;

				tbuf = (char *)alloca(length);

				if (length)
					plugin_callbacks->read_memory(tbuf,
								      buff_address,
								      length);

				if (lx_hdsim_debug && lx_hdsim_debug_data)
					lx_hdsim_debug_dump_buf(tbuf, length);

				if (fwrite(tbuf, length, 1, fs_file) != 1) {
					fprintf(stderr,
						"hdsim device: ERROR - fwrite failed\n");
				} else {
					break;	/* okay ! */
				}
			} else {
				/* read transfer */
				char *tbuf;

				tbuf = (char *)alloca(length);

				if (fread(tbuf, length, 1, fs_file) != 1) {
					fprintf(stderr,
						"hdsim device: ERROR - fread failed\n");
				} else {
					if (lx_hdsim_debug
					    && lx_hdsim_debug_data)
						lx_hdsim_debug_dump_buf(tbuf,
									length);

					plugin_callbacks->write_memory(tbuf,
								       buff_address,
								       length);

					break;	/* okay ! */
				}
			}
		}
	default:
		fprintf(stderr,
			"hdsim device: PERMISSION_ERROR on %s transfer\n",
			(data ? "write" : "read"));

		return 0;
	}
	return 1;
}

/*****************************************************************/

#if 0
static lx_configparam_ty DevParams[] = {
	{"hdsim_file", &(fs_filename), T_STRALLOC,
	 "The file to use as a hard drive. This file must contain a valid ext2 file system of 32Mo."},
	{"hdsim_debug_output_file", &(lx_hdsim_debug_output_filename),
	 T_STRALLOC, "The file in which we dump the debug traces."},
	{"hdsim_do_debug", &(lx_hdsim_debug), T_BOOL,
	 "True if you want to dump debug traces."},
	{"hdsim_do_data_debug", &(lx_hdsim_debug_data), T_BOOL,
	 "True if you want to dump the data being read and wrote."},
	{NULL}
};
#endif

static void lx_hdsim_new_device(void)
{
#if 1
	fs_filename = "hdd.img";
	lx_hdsim_debug_output_filename = "hdd.dbg";
	lx_hdsim_debug = 0;
	lx_hdsim_debug_data = 0;
#endif

	/* We open the file */
	fs_file = fopen(fs_filename, "r+");

	if (lx_hdsim_debug) {
		lx_hdsim_debug_output_file =
		    fopen(lx_hdsim_debug_output_filename, "w");
		setlinebuf(lx_hdsim_debug_output_file);
	}

	if (!fs_file) {
		fprintf(stderr,
			"hdsim device: exiting - could not open file %s\n",
			fs_filename);
		exit(1);
	}

	printf("hdsim device: Simulated Hard Drive initialised\n");
}

static void lx_hdsim_remove_device(void)
{
	fclose(fs_file);
}

static struct memory_bank hd_mb = {
	.base = LX_HDSIM_REGION_BASE,
	.size = LX_HDSIM_REGION_SIZE,
	.read_fn = lx_hdsim_data_read,
	.write_fn = lx_hdsim_data_write,
};
static struct plugin hd_plugin = {
	.num_banks = 1,
	.banks = &hd_mb,
	.close_fn = lx_hdsim_remove_device,
};

struct plugin *init(struct plugin_callbacks *cb)
{
	plugin_callbacks = cb;
	lx_hdsim_new_device();
	return &hd_plugin;
}
