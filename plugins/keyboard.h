void keyboard_open(void);
void keyboard_close(void);

int keyboard_read(int contextID, void *dst, u32 address, u32 bytes);
int keyboard_write(int contextID, const void *src, u32 address, u32 bytes);

#define KEYBOARD_BUFFER_SIZE 128

#define KEYBOARD_PHYSICAL 0x1f100000
#define KEYBOARD_READ_REG  KEYBOARD_PHYSICAL
#define KEYBOARD_ENABLE_REG (KEYBOARD_PHYSICAL+4)

#define KEYBOARD_IRQ 3
