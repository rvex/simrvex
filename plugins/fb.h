#ifndef __FB_H__
#define __FB_H__

void
fb_color_setup(Display * display, const char *appname,
	       Visual ** visual_ret, XStandardColormap ** stdcmap_ret);

#define FB_CTRL_BASE	0x20000000
#define FB_CTRL_SIZE	0x00000400
#define FB_FB_BASE	0x20100000
#define FB_FB_SIZE	0x00500000	/*  1280*1024*32 */

#define FB_KBD_IRQ	5
#define FB_MOUSE_IRQ	6

#endif
