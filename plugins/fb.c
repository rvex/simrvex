/* -*- c-basic-offset:2 -*- */

#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/cursorfont.h>
#include <X11/xpm.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include <pthread.h>
#include <semaphore.h>

#define _GNU_SOURCE
#include <getopt.h>

#include <sim200/plugin.h>
#include <sim200/plugin_helper.h>
#include "fb.h"
#include "teeshirt.xpm"
#include "teeshirt-small.xpm"

#define SIMFB_MAGIC		0x000
#define SIMFB_COMMAND		0x004
#define SIMFB_WIDTH		0x008
#define SIMFB_HEIGHT		0x00c
#define SIMFB_DEPTH		0x010

#define SIMFB_COMMAND_SETMODE	1

#define SIMFB_MAGIC_VAL		0xdeface01

#define SIMFB_KBD_STATUS	0x100
#define SIMFB_KBD_KEY		0x104

#define SIMFB_KBD_STATUS_INT	1

#define SIMFB_MOUSE_STATUS	0x200
#define SIMFB_MOUSE_X		0x204
#define SIMFB_MOUSE_Y		0x208
#define SIMFB_MOUSE_BUTTONS	0x20c

#define SIMFB_MOUSE_STATUS_INT		1
#define SIMFB_MOUSE_STATUS_RESYNC	2

#define SIMFB_LEDS		0x300

/*  Global variables  */

static Display *display;
static GC led_gc, fb_gc;
static Visual *visual;
static int screen_num;
static Window top_win, led_win, fb_win;
static XImage *fb_image;
static Pixmap splash_pixmap;
static int splash_valid = 0;
static int splash_width, splash_height;
static XStandardColormap *stdcmap;
static char *appname;
static Cursor fb_cross_cursor, fb_no_cursor;

static struct plugin_callbacks *plugin_callbacks;

static struct {
	char *name;
	XColor color;
} colors[] = {
	{
	"black"}, {
	"white"}, {
	"red"}, {
	"green"}
};

#define COLORS_BLACK	0
#define COLORS_WHITE	1
#define COLORS_RED	2
#define COLORS_GREEN	3

static u32 ctrl_regs[3];
static u32 kbd_regs[3];
static u32 mouse_regs[4];
static u32 led_regs[1];

static unsigned int fb_width = 1024;
static unsigned int fb_height = 768;
static unsigned fb_depth = 32;
static unsigned int fb_max_width = 1280;
static unsigned int fb_max_height = 1024;
static unsigned fb_max_depth = 32;
static volatile unsigned char *fb_mem;

static sem_t sem;

static volatile int minx = INT_MAX, miny = INT_MAX, maxx = -1, maxy = -1;
static volatile int notified = 0;
static int p[2];
static int motion = 0;

#define DEFAULT_EVENTS				\
	ExposureMask |				\
	KeyPressMask | KeyReleaseMask |	 	\
	ButtonPressMask | ButtonReleaseMask

#define LED_WIN_X	0
#define LED_WIN_Y	0
#define LED_WIN_WIDTH	100
#define LED_WIN_HEIGHT	18
#define FB_WIN_X	0
#define FB_WIN_Y	(LED_WIN_Y+LED_WIN_HEIGHT+(2*LED_WIN_BORDER))
#define LED_WIN_BORDER	1
#define FB_WIN_BORDER	1

/* Forward declarations */
static void fb_thread(void *dummy);

void fb_open(const char *display_name, int iconic)
{
	int argc = 1;
	char *argv[1] = { "SimFB" };
	int result;
	int pad;
	int depth;

	/*  Window variables  */

	int x, y;
	char *window_name = "SimFB";
	char *icon_name = "SimFB";

	/*  Miscellaneous X variables  */

	XSizeHints *size_hints;
	XWMHints *wm_hints;
	XClassHint *class_hints;
	XTextProperty windowName, iconName;
	Pixmap pixmap, pixmap_mask;

	appname = argv[0];

#if 0
	/* We don't need to do this any more, as the only multi-threaded
	 * operation is on the fb_mem, which is protected the by the semaphore.
	 */

	/* Initialise multi-threaded support */
	if (XInitThreads() == 0) {
		fprintf(stderr,
			"%s: Xlib doesn't support multi-threaded apps\n",
			appname);
		exit(EXIT_FAILURE);
	}
#endif

	/*  Allocate memory for our structures  */

	if (!(size_hints = XAllocSizeHints()) ||
	    !(wm_hints = XAllocWMHints()) ||
	    !(class_hints = XAllocClassHint())) {
		fprintf(stderr, "%s: couldn't allocate memory.\n", appname);
		exit(EXIT_FAILURE);
	}

	/*  Connect to X server  */

	if ((display = XOpenDisplay(display_name)) == NULL) {
		fprintf(stderr, "%s: couldn't connect to X server %s\n",
			appname, display_name);
		exit(EXIT_FAILURE);
	}

	screen_num = DefaultScreen(display);
	depth = DefaultDepth(display, screen_num);

	/* Set up the color related details */

	fb_color_setup(display, appname, &visual, &stdcmap);

	/* Create the colors */

	{
		XColor exact_color;
		int i;

		for (i = 0; i < sizeof(colors) / sizeof(colors[0]); i++) {
			if (!XAllocNamedColor(display, stdcmap->colormap,
					      colors[i].name, &colors[i].color,
					      &exact_color)) {
				fprintf(stderr, "%s: Cannot alloc color: %s\n",
					appname, colors[i].name);
				exit(1);
			}
		}
	}

	/* Allocate memory for the frame buffer */

	fb_mem = calloc(fb_max_width * fb_max_height, fb_max_depth / 8);
	if (fb_mem == NULL) {
		fprintf(stderr, "%s: couldn't allocate fb memory\n", appname);
		exit(EXIT_FAILURE);
	}

	/* Create an X Image for our frame buffer */

	if (depth == 8)
		pad = 8;
	else
		pad = 32;

	fb_image = XCreateImage(display, visual,
				depth, ZPixmap,
				0, 0, fb_max_width, fb_max_height, pad, 0);
	if (fb_image == NULL) {
		fprintf(stderr, "%s: couldn't allocate XImage\n", appname);
		exit(EXIT_FAILURE);
	}

	if ((fb_image->data =
	     malloc(fb_image->bytes_per_line * fb_max_height)) == NULL) {
		fprintf(stderr, "%s: couldn't allocate XImage data\n", appname);
		exit(EXIT_FAILURE);
	}

	/*  Set initial window size and position, and create it  */

	x = y = 0;
	top_win = XCreateWindow(display, RootWindow(display, screen_num),
				x, y,
				fb_width + (2 * FB_WIN_BORDER),
				fb_height + (2 * FB_WIN_BORDER) +
				LED_WIN_HEIGHT + (2 * LED_WIN_BORDER), 0, depth,
				InputOutput, visual, 0, NULL);
	XSetWindowColormap(display, top_win, stdcmap->colormap);
	XSetWindowBackground(display, top_win,
			     colors[COLORS_BLACK].color.pixel);

	led_win = XCreateWindow(display, top_win,
				LED_WIN_X, LED_WIN_Y, LED_WIN_WIDTH,
				LED_WIN_HEIGHT, LED_WIN_BORDER, depth,
				InputOutput, visual, 0, NULL);
	XSetWindowColormap(display, led_win, stdcmap->colormap);
	XSetWindowBackground(display, led_win,
			     colors[COLORS_BLACK].color.pixel);
	XSetWindowBorder(display, led_win, colors[COLORS_WHITE].color.pixel);
	XMapWindow(display, led_win);

	fb_win = XCreateWindow(display, top_win,
			       FB_WIN_X, FB_WIN_Y, fb_width, fb_height,
			       FB_WIN_BORDER,
			       depth, InputOutput, visual, 0, NULL);
	XSetWindowColormap(display, fb_win, stdcmap->colormap);
	XSetWindowBackground(display, fb_win, colors[COLORS_BLACK].color.pixel);
	XSetWindowBorder(display, fb_win, colors[COLORS_WHITE].color.pixel);
	XMapWindow(display, fb_win);

	/* Create the pixmap */
	/* Note a failure is not fatal, we just don't set it */

	result = XpmCreatePixmapFromData(display, top_win, teeshirt_small,
					 &pixmap, &pixmap_mask, NULL);

	/*  Set hints for window manager before mapping window  */

	if (XStringListToTextProperty(&window_name, 1, &windowName) == 0) {
		fprintf(stderr,
			"%s: structure allocation for windowName failed.\n",
			appname);
		exit(EXIT_FAILURE);
	}

	if (XStringListToTextProperty(&icon_name, 1, &iconName) == 0) {
		fprintf(stderr,
			"%s: structure allocation for iconName failed.\n",
			appname);
		exit(EXIT_FAILURE);
	}

	size_hints->flags = PPosition | PSize | PMinSize;
	size_hints->min_width = 200;
	size_hints->min_height = 100;

	wm_hints->flags = StateHint | InputHint;
	wm_hints->initial_state = (iconic ? IconicState : NormalState);
	wm_hints->input = True;

	if (result == 0) {
		wm_hints->flags |= IconPixmapHint | IconMaskHint;
		wm_hints->icon_pixmap = pixmap;
		wm_hints->icon_mask = pixmap_mask;
	}

	class_hints->res_name = appname;
	class_hints->res_class = appname;

	XSetWMProperties(display, top_win, &windowName, &iconName, argv, argc,
			 size_hints, wm_hints, class_hints);

	/*  Choose which events we want to handle  */

	XSelectInput(display, led_win, ExposureMask);
	XSelectInput(display, fb_win, DEFAULT_EVENTS);

	/*  Create graphics context  */

	{
		XGCValues values;
		unsigned long valuemask = 0;

		memset(&values, 0, sizeof(values));

		values.function = GXcopy;
		valuemask |= GCFunction;
		values.plane_mask = -1;
		valuemask |= GCPlaneMask;
		values.fill_style = FillSolid;
		valuemask |= GCFillStyle;

		led_gc = XCreateGC(display, led_win, valuemask, &values);
		fb_gc = XCreateGC(display, fb_win, valuemask, &values);
	}

	/* Create the fb cursors */

	{
		static char blank_bitmap[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
		Pixmap blank_pixmap;

		fb_cross_cursor = XCreateFontCursor(display, XC_crosshair);

		blank_pixmap = XCreateBitmapFromData(display, fb_win,
						     blank_bitmap, 8, 8);
		if (blank_pixmap == None) {
			fprintf(stderr, "%s: Unable to create bitmap\n",
				appname);
			exit(1);
		}

		fb_no_cursor =
		    XCreatePixmapCursor(display, blank_pixmap, blank_pixmap,
					&colors[COLORS_BLACK].color,
					&colors[COLORS_BLACK].color, 0, 0);
	}

	/* Set up the pixmap for the splash screen */

	{
		XpmAttributes attributes;
		attributes.valuemask = XpmSize;
		result = XpmCreatePixmapFromData(display, fb_win, teeshirt,
						 &splash_pixmap, NULL,
						 &attributes);
		if (result == 0) {
			splash_valid = 1;
			splash_width = attributes.width;
			splash_height = attributes.height;
		}
	}

	/* Finally, display the window */

	XMapWindow(display, top_win);

	/* Create the synchronisation pipe */

	if (pipe(p) != 0) {
		fprintf(stderr, "%s: Failed to create pipe\n", appname);
		exit(1);
	}

	/* Initialise the shared sem */

	sem_init(&sem, 0, 1);

	/* Kick off the slave thread */
	pluginlib_thread_create(fb_thread, NULL);

}

void fb_close(void)
{

}

static inline void fb_int_clear(u32 * regp, u32 value, u32 mask)
{
	if (value & mask) {
		/* Clearing bit */
		*regp = value & (~mask);
	} else {
		/* Ignoring mask */
		*regp = (*regp & mask) | (value & (~mask));
	}
}

int fb_write_ctrl(int contextID, const void *src, u32 address, u32 bytes)
{
	u32 value = stohl(*(u32 *) src);
	address -= FB_CTRL_BASE;

	switch (address) {
	case SIMFB_COMMAND:
		switch (value) {
		case SIMFB_COMMAND_SETMODE:
			{
				char dummy = 1;
				sem_wait(&sem);
				fb_width = ctrl_regs[0];
				fb_height = ctrl_regs[1];
				fb_depth = ctrl_regs[2];
				write(p[1], &dummy, 1);
				sem_post(&sem);
				break;
			}
		default:
			fprintf(stderr, "Illegal FB command (value %d)\n",
				value);
			exit(1);
		}
		break;
	case SIMFB_WIDTH:
	case SIMFB_HEIGHT:
	case SIMFB_DEPTH:
		ctrl_regs[(address - SIMFB_WIDTH) / 4] = value;
		break;
	case SIMFB_KBD_STATUS:
		sem_wait(&sem);
		if (value & SIMFB_KBD_STATUS_INT) {
			plugin_callbacks->clear_interrupt(FB_KBD_IRQ);
		}
		fb_int_clear(&kbd_regs[(address - SIMFB_KBD_STATUS) / 4],
			     value, SIMFB_KBD_STATUS_INT);
		sem_post(&sem);
		break;
	case SIMFB_MOUSE_STATUS:
		sem_wait(&sem);
		if (value & SIMFB_MOUSE_STATUS_INT) {
			plugin_callbacks->clear_interrupt(FB_MOUSE_IRQ);
		}
		fb_int_clear(&mouse_regs[(address - SIMFB_MOUSE_STATUS) / 4],
			     value, SIMFB_MOUSE_STATUS_INT);
		sem_post(&sem);
		break;
	case SIMFB_LEDS:
		{
			char dummy = 2;
			unsigned long current;

			sem_wait(&sem);
			current = led_regs[(address - SIMFB_LEDS) / 4];
			if (current != value) {
				led_regs[(address - SIMFB_LEDS) / 4] = value;
				write(p[1], &dummy, 1);
			}
			sem_post(&sem);
			break;
		}
	default:
		return 0;
	}
	return 1;
}

int fb_read_ctrl(int contextID, void *dst, u32 address, u32 bytes)
{
	u32 ret;
	address -= FB_CTRL_BASE;

	switch (address) {
	case SIMFB_MAGIC:
		ret = SIMFB_MAGIC_VAL;
		break;
	case SIMFB_WIDTH:
	case SIMFB_HEIGHT:
	case SIMFB_DEPTH:
		ret = ctrl_regs[(address - SIMFB_WIDTH) / 4];
		break;
	case SIMFB_KBD_STATUS:
	case SIMFB_KBD_KEY:
		ret = kbd_regs[(address - SIMFB_KBD_STATUS) / 4];
		break;
	case SIMFB_MOUSE_STATUS:
	case SIMFB_MOUSE_X:
	case SIMFB_MOUSE_Y:
	case SIMFB_MOUSE_BUTTONS:
		ret = mouse_regs[(address - SIMFB_MOUSE_STATUS) / 4];
		break;
	default:
		return 0;
	}
	*(u32 *) dst = htosl(ret);
	return 1;
}

int fb_read_fb(int contextID, void *dst, u32 address, u32 bytes)
{
	address -= FB_FB_BASE;
	if (address >= FB_FB_SIZE) {
		fprintf(stderr, "Illegal FB write (offset %d)\n", address);
		exit(1);
	}

	switch (bytes) {
	case 1:
		*(u8 *) dst = fb_mem[address];
		return 1;
	case 2:
		*(u16 *) dst = htoss(*(u16 *) (&fb_mem[address]));
		return 1;
	case 4:
		*(u32 *) dst = htosl(*(u32 *) (&fb_mem[address]));
		return 1;
	default:
		return 0;
	}
	return 1;
}

/*
 * 16bpp fb -> 32bpp image:
 *    pixel = (red << 19) | (green << 10) | (blue << 3);
 * 32bpp fb -> 32bpp image:
 *    pixel = (red << 16) | (green <<  8) | (blue << 0);
 */
static inline unsigned long
fb_stdcmap_lookup(unsigned long red, int red_bits,
		  unsigned long green, int green_bits,
		  unsigned long blue, int blue_bits)
{
	unsigned long pixel;

#define CALC(val, fb_bits, image_max) ( (((val) * (image_max)) + (1<<(fb_bits-1)) ) >> fb_bits )

	pixel = stdcmap->base_pixel +
	    (CALC(red, red_bits, stdcmap->red_max) * stdcmap->red_mult) +
	    (CALC(green, green_bits, stdcmap->green_max) *
	     stdcmap->green_mult) + (CALC(blue, blue_bits,
					  stdcmap->blue_max) *
				     stdcmap->blue_mult);

#undef CALC

	return pixel;
}

int fb_write_fb(int contextID, const void *src, u32 address, u32 bytes)
{
	u32 value;
	int x, y;
	unsigned tmp;
	unsigned pixel_address;
	unsigned aligned_value;
	int extra;

	address -= FB_FB_BASE;
	if (address >= FB_FB_SIZE) {
		fprintf(stderr, "Illegal FB write offset %d)\n", address);
		exit(1);
	}

	switch (bytes) {
	case 1:
		tmp = fb_mem[address];
		value = *(u8 *) src;
		if (tmp == value)
			return 1;
		fb_mem[address] = value;
		aligned_value = *(u32 *) (&fb_mem[address & ~3]);
		break;
	case 2:
		tmp = *(u16 *) (&fb_mem[address]);
		value = stohs(*(u16 *) src);
		if (tmp == value)
			return 1;
		*(u16 *) (&fb_mem[address]) = value;
		aligned_value = *(u32 *) (&fb_mem[address & ~3]);
		break;
	case 4:
		tmp = *(u32 *) (&fb_mem[address]);
		value = stohl(*(u32 *) src);
		if (tmp == value)
			return 1;
		*(u32 *) (&fb_mem[address]) = value;
		aligned_value = value;
		break;
	default:
		return 0;
	}

	pixel_address = address / (fb_depth / 8);
	y = pixel_address / fb_width;
	x = pixel_address % fb_width;

	sem_wait(&sem);

	if (fb_depth == 32) {
		unsigned int red, green, blue;
		unsigned long pixel;
		red = (aligned_value >> 16) & 0xff;
		green = (aligned_value >> 8) & 0xff;
		blue = (aligned_value >> 0) & 0xff;
		pixel = fb_stdcmap_lookup(red, 8, green, 8, blue, 8);
		XPutPixel(fb_image, x, y, pixel);
		extra = 0;
	} else {
		unsigned int red, green, blue;
		unsigned int pixel;

		if (bytes == 2) {
			red = (value >> 11) & 0x1f;
			green = (value >> 5) & 0x3f;
			blue = (value >> 0) & 0x1f;
			pixel = fb_stdcmap_lookup(red, 5, green, 6, blue, 5);
			XPutPixel(fb_image, x, y, pixel);
			extra = 0;
		} else {
			x &= ~1;

			red = (aligned_value >> 11) & 0x1f;
			green = (aligned_value >> 5) & 0x3f;
			blue = (aligned_value >> 0) & 0x1f;
			pixel = fb_stdcmap_lookup(red, 5, green, 6, blue, 5);
			XPutPixel(fb_image, x, y, pixel);

			red = (aligned_value >> (16 + 11)) & 0x1f;
			green = (aligned_value >> (16 + 5)) & 0x3f;
			blue = (aligned_value >> (16 + 0)) & 0x1f;
			pixel = fb_stdcmap_lookup(red, 5, green, 6, blue, 5);
			XPutPixel(fb_image, x + 1, y, pixel);
			extra = 1;
		}
	}

	if (x < minx)
		minx = x;
	if (y < miny)
		miny = y;
	if ((x + extra) > maxx)
		maxx = x + extra;
	if (y > maxy)
		maxy = y;
	if (!notified) {
		char dummy = 0;
		notified = 1;
		write(p[1], &dummy, 1);
	}

	sem_post(&sem);
	return 1;
}

static void fb_display_led(void)
{
#define LED_RADIUS	5
#define LED_CENTER	(LED_WIN_HEIGHT / 2)
#define LED_GAP		5
	int i;
	for (i = 0; i < 6; i++) {
		int x;
		int color;
		x = (LED_CENTER - LED_RADIUS) +
		    (i * ((LED_RADIUS * 2) + LED_GAP));
		if (i == 0) {
			color = colors[COLORS_GREEN].color.pixel;
		} else if (led_regs[0] & (1 << (i - 1))) {
			color = colors[COLORS_RED].color.pixel;
		} else {
			color = colors[COLORS_BLACK].color.pixel;
		}
		XSetForeground(display, led_gc, color);
		XFillArc(display, led_win, led_gc,
			 x, LED_CENTER - LED_RADIUS,
			 LED_RADIUS * 2, LED_RADIUS * 2, 0, 360 * 64);
		XSetForeground(display, led_gc,
			       colors[COLORS_WHITE].color.pixel);
		XDrawArc(display, led_win, led_gc, x, LED_CENTER - LED_RADIUS,
			 LED_RADIUS * 2, LED_RADIUS * 2, 0, 360 * 64);
	}
}

static void fb_set_cursor(int hide)
{
	XDefineCursor(display, fb_win,
		      (hide && motion) ? fb_no_cursor : fb_cross_cursor);
}

static void fb_draw_fb(int x, int y, int width, int height)
{
	XPutImage(display, fb_win, fb_gc, fb_image, x, y, x, y, width, height);
#if 0
	{
		printf("fb_refresh: %d %d, %d %d\n", x, y, width, height);
		XPoint points[5] = {
			{x, y},
			{x + width - 1, y},
			{x + width - 1, y + height - 1},
			{x, y + height - 1},
			{x, y}
		};
		XSetForeground(display, fb_gc, colors[COLORS_RED].color.pixel);
		XDrawLines(display, fb_win, fb_gc, points, 5, CoordModeOrigin);
	}
#endif
	XFlush(display);
}

static void fb_update(void)
{
	if (splash_valid) {
		splash_valid = 0;
		XClearArea(display, fb_win, 0, 0, fb_width, fb_height, False);
	}

	sem_wait(&sem);
	fb_draw_fb(minx, miny, (maxx - minx) + 1, (maxy - miny) + 1);
	minx = INT_MAX;
	miny = INT_MAX;
	maxx = -1;
	maxy = -1;
	notified = 0;
	sem_post(&sem);
}

static void fb_expose(XExposeEvent * event)
{
	if (splash_valid) {
		XCopyArea(display, splash_pixmap, fb_win, fb_gc, 0, 0,
			  splash_width, splash_height,
			  (fb_width - splash_width) / 2,
			  (fb_height - splash_height) / 2);
	} else {
		fb_draw_fb(event->x, event->y, event->width, event->height);
	}
}

static void fb_resize(void)
{
	sem_wait(&sem);
	XResizeWindow(display, top_win,
		      fb_width + (2 * FB_WIN_BORDER),
		      fb_height + (2 * FB_WIN_BORDER) + LED_WIN_HEIGHT +
		      (2 * LED_WIN_BORDER));
	XResizeWindow(display, fb_win, fb_width, fb_height);
	fb_set_cursor(0);
	sem_post(&sem);
}

/*
 * Ideally we should be returning keycodes here, but that gets us into
 * the painful business of handling keymaps, for different machines.
 * So instead return the unshifted keycode.
 */
static void fb_keypress(XKeyEvent * event)
{
	unsigned int code;
	KeySym key;

	code = event->keycode;
	key = XkbKeycodeToKeysym(display, code, 0, 0);
	if (key == NoSymbol) {
		fprintf(stderr, "%s: unable to translate keycode %d\n", appname,
			code);
		return;
	}

	if ((key == XK_F1) || (key == XK_F2)) {
		if (event->type == KeyPress) {
			motion ^= PointerMotionMask;
			XSelectInput(display, fb_win, DEFAULT_EVENTS | motion);
			fb_set_cursor(key == XK_F1);
			sem_wait(&sem);
			mouse_regs[0] |= SIMFB_MOUSE_STATUS_RESYNC;
			sem_post(&sem);
		}
		return;
	}

	if (((key & (~0xff)) != 0) && ((key & (~0xff)) != 0xff00)) {
		fprintf(stderr, "%s: unable to compact keycode %lx\n",
			appname, (unsigned long)key);
		return;
	}
	key &= 0x1ff;
	if (event->type == KeyRelease)
		key |= 0x10000;
	sem_wait(&sem);
	kbd_regs[0] |= SIMFB_KBD_STATUS_INT;
	kbd_regs[1] = key;
	plugin_callbacks->set_interrupt(FB_KBD_IRQ);
	sem_post(&sem);
}

static void fb_mouse(XEvent * event)
{
	static int buttons;

	sem_wait(&sem);

	switch (event->type) {
	case ButtonPress:
		buttons |= (1 << (event->xbutton.button - 1));
		break;
	case ButtonRelease:
		buttons &= ~(1 << (event->xbutton.button - 1));
		break;
	}

	mouse_regs[0] |= SIMFB_MOUSE_STATUS_INT;
	mouse_regs[1] = event->xmotion.x;
	mouse_regs[2] = event->xmotion.y;
	mouse_regs[3] = buttons;
	plugin_callbacks->set_interrupt(FB_MOUSE_IRQ);
	sem_post(&sem);
}

static void fb_thread(void *dummy)
{
	int fd;
	int max;
	struct timeval timeout;
	struct timeval *timeoutp = NULL;

	fd = ConnectionNumber(display);
	max = fd + 1;
	if (p[0] > fd)
		max = p[0] + 1;

	while (1) {
		int result;
		fd_set fds;

		while (XEventsQueued(display, QueuedAfterReading) > 0) {
			XEvent report;

			XNextEvent(display, &report);

			switch (report.type) {

			case Expose:
				if (report.xexpose.window == led_win) {
					fb_display_led();
				} else {
					fb_expose(&report.xexpose);
				}
				break;
			case KeyPress:
			case KeyRelease:
				fb_keypress(&report.xkey);
				break;

			case ButtonPress:
			case ButtonRelease:
			case MotionNotify:
				fb_mouse(&report);
				break;

			}
		}
		XFlush(display);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		FD_SET(p[0], &fds);

		result = select(max, &fds, NULL, NULL, timeoutp);
		if (result < 0) {
			perror("select");
			exit(1);
		}
		if ((result == 0) || (timeoutp != NULL)) {
			/* Timeout
			 * We also come here if we are notified and there is a timeout
			 * pending. This is to avoid a potential starvation problem where
			 * we keep getting notified faster than the timeout, and so the
			 * timeout never occurs. A better way to fix this would be to
			 * update the timeout (linux does this for us, but it is a pain
			 * in Solaris), and because each loop iteration does an XFlush,
			 * its worth doing all the X related calls in one go.
			 */
			timeoutp = NULL;
			fb_update();
		}
		if (FD_ISSET(p[0], &fds)) {
			/* Notified */
			char dummy[16];
			int count;
			char *c;
			count = read(p[0], dummy, 16);
			for (c = dummy; c < &dummy[count]; c++) {
				switch (*c) {
				case 0:
					timeout.tv_sec = 0;
					timeout.tv_usec = 100000;
					timeoutp = &timeout;
					break;
				case 1:
					fb_resize();
					break;
				case 2:
					fb_display_led();
					break;
				default:
					fprintf(stderr,
						"%s: Illegal inter thread message %d\n",
						appname, *c);
					break;
				}
			}
		}
	}
}

static struct memory_bank fb_mb[2] = {
	{
	 .base = FB_CTRL_BASE,
	 .size = FB_CTRL_SIZE,
	 .read_fn = fb_read_ctrl,
	 .write_fn = fb_write_ctrl,
	 }, {
	     .base = FB_FB_BASE,
	     .size = FB_FB_SIZE,
	     .read_fn = fb_read_fb,
	     .write_fn = fb_write_fb,
	     }
};
static struct plugin fb_plugin = {
	.num_banks = 2,
	.banks = fb_mb,
	.close_fn = fb_close,
};

static struct option long_options[] = {
	{"display", 1, 0, 'd'},
	{"iconic", 0, 0, 'i'},
	{0, 0, 0, 0}
};

struct plugin *init(struct plugin_callbacks *cb, int argc, char *argv[])
{
	char c;
	char *display = NULL;
	int iconic = 0;

	plugin_callbacks = cb;

	optind = 1;		/* Reset in code somebody else has used getopt */
	while ((c = getopt_long(argc, argv, "d:i", long_options, NULL)) != -1) {
		switch (c) {
		case 'd':
			display = optarg;
			break;
		case 'i':
			iconic = 1;
			break;
		default:
			exit(1);
		}
	}

	fb_open(display, iconic);
	return &fb_plugin;
}
