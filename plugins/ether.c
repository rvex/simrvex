#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <linux/if.h>
#include <linux/if_tun.h>

#include <sim200/plugin.h>
#include <sim200/plugin_helper.h>
#include "ether.h"

static struct plugin_callbacks *plugin_callbacks;

/* Small helper routine to fork off a helper script to configure
 * networking on the host 
 */
static int run(char *cmd, ...)
{
	pid_t pid;
	int status = 0;
	int i;
	char *child_argv[16];
	va_list ap;

	va_start(ap, cmd);

	child_argv[0] = cmd;
	i = 1;
	do {
		child_argv[i] = va_arg(ap, char *);
		if (i == 15) {
			printf("Too many args\n");
			return -1;
		}
	} while (child_argv[i++]);

	va_end(ap);

	if ((pid = fork()) == -1) {
		printf("Failed to fork\n");
		exit(1);
	}

	if (pid) {
		wait(&status);
	} else {
		execv(cmd, child_argv);
		printf("Failed to exec %s\n", cmd);
		exit(1);
	}

	if (WIFEXITED(status)) {
		return WEXITSTATUS(status);
	}

	return -1;
}

/* Helper function to allocate the next available TUN device.
 * We use another thread to read it rather than poll
 */

static int tun_alloc(char *dev)
{
	struct ifreq ifr;
	int fd, err;

	if ((fd = open("/dev/net/tun", O_RDWR)) < 0)
		return -1;

	memset(&ifr, 0, sizeof(ifr));

	/* Flags: IFF_TUN   - TUN device (no Ethernet headers) 
	 *        IFF_TAP   - TAP device  
	 *
	 *        IFF_NO_PI - Do not provide packet information  
	 */
	ifr.ifr_flags = IFF_TAP | IFF_NO_PI;
	if (*dev)
		strncpy(ifr.ifr_name, dev, IFNAMSIZ);

	if ((err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0) {
		close(fd);
		return err;
	}

	strcpy(dev, ifr.ifr_name);
	return fd;
}

static void init_state(struct ether_device *e)
{
	e->q.len = 0;
	e->rx_addr = 0;
	e->rx_len = 0;
	e->enabled = 0;

	plugin_callbacks->clear_interrupt(TAP_ETHER_IRQ);
}

static struct ether_device ether;

static void ether_thread(void *arg)
{
	struct ether_device *e = (struct ether_device *)arg;
	u32 result;

	for (;;) {
		result = read(e->fd, e->q.buff, ETHER_BUFFER_SIZE);

		if (result == -1) {
			perror("read on TAP fd\n");
			e->enabled = 0;
			break;
		}

		/* We have nothing in the buffer, otherwise we just silently 
		 * drop the packet in the bin
		 */
		if (e->enabled && e->q.len == 0) {
			/* We have some data available */
			e->q.len = result;
			e->rx_len = e->q.len;	/* Store length in register */
			/* Raise an interrupt */
			plugin_callbacks->set_interrupt(TAP_ETHER_IRQ);
			sem_wait(&(e->sync_sem));
		}
	}

}

static void ether_init(struct ether_device *e)
{
	init_state(e);

	strcpy(e->dev_name, "tap%d");

	e->fd = tun_alloc(e->dev_name);
	e->init_ok = 1;

	printf("TAP ethernet device initialisation ");
	if (e->fd < 0) {
		perror("TAP device init failed");
		e->init_ok = 0;
		return;
	} else {
		printf("successful\n");
	}

	if (e->init_ok == 0)
		return;

	sem_init(&(e->sync_sem), 0, 0);

	/* Now spawn a thread */
	pluginlib_thread_create(ether_thread, e);

	/* Ok, now invoke a helper program to bring up the network */
	if (run("./tap-configure", e->dev_name, NULL) != 0) {
		printf("TAP helper program failed!!\n");
	}
}

static void ether_destroy(struct ether_device *e)
{
	if (e->init_ok && e->fd >= 0) {
		e->enabled = 0;
		close(e->fd);
	}
	printf("TAP ethernet device shutdown\n");

}

void ether_open(void)
{
	ether_init(&ether);
}

void ether_close(void)
{
	ether_destroy(&ether);
}

int ether_read(int contextID, void *dst, u32 address, u32 bytes)
{
	u32 val;
	struct ether_device *e = &ether;

	switch (address) {
	case TAP_ETHER_TX_ADDR:
		val = e->tx_addr;
		break;
	case TAP_ETHER_TX_LEN:
		val = e->tx_len;
		break;
	case TAP_ETHER_RX_ADDR:
		val = e->rx_addr;
		break;
	case TAP_ETHER_RX_LEN:
		val = e->rx_len;
		break;
	case TAP_ETHER_ENABLE:
		val = e->enabled;
		break;
	default:
		return 0;
	}

	*(unsigned *)dst = val;
	return 1;
}

static void transmit_packet(struct ether_device *e)
{
	int res;
	unsigned len;
	unsigned char *from = e->tx_buff;

	len = e->tx_len;

	while (len) {
		res = write(e->fd, from, len);
		if (res < 0) {
			perror("Writing to TAP socket\n");
			break;
		}
		from += res;
		len -= res;
	}

}

int ether_write(int contextID, const void *src, u32 address, u32 bytes)
{
	struct ether_device *e = &ether;
	u32 val = *(u32 *) src;

	switch (address) {
	case TAP_ETHER_TX_ADDR:
		e->tx_addr = val;
		break;
	case TAP_ETHER_TX_LEN:
		if (val > ETHER_BUFFER_SIZE) {
			val = ETHER_BUFFER_SIZE;
		}
		e->tx_len = val;
		if (!e->enabled)
			break;
		/* We don't have to copy out really */
		/* Copy out the packet to transmit */
		plugin_callbacks->read_memory(e->tx_buff, e->tx_addr,
					      e->tx_len);
		/* Now trigger a transmit */
		transmit_packet(e);
		break;
	case TAP_ETHER_RX_ADDR:
		e->rx_addr = val;
		if (!e->enabled)
			break;
		if (e->q.len) {
			if (e->rx_addr) {
				plugin_callbacks->write_memory(e->q.buff,
							       e->rx_addr,
							       e->q.len);
			}
			/* Switch off the interrupt */
			plugin_callbacks->clear_interrupt(TAP_ETHER_IRQ);
			e->q.len = 0;
			sem_post(&(e->sync_sem));
		}
		break;
	case TAP_ETHER_RX_LEN:
		/* Writes to this register are ignored */
		break;
	case TAP_ETHER_ENABLE:
		e->enabled = val;
		if (!e->enabled) {
			init_state(e);
		}
		break;
	default:
		return 0;
	}
	return 1;
}

static struct memory_bank ether_mb = {
	.base = TAP_ETHER_BASE,
	.size = 0x14,
	.read_fn = ether_read,
	.write_fn = ether_write,
};
static struct plugin ether_plugin = {
	.num_banks = 1,
	.banks = &ether_mb,
	.close_fn = ether_close,
};

struct plugin *init(struct plugin_callbacks *cb)
{
	plugin_callbacks = cb;
	ether_open();
	return &ether_plugin;
}
