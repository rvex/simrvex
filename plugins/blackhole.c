#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sim200/plugin.h>

static struct plugin_callbacks *plugin_callbacks;

#define BLACKHOLE_BASE  0x20800000

#define BLACKHOLE_SIZE  (8192)

void blackhole_open(void)
{
}

void blackhole_close(void)
{
}

int blackhole_read(int contextID, void *dst, u32 address, u32 bytes)
{
	//  fprintf(stderr,"Blackhole read addr %08x, byte %d\n",address,bytes);

	return 1;
}

int blackhole_write(int contextID, const void *src, u32 address, u32 bytes)
{
	//  fprintf(stderr,"asc write addr %08x, byte %d - ",address,bytes);

	//  if(address==BLACKHOLE_BASE+0x4) 
	//    if(isprint(p[0])||isspace(p[0])) fputc(p[0],stderr);

	//  fprintf(stderr,"\n");

	return 1;
}

#define MAX_REGIONS 16

static struct memory_bank blackhole_mb[MAX_REGIONS];

static struct plugin blackhole_plugin = {
	.num_banks = 0,
	.banks = blackhole_mb,
	.close_fn = blackhole_close,
};

struct plugin *init(struct plugin_callbacks *cb, int argc, char *argv[])
{
	int i, j;
	int num_regions;

	plugin_callbacks = cb;
	blackhole_open();

	if (argc <= 1) {
		printf("No regions given!!\n");
		return NULL;
	}

	if (((argc / 2) * 2) == argc) {
		printf("Incorrect usage: Must be <addr> <size>\n");
		return NULL;
	}

	num_regions = argc / 2;

	if (num_regions >= MAX_REGIONS) {
		printf("Too many regions - max %d\n", MAX_REGIONS);
		return NULL;
	}

	blackhole_plugin.num_banks = num_regions;

	for (i = 0, j = 1; i < num_regions; i++, j += 2) {
		blackhole_mb[i].base = strtol(argv[j], NULL, 16);
		blackhole_mb[i].size = strtol(argv[j + 1], NULL, 16);
		blackhole_mb[i].read_fn = blackhole_read;
		blackhole_mb[i].write_fn = blackhole_write;
		printf("Blackhole from 0x%x -> 0x%x\n",
		       blackhole_mb[i].base,
		       blackhole_mb[i].base + blackhole_mb[i].size - 1);

	}

	return &blackhole_plugin;
}
