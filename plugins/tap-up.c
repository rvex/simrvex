#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static int run(char *cmd, ...)
{
	pid_t pid;
	int status = 0;
	int i = 0;
	char *child_argv[16];
	va_list ap;

	va_start(ap, cmd);

	child_argv[0] = cmd;
	i = 1;
	do {
		child_argv[i] = va_arg(ap, char *);
		if (i == 15) {
			printf("Too many args\n");
			return 1;
		}
	} while (child_argv[i++]);

	va_end(ap);

	if ((pid = fork()) == -1) {
		printf("Failed to fork\n");
		exit(1);
	}

	if (pid) {
		wait(&status);
	} else {
		execv(cmd, child_argv);
		printf("Failed to exec %s\n", cmd);
	}

	if (WIFEXITED(status)) {
		return WEXITSTATUS(status);
	}

	return -1;
}

int main(int argc, char *argv[])
{
	char *dev;
	char *sim_ip, *host_ip, *netmask, *host_dev;

	struct in_addr host_ip_addr, netmask_addr, bcast_addr;

	if (argc != 6) {
		printf
		    ("usage: %s device simulator-ip host-ip netmask host_dev\n",
		     argv[0]);
		exit(1);
	}

	dev = argv[1];
	sim_ip = argv[2];
	host_ip = argv[3];
	netmask = argv[4];
	host_dev = argv[5];

	if ((inet_aton(host_ip, &host_ip_addr) == 0) ||
	    (inet_aton(netmask, &netmask_addr) == 0)) {
		fprintf(stderr, "Unable to parse address\n");
		exit(1);
	}
	bcast_addr.s_addr = (host_ip_addr.s_addr & netmask_addr.s_addr) |
	    (INADDR_BROADCAST & ~netmask_addr.s_addr);

	if (run("/sbin/ifconfig", dev, host_ip,
		"netmask", netmask,
		"broadcast", inet_ntoa(bcast_addr), NULL) != 0) {
		printf("Failed to configure interface %s\n", dev);
		exit(1);
	}

	if (run("/sbin/route", "add", "-host", sim_ip, "dev", dev, NULL) != 0) {
		printf("Failed to setup route\n");
		exit(2);
	}

	if (run("/sbin/arp", "-Ds", sim_ip, host_dev, "pub", NULL) != 0) {
		printf("Proxy arp request failed\n");
		exit(3);
	}

	exit(0);

}
