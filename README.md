
simrvex: r-VEX functional simulator
===================================

This is fast functional simulator for the r-VEX, based on the simulator for
the ST200 series of processors. It generates trace information, either full or
via a circular buffer. It also supports plugins, which allows devices such as
ethernet and graphics devices to be simulated as well.

Required software packages:

 - binutils
 - binutils-dev
 - libX11
 - libX11-dev
 - libxpm
 - libxpm-dev


Build instructions
==================

Its not too hard. You can optionally set some flags:

    export CFLAGS="-O3 -march=native -funroll-loops"
    export CXXFLAGS="-O3 -march=native -funroll-loops"

Create a build directory:
    
    mkdir build/
    cd build/

Run configure:
    
    ../configure --prefix=/tmp

And run make:

    make -j11
    make install #optional
